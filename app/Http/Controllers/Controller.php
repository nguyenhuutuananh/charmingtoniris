<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Config;
use SEO;
use SEOMeta;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function __construct()
    {
        $public_configs = Config::get_all();
        \View::share('public_configs', $public_configs);
        SEO::setTitle(__c('web_title'));
        SEOMeta::setDescription(__c('descriptions'));
        SEOMeta::addKeyword(explode(',', __c('keywords')));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SEOMeta;

class GroundController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index() {
        SEOMeta::setTitle("Mặt bằng");
        return view('gemriver.mat_bang.index');
    }
    public function tang_1() {
        SEOMeta::setTitle("Tầng 1");
        return view('gemriver.mat_bang.tang_1');
    }
    public function tang_2() {
        SEOMeta::setTitle("Tầng 2");
        return view('gemriver.mat_bang.tang_2');
    }
    public function tang_3() {
        SEOMeta::setTitle("Tầng 3");
        return view('gemriver.mat_bang.tang_3');
    }
    public function tang_5() {
        SEOMeta::setTitle("Tầng 5");
        return view('gemriver.mat_bang.tang_5');
    }
    public function tang_6_32() {
        SEOMeta::setTitle("Tầng 6 - 32");
        return view('gemriver.mat_bang.tang_6_32');
    }
    public function tang_33_34() {
        SEOMeta::setTitle("Tầng 33 - 34");
        return view('gemriver.mat_bang.tang_33_34');
    }

    public function can_ho_a() {
        SEOMeta::setTitle("Căn hộ A");
        return view('gemriver.mat_bang.can_ho.can_ho_a');
    }
    public function can_ho_b1() {
        SEOMeta::setTitle("Căn hộ B1");
        return view('gemriver.mat_bang.can_ho.can_ho_b1');
    }
    public function can_ho_b2() {
        SEOMeta::setTitle("Căn hộ B2");
        return view('gemriver.mat_bang.can_ho.can_ho_b2');
    }
    public function can_ho_b3() {
        SEOMeta::setTitle("Căn hộ B3");
        return view('gemriver.mat_bang.can_ho.can_ho_b3');
    }
    public function can_ho_b4() {
        SEOMeta::setTitle("Căn hộ B4");
        return view('gemriver.mat_bang.can_ho.can_ho_b4');
    }
    public function can_ho_b5() {
        SEOMeta::setTitle("Căn hộ B5");
        return view('gemriver.mat_bang.can_ho.can_ho_b5');
    }
    public function can_ho_b6() {
        SEOMeta::setTitle("Căn hộ B6");
        return view('gemriver.mat_bang.can_ho.can_ho_b6');
    }
    public function can_ho_b7() {
        SEOMeta::setTitle("Căn hộ B7");
        return view('gemriver.mat_bang.can_ho.can_ho_b7');
    }
    public function can_ho_b8() {
        SEOMeta::setTitle("Căn hộ B8");
        return view('gemriver.mat_bang.can_ho.can_ho_b8');
    }
    public function can_ho_c1() {
        SEOMeta::setTitle("Căn hộ C1");
        return view('gemriver.mat_bang.can_ho.can_ho_c1');
    }
    public function can_ho_c2() {
        SEOMeta::setTitle("Căn hộ C2");
        return view('gemriver.mat_bang.can_ho.can_ho_c2');
    }
    public function can_ho_c3() {
        SEOMeta::setTitle("Căn hộ C3");
        return view('gemriver.mat_bang.can_ho.can_ho_c3');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProjectCategory;
use App\Project;

class ProjectController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function list($slug) {
        $project_category = ProjectCategory::where('slug', $slug)->first();
        if($project_category) {
            return view('frontpage.du_an.danh_sach', ['project_category' => $project_category]);
        }
        return view('static_page.404');
    }
    public function detail($category_slug, $slug) {
        $project = Project::where('slug', $slug)->first();
        if($project) {
            if($category_slug != $project->category->slug) {
                return view('static_page.404', ['suggestPage' => route('project.detail', ['category_slug' => $project->category->slug, 'slug' => $project->slug])]);        
            }
            return view('frontpage.du_an.chi_tiet', ['project' => $project]);
        }
        return view('static_page.404');
    }
    public function search(Request $request) {
        $pn = $request->input('phong-ngu');
        $gia_thue = $request->input('gia-thue');
        $gia_ban = $request->input('gia-ban');
        $noi_that = $request->input('noi-that');
        
        $projects = Project::orderBy('price', 'asc');

        if($pn && is_numeric($pn)) {
            $projects->where('bed_rooms', '=', $pn);
        }
        if($gia_thue) {
            if($gia_thue == 'Dưới 20 triệu') {
                $projects->where('price', '<', 20 * 1000 * 1000);
            }
            else if($gia_thue == '20 - 30 triệu') {
                $projects->where('price', '>=', 20 * 1000 * 1000);
                $projects->where('price', '<',  30 * 1000 * 1000);
            }
            else if($gia_thue == '30 - 40 triệu') {
                $projects->where('price', '>=', 30 * 1000 * 1000);
                $projects->where('price', '<',  40 * 1000 * 1000);
            }
            else if($gia_thue == 'Trên 50 triệu') {
                $projects->where('price', '>=', 50 * 1000 * 1000);
            }
        }
        if($gia_ban) {
            if($gia_ban == 'dưới 2 tỷ') {
                $projects->where('price', '<', 2 * 1000 * 1000 * 1000);
            }
            else if($gia_ban == '2 - 3 tỷ') {
                $projects->where('price', '>=', 2 * 1000 * 1000 * 1000);
                $projects->where('price', '<',  3 * 1000 * 1000 * 1000);
            }
            else if($gia_ban == '3 - 4 tỷ') {
                $projects->where('price', '>=', 3 * 1000 * 1000 * 1000);
                $projects->where('price', '<',  4 * 1000 * 1000 * 1000);
            }
            else if($gia_ban == '4 - 5 tỷ') {
                $projects->where('price', '>=', 4 * 1000 * 1000 * 1000);
                $projects->where('price', '<',  5 * 1000 * 1000 * 1000);
            }
            else if($gia_ban == 'trên 5 tỷ') {
                $projects->where('price', '>=', 5 * 1000 * 1000 * 1000);
            }
        }
        if($noi_that) {
            $projects->where('furniture_id', '=', $noi_that);
        }
        
        return view('frontpage.du_an.search_result', ['projects' => $projects]);
    }
}

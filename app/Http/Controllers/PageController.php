<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use SEO;

class PageController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function detail($slug) {
        $page = Page::where('slug', $slug)->first();
        if($page) {
            return view('gemriver.trang.detail', ['page' => $page]);
        }
        return view('static_page.404');
    }
    public function tong_quan() {
        SEO::setTitle("Tổng quan");
        return view('gemriver.trang.tong_quan');
    }
    public function chu_dau_tu() {
        SEO::setTitle("Chủ đầu tư");
        return view('gemriver.trang.chu_dau_tu');
    }
    public function vi_tri() {
        SEO::setTitle("Vị trí");
        return view('gemriver.trang.vi_tri');
    }
    public function tien_ich() {
        SEO::setTitle("Tiện ích");
        return view('gemriver.trang.tien_ich');
    }
    public function thu_vien() {
        SEO::setTitle("Thư viện");
        return view('gemriver.trang.thu_vien');
    }
}

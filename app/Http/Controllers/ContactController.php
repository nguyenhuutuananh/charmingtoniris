<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use Illuminate\Support\Facades\Mail;
use App\SpecialContact;

class ContactController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        return view('gemriver.contact');
    }
    public function save(Request $request)
    {
        $secret = "6Lfv11QUAAAAADzwvnPM85ZWkjRGFRIvWyn-8yZN";
        $response = $request->input("g-recaptcha-response");

        $name = $request->input('name');
        $email = $request->input('email');
        $phone_number = $request->input('phone_number');

        
        if (!trim($name) || !trim($phone_number)) {
            return response()->json(['result' => false, 'message' => 'Chưa nhập đầy đủ thông tin!']);
        } else {
            // $verify = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$response}");
            // $captcha_success = json_decode($verify);
            // if ($captcha_success->success == false) {
            //     return response()->json(['result' => false, 'message' => 'Xác thực thất bại!']);
            // } else if ($captcha_success->success == true) {
                $new_contact = new Contact();
                $new_contact->fill($request->except(['_token', 'g-recaptcha-response']));
                if($new_contact->save()) {
                    $data = $request->except(['_token', 'g-recaptcha-response']);
                    
                    $emails = explode(',', __c('email'));
                    $emails = array_filter($emails, function($value) { return trim($value) !== ''; });
                    $emails = array_map(function($value) { return trim($value); }, $emails);
                    // Add more emails
                    // $emails[] = '';

                    $mail_sent = true;
                    try {
                        Mail::send('email.contact', $data, function($message) use ($emails) {
                            $message->to($emails, 'Liên hệ - ' . __c('web_title'))
                                    ->subject('Liên hệ - ' . __c('web_title'));
                            $message->from('daihoc.demo@gmail.com','Web Sender');
                        });
                    }
                    catch(\Exception $e) {
                        $mail_sent = $emails;
                    }

                    return response()->json(['result' => true, 'message' => 'Gửi liên hệ thành công', 'mail' => $mail_sent]);
                }
                else {
                    return response()->json(['result' => false, 'message' => 'Xác thực thất bại !']);
                }
            // }
        }
    }
    public function save_special(Request $request)
    {
        $secret = "6Lfv11QUAAAAADzwvnPM85ZWkjRGFRIvWyn-8yZN";
        $response = $request->input("g-recaptcha-response");

        $project_category = $request->input('project_category');
        $thap = $request->input('thap');
        $tang = $request->input('tang');
        $vi_tri_can = $request->input('vi_tri_can');
        $furniture = $request->input('furniture');
        $hien_trang_nha = $request->input('hien_trang_nha');
        $gia_ban = $request->input('gia_ban');

        
        if (!trim($project_category) || !trim($thap) || !trim($tang) || !trim($vi_tri_can) || !trim($furniture) || !trim($hien_trang_nha) || !trim($gia_ban)) {
            return response()->json(['result' => false, 'message' => 'Chưa nhập đầy đủ thông tin!']);
        } else {
            $verify = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$response}");
            $captcha_success = json_decode($verify);
            if ($captcha_success->success == false) {
                return response()->json(['result' => false, 'message' => 'Xác thực thất bại!']);
            } else if ($captcha_success->success == true) {
                $new_contact = new SpecialContact();
                $new_contact->fill($request->except(['_token', 'g-recaptcha-response']));
                if($new_contact->save()) {
                    $data = $request->except(['_token', 'g-recaptcha-response']);
                    
                    $emails = explode(',', __c('email'));
                    $emails = array_filter($emails, function($value) { return trim($value) !== ''; });

                    // Add more emails
                    // $emails[] = '';

                    Mail::send('email.contact', $data, function($message) use ($emails) {
                        $message->to($emails, 'Liên hệ - ' . __c('web_title'))
                                ->subject('Liên hệ - ' . __c('web_title'));
                        $message->from('daihoc.demo@gmail.com','Web Sender');
                    });

                    return response()->json(['result' => true, 'message' => 'Gửi liên hệ thành công']);
                }
                else {
                    return response()->json(['result' => false, 'message' => 'Xác thực thất bại !']);
                }
            }
        }
    }
    public function save_email(Request $request) {
        $email = $request->input('email');
        $valid_email = filter_var( $email, FILTER_VALIDATE_EMAIL );
        if (!trim($email) ) {
            return response()->json(['result' => false, 'message' => 'Chưa nhập đầy đủ thông tin!']);
        } 
        else if(!$valid_email) {
            return response()->json(['result' => false, 'message' => 'Email không đúng định dạng!']);
        }
        else {
            
            
            $new_contact = new Contact();
            $new_contact->fill($request->except(['_token', 'g-recaptcha-response']));
            $new_contact->name = "Footer";
            if($valid_email && $new_contact->save()) {
                $data = $request->except(['_token', 'g-recaptcha-response']);
                $emails = [__c('email'), 'thinhsy0000@gmail.com', 'nhungocdiaoc@gmail.com'];
                Mail::send('email.contact', $data, function($message) use ($emails) {
                    $message->to($emails, 'Liên hệ - ' . __c('web_title'))
                            ->subject('Liên hệ - ' . __c('web_title'));
                    $message->from('daihoc.demo@gmail.com','Web Sender');
                });
                return response()->json(['result' => true, 'message' => 'Gửi liên hệ thành công']);
            }
            else {
                return response()->json(['result' => false, 'message' => 'Xác thực thất bại !']);
            }
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project;
use App\ProjectImage;
use Yajra\Datatables\Datatables;
use Validator;
class OtherProjectController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->middleware('auth');
    }
    // List
    public function index() {
        return view('admin.project.list');
    }
    public function ajax_list(Datatables $datatables) {
        return $datatables->eloquent(Project::query())
                            ->addColumn('action', 'admin.project.datatables.edit_delete')
                            ->rawColumns(['name', 'action'])
                            ->orderColumn('id', '-id $1')
                            ->make(true);
    }
    // Create
    public function create() {
        return view('admin.project.create');
    }
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:6|max:191',
            'project_images' => 'array|min:1',
            'project_images.*' => 'required',
        ]);
        if($validator->fails()) {
            return redirect()->route('admin_project_create')
                            ->withErrors($validator)
                            ->withInput();
        }
        $project = new Project;
        $project->_project_type = 1;
        $project->fill($request->except(['_token']));
        $project->slug = str_slug($project->name, '-') . '-' . rand(1000,2000);

        if($project->save()) {
            $project->slug = str_slug($project->name, '-') . '-' . $project->id;
            $project->save();
            $arrayImageObjects = array_map(function($url) {
                return [
                    'name' => basename($url),
                    'url' => $url
                ];
            }, $request->project_images);
            $project->images()->createMany($arrayImageObjects);
            return redirect()->route('admin_project_list');
        }
        else {
            return redirect()->route('admin_project_create')
                    ->withErrors($project->getErrors())
                    ->withInput();
        }
    }

    // Edit
    public function edit($project_id) {
        $project = Project::find($project_id);
        if($project)
        {
            return view('admin.project.edit', ['project' => $project]);    
        }
        else
        {
            return redirect()->route('admin_project_list');
        }
        
    }
    public function update($project_id, Request $request) {
        $project = Project::find($project_id);
        if($project)
        {
            $validator = Validator::make($request->all(), [
                'name' => 'required|min:6|max:191',
                'project_images' => 'array|min:1',
                'project_images.*' => 'required',
            ]);
            if($validator->fails()) {
                return redirect()->route('admin_project_edit', ['project' => $project_id])
                                ->withErrors($validator)
                                ->withInput();
            }
            $project->fill($request->except(['_token']));
            $project->slug = str_slug($project->name, '-') . '-' . $project_id;
            if($project->save()) {
                $project->slug = str_slug($project->name, '-') . '-' . $project->id;
                $project->save();
                // Delete if not exists
                ProjectImage::where('project_id', $project_id)->whereNotIn('url', $request->project_images)->delete();

                $arrayImageObjects = array_filter($request->project_images, function($url) use($project_id) {
                    return ProjectImage::where('url', $url)->where('project_id', $project_id)->count() == 0;
                });

                $arrayImageObjects = array_map(function($url) {
                    return [
                        'name' => basename($url),
                        'url' => $url
                    ];
                }, $arrayImageObjects);
                $project->images()->createMany($arrayImageObjects);
                return redirect()->route('admin_project_edit', ['project' => $project_id]);
            }
            else {
                return redirect()->route('admin_project_edit', ['project' => $project_id])
                        ->withErrors($project->getErrors())
                        ->withInput();
            }
            return view('admin.project.edit', ['project' => $project]);    
        }
        else
        {
            return redirect()->route('admin_project_list');
        }
    }
    
    // Delete
    public function delete($project_id) {
        $project = Project::find($project_id);
        if($project)
        {
            $project->delete();
            return redirect()->route('admin_project_list');
        }
        else
        {
            return redirect()->route('admin_project_list');
        }
    }
}

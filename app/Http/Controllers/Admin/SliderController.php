<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Validator;
use App\SliderDetail;
use App\Slider;

class SliderController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->middleware('auth');
    }
    // List
    public function index() {
        return view('admin.slider_detail.list');
    }
    public function ajax_list(Datatables $datatables) {
        return $datatables->eloquent(SliderDetail::query())
                            ->addColumn('action', 'admin.slider_detail.datatables.edit_delete')
                            ->editColumn('imageUrl', function(SliderDetail $slider_detail) {
                                return '<img width="50px" src="'. url($slider_detail->imageUrl) .'" />';
                            })
                            ->editColumn('slider_id', function(SliderDetail $slider_detail) {
                                return Slider::find($slider_detail->slider_id)->title;
                            })
                            ->rawColumns(['title', 'action', 'imageUrl', 'slider_id'])
                            ->orderColumn('id', '-id $1')
                            ->make(true);
    }
    // Create
    public function create() {
        return view('admin.slider_detail.create');
    }
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'title' => 'required|min:6|max:191',
            'imageUrl' => 'required',
        ]);
        if($validator->fails()) {
            return redirect()->route('admin_slider_detail_create')
                            ->withErrors($validator)
                            ->withInput();
        }
        $slider_detail = new SliderDetail;
        $slider_detail->fill($request->except(['_token']));
        $slider_detail->slider_id = 1;
        if($slider_detail->save()) {
            $request->session()->flash('success_message', 'Tạo thành công!');
            return redirect()->route('admin_slider_detail_list');
        }
        else {
            return redirect()->route('admin_slider_detail_create')
                    ->withErrors($slider_detail->getErrors())
                    ->withInput();
        }
    }

    // Edit
    public function edit($slider_detail_id) {
        $slider_detail = SliderDetail::find($slider_detail_id);
        if($slider_detail)
        {
            
            return view('admin.slider_detail.edit', ['slider_detail' => $slider_detail]);    
        }
        else
        {
            return redirect()->route('admin_slider_detail_list');
        }
        
    }
    public function update($slider_detail_id, Request $request) {
        
        $slider_detail = SliderDetail::find($slider_detail_id);
        if($slider_detail)
        {
            $validator = Validator::make($request->all(), [
                'title' => 'required|min:6|max:191',
                'imageUrl' => 'required',
            ]);
            if($validator->fails()) {
                return redirect()->route('admin_slider_detail_edit', ['slider_detail' => $slider_detail_id])
                                ->withErrors($validator)
                                ->withInput();
            }
            $slider_detail->fill($request->except(['_token']));
            if($slider_detail->save()) {
                $request->session()->flash('success_message', 'Chỉnh sửa thành công!');
                return redirect()->route('admin_slider_detail_edit', ['slider_detail' => $slider_detail_id]);
            }
            else {
                return redirect()->route('admin_slider_detail_edit', ['slider_detail' => $slider_detail_id])
                        ->withErrors($slider_detail->getErrors())
                        ->withInput();
            }
            return view('admin.slider_detail.edit', ['slider_detail' => $slider_detail]);    
        }
        else
        {
            return redirect()->route('admin_slider_detail_list');
        }
    }
    
    // Delete
    public function delete($slider_detail_id) {
        $slider_detail = SliderDetail::find($slider_detail_id);
        if($slider_detail)
        {
            $slider_detail->delete();
            return redirect()->route('admin_slider_detail_list');
        }
        else
        {
            return redirect()->route('admin_slider_detail_list');
        }
    }
}

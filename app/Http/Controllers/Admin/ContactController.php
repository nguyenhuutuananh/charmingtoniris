<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact;
use App\Project;
use Yajra\Datatables\Datatables;
use App\SpecialContact;

class ContactController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        parent::__construct();
    }
    // List
    public function index() {
        return view('admin.contact.list');
    }
    public function ajax_list(Datatables $datatables) {
        return $datatables->eloquent(Contact::query())
                            ->editColumn('project_id', function(Contact $contact) {
                                if($contact->project_id) {
                                    $project = Project::find($contact->project_id);
                                    return '<b><a target="_blank" href="'.route('project_detail', ['project_id' => $project->id, 'project_slug' => $project->slug]).'">
                                    '.
                                    $project->name
                                    .'
                                    </a></b>';
                                }
                                else {
                                    return 'Không chọn';
                                }
                                
                            })
                            ->editColumn('is_receive_price', function(Contact $contact) {
                                if($contact->is_receive_price) {
                                    return 'Có';
                                }
                                else {
                                    return 'Không';
                                }
                            })
                            ->editColumn('is_get_analysis', function(Contact $contact) {
                                if($contact->is_get_analysis) {
                                    return 'Có';
                                }
                                else {
                                    return 'Không';
                                }
                            })
                            ->addColumn('action', 'admin.contact.datatables.edit_delete')
                            ->rawColumns(['project_id', 'action'])
                            ->orderColumn('id', '-id $1')
                            ->make(true);
    }
    // List
    public function index_special() {
        return view('admin.special_contact.list');
    }
    public function ajax_list_special(Datatables $datatables) {
        return $datatables->eloquent(SpecialContact::query())
                            ->addColumn('action', 'admin.contact.datatables.edit_delete')
                            ->rawColumns(['action'])
                            ->orderColumn('id', '-id $1')
                            ->make(true);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\PostCategory;
use Yajra\Datatables\Datatables;
use Validator;

class PostCategoryController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->middleware('auth');
    }
    // List
    public function index() {
        return view('admin.post_category.list');
    }
    public function ajax_list(Datatables $datatables) {
        return $datatables->eloquent(PostCategory::query())
                            ->addColumn('action', 'admin.post_category.datatables.edit_delete')
                            ->rawColumns(['title', 'action'])
                            ->orderColumn('id', '-id $1')
                            ->make(true);
    }
    // Create
    public function create() {
        return view('admin.post_category.create');
    }
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'title' => 'required|min:2|max:191',
        ]);
        if($validator->fails()) {
            return redirect()->route('admin_post_category_create')
                            ->withErrors($validator)
                            ->withInput();
        }
        $post_category = new PostCategory;
        $post_category->fill($request->except(['_token']));
        $post_category->slug = str_slug($post_category->title, '-') . '-' . rand(1000,2000);

        if($post_category->save()) {
            $post_category->slug = str_slug($post_category->title, '-') . '-' . $post_category->id;
            $post_category->save();
            $request->session()->flash('success_message', 'Tạo thành công!');
            return redirect()->route('admin_post_category_list');
        }
        else {
            return redirect()->route('admin_post_category_create')
                    ->withErrors($post_category->getErrors())
                    ->withInput();
        }
    }

    // Edit
    public function edit($post_category_id) {
        $post_category = PostCategory::find($post_category_id);
        if($post_category)
        {
            return view('admin.post_category.edit', ['post_category' => $post_category]);    
        }
        else
        {
            return redirect()->route('admin_post_category_list');
        }
        
    }
    public function update($post_category_id, Request $request) {
        $post_category = PostCategory::find($post_category_id);
        if($post_category)
        {
            $validator = Validator::make($request->all(), [
                'title' => 'required|min:2|max:191',
            ]);
            if($validator->fails()) {
                return redirect()->route('admin_post_category_edit', ['post_category' => $post_category_id])
                                ->withErrors($validator)
                                ->withInput();
            }
            $post_category->fill($request->except(['_token']));
            $post_category->slug = str_slug($post_category->title, '-') . '-' . $post_category_id;
            if($post_category->save()) {
                $post_category->slug = str_slug($post_category->name, '-') . '-' . $post_category->id;
                $post_category->save();
                $request->session()->flash('success_message', 'Chỉnh sửa thành công!');
                return redirect()->route('admin_post_category_edit', ['post_category' => $post_category_id]);
            }
            else {
                return redirect()->route('admin_post_category_edit', ['post_category' => $post_category_id])
                        ->withErrors($post_category->getErrors())
                        ->withInput();
            }
            return view('admin.post_category.edit', ['post_category' => $post_category]);    
        }
        else
        {
            return redirect()->route('admin_post_category_list');
        }
    }
    
    // Delete
    public function delete($post_category_id) {
        $post_category = PostCategory::find($post_category_id);
        if($post_category)
        {
            $post_category->delete();
            return redirect()->route('admin_post_category_list');
        }
        else
        {
            return redirect()->route('admin_post_category_list');
        }
    }
}

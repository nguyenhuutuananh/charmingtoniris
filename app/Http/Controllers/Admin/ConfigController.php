<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Config;

class ConfigController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->middleware('auth');
    }
    public function index() {
        $configs = Config::get_all();
        return view('admin.config.show', ['configs' => $configs]);
    }
    public function tong_quan() {
        $configs = Config::get_all();
        return view('admin.config.tong_quan', ['configs' => $configs]);
    }
    public function vi_tri() {
        $configs = Config::get_all();
        return view('admin.config.vi_tri', ['configs' => $configs]);
    }
    public function chu_dau_tu() {
        $configs = Config::get_all();
        return view('admin.config.chu_dau_tu', ['configs' => $configs]);
    }
    public function tien_ich() {
        $configs = Config::get_all();
        return view('admin.config.tien_ich', ['configs' => $configs]);
    }
    public function save(Request $request) {
        foreach($request->except(['_token']) as $key=>$value) {
            $config = Config::where(['name' => $key])->first();
            if($config) {
                $config->value = $value;
                if(!$config->save()) {
                    return redirect()->route('admin_config_page')
                        ->withErrors($config->getErrors())
                        ->withInput();
                }
            }
        }
        $request->session()->flash('success_message', 'Các thiết lập đã được lưu!');
        return redirect()->back();
    }
}

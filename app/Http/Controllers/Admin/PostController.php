<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\PostCategory;
use Yajra\Datatables\Datatables;
use Validator;

class PostController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->middleware('auth');
    }
    // List
    public function index() {
        return view('admin.post.list');
    }
    public function ajax_list(Datatables $datatables) {
        return $datatables->eloquent(Post::query())
                            ->addColumn('action', 'admin.post.datatables.edit_delete')
                            ->editColumn('imageUrl', function(Post $post) {
                                return '<img width="50px" src="'. url($post->imageUrl) .'" />';
                            })
                            ->editColumn('category_id', function(Post $post) {
                                return '<b><a href="'.route('admin_post_category_edit', ['post_category_id' => $post->category_id]).'">
                                '.
                                PostCategory::find($post->category_id)->title
                                .'
                                </a></b>';
                            })
                            ->rawColumns(['title', 'action', 'imageUrl', 'category_id'])
                            ->orderColumn('id', '-id $1')
                            ->make(true);
    }
    // Create
    public function create() {
        $postCategories = PostCategory::all();
        $postCategoryList = array();
        foreach($postCategories as $postCategory) {
            $postCategoryList[$postCategory->id] = $postCategory->title;
        }
        return view('admin.post.create', ['postCategoryList' => $postCategoryList]);
    }
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'title' => 'required|min:6|max:191',
            'description' => 'required|min:6|max:365',
            'content' => 'required|min:6',
            'imageUrl' => 'required',
            'category_id' => 'required',
        ]);
        if($validator->fails()) {
            return redirect()->route('admin_post_create')
                            ->withErrors($validator)
                            ->withInput();
        }
        $post = new Post;
        $post->fill($request->except(['_token']));
        $post->slug = str_slug($post->title, '-') . '-' . rand(1000,2000);

        if($post->save()) {
            $post->slug = str_slug($post->title, '-') . '-' . $post->id;
            $post->save();
            $request->session()->flash('success_message', 'Tạo thành công!');
            return redirect()->route('admin_post_list');
        }
        else {
            return redirect()->route('admin_post_create')
                    ->withErrors($post->getErrors())
                    ->withInput();
        }
    }

    // Edit
    public function edit($post_id) {
        $post = Post::find($post_id);
        if($post)
        {
            $postCategories = PostCategory::all();
            $postCategoryList = array();
            foreach($postCategories as $postCategory) {
                $postCategoryList[$postCategory->id] = $postCategory->title;
            }
            return view('admin.post.edit', ['post' => $post, 'postCategoryList' => $postCategoryList]);    
        }
        else
        {
            return redirect()->route('admin_post_list');
        }
        
    }
    public function update($post_id, Request $request) {
        
        $post = Post::find($post_id);
        if($post)
        {
            $validator = Validator::make($request->all(), [
                'title' => 'required|min:6|max:191',
                'description' => 'required|min:6|max:365',
                'content' => 'required|min:6',
                'imageUrl' => 'required',
                'category_id' => 'required',
            ]);
            if($validator->fails()) {
                return redirect()->route('admin_post_edit', ['post' => $post_id])
                                ->withErrors($validator)
                                ->withInput();
            }
            $post->fill($request->except(['_token']));
            $post->slug = str_slug($post->title, '-') . '-' . $post_id;
            if($post->save()) {
                $request->session()->flash('success_message', 'Chỉnh sửa thành công!');
                return redirect()->route('admin_post_edit', ['post' => $post_id]);
            }
            else {
                return redirect()->route('admin_post_edit', ['post' => $post_id])
                        ->withErrors($post->getErrors())
                        ->withInput();
            }
            return view('admin.post.edit', ['post' => $post]);    
        }
        else
        {
            return redirect()->route('admin_post_list');
        }
    }
    
    // Delete
    public function delete($post_id) {
        $post = Post::find($post_id);
        if($post)
        {
            $post->delete();
            return redirect()->route('admin_post_list');
        }
        else
        {
            return redirect()->route('admin_post_list');
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;
use Validator;
use Yajra\Datatables\Datatables;

class PageController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->middleware('auth');
    }
    // List
    public function index() {
        return view('admin.page.list');
    }
    public function ajax_list(Datatables $datatables) {
        return $datatables->eloquent(Page::query())
                            ->addColumn('action', 'admin.page.datatables.edit_delete')
                            ->rawColumns(['title', 'action', ])
                            ->orderColumn('id', '-id $1')
                            ->make(true);
    }
    // Edit
    public function edit($page_id) {
        $page = Page::find($page_id);
        if($page)
        {
            return view('admin.page.edit', ['page' => $page]);    
        }
        else
        {
            return redirect()->route('admin_page_list');
        }
        
    }
    public function update($page_id, Request $request) {
        
        $page = Page::find($page_id);
        if($page)
        {
            $validator = Validator::make($request->all(), [
                'title' => 'required|min:6|max:191',
                'content' => 'required|min:6',
            ]);
            if($validator->fails()) {
                return redirect()->route('admin_page_edit', ['page' => $page_id])
                                ->withErrors($validator)
                                ->withInput();
            }
            $page->fill($request->except(['_token']));
            $page->slug = str_slug($page->title, '-') . '-' . $page_id;
            if($page->save()) {
                $request->session()->flash('success_message', 'Chỉnh sửa thành công!');
                return redirect()->route('admin_page_edit', ['page' => $page_id]);
            }
            else {
                return redirect()->route('admin_page_edit', ['page' => $page_id])
                        ->withErrors($page->getErrors())
                        ->withInput();
            }
            return view('admin.page.edit', ['page' => $page]);    
        }
        else
        {
            return redirect()->route('admin_page_list');
        }
    }
}

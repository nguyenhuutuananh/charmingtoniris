<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Partner;
use Yajra\Datatables\Datatables;
use Validator;

class PartnerController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->middleware('auth');
    }
    // List
    public function index() {
        return view('admin.partner.list');
    }
    public function ajax_list(Datatables $datatables) {
        return $datatables->eloquent(Partner::query())
                            ->addColumn('action', 'admin.partner.datatables.edit_delete')
                            ->editColumn('imageUrl', function(Partner $partner) {
                                return '<img width="50px" src="'. url($partner->imageUrl) .'" />';
                            })
                            ->rawColumns(['title', 'action', 'imageUrl', 'category_id'])
                            ->orderColumn('id', '-id $1')
                            ->make(true);
    }
    // Create
    public function create() {
        
        return view('admin.partner.create');
    }
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'title' => 'required|min:6|max:191',
            'description' => 'required|min:6',
            'sub_title' => 'required|min:6',
            'imageUrl' => 'required',
            'link' => 'required',
        ]);
        if($validator->fails()) {
            return redirect()->route('admin_partner_create')
                            ->withErrors($validator)
                            ->withInput();
        }
        $partner = new Partner;
        $partner->fill($request->except(['_token']));
        if($partner->save()) {
            $request->session()->flash('success_message', 'Tạo thành công!');
            return redirect()->route('admin_partner_list');
        }
        else {
            return redirect()->route('admin_partner_create')
                    ->withErrors($partner->getErrors())
                    ->withInput();
        }
    }

    // Edit
    public function edit($partner_id) {
        $partner = Partner::find($partner_id);
        if($partner)
        {
            
            return view('admin.partner.edit', ['partner' => $partner]);    
        }
        else
        {
            return redirect()->route('admin_partner_list');
        }
        
    }
    public function update($partner_id, Request $request) {
        
        $partner = Partner::find($partner_id);
        if($partner)
        {
            $validator = Validator::make($request->all(), [
                'title' => 'required|min:6|max:191',
            'description' => 'required|min:6',
            'sub_title' => 'required|min:6',
            'imageUrl' => 'required',
            'link' => 'required',
            ]);
            if($validator->fails()) {
                return redirect()->route('admin_partner_edit', ['partner' => $partner_id])
                                ->withErrors($validator)
                                ->withInput();
            }
            $partner->fill($request->except(['_token']));
            if($partner->save()) {
                $request->session()->flash('success_message', 'Chỉnh sửa thành công!');
                return redirect()->route('admin_partner_edit', ['partner' => $partner_id]);
            }
            else {
                return redirect()->route('admin_partner_edit', ['partner' => $partner_id])
                        ->withErrors($partner->getErrors())
                        ->withInput();
            }
            return view('admin.partner.edit', ['partner' => $partner]);    
        }
        else
        {
            return redirect()->route('admin_partner_list');
        }
    }
    
    // Delete
    public function delete($partner_id) {
        $partner = Partner::find($partner_id);
        if($partner)
        {
            $partner->delete();
            return redirect()->route('admin_partner_list');
        }
        else
        {
            return redirect()->route('admin_partner_list');
        }
    }
}

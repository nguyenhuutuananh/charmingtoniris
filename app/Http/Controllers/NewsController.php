<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\PostCategory;

use SEOMeta;
use OpenGraph;
use Twitter;
use SEO;

class NewsController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index() {
        SEO::setTitle('Tin tức');
        return view('gemriver.tin_tuc.list');
    }    
    public function detail($slug) {
        $post = Post::where('slug', $slug)->first();
        if($post) {
            SEOMeta::setTitle($post->title);
            SEOMeta::setDescription($post->description);
            SEOMeta::addKeyword(explode(',', __c('keywords')));

            // Namespace URI: http://ogp.me/ns/article#
            // article
            OpenGraph::setTitle($post->title)
            ->setDescription($post->description)
            ->setType('article')
            ->addImage(url($post->imageUrl), ['height' => 300, 'width' => 300])
            ->setArticle([
                'published_time' => $post->created_at->toW3CString(),
                'modified_time' => $post->updated_at->toW3CString(),
                'section' => 'Tin tức',
                'tag' => $post->seo_keywords
            ]);

            return view('gemriver.tin_tuc.detail', ['post' => $post]);
        }
        return view('static_page.404');
    }
}

<?php
use App\Config;
if(!function_exists('__c')) {
    function __c($name) {
        $config = Config::where('name', $name)->first();
        if($config) {
            
            return $config->value;
        }
        return "";
    }
}
if(!function_exists('__ctitle')) {
    function __ctitle($name) {
        $config = Config::where('name', $name)->first();
        if($config) {
            
            return $config->title;
        }
        return "";
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Furniture extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];
}

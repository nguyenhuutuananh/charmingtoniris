<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Project extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];
    use Sluggable;
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
    public function furniture() {
        return $this->belongsTo(Furniture::class, 'furniture_id');
    }
    public function images() {
        return $this->hasMany(ProjectImage::class, 'project_id');
    }
    public function category() {
        return $this->belongsTo(ProjectCategory::class, 'category_id');
    }
}

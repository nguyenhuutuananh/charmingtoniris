<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Post extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];
    use Sluggable;
    public function category() {
        return $this->belongsTo(PostCategory::class, 'category_id');
    }
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}

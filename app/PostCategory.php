<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class PostCategory extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];
    use Sluggable;
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
    public function posts() {
        return $this->hasMany(Post::class, 'category_id');
    }
}

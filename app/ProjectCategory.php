<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class ProjectCategory extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];
    use Sluggable;
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
    public function projects() {
        return $this->hasMany(Project::class, 'category_id');
    }
}

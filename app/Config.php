<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $fillable = ['value'];
    
    public static function get_all() {
        $configs = Config::all();
        $config_array = new \stdClass();
        foreach($configs as $key=>$value) {
            $key_code = $value->name;
            $config_array->$key_code = $value->value;
        }
        return $config_array;
    }
}

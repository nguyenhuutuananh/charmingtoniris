<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

//
Route::get('/gioi-thieu/tong-quan.html', 'PageController@tong_quan')->name('tong_quan');
Route::get('/gioi-thieu/chu-dau-tu.html', 'PageController@chu_dau_tu')->name('chu_dau_tu');

Route::get('/vi-tri.html', 'PageController@vi_tri')->name('vi_tri');
Route::get('/tien-ich.html', 'PageController@tien_ich')->name('tien_ich');
Route::get('/thu-vien.html', 'PageController@thu_vien')->name('thu_vien');

Route::get('/mat-bang', 'GroundController@index')->name('mat_bang');
Route::get('/mat-bang/tang-1.html', 'GroundController@tang_1')->name('mat_bang_tang_1');
Route::get('/mat-bang/tang-2.html', 'GroundController@tang_2')->name('mat_bang_tang_2');
Route::get('/mat-bang/tang-3.html', 'GroundController@tang_3')->name('mat_bang_tang_3');
Route::get('/mat-bang/tang-5.html', 'GroundController@tang_5')->name('mat_bang_tang_5');
Route::get('/mat-bang/tang-6-32.html', 'GroundController@tang_6_32')->name('mat_bang_tang_6_32');
Route::get('/mat-bang/tang-33-34.html', 'GroundController@tang_33_34')->name('mat_bang_tang_33_34');

Route::get('/mat-bang/can-ho-a.html', 'GroundController@can_ho_a')->name('can_ho_a');
Route::get('/mat-bang/can-ho-b1.html', 'GroundController@can_ho_b1')->name('can_ho_b1');
Route::get('/mat-bang/can-ho_b2.html', 'GroundController@can_ho_b2')->name('can_ho_b2');
Route::get('/mat-bang/can-ho_b3.html', 'GroundController@can_ho_b3')->name('can_ho_b3');
Route::get('/mat-bang/can-ho_b4.html', 'GroundController@can_ho_b4')->name('can_ho_b4');
Route::get('/mat-bang/can-ho_b5.html', 'GroundController@can_ho_b5')->name('can_ho_b5');
Route::get('/mat-bang/can-ho_b6.html', 'GroundController@can_ho_b6')->name('can_ho_b6');
Route::get('/mat-bang/can-ho_b7.html', 'GroundController@can_ho_b7')->name('can_ho_b7');
Route::get('/mat-bang/can-ho_b8.html', 'GroundController@can_ho_b8')->name('can_ho_b8');
Route::get('/mat-bang/can-ho_c1.html', 'GroundController@can_ho_c1')->name('can_ho_c1');
Route::get('/mat-bang/can-ho-c2.html', 'GroundController@can_ho_c2')->name('can_ho_c2');
Route::get('/mat-bang/can-ho-c3.html', 'GroundController@can_ho_c3')->name('can_ho_c3');

Route::get('/', 'HomeController@index')->name('home');
Route::get('/page/{slug}.html', 'PageController@detail')->where('slug', '[a-z0-9\-]*?')->name('page.detail');

Route::get('/tin-tuc.html', 'NewsController@index')->name('news');
Route::get('/tin-tuc/{slug}.html', 'NewsController@detail')->where('slug', '[a-z0-9\-]*?')->name('news.detail');


Route::get('/lien-he.html', 'ContactController@index')->name('contact');
Route::post('/lien-he/save', 'ContactController@save')->name('contact.save');
Route::post('/lien-he/save_special', 'ContactController@save_special')->name('contact.save_special');
// Route::post('/lien-he/save-email', 'ContactController@save_email')->name('contact.save');

// Route::get('/{slug}.html', 'ProjectController@list')->where('slug', '[a-z0-9\-]*?')->name('project.list');
// Route::get('/{category_slug}/{slug}.html', 'ProjectController@detail')->where('category_slug', '[a-z0-9\-]*?')->where('slug', '[a-z0-9\-]*?')->name('project.detail');

// Admin panel
Route::prefix('admin')->group(function () {
    // Home
    Route::get('/', 'Admin\HomeController@index')->name('admin_home');
    
    // Project
    // List
    Route::get('/project', 'Admin\ProjectController@index')->name('admin_project_list');
    Route::get('/project/ajax_list', 'Admin\ProjectController@ajax_list')->name('admin_project_ajax_list');
    // Create
    Route::get('/project/create', 'Admin\ProjectController@create')->name('admin_project_create');
    Route::post('project/create', 'Admin\ProjectController@store');
        
    // Edit
    Route::get('/project/edit/{project}', 'Admin\ProjectController@edit')->name('admin_project_edit');
    Route::post('project/edit/{project}', 'Admin\ProjectController@update');
    
    // Delete
    Route::get('/project/delete/{project}', 'Admin\ProjectController@delete')->name('admin_project_delete');

    // Post
    // List
    Route::get('/post', 'Admin\PostController@index')->name('admin_post_list');
    Route::get('/post/ajax_list', 'Admin\PostController@ajax_list')->name('admin_post_ajax_list');
    // Create
    Route::get('/post/create', 'Admin\PostController@create')->name('admin_post_create');
    Route::post('post/create', 'Admin\PostController@store');
        
    // Edit
    Route::get('/post/edit/{post}', 'Admin\PostController@edit')->name('admin_post_edit');
    Route::post('post/edit/{post}', 'Admin\PostController@update');
    
    // Delete
    Route::get('/post/delete/{post}', 'Admin\PostController@delete')->name('admin_post_delete');

    // Post Category
    // List
    Route::get('/post-category', 'Admin\PostCategoryController@index')->name('admin_post_category_list');
    Route::get('/post-category/ajax_list', 'Admin\PostCategoryController@ajax_list')->name('admin_post_category_ajax_list');
    // Create
    Route::get('/post-category/create', 'Admin\PostCategoryController@create')->name('admin_post_category_create');
    Route::post('post-category/create', 'Admin\PostCategoryController@store');
        
    // Edit
    // Route::get('/post-category/edit/{post_category}', 'Admin\PostCategoryController@edit')->name('admin_post_category_edit');
    // Route::post('post-category/edit/{post_category}', 'Admin\PostCategoryController@update');
    Route::get('/post-category/edit/{post_category}', function() {
        return redirect()->route('admin_home');
    })->name('admin_post_category_edit');
    // Route::post('post-category/edit/{post_category}', function() {
    //     return redirect()->route('admin_home');
    // });

    // Delete
    // Route::get('/post-category/delete/{post_category}', 'Admin\PostCategoryController@delete')->name('admin_post_category_delete');

    // Page
    // List
    Route::get('/page', 'Admin\PageController@index')->name('admin_page_list');
    Route::get('/page/ajax_list', 'Admin\PageController@ajax_list')->name('admin_page_ajax_list');
            
    // Edit
    Route::get('/page/edit/{page}', 'Admin\PageController@edit')->name('admin_page_edit');
    Route::post('page/edit/{page}', 'Admin\PageController@update');

    // Slider detail
    // List
    Route::get('/slider_detail', 'Admin\SliderController@index')->name('admin_slider_detail_list');
    Route::get('/slider_detail/ajax_list', 'Admin\SliderController@ajax_list')->name('admin_slider_detail_ajax_list');
    // Create
    Route::get('/slider_detail/create', 'Admin\SliderController@create')->name('admin_slider_detail_create');
    Route::post('slider_detail/create', 'Admin\SliderController@store');
        
    // Edit
    Route::get('/slider_detail/edit/{slider_detail}', 'Admin\SliderController@edit')->name('admin_slider_detail_edit');
    Route::post('slider_detail/edit/{slider_detail}', 'Admin\SliderController@update');
    
    // Delete
    Route::get('/slider_detail/delete/{slider_detail}', 'Admin\SliderController@delete')->name('admin_slider_detail_delete');

    // Post Category
    // List
    Route::get('/post-category', 'Admin\PostCategoryController@index')->name('admin_post_category_list');
    Route::get('/post-category/ajax_list', 'Admin\PostCategoryController@ajax_list')->name('admin_post_category_ajax_list');
    // Create
    Route::get('/post-category/create', 'Admin\PostCategoryController@create')->name('admin_post_category_create');
    Route::post('post-category/create', 'Admin\PostCategoryController@store');

    // Contact
    // List
    Route::get('/contact', 'Admin\ContactController@index')->name('admin_contact_list');
    Route::get('/contact/ajax_list', 'Admin\ContactController@ajax_list')->name('admin_contact_ajax_list');
    
    // Special Contact
    // List
    Route::get('/contact-special', 'Admin\ContactController@index_special')->name('admin_contact_special_list');
    Route::get('/contact-special/ajax_list', 'Admin\ContactController@ajax_list_special')->name('admin_contact_special_ajax_list');

    // Change Config
    Route::prefix('config')->group(function() {
        Route::get('/', 'Admin\ConfigController@index')->name('admin_config_page');
        Route::post('/', 'Admin\ConfigController@save');
    });
    Route::prefix('tong-quan')->group(function() {
        Route::get('/', 'Admin\ConfigController@tong_quan')->name('admin_config_tong_quan');
        Route::post('/', 'Admin\ConfigController@save');
    });
    Route::prefix('vi-tri')->group(function() {
        Route::get('/', 'Admin\ConfigController@vi_tri')->name('admin_config_vi_tri');
        Route::post('/', 'Admin\ConfigController@save');
    });
    Route::prefix('tien-ich')->group(function() {
        Route::get('/', 'Admin\ConfigController@tien_ich')->name('admin_config_tien_ich');
        Route::post('/', 'Admin\ConfigController@save');
    });
    Route::prefix('chu-dau-tu')->group(function() {
        Route::get('/', 'Admin\ConfigController@chu_dau_tu')->name('admin_config_chu_dau_tu');
        Route::post('/', 'Admin\ConfigController@save');
    });

    // Post
    // List
    Route::get('/partner', 'Admin\PartnerController@index')->name('admin_partner_list');
    Route::get('/partner/ajax_list', 'Admin\PartnerController@ajax_list')->name('admin_partner_ajax_list');
    // Create
    Route::get('/partner/create', 'Admin\PartnerController@create')->name('admin_partner_create');
    Route::post('partner/create', 'Admin\PartnerController@store');
        
    // Edit
    Route::get('/partner/edit/{partner}', 'Admin\PartnerController@edit')->name('admin_partner_edit');
    Route::post('partner/edit/{partner}', 'Admin\PartnerController@update');
    
    // Delete
    Route::get('/partner/delete/{partner}', 'Admin\PartnerController@delete')->name('admin_partner_delete');

});

// Route::prefix('admin')->group(function() {
//     // without auth
//     Route::get('/login', 'Admin\LoginController@showLoginForm')->name('admin.login');
//     Route::post('/login', 'Admin\LoginController@login');
//     Route::get('/logout', 'Admin\LoginController@logout');
//     // with auth
//     Route::middleware('auth')->group(function() {
//         Route::get('/', 'Admin\HomeController@index')->name('admin.home');
//         Route::get('/settings', function () {
//             return "OK";
//         })->name('admin.settings');
//     });
// });
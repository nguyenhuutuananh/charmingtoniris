﻿function is_touch_device() {
    var a = document.createElement("div");
    return a.setAttribute("ongesturestart", "return;"), "function" == typeof a.ongesturestart
}

function init_map(mapid) {
    var contentString = $(".lbl_map_address").html(),
        map_coordinate_x = eval($(".map_coordinate_x").html()),
        map_coordinate_y = eval($(".map_coordinate_y").html()),
        mapOptions = {
            zoom: 15,
            center: new google.maps.LatLng(map_coordinate_x, map_coordinate_y)
        },
        map = new google.maps.Map(document.getElementById(mapid), mapOptions),
        triangleCoords = [{
            lat: 10.73836,
            lng: 106.74084
        }, {
            lat: 10.73886,
            lng: 106.74143
        }, {
            lat: 10.73805,
            lng: 106.74218
        }, {
            lat: 10.73753,
            lng: 106.74159
        }],
        bermudaTriangle = new google.maps.Polygon({
            paths: triangleCoords,
            strokeColor: "#FF0000",
            strokeOpacity: .8,
            strokeWeight: 2,
            fillColor: "#FF8800",
            fillOpacity: .35
        });
    bermudaTriangle.setMap(map);
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(map_coordinate_x, map_coordinate_y),
        map: map,
        icon: "/template/tint/images/ico-map-dot.svg",
        title: "Click to zoom"
    }),
        infowindow = new google.maps.InfoWindow({
            content: contentString
        });
    google.maps.event.addListener(map, "center_changed", function () {
        window.setTimeout(function () {
            map.panTo(marker.getPosition())
        }, 6e4)
    }), google.maps.event.addListener(marker, "click", function () {
        infowindow.open(map, marker)
    })
}

function checkOnline() {
    $(".support-online a").each(function () {
        var a = $(this).attr("nickname"),
            e = $(this).attr("nicktype"),
            r = $(this);
        $.ajax({
            type: "GET",
            cache: !1,
            url: "/AJAX.aspx",
            data: "func=CHECKSTATUS&nickname=" + a + "&nicktype=" + e,
            dataType: "html",
            success: function (a) {
                "yahoo" == e ? "01" == a ? $(r).children("span").addClass("yahooonline") : $(r).children("span").addClass("yahoooffline") : "2" == a ? $(r).children("span").addClass("skypeonline") : $(r).children("span").addClass("skypeoffline")
            },
            error: function (a) { }
        })
    })
}

function equalHeight(a) {
    tallest = 0, a.each(function () {
        thisHeight = $(this).height(), thisHeight > tallest && (tallest = thisHeight)
    }), a.height(tallest)
}

function AddToCart(a, e, r, t, o, l) {
    $.ajax({
        type: "GET",
        url: "/AJAX.aspx",
        data: "func=cart&id=" + a + "&att=" + e + "&name=" + r + "&qty=" + t + "&price=" + o + "&pic=" + l,
        dataType: "html",
        success: function (a) {
            jAlert(a)
        },
        error: function (a) {
            jAlert(a)
        }
    })
}

function Captcha() {
    var a = $(".imgCatcha");
    $.ajax({
        type: "GET",
        cache: !1,
        url: "/AJAX.aspx",
        data: "func=CAPTCHA",
        dataType: "html",
        success: function (e) {
            $(a).attr("src", e)
        },
        error: function (a) { }
    })
}

function DynamicCaptcha(a, e) {
    $.ajax({
        type: "GET",
        cache: !1,
        url: "/AJAX.aspx",
        data: "func=DYNAMICCAPTCHA&key=" + e,
        dataType: "html",
        success: function (e) {
            $(a).attr("src", e)
        },
        error: function (a) { }
    })
}

function RequiredEmptyField(a, e) {
    return "" != $(a).val().trim() || ($.Msg.show(e), !1)
}

function AreaMaxLength(a, e, r) {
    var t = a.MaxLength;
    null == t && (t = r);
    var o = parseInt(t);
    8 != e.keyCode && 46 != e.keyCode && 37 != e.keyCode && 38 != e.keyCode && 39 != e.keyCode && 40 != e.keyCode && a.value.length > o - 1 && (window.event ? e.returnValue = !1 : e.preventDefault())
}

function Valid_Schedule(a, e, r, t, o, l, s, n, i, d, c, v) {
    var u = "",
        m = "<br>",
        p = $(a),
        C = $(t),
        h = $(s),
        f = $(d);
    return "" == p.val() || p.val() == r ? ($(p).addClass("error"), u += "- " + e + m) : $(p).removeClass("error"), 1 == /[A-Za-z]$/.test(C.val()) || "" == C.val() || C.val() == l ? ($(C).addClass("error"), u += "- " + o + m) : $(C).removeClass("error"), "" == h.val() || h.val() == i ? ($(h).addClass("error"), u += "- " + n + m) : $(h).removeClass("error"), 0 == /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/.test(f.val()) || "" == f.val() || f.val() == v ? ($(f).addClass("error"), u += "- " + c + m) : $(f).removeClass("error"), "" == u || (bootbox.alert(u), !1)
}

function Valid_Profile(a, e, r, t, o, l, s, n) {
    var i = "",
        d = "<br />",
        c = $(a),
        v = $(r),
        u = $(o),
        m = $(s);
    return "" == c.val() ? ($(c).addClass("error"), i += "- " + e + d) : $(c).removeClass("error"), 0 == /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/.test(v.val()) || "" == v.val() ? ($(v).addClass("error"), i += "- " + t + d) : $(v).removeClass("error"), /[A-Za-z]$/.test(u.val()) || "" == u.val() ? ($(u).addClass("error"), i += "- " + l + d) : $(u).removeClass("error"), "" == m.val() ? ($(m).addClass("error"), i += "- " + n + d) : $(m).removeClass("error"), "" == i
}

function Valid_Contact(a, e, r, t, o, l, s, n, i, d, c, v, u, m, p) {
    var C = "",
        h = "- ",
        f = "<br>",
        g = $(a),
        A = $(t),
        b = $(s),
        w = $(d),
        y = $(u),
        z = "";
    return $.ajax({
        type: "GET",
        cache: !1,
        url: "/AJAX.aspx",
        async: !1,
        data: "func=checkcaptcha&captcha=" + $(y).val(),
        dataType: "html",
        success: function (a) {
            return z = a, !1
        }
    }), "" == g.val() || g.val() == r ? ($(g).addClass("error"), C += h + e + f) : $(g).removeClass("error"), 0 == /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/.test(b.val()) || "" == b.val() || b.val() == i ? ($(b).addClass("error"), C += h + n + f) : $(b).removeClass("error"), 1 == /[A-Za-z]$/.test(A.val()) || "" == A.val() || A.val() == l ? ($(A).addClass("error"), C += h + o + f) : $(A).removeClass("error"), "" == w.val() || w.val() == v ? ($(w).addClass("error"), C += h + c + f) : $(w).removeClass("error"), "" == y.val() || "False" == z || y.val() == p ? ($(y).addClass("error"), C += h + m + f) : $(y).removeClass("error"), "" == C || (bootbox.alert(C), !1)
}

function Valid_NewsLetter(a, e, r, t, o, l, s, n, i) {
    var d = "",
        c = "<br>",
        v = $(a),
        u = $(t),
        m = $(s);
    return 1 == /[(0-9!@#$%^&*(+_.?/`~)]/.test(v.val()) || "" == v.val() || v.val() == r ? ($(v).addClass("error"), d += "- " + e + c) : $(v).removeClass("error"), 1 == /[A-Za-z]$/.test(u.val()) || "" == u.val() || u.val() == l ? ($(u).addClass("error"), d += "- " + o + c) : $(u).removeClass("error"), 0 == /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/.test(m.val()) || "" == m.val() || m.val() == i ? ($(m).addClass("error"), d += "- " + n + c) : $(m).removeClass("error"), "" == d || (bootbox.alert(d), !1)
}

function Valid_NewsLetter2(a, e, r, t, o, l, s, n, i) {
    var d = "",
        c = "<br>",
        v = $(a),
        u = $(t),
        m = $(s);
    return "" == v.val() || v.val() == r ? ($(v).addClass("error"), d += "- " + e + c) : $(v).removeClass("error"), 1 == /[A-Za-z]$/.test(u.val()) || "" == u.val() || u.val() == l ? ($(u).addClass("error"), d += "- " + o + c) : $(u).removeClass("error"), 0 == /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/.test(m.val()) || "" == m.val() || m.val() == i ? ($(m).addClass("error"), d += "- " + n + c) : $(m).removeClass("error"), "" == d || (bootbox.alert(d), !1)
}

function Valid_NewsLetter3(a, e, r, t, o, l, s, n, i) {
    var d = "",
        c = "<br>",
        v = $(a),
        u = $(t),
        m = $(s);
    return "" == v.val() || v.val() == r ? ($(v).addClass("error"), d += "- " + e + c) : $(v).removeClass("error"), 1 == /[A-Za-z]$/.test(u.val()) || "" == u.val() || u.val() == l ? ($(u).addClass("error"), d += "- " + o + c) : $(u).removeClass("error"), 0 == /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/.test(m.val()) || "" == m.val() || m.val() == i ? ($(m).addClass("error"), d += "- " + n + c) : $(m).removeClass("error"), "" == d || (bootbox.alert(d), !1)
}

function Valid_Register(a, e, r, t, o, l, s, n, i, d, c) {
    var v = "",
        u = "- ",
        m = "<br>",
        p = $(a),
        C = $(o),
        h = $(r),
        f = $(s),
        g = $(i),
        A = "";
    return $.ajax({
        type: "GET",
        cache: !1,
        url: "/AJAX.aspx",
        async: !1,
        data: "func=CHECKDYNAMICCAPTCHA&captcha=" + $(g).val() + "&key=" + c,
        dataType: "html",
        success: function (a) {
            return A = a, !1
        }
    }), "" == p.val() ? ($(p).addClass("error"), v += u + e + m) : $(p).removeClass("error"), 0 == /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/.test(C.val()) || "" == C.val() ? ($(C).addClass("error"), v += u + l + m) : $(C).removeClass("error"), "" == h.val() ? ($(h).addClass("error"), v += u + t + m) : $(h).removeClass("error"), "" == f.val() ? ($(f).addClass("error"), v += u + n + m) : $(f).removeClass("error"), "" == g.val() || "False" == A ? ($(g).addClass("error"), v += u + d + m) : $(g).removeClass("error"), "" == v || (bootbox.alert(v), !1)
}

function Valid_Consultant(a, e, r, t) {
    var o = "",
        l = $(a),
        s = $(r);
    return "" == l.val() ? ($(l).addClass("error"), o += "- " + e + "<br>") : $(l).removeClass("error"), "" == s.val() || 1 == /[A-Za-z]$/.test(s.val()) ||  s.val() == l ? ($(s).addClass("error"), o += "- " + t + "<br>") : $(s).removeClass("error"), "" == o || (bootbox.alert(o), !1)
}

function Valid_FormRegister(a, e, r, t, o, l, s, n, i, d, c, v, u, m) {
    var p = "",
        C = "<br>",
        h = $(a),
        f = $(r),
        g = $(s),
        A = $(o);
    return "" == h.val() ? ($(h).addClass("error"), p += "- " + e + C) : $(h).removeClass("error"), "" == g.val() ? ($(g).addClass("error"), p += "- " + n + C) : $(g).removeClass("error"), "" == f.val() || 0 == /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/.test(f.val()) ? ($(f).addClass("error"), p += "- " + t + C) : $(f).removeClass("error"), "" == A.val() ? ($(A).addClass("error"), p += "- " + l + C) : $(A).removeClass("error"), "" == p || (bootbox.alert(p), !1)
}

function Valid_faqs(a, e, r, t, o, l, s, n, i, d, n, c) {
    var v = "",
        u = $(a),
        m = $(d),
        p = $(s),
        C = $(t);
    return "" == u.val() || u.val() == r ? ($(u).addClass("error"), v += "- " + e + "\n") : $(u).removeClass("error"), 0 != /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/.test(m.val()) && "" != m.val() && m.val() != c || "" != p.val() && p.val() != i ? $(m).removeClass("error") : ($(m).addClass("error"), v += "- " + n + "\n"), "" == C.val() || C.val() == l ? ($(C).addClass("error"), v += "- " + o + "\n") : $(C).removeClass("error"), "" == v || (swal("Vui lòng kiểm tra lại:", v), !1)
}

function checkFileExt(a, e) {
    var r = a.value;
    if (-1 == r.indexOf(".")) return !1;
    var t = "",
        o = r.substring(r.lastIndexOf(".") + 1).toLowerCase();
    switch (e) {
        case "picture":
            t = "jpg,png,gif";
            break;
        case "document":
            t = "doc,docx,xls,xlsx,ppt,pptx,mdb,mdbx,pdf,txt,zip,rar,7z,cab,iso";
            break;
        case "media":
            t = "mp3,flv,wmv,mpg,mp4,ogg";
            break;
        case "compress":
            t = "zip,rar,7z"
    }
    var l = new Array;
    l = t.split(",");
    for (var s = 0; s < l.length; s++)
        if (o == l[s]) return !0;
    return $.Msg.show(o.toUpperCase() + " - file type not supported!"), a.value = "", !1
}

function openBox(a, e, r) {
    var t = ($(window).width() - a) / 2,
        o = ($(window).height() - e) / 2;
    newParameter = "width=" + a + ",height=" + e + ",addressbar=no,scrollbars=yes,toolbar=no,top=" + o + ",left=" + t + ", resizable=no", newWindow = window.open(r, "a", newParameter), newWindow.focus()
}

function getCookie(a) {
    var e, r, t, o = document.cookie.split(";");
    for (e = 0; e < o.length; e++)
        if (r = o[e].substr(0, o[e].indexOf("=")), t = o[e].substr(o[e].indexOf("=") + 1), (r = r.replace(/^\s+|\s+$/g, "")) == a) return unescape(t)
}

function setCookie(a, e, r) {
    var t = new Date;
    t.setDate(t.getDate() + r);
    var o = escape(e) + (null == r ? "" : "; expires=" + t.toUTCString());
    document.cookie = a + "=" + o
}

function Valid_SendEmail(a, e, r, t, o, l, s, n, i, d, c) {
    var v = "",
        u = "- ",
        m = "<br />",
        p = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/,
        C = $(e),
        h = $(t),
        f = $(l),
        g = $(n),
        A = $(d);
    return "" == C.val() ? ($(C).addClass("error"), v += u + r + m) : $(C).removeClass("error"), 0 == p.test(h.val()) || "" == h.val() ? ($(h).addClass("error"), v += u + o + m) : $(h).removeClass("error"), "" == f.val() ? ($(f).addClass("error"), v += u + s + m) : $(f).removeClass("error"), 0 == p.test(g.val()) || "" == g.val() ? ($(g).addClass("error"), v += u + i + m) : $(g).removeClass("error"), "" == A.val() ? ($(A).addClass("error"), v += u + c + m) : $(A).removeClass("error"), "" == v || (jAlert(v), !1)
}
Number.prototype.formatMoney = function (a, e, r, t) {
    a = isNaN(a = Math.abs(a)) ? 2 : a, e = void 0 !== e ? e : "$", r = r || ",", t = t || ".";
    var o = this,
        l = o < 0 ? "-" : "",
        s = parseInt(o = Math.abs(+o || 0).toFixed(a), 10) + "",
        n = (n = s.length) > 3 ? n % 3 : 0;
    return e + l + (n ? s.substr(0, n) + r : "") + s.substr(n).replace(/(\d{3})(?=\d)/g, "$1" + r) + (a ? t + Math.abs(o - s).toFixed(a).slice(2) : "")
}, $(function () {
    var a = window.location.pathname.toString();
    if (-1 != a.indexOf(".html")) {
        var e = a.split("/"),
            r = $(".navbar-gem ul.navbar-nav  li:not(.language) > a");
        $.each(r, function () {
            if ($(this).attr("href")) {
                var a = $(this).attr("href").toString().split("/");
                if (e.length > 1 && a.length > 1 && e[1].toString().replace(".html", "") == a[1].toString().replace(".html", "")) return $(this).parent().addClass("active"), 0, !1
            }
        })
    } else $(".page-header .nav li:first").addClass("active");
    jQuery.fn.ForceNumericOnly = function () {
        return this.each(function () {
            $(this).keydown(function (a) {
                var e = a.charCode || a.keyCode || 0;
                return 8 == e || 9 == e || 46 == e || e >= 37 && e <= 40 || e >= 48 && e <= 57 || e >= 96 && e <= 105
            })
        })
    }, $.fn.center = function () {
        this.css({
            left: ($(document).width() - $(this).outerWidth()) / 2,
            top: ($(window).height() - $(this).outerHeight()) / 2
        })
    }, $(".nav-left ul > li:last, .nav-right ul > li:last,.page-nav .mainnav ul li:last,.main-content ul.subnav li:last").addClass("last"), $('.nav-left ul > li:first,.nav-right ul > li:first,.social a:first,.main-content ul.subnav li:first, .booking-form .row .column-1 input[type="radio"]:first').addClass("first"), $(".content-sitemap .column  ul").each(function () {
        $(this).children("li:last").addClass("last")
    }), $(".AllowOnlyNumericInputs").ForceNumericOnly(), $(".imgRefreshCaptcha").click(function () {
        Captcha()
    }), $(".gotop").click(function () {
        return $("html, body").animate({
            scrollTop: 0
        }, 500), !1
    }), $(".page-project-master-detail .bt-back").click(function () {
        window.history.back()
    }), $(".btn-toggle-search").click(function () {
        $(this).next().toggleClass("hide")
    }), $(window).scroll(function () {
        $(this).scrollTop() > 0 ? ($(".page-header").addClass("fixed"), $("#gotop").fadeIn()) : ($(".page-header").removeClass("fixed"), $("#gotop").fadeOut())
    }), $(document).click(function (a) {
        $(a.target).is("a") || $(".collapse").collapse("hide")
    }), $(".navbar-ex1-collapse").on("show.bs.collapse", function () {
        $(".page-header .navbar-toggle").addClass("active")
    }), $(".navbar-ex1-collapse").on("hide.bs.collapse", function () {
        $(".page-header .navbar-toggle").removeClass("active")
    }), jQuery(window).trigger("resize").trigger("scroll"), $(".error").click(function () {
        $(this).removeClass(".error")
    }), checkOnline(), $(".footer-nav .nav").html($(".page-header .header-left ul.navigation").html())
}), $.urlParam = function (a) {
    var e = new RegExp("[?&]" + a + "=([^&#]*)").exec(window.location.href);
    return null == e ? null : e[1] || 0
};
let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css');

// Admin
// SASS Bootstrap
mix.sass('resources/assets/admin/sass/app.scss', 'public/admin-template/bootstrap/css');
// Script Bootstrap
mix.js('resources/assets/admin/js/app.js', 'public/admin-template/bootstrap/js');

mix.styles([
        // Bootstrap
        'public/admin-template/bootstrap/css/app.css',
        // Animate.css
        'resources/assets/admin/plugins/animate.css/animate.min.css',

        // Nprogress
        'resources/assets/admin/plugins/nprogress/nprogress.css',

        // iCheck 
        'resources/assets/admin/plugins/iCheck/skins/flat/green.css',

        // bootstrap-progressbar
        'resources/assets/admin/plugins/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css',

        // JQVMap 
        'resources/assets/admin/plugins/jqvmap/dist/jqvmap.min.css',

        // bootstrap-daterangepicker
        'resources/assets/admin/plugins/bootstrap-daterangepicker/daterangepicker.css',

        // Select2 
        'resources/assets/admin/plugins/select2/dist/css/select2.min.css',

        // Switchery 
        'resources/assets/admin/plugins/switchery/dist/switchery.min.css',

        // starrr 
        'resources/assets/admin/plugins/starrr/dist/starrr.css',

        // PNotify 
        'resources/assets/admin/plugins/pnotify/dist/pnotify.css',
        'resources/assets/admin/plugins/pnotify/dist/pnotify.buttons.css',
        'resources/assets/admin/plugins/pnotify/dist/pnotify.nonblock.css',
        
        // Datatables 
        'resources/assets/admin/plugins/datatables.net-bs/css/dataTables.bootstrap.min.css',
        'resources/assets/admin/plugins/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
        'resources/assets/admin/plugins/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
        'resources/assets/admin/plugins/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
        'resources/assets/admin/plugins/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',

        // bootstrap-select
        'resources/assets/admin/plugins/bootstrap-select/css/bootstrap-select.min.css',

        // Custom
        'resources/assets/admin/custom.css',
    ], 'public/admin-template/css/app.css')
    .scripts([
        // Bootstrap
        'public/admin-template/bootstrap/js/app.js',

        // Nprogress
        'resources/assets/admin/plugins/nprogress/nprogress.js',

        // FastClick 
        'resources/assets/admin/plugins/fastclick/lib/fastclick.js',

        // Chart.js
        'resources/assets/admin/plugins/Chart.js/dist/Chart.min.js',

        // gauge.js
        'resources/assets/admin/plugins/gauge.js/dist/gauge.min.js',

        // bootstrap-progressbar
        'resources/assets/admin/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js',

        // iCheck 
        'resources/assets/admin/plugins/iCheck/icheck.min.js',

        // Skycons 
        'resources/assets/admin/plugins/skycons/skycons.js',

        // Flot 
        'resources/assets/admin/plugins/Flot/jquery.flot.js',
        'resources/assets/admin/plugins/Flot/jquery.flot.pie.js',
        'resources/assets/admin/plugins/Flot/jquery.flot.time.js',
        'resources/assets/admin/plugins/Flot/jquery.flot.stack.js',
        'resources/assets/admin/plugins/Flot/jquery.flot.resize.js',

        // Flot plugins
        'resources/assets/admin/plugins/flot.orderbars/js/jquery.flot.orderBars.js',
        'resources/assets/admin/plugins/flot-spline/js/jquery.flot.spline.min.js',
        'resources/assets/admin/plugins/flot.curvedlines/curvedLines.js',

        // DateJS 
        'resources/assets/admin/plugins/DateJS/build/date.js',

        // JQVMap 
        'resources/assets/admin/plugins/jqvmap/dist/jquery.vmap.js',
        'resources/assets/admin/plugins/jqvmap/dist/maps/jquery.vmap.world.js',
        'resources/assets/admin/plugins/jqvmap/examples/js/jquery.vmap.sampledata.js',

        // bootstrap-daterangepicker
        'resources/assets/admin/plugins/moment/min/moment.min.js',
        'resources/assets/admin/plugins/bootstrap-daterangepicker/daterangepicker.js',

        // jQuery Tags Input
        'resources/assets/admin/plugins/jquery.tagsinput/src/jquery.tagsinput.js',

        // Switchery 
        'resources/assets/admin/plugins/switchery/dist/switchery.min.js',

        // Select2 
        'resources/assets/admin/plugins/select2/dist/js/select2.full.min.js',

        // Parsley 
        'resources/assets/admin/plugins/parsleyjs/dist/parsley.min.js',

        // Autosize 
        'resources/assets/admin/plugins/autosize/dist/autosize.min.js',

        // starrr 
        'resources/assets/admin/plugins/starrr/dist/starrr.js',

        // jQuery autocomplete
        'resources/assets/admin/plugins/devbridge-autocomplete/dist/jquery.autocomplete.min.js',

        // PNotify 
        'resources/assets/admin/plugins/pnotify/dist/pnotify.js',
        'resources/assets/admin/plugins/pnotify/dist/pnotify.buttons.js',
        'resources/assets/admin/plugins/pnotify/dist/pnotify.nonblock.js',

        // Datatables   
        'resources/assets/admin/plugins/datatables.net/js/jquery.dataTables.min.js',
        'resources/assets/admin/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js',
        'resources/assets/admin/plugins/datatables.net-buttons/js/dataTables.buttons.min.js',
        'resources/assets/admin/plugins/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
        'resources/assets/admin/plugins/datatables.net-buttons/js/buttons.flash.min.js',
        'resources/assets/admin/plugins/datatables.net-buttons/js/buttons.html5.min.js',
        'resources/assets/admin/plugins/datatables.net-buttons/js/buttons.print.min.js',
        'resources/assets/admin/plugins/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
        'resources/assets/admin/plugins/datatables.net-keytable/js/dataTables.keyTable.min.js',
        'resources/assets/admin/plugins/datatables.net-responsive/js/dataTables.responsive.min.js',
        'resources/assets/admin/plugins/datatables.net-responsive-bs/js/responsive.bootstrap.js',
        'resources/assets/admin/plugins/datatables.net-scroller/js/dataTables.scroller.min.js',
        'resources/assets/admin/plugins/jszip/dist/jszip.min.js',
        'resources/assets/admin/plugins/pdfmake/build/pdfmake.min.js',
        'resources/assets/admin/plugins/pdfmake/build/vfs_fonts.js',

        // bootstrap-select
        'resources/assets/admin/plugins/bootstrap-select/js/bootstrap-select.min.js',
        'resources/assets/admin/plugins/bootstrap-select/js/defaults-vi_VN.min.js',

        // Custom
        'resources/assets/admin/custom.js',

    ], 'public/admin-template/js/app.js');

mix.copyDirectory('resources/assets/admin/plugins/font-awesome', 'public/font-awesome/');

// mix.version();

mix.disableNotifications();
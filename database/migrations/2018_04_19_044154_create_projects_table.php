<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('title');
            $table->string('slug');
            $table->mediumText('description')->nullable();
            $table->longText('content')->nullable();
            $table->string('price_text')->nullable();
            $table->decimal('price', 12, 0)->nullable();

            $table->integer('acreages')->nullable()->default(0);
            $table->integer('bed_rooms')->nullable()->default(0);
            $table->integer('toilets')->nullable()->default(0);
            $table->integer('garages')->nullable()->default(0);

            $table->string('management_fee')->nullable();
            $table->string('vat')->nullable();
            $table->string('motorcycle_fee')->nullable();
            $table->string('car_fee')->nullable();

            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('project_categories');

            $table->integer('furniture_id')->unsigned();
            $table->foreign('furniture_id')->references('id')->on('furniture');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special_contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('project_category');
            $table->string('thap');
            $table->string('tang');
            $table->string('vi_tri_can');
            $table->string('furniture');
            $table->string('hien_trang_nha');
            $table->string('gia_ban');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('special_contacts');
    }
}

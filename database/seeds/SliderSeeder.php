<?php

use Illuminate\Database\Seeder;
use App\Slider;

class SliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        // Slider
        $slider = new Slider();
        $slider->title = "Home slider";
        $slider->save();

        $slider->images()->createMany([
            ['title' => 'Ảnh 1', 'imageUrl' => '/gem/assets/uploads/myfiles/images/trang-chu/banner.jpg'],
            ['title' => 'Ảnh 2', 'imageUrl' => '/gem/assets/uploads/myfiles/images/trang-chu/banner-ngay.jpg'],
        ]);

        // Slider
        $slider = new Slider();
        $slider->title = "Gallery";
        $slider->save();

        $slider->images()->createMany([
            ['title' => 'Phối cảnh dự án', 'imageUrl' => '/gem/assets/uploads/images/post/phoi-canh-du-an-nho_632018134734.jpg'],
            ['title' => 'Hình ảnh thực tế nhà mẫu', 'imageUrl' => '/gem/assets/uploads/images/post/phoi-canh-can-ho-nho_63201814421.jpg'],
            ['title' => 'Sảnh căn hộ, thang máy', 'imageUrl' => '/gem/assets/uploads/images/post/sanhavt_26122017152640.jpg'],
            ['title' => 'Tiện ích ngoại khu', 'imageUrl' => '/gem/assets/uploads/images/post/tienichnk_26122017154034.jpg'],
        ]);

        // Slider
        $slider = new Slider();
        $slider->title = "Tiện ích";
        $slider->save();

        $slider->images()->createMany([
            ['title' => 'Khu vui chơi trẻ em', 'imageUrl' => '/gem/assets/uploads/myfiles/images/tien-ich/he-thong-khu-du-lich-nghi-duong.jpg'],
            ['title' => 'Khu ẩm thực BBQ', 'imageUrl' => '/gem/assets/uploads/myfiles/images/tien-ich/duong-nguyen-hue.jpg'],
            ['title' => 'Sảnh của tòa nhà', 'imageUrl' => '/gem/assets/uploads/myfiles/images/tien-ich/trung-tam-mua-sam.jpg'],
            ['title' => 'Hệ thống siêu thị đa dạng', 'imageUrl' => '/gem/assets/uploads/myfiles/images/tien-ich/he-thong-sieu-thi.jpg'],
        ]);
    }
}

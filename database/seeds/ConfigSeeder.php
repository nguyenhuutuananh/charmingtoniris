<?php

use Illuminate\Database\Seeder;
use App\Config;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $config = new Config();
        $config->name = "web_title";
        $config->title = "Tiêu đề trang web";
        $config->value = "Charmington Iris";
        $config->save();

        $config = new Config();
        $config->name = "web_sub_title";
        $config->title = "Tiêu đề sub";
        $config->value = "Vịnh Hạ Long giữa lòng Sài Gòn";
        $config->save();
        
        $config = new Config();
        $config->name = "descriptions";
        $config->title = "SEO Description";
        $config->value = "";
        $config->save();

        $config = new Config();
        $config->name = "keywords";
        $config->title = "SEO Keywords";
        $config->value = "Charmington Iris";
        $config->save();

        
        $config = new Config();
        $config->name = "address";
        $config->title = "Địa chỉ liên hệ";
        $config->value = "346 Bến Vân Đồn, Phường 1, Quận 4, TP HCM ";
        $config->save();

        $config = new Config();
        $config->name = "email";
        $config->title = "Emaiil";
        $config->value = "admin@lienhe.com";
        $config->save();

        $config = new Config();
        $config->name = "logo";
        $config->title = "Logo";
        $config->value = "/files/1/logo[1].png";
        $config->save();

        $config = new Config();
        $config->name = "top_banner";
        $config->title = "Top banner";
        $config->value = "/files/1/web_configs/banner.png";
        $config->save();

        $config = new Config();
        $config->name = "hotline";
        $config->title = "Hotline";
        $config->value = "0902 43 90 XX";
        $config->save();

        $config = new Config();
        $config->name = "bottom_web_latitude";
        $config->title = "Latitude";
        $config->value = "10.755774";
        $config->save();

        $config = new Config();
        $config->name = "bottom_web_longtitude";
        $config->title = "Longtitude";
        $config->value = "106.6901332";
        $config->save();

        $config = new Config();
        $config->name = "favicon";
        $config->title = "Favicon:";
        $config->value = "/favicon.ico";
        $config->save();

        $config = new Config();
        $config->name = "youtube_id_introduction";
        $config->title = "ID Youtube giới thiệu (VD: mdR4FL3oemA) :";
        $config->value = "mdR4FL3oemA";
        $config->save();

        $config = new Config();
        $config->name = "facebook_link";
        $config->title = "Facebook link";
        $config->value = "#";
        $config->save();

        $config = new Config();
        $config->name = "gplus_link";
        $config->title = "Google+ link";
        $config->value = "#";
        $config->save();

        $config = new Config();
        $config->name = "youtube_link";
        $config->title = "Youtube link";
        $config->value = "#";
        $config->save();

        $config = new Config();
        $config->name = "twitter_link";
        $config->title = "Twitter link";
        $config->value = "#";
        $config->save();

        $config = new Config();
        $config->name = "instagram_link";
        $config->title = "Instagram link";
        $config->value = "#";
        $config->save();

        // Thông tin chung
        $config = new Config();
        $config->name = "dien_tich";
        $config->title = "Tổng diện tích";
        $config->value = "6.7 ha";
        $config->save();
        
        $config = new Config();
        $config->name = "so_block";
        $config->title = "Tổng số block";
        $config->value = "12 block";
        $config->save();
        
        $config = new Config();
        $config->name = "tong_can_ho";
        $config->title = "Tổng căn hộ";
        $config->value = "3175 căn hộ";
        $config->save();

        $config = new Config();
        $config->name = "so_tang";
        $config->title = "Số tầng của từng block";
        $config->value = "33-34 tầng";
        $config->save();

        $config = new Config();
        $config->name = "mat_do_xay_dung_khoi_de";
        $config->title = "Mật độ xây dựng khối đế";
        $config->value = "31%";
        $config->save();

        $config = new Config();
        $config->name = "dien_tich_xay_dung";
        $config->title = "Diện tích sàn xây dựng";
        $config->value = "379.467 m2";
        $config->save();

        // Chủ đầu tư
        $config = new Config();
        $config->name = "chu_dau_tu_mo_ta";
        $config->title = "Chủ đầu tư: Mô tả";
        $config->value = "Top 10 nhà phát triển bất động sản hàng đầu 
        Việt Nam theo BCI Asia Awards.";
        $config->save();

        $config = new Config();
        $config->name = "chu_dau_tu_noi_dung";
        $config->title = "Chủ đầu tư: Nội dung";
        $config->value = "Tập đoàn Đất Xanh đã có 14 năm uy tín trên thị trường bất động sản Việt Nam với mô hình chiến lược khép kín “Bất động sản – Tài Chính - Xây dựng”. Đất Xanh đã khai thác tối đa thế mạnh của mình dựa trên năng lực, kinh nghiệm và đội ngũ nhân viên chuyên nghiệp để không ngừng tạo ra các sản phẩm chất lượng tốt, đáp ứng nhu cầu khách hàng, nâng cao vị thế và uy tín của thương hiệu trên thị trường. Vừa qua, Đất Xanh đã vinh dự nhận giải thưởng “Top 10 nhà phát triển bất động sản hàng đầu Việt Nam” (theo BCI Asia Awards 2015), Top 50 doanh nghiệp kinh doanh hiệu quả nhất Việt Nam (theo bình chọn của Tạp chí Nhịp cầu đầu tư); Top Thương hiệu – Dịch vụ - Sản phẩm uy tín ASEAN 2015” và giải thưởng “Thương hiệu nổi tiếng Châu Á – Thái Bình Dương. Trong năm 2015, Đất Xanh liên tục thực hiện các thương vụ M&A để “thâu tóm” các quỹ đất sạch, giá tốt và dự kiến chào bán trên thị trường các căn hộ thương mại cao cấp ở khu vực Quận 7, Quận 8, Quận 9, Bình Thạnh và Thủ Đức trong thời gian tới.";
        $config->save();

        $config = new Config();
        $config->name = "chu_dau_tu_hinh_anh";
        $config->title = "Chủ đầu tư: Hình ảnh";
        $config->value = "/gem/assets/uploads/myfiles/images/gioi-thieu/dat-xanh.png";
        $config->save();

        $config = new Config();
        $config->name = "chu_dau_tu_link";
        $config->title = "Chủ đầu tư: Trang web";
        $config->value = "https://www.datxanh.vn/";
        $config->save();



        // Tổng quan
        $config = new Config();
        $config->name = "tong_quan_tieu_de";
        $config->title = "Tổng quan: Tiêu đề";
        $config->value = "Tuyệt tác của<br><span>đá và nước</span>";
        $config->save();

        $config = new Config();
        $config->name = "tong_quan_mo_ta";
        $config->title = "Tổng quan: mô tả";
        $config->value = "Căn hộ Gem Riverside sẽ là một trong những dự án ăn khách nhất vào cuối năm 2017 từ những thế mạnh của chủ đầu tư Đất Xanh mang lại. Cùng với hàng loạt dự án đình đám tại trục Đại lộ Phạm Văn Đồng, Tp. Hồ Chí Minh, Đất Xanh dần khẳng định vị thế số 1 tại khu vực này nói riêng và thị trường khu Đông, Sài Gòn nói chung. Tiếp nối những thành công đó, Tập đoàn Đất Xanh chính thức công bố dự án căn hộ Gem Riverside. Dự án căn hộ đẳng cấp sang trọng với quy mô lớn và công viên mặt nước nội khu lên tới 7ha duy nhất tại Quận 2.";
        $config->save();

        $config = new Config();
        $config->name = "tong_quan_hinh_anh";
        $config->title = "Tổng quan: hình mô tả";
        $config->value = "/gem/assets/uploads/myfiles/images/gioi-thieu/gioi-thieu.jpg";
        $config->save();

        $config = new Config();
        $config->name = "ly_do_khach_hang_chon";
        $config->title = "Hình ảnh lý do khách hàng chọn";
        $config->value = "/gem/assets/uploads/myfiles/images/gioi-thieu/ly-do.svg";
        $config->save();

        $config = new Config();
        $config->name = "ly_do_khach_hang_chon_small_device";
        $config->title = "Hình ảnh lý do khách hàng chọn (màn hình nhỏ)";
        $config->value = "/gem/template/tint/images/tmp/reason-tab.svg";
        $config->save();
        $config = new Config();
        $config->name = "ly_do_khach_hang_chon_mobile";
        $config->title = "Hình ảnh lý do khách hàng chọn (màn hình điện thoại)";
        $config->value = "/gem/template/tint/images/tmp/reason-m.svg";
        $config->save();

        // Vị trí

        $config = new Config();
        $config->name = "vi_tri_mo_ta";
        $config->title = "Vị trí: mô tả";
        $config->value = "<p>Quận 2 hiện đang là khu vực có tốc độ đô thị hóa nhanh, sự phát triển bứt phá về hạ tầng giao thông, và được nhận định là quận sở hữu vị trí bất động sản đẹp nhất tại khu Đông Sài Gòn. Với quy hoạch hiện đại và đồng bộ, cùng nhiều khu đô thị kiểu mới mọc lên liên tiếp, Quận 2 hứa hẹn là khu vực chuyển mình không ngừng trong tương lai, tạo nên một diện mạo mới hiện đại, cũng như mở rộng thị trường bất động sản sôi động cho thành phố Hồ Chí Minh.</p>
        <p>Hòa mình vào xu hướng trải nghiệm cuộc sống hoàn mỹ kiểu mới, dự án Gem Riverside của nhà phát triển dự án Tập đoàn Đất xanh đã và đang từng bước hoàn thiện, mang đến cho cư dân sở hữu căn hộ cơ hội an cư đẳng cấp và tiện nghi chuẩn phong cách Singapore. Chỉ cần 20 phút di chuyển từ trung tâm TP.HCM, cư dân tương lai của Gem Riverside sẽ đến với thiên đường nghỉ dưỡng, tuyệt tác căn hộ ven sông, khiến mọi giác quan được đánh thức với cảnh sắc thiên nhiên bình yên, thanh mát cùng cảnh quan đẹp đậm chất thơ.</p>";
        $config->save();

        $config = new Config();
        $config->name = "vi_tri_hinh_anh";
        $config->title = "Vị trí: hình mô tả";
        $config->value = "/gem/assets/uploads/myfiles/images/vi-tri/vi-tri.jpg";
        $config->save();

        $config = new Config();
        $config->name = "vi_tri_hinh_anh_trang_chu";
        $config->title = "Vị trí: hình trang chủ";
        $config->value = "/gem/assets/uploads/myfiles/images/vi-tri/gem-map-final.gif";
        $config->save();

        $config = new Config();
        $config->name = "vi_tri_mo_ta_chung";
        $config->title = "Vị trí: Mô tả chung";
        $config->value = "Mô tả chung";
        $config->save();

        // section_5
        $config = new Config();
        $config->name = "trang_chu_section_5";
        $config->title = "Trang chủ: tiêu đề section 5";
        $config->value = "VỊNH HẠ LONG GIỮA LÒNG SÀI GÒN";
        $config->save();

        $config = new Config();
        $config->name = "trang_chu_section_5_content";
        $config->title = "Trang chủ: nội dung section 5";
        $config->value = "<p>Với lịch sử hình thành từ lâu đời và truyền thuyết về nguồn cội dòng dõi “con rồng cháu tiên”,
        Vịnh Hạ Long trở thành niềm tự hào của đất nước Việt Nam khi thắng cảnh thiên nhiền này
        được UNESCO công nhận là di sản thiên nhiên thế giới vào năm 1994, và được tổ chức New7Wonder
        công nhận là “1 trong 7 kỳ quan thiên nhiên thế giới mới” vào năm 2011. Quả thật, từ
        hơn 500 năm về trước, vẻ đẹp của vịnh Hạ Long đã được thi hào Nguyễn Trãi ca tụng và
        ví von là \"kỳ quan đá dựng giữa trời cao\"trong những áng văn chương lưu truyền.</p>
    <p>Không chỉ có vậy, ngược dòng chảy lịch sử trở về tìm hiểu về thời cha ông xa xưa, Vịnh Hạ
        Long không chỉ mang trong mình vẻ đẹp non nước hùng vĩ mà còn chứa đựng trong mình những
        truyền thuyết hào hùng về quá trình chống giặc ngoại xâm lăng. Tương truyền rằng con
        dân Đại Việt với nguồn cội thanh cao “con Rồng cháu Tiên” nên đã được Ngọc Hoàng cử đàn
        rồng hạ giới giúp con dân Đại Việt đánh đuổi giặc ra khỏi bờ cõi. Sau khi giặc tan, thấy
        cảnh mặt đất thanh bình, cây cối tươi tốt, đức tính con người cần cù, chịu khó, đoàn
        kết giúp đỡ nhau, đàn rồng đã không trở về trời mà quyết định ở lại hạ giới, nơi vừa
        diễn ra trận chiến để muôn đời trấn giữ bờ cõi Đại Việt. Vị trí Rồng đáp xuống là vịnh
        nước xanh tươi, núi non hùng vĩ, từ đó được gọi tên là vịnh Hạ Long.</p>
    <p>Vẻ đẹp kỳ quan thiên nhiên Vịnh Hạ Long hiện ra sống động như một bức tranh thủy mặc mà tạo
        hóa đã ưu ái ban tặng cho Việt Nam. Dưới bàn tay của mẹ thiên nhiên, những khối núi đá
        vôi quần tụ với nhau, nổi bật trên mặt nước xanh biếc đặc trưng, đã trở thành thương
        hiệu du lịch, là biểu tượng cảnh sắc thiên nhiên độc đáo mà bất cứ ai cùng đều mong ước
        một lần được đặt chân đến, tận mắt nhìn ngắm cho thỏa lòng mê say.</p>
    <p>Và với quyết tâm mang vẻ đẹp rất riêng của Vịnh Hạ Long vào công trình hạ tầng hiện đại,
        những kiến trúc sư xuất sắc của tập đoàn CPG Consultants hàng đầu tại đất nước Singapore
        đã dành tâm huyết, mang thần thái vẻ đẹp kỳ quan này vào công trình Gem Riverside với
        thông điệp “Vịnh Hạ Long giữa lòng Sài Gòn”!</p>
    <p>Giữa Sài Gòn hối hả, người người tấp nập, Gem Riverside trở thành chốn bình yên sau chuỗi
        ngày dài lao động hăng say. Tọa lạc tại Quận 2 - vị trí trọng yếu phát triển nhất của
        khu Đông thành phố, trên trục đường song hành cao tốc Long Thành- Dầu Giây, một kỳ quan
        “Vịnh Hạ Long” rất riêng với tên gọi Gem Riverside hiên ngang nổi bật giữa lòng phố thị
        Sài Gòn với 12 block cao tầng sắp xếp vị trí uốn lượn như đàn mãnh rồng nằm phục trên
        mặt sông xanh. Gem Riverside nổi bật với hai mặt tiền hướng sông thoáng mát, hội tủ hơn
        45 tiện ích nội khu cao cấp và hơn 60 tiện ích xung quanh. Bên cạnh đó, hệ thống giao
        thông thuận lợi, cũng như uy tín lâu năm từ chủ đầu tư Tập đoàn Đất Xanh, cư dân tương
        lai của Gem riverside sẽ hoàn toàn an tâm với sự chọn lựa thông minh này. Có thể khẳng
        định, Gem Riverside chính là lựa chọn sống đẳng cấp, sang trọng và tinh tế hàng đầu dành
        thế hệ cư dân hiện đại ngày nay.</p>";
        $config->save();

        // section_6
        $config = new Config();
        $config->name = "trang_chu_section_6";
        $config->title = "Trang chủ: tiêu đề section 6";
        $config->value = "NGHỈ DƯỠNG CAO CẤP VÀ NGHỈ NGƠI LÝ TƯỞNG";
        $config->save();

        $config = new Config();
        $config->name = "trang_chu_section_6_content";
        $config->title = "Trang chủ: nội dung section 6";
        $config->value = "<p>Dưới bàn tay thiết kế và xây dựng từ những kiến trúc sư hàng đầu của nhà thầu CPG Consultants, Singapore là quốc gia nổi tiếng với nhiều công trình kiến trúc hiện đại, tiện nghi và nổi bật tại nhiều nước khác nhau. Tại Việt Nam, Tập đoàn Đất Xanh đã tin tưởng chọn lựa nhà thầu CPG Consultants để đảm nhận dự án gem Riverside với mục tiêu...
        </p>";
        $config->save();

        $config = new Config();
        $config->name = "trang_chu_section_6_image";
        $config->title = "Trang chủ: hình section 6";
        $config->value = "/gem/assets/uploads/myfiles/images/gioi-thieu/nghi-duong.jpg";
        $config->save();

        // Tiện ích
        $config = new Config();
        $config->name = "cot_tien_ich_1";
        $config->title = "Tiện ích: Cột 1";
        $config->value = "<ul>
        <li>
            <span><strong>5</strong> phút</span></li>
        <li>
            Khu liên hợp thể thao quốc gia Rạch Chiếc</li>
        <li>
            Cao tốc Long Thành - Dầu Giây</li>
        <li>
            Tuyến metro số 1 &amp; số 2</li>
        <li>
            Thảo Cầm Viên Sài Gòn</li>
        <li>
            Vincom Mega Mall Thảo Điền</li>
        <li>
            Lotte Cinema Cantavil</li>
    </ul>";
        $config->save();

        $config = new Config();
        $config->name = "cot_tien_ich_2";
        $config->title = "Tiện ích: Cột 2";
        $config->value = "<ul>
        <li>
            <span><strong>5</strong> phút</span></li>
        <li>
            Khu liên hợp thể thao quốc gia Rạch Chiếc</li>
        <li>
            Cao tốc Long Thành - Dầu Giây</li>
        <li>
            Tuyến metro số 1 &amp; số 2</li>
        <li>
            Thảo Cầm Viên Sài Gòn</li>
        <li>
            Vincom Mega Mall Thảo Điền</li>
        <li>
            Lotte Cinema Cantavil</li>
    </ul>";
        $config->save();

        $config = new Config();
        $config->name = "cot_tien_ich_3";
        $config->title = "Tiện ích: Cột 3";
        $config->value = "<ul>
        <li>
            <span><strong>5</strong> phút</span></li>
        <li>
            Khu liên hợp thể thao quốc gia Rạch Chiếc</li>
        <li>
            Cao tốc Long Thành - Dầu Giây</li>
        <li>
            Tuyến metro số 1 &amp; số 2</li>
        <li>
            Thảo Cầm Viên Sài Gòn</li>
        <li>
            Vincom Mega Mall Thảo Điền</li>
        <li>
            Lotte Cinema Cantavil</li>
    </ul>";
        $config->save();
    }
}

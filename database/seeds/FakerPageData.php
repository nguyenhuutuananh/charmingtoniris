<?php

use Illuminate\Database\Seeder;
use App\Page;
use App\Partner;

class FakerPageData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $officetel_content = <<<EOT
            <!-- AddThis Sharing Buttons above -->
            <p>
                <span style="color: #0000ff; font-size: 18pt;">
                    <strong>Mở bán 138 căn hộ Office-Tel dự án TNR The GoldView.</strong>
                </span>
            </p>
            <div class="motamoi_remove_class">
                <img class="size-full wp-image-1211 alignnone" src="http://canhothegoldview.com/mucchucnang/uploads/2017/11/mui-ten-dong-2.gif"
                    alt="" width="20" height="11" /> Giá siêu rẻ chỉ từ
                <strong>
                    <span style="color: #ff0000;">
                        <em>33 tr/m2. rẻ hơn 20-30 triệu</em>
                    </span>
                </strong> so với những dự án cùng vị trí.
                <br />
                <img class="size-full wp-image-1211 alignnone" src="http://canhothegoldview.com/mucchucnang/uploads/2017/11/mui-ten-dong-2.gif"
                    alt="" width="20" height="11" />
                <span style="color: #ff6600;">
                    <span style="color: #333333;">Diện tích
                        <em>
                            <span style="color: #ff0000;">
                                <strong>50m2, 63m2, 68m2, 80m2, 90m2, 92m2, 100m2, 117m2 và 132m2</strong>
                            </span>
                        </em>
                    </span>
                </span>
                <strong>
                    <span style="color: #ff6600;">
                        <span style="color: #333333;">.</span>
                    </span>
                </strong>
                <br />
                <img class="size-full wp-image-1211 alignnone" src="http://canhothegoldview.com/mucchucnang/uploads/2017/11/mui-ten-dong-2.gif"
                    alt="" width="20" height="11" /> Giao nhà Tháng 12/2017
                <br />
                <img class="size-full wp-image-1211 alignnone" src="http://canhothegoldview.com/mucchucnang/uploads/2017/11/mui-ten-dong-2.gif"
                    alt="" width="20" height="11" /> Nhận đặt chỗ 100 tr/1 vị trí.
                <br />
                <img class="size-full wp-image-1211 alignnone" src="http://canhothegoldview.com/mucchucnang/uploads/2017/11/mui-ten-dong-2.gif"
                    alt="" width="20" height="11" /> Liên hệ SĐT:
                <strong>
                    <span style="color: #0000ff;">0909040965
                        <span style="color: #333333;">hoặc</span> 0909296565</span>
                </strong>
            </div>
            <p>&nbsp;</p>
            <p>
                <img class="alignnone size-large wp-image-1171" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0001-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0001-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0001-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0001-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
            </p>
            <p>
                <img class="alignnone size-large wp-image-1169" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0038-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0038-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0038-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0038-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1172" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0002-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0002-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0002-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0002-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1173" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0003-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0003-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0003-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0003-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1207" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0037-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0037-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0037-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0037-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1205" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0035-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0035-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0035-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0035-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1204" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0034-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0034-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0034-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0034-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1203" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0033-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0033-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0033-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0033-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1202" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0032-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0032-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0032-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0032-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1201" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0031-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0031-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0031-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0031-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1200" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0030-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0030-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0030-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0030-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1199" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0029-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0029-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0029-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0029-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1198" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0028-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0028-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0028-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0028-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1197" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0027-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0027-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0027-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0027-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1196" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0026-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0026-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0026-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0026-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1195" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0025-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0025-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0025-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0025-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1194" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0024-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0024-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0024-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0024-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1193" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0023-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0023-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0023-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0023-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1192" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0022-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0022-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0022-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0022-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1191" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0021-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0021-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0021-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0021-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1190" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0020-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0020-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0020-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0020-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1189" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0019-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0019-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0019-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0019-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1188" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0018-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0018-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0018-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0018-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1187" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0017-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0017-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0017-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0017-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1186" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0016-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0016-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0016-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0016-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1185" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0015-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0015-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0015-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0015-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1184" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0014-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0014-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0014-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0014-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1183" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0013-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0013-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0013-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0013-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1182" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0012-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0012-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0012-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0012-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1181" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0011-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0011-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0011-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0011-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1179" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0009-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0009-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0009-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0009-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1178" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0008-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0008-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0008-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0008-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1177" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0007-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0007-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0007-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0007-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1176" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0006-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0006-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0006-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0006-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1175" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0005-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0005-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0005-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0005-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1174" src="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0004-1170x658.jpg"
                    alt="" width="1170" height="658" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/10/0004-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0004-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/10/0004-768x432.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
            </p>
EOT;
        $pricing_content = <<<EOT
            <!-- AddThis Sharing Buttons above --><p><strong>Chính sách bán hàng mới nhất 11/2017</strong></p>
            <ul>
            <li><span style="font-size: 12pt;"><span class="txt-animation">Event Lì Xì Lộc Vàng SJC 9999 – </span>Thanh Toán chỉ 1,050 Tỷ là Nhận Nhà</span></li>
            <li><span style="font-size: 12pt;"><span class="txt-animation">Trả Góp 3% / Tháng – Hỗ Trợ Lãi Suất 0% / Năm </span></span></li>
            <li><span style="font-size: 12pt;">Cam kết cho thuê ✓ 24 Triệu / Tháng – Miễn 10 năm Phí Quản Lý</span></li>
            </ul>
            <p><strong>Chính sách bán hàng tháng 9- duy nhất chỉ có trong năm 2017</strong></p>
            <ul class="ul2">
            <li class="li1">Khách hàng mua 02 căn trở lên được chiết khấu lên đến 19% cho mỗi căn hộ</li>
            <li class="li1">Hoặc CDT cam kết cho thuê trong vòng 2 năm đầu tiên</li>
            <li class="li1">Hoặc khách hàng được vay ngân hàng với ưu đãi lãi suất 0% cho năm đầu tiên. Đặt biệt được Ân hạn nợ gốc năm đầu tiên. Tức khách hàng chỉ cần có trong tay 1,050 tỷ là có ngay căn hộ 2PN để ở trong năm đầu tiên không phải thanh toán khoảng tiền nào nữa cả.</li>
            <li class="li1">Hoặc khách hàng chỉ thanh toán đến 60% nhận nhà, còn lại trả trong vòng 1 năm không lãi suất</li>
            <li class="li1">Hoặc được miễn phí quản lý 10 năm đầu tiên</li>
            </ul>
            <ul class="ul1">
            <li class="li1">Giá bán căn hộ cực kỳ ưu đãi trong đợt giao nhà với<span style="color: #ff0000;"><strong> giá bán từ 32 triệu/m2</strong> </span>cho căn hộ giao hoàn thiện cơ bản.</li>
            <li class="li1">Số lượng căn hộ còn lại chỉ gần 100 căn cuối cùng</li>
            <li class="li1">Dự án đã giao nhà từ 01/09/2017- đối với những căn hộ giao nhà thô</li>
            <li class="li1"><span style="color: #ff0000;"><strong>Bốc thăm trúng xe Mercedes C250 &#8211; trị giá 1,7 tỷ vào ngày 22/10/2017</strong></span></li>
            </ul>
            <p class="p1">Quý khách hàng có nhu cầu vui lòng liên hệ sớm để được chuyên viên công ty chúng tôi tư vấn và chọn căn đẹp nhất.</p>
            <p class="p1"><span style="font-size: 14pt; color: #ff0000;"><strong>HOTLINE: 0909296565<br />
            </strong></span></p>
            <p class="p1">Viber/zalo</p>
EOT;
        $model_house = <<<EOT
            <!-- AddThis Sharing Buttons above -->
            <p style="text-align: justify;">Quý khách sẽ được tham quan
                <b>nhà mẫu TNR THE GOLDVIEW</b> với các loại diện tích tiêu biểu 1,2,3 phòng ngủ khi nhà mẫu sẽ có từ ngày 19/07/2015. Việc
                bắt đầu cho đăng kí tham quan
                <b>căn hộ mẫu TNR THE GOLDVIEW</b> sẽ tạo cho khách hàng có quyết định cuối cùng là có nên chọn căn hộ TNR THE GOLDVIEW
                để an cư lạc nghiệp hay đầu tư sinh lợi hay không? Cũng như câu nói “trăm nghe không bằng một thấy”, với thiết kế sang
                trọng cùng nội thất hiện đại mà dự án mang lại, chắc chắc rằng quý khách sẽ vô cùng hài lòng về dự án này.
                <a href="http://canhothegoldview.com/can-ho-mau/1pn/"
                    rel="attachment wp-att-937">
                    <img class="aligncenter size-full wp-image-937" src="http://canhothegoldview.com/mucchucnang/uploads/2017/05/1pn.jpg"
                        alt="" width="730" height="465" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/05/1pn.jpg 730w, http://canhothegoldview.com/mucchucnang/uploads/2017/05/1pn-370x235.jpg 370w"
                        sizes="(max-width: 730px) 100vw, 730px" />
                </a>
                <a href="http://canhothegoldview.com/can-ho-mau/2pn-a/" rel="attachment wp-att-938">
                    <img class="aligncenter size-medium wp-image-938" src="http://canhothegoldview.com/mucchucnang/uploads/2017/05/2pn-a.jpg"
                        alt="" width="730" height="501" />
                </a>
                <a href="http://canhothegoldview.com/can-ho-mau/2pn-b/" rel="attachment wp-att-939">
                    <img class="aligncenter size-medium wp-image-939" src="http://canhothegoldview.com/mucchucnang/uploads/2017/05/2pn-b.jpg"
                        alt="" width="730" height="489" />
                </a>
                <a href="http://canhothegoldview.com/can-ho-mau/3pn/" rel="attachment wp-att-940">
                    <img class="aligncenter size-medium wp-image-940" src="http://canhothegoldview.com/mucchucnang/uploads/2017/05/3pn.jpg"
                        alt="" width="730" height="489" />
                </a>
            </p>
            <p style="text-align: justify;">Khu nhà mẫu
                <strong>The
                    <span style="color: #000080;">Gold</span>
                    <span style="color: #ff9900;">View</span>
                </strong> tại
                <b class="red">“
                    <span style="color: #0000ff;">131 Nguyễn Khoái, P1, Quận 4, TP.Hồ Chí Minh</span>”</b>. Đặt hẹn ngay thời gian xem nhà mẫu để chúng tôi có thể
                tiếp đón bạn 1 cách tốt nhất.</p>
            <div class="dkn">
                <img class="aligncenter" src="http://duanthegoldviewquan4.com/wp-content/uploads/2016/06/Untitled-2-1.png" alt="đăng kí xem nhà mẫu goldview"
                    width="250" />Hoặc liên hệ theo:
                <br /> Điện thoại:
                <a href="tel:0909040965">
                    <strong>0909 0409 65</strong>
                </a> Email:
                <span style="color: #0000ff;">
                    <strong>huanbuivn@gmail.com</strong>
                </span>
EOT;
        $video = <<<EOT
            <!-- AddThis Sharing Buttons above -->
            <p>
                <strong>TNR THE GOLDVIEW</strong> được thiết kế và thi công với hệ thống an ninh bảo mật tuyệt đối, các thiết bị bảo vệ tân tiếnbên
                cạnh hệ thống tiện ích 5 sao mà dự án sở hữu như: hệ thống khu vui chơi, giải trí, hồ bơi, trung tâm thương mại dịch
                vụ, khu vườn treo xanh, cafe sân thượng, phòng sinh hoạt cộng đồng, phòng tập Gym – yoga, spa, gian hàng ăn uống,…</p>
            <h2>Cùng xem video clip về TNR THE GOLDVIEW:</h2>
            <p>
                <iframe src="https://www.youtube.com/embed/0qtt35xhMr8?rel=0" width="853" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
            </p>
            <p style="text-align: center;">
                <strong>video TNR THE GOLDVIEW 1</strong>
            </p>
            <p>Vừa qua ngày 21/6 TNR Holdings đã tổ chức sự kiện tại GEM Center 8 Nguyễn Bỉnh Khiêm để chính thức công bố dự án Gold View
                quận 4 với hơn 600 khách hàng và các bạn báo chí truyền thông, cùng xem
                <strong>video TNR THE GOLDVIEW</strong>
            </p>
            <p>
                <iframe src="https://www.youtube.com/embed/1hH4pXpN_FQ?rel=0" width="853" height="480" frameborder="0" allowfullscreen="allowfullscreen"
                    data-mce-fragment="1"></iframe>
            </p>
            <p style="text-align: center;">
                <strong>video TNR THE GOLDVIEW 2</strong>
            </p>
            <p>
                <iframe src="https://www.youtube.com/embed/SFSRIR-P6Ws?rel=0" width="853" height="480" frameborder="0" allowfullscreen="allowfullscreen"
                    data-mce-fragment="1"></iframe>
            </p>
            <p style="text-align: center;">
                <strong>Căn hộ mẫu TNR THE GOLDVIEW</strong>
            </p>
            <p>
                <iframe src="https://www.youtube.com/embed/i-GsOaWpQtA?rel=0" width="853" height="480" frameborder="0" allowfullscreen="allowfullscreen"
                    data-mce-fragment="1"></iframe>
            </p>
            <p style="text-align: center;">View nhìn quận 5 đến quận 1 dự án
                <strong>TNR THE GOLDVIEW</strong>
            </p>
            <p>
                <iframe src="https://www.youtube.com/embed/2t9ikSL0xb8?rel=0" width="853" height="480" frameborder="0" allowfullscreen="allowfullscreen"
                    data-mce-fragment="1"></iframe>
            </p>
            <p style="text-align: center;">View 360 độ từ dự án
                <strong>TNR THE GOLDVIEW</strong>
            </p>
EOT;
        $hinhanh = <<<EOT
            <!-- AddThis Sharing Buttons above -->
            <p>
                <img class="aligncenter size-medium wp-image-365" src="../mucchucnang/uploads/2016/07/An-ninh-800x647.jpg" alt="An ninh"
                    width="800" height="647" srcset="http://canhothegoldview.com/mucchucnang/uploads/2016/07/An-ninh-800x647.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2016/07/An-ninh-768x621.jpg 768w, http://canhothegoldview.com/mucchucnang/uploads/2016/07/An-ninh.jpg 1000w"
                    sizes="(max-width: 800px) 100vw, 800px" />
            </p>
            <p>
                <img class="size-medium wp-image-366 aligncenter" src="../mucchucnang/uploads/2016/07/Cham-soc-suc-khoe-800x533.jpg" alt="Beautiful young female doctor talking to an elderly patient in a wheelchair. Horizontal shot."
                    width="800" height="533" srcset="http://canhothegoldview.com/mucchucnang/uploads/2016/07/Cham-soc-suc-khoe-800x533.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2016/07/Cham-soc-suc-khoe-768x511.jpg 768w, http://canhothegoldview.com/mucchucnang/uploads/2016/07/Cham-soc-suc-khoe.jpg 1000w"
                    sizes="(max-width: 800px) 100vw, 800px" />
            </p>
            <p>
                <img class="aligncenter size-medium wp-image-367" src="../mucchucnang/uploads/2016/07/slide-2-800x450.jpg" alt="slide-2"
                    width="800" height="450" srcset="http://canhothegoldview.com/mucchucnang/uploads/2016/07/slide-2-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2016/07/slide-2-768x432.jpg 768w, http://canhothegoldview.com/mucchucnang/uploads/2016/07/slide-2-1170x658.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2016/07/slide-2.jpg 1300w"
                    sizes="(max-width: 800px) 100vw, 800px" />
                <img class="aligncenter size-medium wp-image-368" src="../mucchucnang/uploads/2016/07/tien-ich-the-gold-view-1-800x450.jpg"
                    alt="tien-ich-the-gold-view-1" width="800" height="450" srcset="http://canhothegoldview.com/mucchucnang/uploads/2016/07/tien-ich-the-gold-view-1-800x450.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2016/07/tien-ich-the-gold-view-1-768x432.jpg 768w, http://canhothegoldview.com/mucchucnang/uploads/2016/07/tien-ich-the-gold-view-1.jpg 1000w"
                    sizes="(max-width: 800px) 100vw, 800px" />
                <img class="aligncenter size-medium wp-image-369" src="../mucchucnang/uploads/2016/07/tien-ich-the-gold-view-5-800x500.png"
                    alt="tien-ich-the-gold-view-5" width="800" height="500" srcset="http://canhothegoldview.com/mucchucnang/uploads/2016/07/tien-ich-the-gold-view-5.png 800w, http://canhothegoldview.com/mucchucnang/uploads/2016/07/tien-ich-the-gold-view-5-768x480.png 768w"
                    sizes="(max-width: 800px) 100vw, 800px" />
                <img class="aligncenter size-medium wp-image-370" src="../mucchucnang/uploads/2016/07/tien-ich-the-gold-view-6-800x500.png"
                    alt="tien-ich-the-gold-view-6" width="800" height="500" srcset="http://canhothegoldview.com/mucchucnang/uploads/2016/07/tien-ich-the-gold-view-6.png 800w, http://canhothegoldview.com/mucchucnang/uploads/2016/07/tien-ich-the-gold-view-6-768x480.png 768w"
                    sizes="(max-width: 800px) 100vw, 800px" />
                <img class="aligncenter size-medium wp-image-371" src="../mucchucnang/uploads/2016/07/tien-ich-the-gold-view-12-800x458.jpg"
                    alt="tien-ich-the-gold-view-12" width="800" height="458" srcset="http://canhothegoldview.com/mucchucnang/uploads/2016/07/tien-ich-the-gold-view-12-800x458.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2016/07/tien-ich-the-gold-view-12-768x440.jpg 768w, http://canhothegoldview.com/mucchucnang/uploads/2016/07/tien-ich-the-gold-view-12.jpg 1000w"
                    sizes="(max-width: 800px) 100vw, 800px" />
                <img class="aligncenter size-medium wp-image-372" src="../mucchucnang/uploads/2016/07/tien-ich-the-gold-view-14-800x454.jpg"
                    alt="tien-ich-the-gold-view-14" width="800" height="454" srcset="http://canhothegoldview.com/mucchucnang/uploads/2016/07/tien-ich-the-gold-view-14-800x454.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2016/07/tien-ich-the-gold-view-14-768x436.jpg 768w, http://canhothegoldview.com/mucchucnang/uploads/2016/07/tien-ich-the-gold-view-14.jpg 1000w"
                    sizes="(max-width: 800px) 100vw, 800px" />
            </p>
EOT;
        $tienich = <<<EOT
            <!-- AddThis Sharing Buttons above -->
            <p style="text-align: justify;">Căn hộ cao cấp
                <strong>TNR THE GOLDVIEW</strong> được chủ đầu tư chú trọng xây dựng hệ thống tiện ích cao cấp, hiện đại bậc nhất hiện nay với
                nhiều thương hiệu nổi tiếng để phục vụ tốt nhất cho nhu cầu sinh hoạt và nghỉ ngơi trong những giây phút căng thẳng mệt
                mỏi hoặc cuối tuần bạn có thể cùng gia đinh tham gia những buổi dã ngoại, tiệc nướng ngoài trời mang lại không khí ấm
                cúng, những giây phút tuyệt vời cho gia đình bạn.</p>
            <p style="text-align: justify;">Các tiện ích cao cấp của
                <strong>căn hộ TNR THE GOLDVIEW</strong> như:</p>
            <p style="text-align: justify;">
                <img class="aligncenter size-medium wp-image-6731" src="../../www.canhothegoldview.org/wp-content/uploads/2016/03/slide-3-870x489.jpg"
                    alt="tien ich TNR THE GOLDVIEW" width="870" height="489" />
            </p>
            <p style="text-align: justify;">
                <strong>Hồ bơi hiện đại</strong>
            </p>
            <p style="text-align: justify;">Lấy ý tưởng mang đến cho bạn và gia đình một cuộc sống đẳng cấp, hiện đại, một nơi thư giãn tuyệt vời sau một ngày làm việc
                mệt nhọc với hồ bơi hiện đại rộng bật nhất.</p>
            <p style="text-align: justify;">Toàn khu căn hộ TNR THE GOLDVIEW có hồ bơi lớn rộng 490m2 ở tầng 5 theo tiêu chuẩn hồ bơi tràn view nhìn ra đường Bến Vân
                Đồn và Kênh Bến Nghé. Ngoài ra có thêm hồ bơi trẻ em rộng 117m2 ở tầng 5.</p>
            <p style="text-align: justify;">Một điểm nhấn của dự án là có hồ bơi cảnh quan trên tầng 27 và công viên đi bộ thoáng mát và sang trọng</p>
            <p style="text-align: justify;">
                <img class="aligncenter size-medium wp-image-6741" src="../../www.canhothegoldview.org/wp-content/uploads/2015/04/Tien-ich-13-870x434.jpg"
                    alt="tien ich TNR THE GOLDVIEW" width="870" height="434" />
            </p>
            <p style="text-align: justify;">View nhìn của căn hộ ra hồ bơi tầng 5 và kênh Bến Nghé, Q1…</p>
            <p style="text-align: justify;">
                <img class="aligncenter size-medium wp-image-6740" src="../../www.canhothegoldview.org/wp-content/uploads/2015/04/Tien-ich-12-870x435.jpg"
                    alt="tien ich TNR THE GOLDVIEW" width="870" height="435" />
            </p>
            <p style="text-align: justify;">Hồ bơi cảnh quan tầng 27- Block B</p>
            <p style="text-align: justify;">
                <img class="aligncenter size-medium wp-image-6742" src="../../www.canhothegoldview.org/wp-content/uploads/2015/04/Tien-ich-14-870x241.jpg"
                    alt="tien ich TNR THE GOLDVIEW" width="870" height="241" />
            </p>
            <p style="text-align: justify;">View nhìn hồ bơi tầng 5 ra kênh Bến Nghé</p>
            <p style="text-align: justify;">
                <strong>Nhà trẻ nội khu</strong>
            </p>
            <p style="text-align: justify;">Tại
                <strong>TNR THE GOLDVIEW</strong> cư dân sẽ không phải lo lắng về vấn để phải đưa rước con ở những nhà trẻ cách xa bởi nhà trẻ
                nội the căn hộ
                <strong>TNR THE GOLDVIEW</strong> được chú trọng đầu tư với đội ngũ giáo viên yêu nghề, tận tụy và yêu thương trẻ em.</p>
            <p style="text-align: justify;">
                <img class="aligncenter size-medium wp-image-6743" src="../../www.canhothegoldview.org/wp-content/uploads/2015/04/tien-ich-the-gold-view-11-870x479.jpg"
                    alt="tien ich TNR THE GOLDVIEW" width="870" height="479" />
            </p>
            <p style="text-align: justify;">
                <strong>Spa</strong>
            </p>
            <p style="text-align: justify;">Khu Gym &amp; Spa hiện đại sẽ là điểm đến yêu thích của mọi người. Đặc biệt là giới trẻ. Dịch vụ hoàn hảo nơi đây không chỉ
                giúp bạn phục hồi sức khỏe mà còn mang đến cho bạn sự thư giãn tuyệt vời, sảng khoái.</p>
            <p style="text-align: justify;">
                <img class="aligncenter size-medium wp-image-6406" src="../../www.canhothegoldview.org/wp-content/uploads/2015/04/Gym-can-ho-the-goldview-870x489.jpg"
                    alt="Gym- can-ho-the-goldview" width="870" height="489" />
            </p>
            <p style="text-align: justify;">
                <strong>Khu thể thao</strong>
            </p>
            <p style="text-align: justify;">Sân thể thao trung tâm được đầu tư nhằm đáp ứng được trọn vẹn nhất những nhu cầu rèn luyện sức khỏe và vui chơi giải trí
                cho cư dân của. Đây hứa hẹn sẽ ra nơi gắn kết cộng đồng xích lại gần nhau hơn trong các hoạt động thể thao.</p>
            <p style="text-align: justify;">
                <strong>Căn hộ dịch vụ
                    <br />
                </strong>
            </p>
            <p style="text-align: justify;">Chủ đầu tư dự án cam kết, căn hộ dịch vụ sẽ bao gồm những thương hiệu nổi tiếng và uy tín trên thị trường. Chỉ cần bước chân
                ra khỏi cửa và thêm mấy phút đi dạo trên con đường đầy cây xanh và nắng vàng, bạn sẽ đến ngày khu mua sắm của các gian
                hàng.</p>
            <p style="text-align: justify;">
                <img class="aligncenter size-medium wp-image-6703" src="../../www.canhothegoldview.org/wp-content/uploads/2015/12/sanh-thegoldview-20151-870x399.jpg"
                    alt="tien ich TNR THE GOLDVIEW" width="870" height="399" />
                <img class="aligncenter size-medium wp-image-6744" src="../../www.canhothegoldview.org/wp-content/uploads/2015/04/Tien-ich-3-870x434.jpg"
                    alt="tien ich TNR THE GOLDVIEW" width="870" height="434" />
            </p>
            <p style="text-align: justify;">
                <strong>Nhà hàng sang trọng</strong>
            </p>
            <p style="text-align: justify;">Nhà hàng thuộc khu căn hộ TNR THE GOLDVIEW sẽ cho bạn một không gian sống cao cấp phải đi liền với những dịch vụ cao cấp
                với những món ngon, hợp khẩu vị,…</p>
            <p style="text-align: justify;">
                <img class="aligncenter size-medium wp-image-6745" src="../../www.canhothegoldview.org/wp-content/uploads/2015/04/tien-ich-the-gold-view-1-870x489.jpg"
                    alt="tien ich TNR THE GOLDVIEW" width="870" height="489" />
            </p>
            <p style="text-align: justify;">Trung tâm thương mại sang trọng và đa dạng tiện ích phục vụ cho cư dân trong khu căn hộ TNR THE GOLDVIEW</p>
            <p style="text-align: justify;">
                <img class="aligncenter size-medium wp-image-6746" src="../../www.canhothegoldview.org/wp-content/uploads/2015/04/tien-ich-the-gold-view-3-870x348.jpg"
                    alt="tien ich TNR THE GOLDVIEW" width="870" height="348" />
            </p>
EOT;
        $vitri = <<<EOT
            <!-- AddThis Sharing Buttons above -->
            <p style="text-align: justify;">Tọa lạc tại 346 Bến Vân Đồn, Phường 1, Quận 4, TP HCM vị trí chiến lược ,
                <strong>TNR THE GOLDVIEW</strong> là giao điểm của các trục đường chính: Đại lộ Võ Văn Kiệt, đường Bến Vân Đồn, đường Nguyễn
                Thái Học, đường Hoàng Diệu, cầu Ông Lãnh,… Với vị trí giao thông cực kỳ thuận lợi, từ căn hộ
                <strong>TNR THE GOLDVIEW</strong> đây cư dân dễ dàng di chuyển tới quận 1 trung tâm thành phố chỉ mất 5 phút, có thể giao thông
                dễ dàng di chuyển đến các quận lân cận như Quận 1, quận 4, quận 3, quận 10, quận 11, Bình Thạnh… Đây cũng là căn hộ được
                dự đoán tốt nhất quận 4 TP. HCM trong năm 2015.</p>
            <p style="text-align: justify;">
                <p style="text-align: justify;">
                    <a href="vitrithegoldview/index.html" rel="attachment wp-att-852">
                        <img class="aligncenter wp-image-852 size-large" src="../mucchucnang/uploads/2016/07/vitrithegoldview-1170x1002.jpg" alt="vi tri du an TNR THE GOLDVIEW"
                            width="1170" height="1002" srcset="http://canhothegoldview.com/mucchucnang/uploads/2016/07/vitrithegoldview-1170x1002.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2016/07/vitrithegoldview-800x685.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2016/07/vitrithegoldview-768x657.jpg 768w, http://canhothegoldview.com/mucchucnang/uploads/2016/07/vitrithegoldview.jpg 1501w"
                            sizes="(max-width: 1170px) 100vw, 1170px" />
                    </a>Những thuận lợi từ vị trí đã giúp
                    <strong>TNR THE GOLDVIEW</strong> không những được thừa hưởng những tiện nghi, tiện ích hiện đại của khu trung tâm quận 4,
                    mà còn là của cả thành phố như: Hệ thống đại siêu thị, trung tâm thương mại, trường học, nhà trẻ, bệnh viện, trung
                    tâm hành chính,…</p>
                <p style="text-align: justify;">Được nằm cạnh mặt tiền đường Bến Vân Đồn, giao điểm các con đường huyết mạch vị trí của
                    <strong>TNR THE GOLDVIEW</strong> càng trở nên hoàn hảo hơn với địa thế phong thủy giúp kiến tạo nên một không gian sống
                    bình yên, hài hòa và thịnh vượng, mang lại cho bạn những giá trị tinh tế nhất mà bạn xứng đáng được sở hữu.</p>
                <p style="text-align: justify;">
                    <img class="aligncenter size-medium wp-image-6750" src="../../www.canhothegoldview.org/wp-content/uploads/2015/04/view-nhin-the-gold-view-870x577.jpg"
                        alt="view nhin TNR THE GOLDVIEW" width="870" height="577" />
                </p>
                <p style="text-align: justify;">Hội tụ đủ các yếu tố thuận lợi về vị trí, giao thông, phương án thiết kế tiên tiến, hiện đại, tiết kiệm năng lượng với
                    đầy đủ tiện nghi, và được phát triển bởi nhà đầu tư chuyên nghiệp.</p>
                <p style="text-align: justify;">Từ
                    <strong>TNR THE GOLDVIEW</strong>, cư dân có thể ngắm nhìn toàn cảnh thành phố, và dịu mát tầm mắt với những khoảng xanh
                    mênh mông của thiên nhiên cây cỏ hay tận hưởng từng luồng gió trong lành từ dòng sông thơ mộng để cảm nhận một cách
                    trọn vẹn vẻ đẹp hài hòa của nơi đây, để mỗi ngày trôi qua đều là sự trải nghiệm cuộc sống tuyệt vời.</p>
                <p style="text-align: justify;">
                    <img class="aligncenter size-medium wp-image-6748" src="../../www.canhothegoldview.org/wp-content/uploads/2015/04/Phoi-canh-2-870x559.jpg"
                        alt="phoi can TNR THE GOLDVIEW" width="870" height="559" />
                </p>
                <p style="text-align: justify;">Xung quanh
                    <strong>TNR THE GOLDVIEW</strong> là các tuyến đường huyết mạch vì thế mà vị trí của
                    <strong>TNR THE GOLDVIEW</strong> càng trở nên hoàn hảo hơn với địa thế phong thủy giúp tạo nên một không gian sống bình
                    yên, tiện nghi và thịnh vượng, mang lại cho bạn những giá trị tinh tế nhất mà bạn xứng đáng được sở hữu.</p>
            </p>
EOT;
        // officetel
        $page = new Page();
        $page->title = "Officetel TNR The Goldview";
        $page->description = "";
        $page->content = $officetel_content;
        $page->imageUrl = "";
        $page->save();

        // bảng giá
        $page = new Page();
        $page->title = "Bảng giá và chính sách thanh toán căn hộ TNR THE GOLDVIEW";
        $page->description = "";
        $page->content = $pricing_content;
        $page->imageUrl = "";
        $page->save();
        
        // nhà mẫu
        $page = new Page();
        $page->title = "Căn Hộ Mẫu";
        $page->description = "";
        $page->content = $model_house;
        $page->imageUrl = "";
        $page->save();

        // Video
        $page = new Page();
        $page->title = "Video dự án căn hộ TNR THE GOLDVIEW";
        $page->description = "";
        $page->content = $video;
        $page->imageUrl = "";
        $page->save();

        // Hinh anh
        $page = new Page();
        $page->title = "Hình ảnh";
        $page->description = "";
        $page->content = $hinhanh;
        $page->imageUrl = "";
        $page->save();

        // Tiện ích
        $page = new Page();
        $page->title = "Tiện ích";
        $page->description = "";
        $page->content = $tienich;
        $page->imageUrl = "";
        $page->save();

        // Vị trí
        $page = new Page();
        $page->title = "Vị trí";
        $page->description = "";
        $page->content = $vitri;
        $page->imageUrl = "";
        $page->save();

        $partner = new Partner();
        $partner->title = 'VietinBank';
        $partner->sub_title = 'NGÂN HÀNG BẢO LÃNH';
        $partner->imageUrl = '/gem/assets/uploads/myfiles/images/gioi-thieu/vietin-bank.png';
        $partner->description = 'VietinBank "Nâng giá trị cuộc sống" <br>
            <br>
            Là ngân hàng có bề dày lịch sử, kinh nghiệm và uy tín trong và ngoài nước qua việc cung cấp các dịch vụ ngân hàng truyền thống và hiện đại với chất lượng cao, với mạng lưới 151 chi nhánh phủ rộng khắp cả nước<br>
            <br>
            Mặt khác, nhiều năm qua "VietinBank" đã chứng minh được:</p>
            <ul>
                <li style="text-align: justify;">
                    Khả năng cung cấp giải pháp tài chính hiệu quả.</li>
                <li style="text-align: justify;">
                    Khả năng thu xếp nguồn tài chính lớn.</li>
                <li style="text-align: justify;">
                    Nhanh chóng với những điều kiện ưu đãi.</li>
                <li style="text-align: justify;">
                    Chương trình chăm sóc khách hàng chu đáo.</li>
            </ul>
            <p style="text-align: justify;">
                Sự đồng hành của VietinBank nói chung và các chi nhánh trực thuộc hệ thống của VietinBank nói riêng, cùng với Đất Xanh sẽ hợp sức tạo nên sức mạnh cộng hưởng, nhằm hiện thực hóa ước mơ sở hữu nhà ở hiện đại của người dân Việt Nam, cùng nhau đóng góp cho sự phát triển của xã hội.
            </p>
        ';
        $partner->link = "https://www.vietinbank.vn/";
        $partner->save();
    }
}

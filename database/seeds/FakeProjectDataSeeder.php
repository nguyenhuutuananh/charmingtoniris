<?php

use Illuminate\Database\Seeder;
use App\ProjectCategory;
use App\Furniture;
use App\Project;

class FakeProjectDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        
        // Dự án bán
        $project_category = new ProjectCategory();
        $project_category->title = "Căn hộ bán";
        $project_category->save();

        $project_category = new ProjectCategory();
        $project_category->title = "Căn hộ cho thuê";
        $project_category->save();
        
        // Nội thất
        $furniture = new Furniture();
        $furniture->title = "Cơ bản";
        $furniture->save();

        $furniture = new Furniture();
        $furniture->title = "Đầy đủ";
        $furniture->save();

        // Project rent
        for ($i = 0; $i < 10; $i++) {
            $project = new Project();
            $project->title = "Bán căn hộ " . $faker->name();
            $project->description = $faker->sentence(100);
            $project->content = $faker->sentence(500);
            $price = $faker->randomElement(array(
                $faker->numberBetween(500, 999), $faker->numberBetween(1, 20)
            ));
            $project->price_text = $price > 500 ? $price . " triệu" : $price . " tỉ";
            $project->price = $price <= 20 ? $price * 1000 * 1000 * 1000 : $price * 1000 * 1000;
            $project->acreages = $faker->numberBetween(100, 200);
            $project->bed_rooms = $faker->numberBetween(1, 5);
            $project->toilets = $faker->numberBetween(1, 5);
            $project->garages = $faker->numberBetween(1, 3);
            $project->management_fee = $faker->numberBetween(20, 200) * 1000;
            $project->vat = $faker->randomElement(array(
                'Đã bao gồm', "Chưa bao gồm"
            ));
            $project->motorcycle_fee = $faker->numberBetween(5, 20) * 1000;
            $project->car_fee = $faker->numberBetween(30, 50) * 1000;
            $project->category_id = 1;
            $project->furniture_id = $faker->numberBetween(1, 2);
            $project->save();

            $arrayImages = $faker->randomElements(
                array(
                    array('imageUrl' => '\files\1\demo_canho\1.jpg', 'name' => '1'),
                    array('imageUrl' => '\files\1\demo_canho\2.jpg', 'name' => '2'),
                    array('imageUrl' => '\files\1\demo_canho\3.jpg', 'name' => '3'),
                    array('imageUrl' => '\files\1\demo_canho\4.jpg', 'name' => '4'),
                    array('imageUrl' => '\files\1\demo_canho\5.jpg', 'name' => '5'),
                    array('imageUrl' => '\files\1\demo_canho\6.jpg', 'name' => '6'),
                ),
                $faker->numberBetween(2, 6)
            );
            $project->images()->createMany($arrayImages);
        }
        for ($i = 0; $i < 10; $i++) {
            $project = new Project();
            $project->title = "Bán căn hộ " . $faker->name();
            $project->description = $faker->sentence(100);
            $project->content = $faker->sentence(500);
            $price = $faker->randomElement(array(
                $faker->numberBetween(10, 30), $faker->numberBetween(31, 90)
            ));
            $project->price_text = $price > 30 ? $price . " triệu" : "$" . ($price * 100) . "";
            $project->price = $price > 30 ? $price * 1000 * 1000 : $price * 100 * 22767;
            $project->acreages = $faker->numberBetween(100, 200);
            $project->bed_rooms = $faker->numberBetween(1, 5);
            $project->toilets = $faker->numberBetween(1, 5);
            $project->garages = $faker->numberBetween(1, 3);
            $project->management_fee = $faker->numberBetween(20, 200) * 1000;
            $project->vat = $faker->randomElement(array(
                'Đã bao gồm', "Chưa bao gồm"
            ));
            $project->motorcycle_fee = $faker->numberBetween(5, 20) * 1000;
            $project->car_fee = $faker->numberBetween(30, 50) * 1000;
            $project->category_id = 2;
            $project->furniture_id = $faker->numberBetween(1, 2);
            $project->save();

            $arrayImages = $faker->randomElements(
                array(
                    array('imageUrl' => '\files\1\demo_canho\1.jpg', 'name' => '1'),
                    array('imageUrl' => '\files\1\demo_canho\2.jpg', 'name' => '2'),
                    array('imageUrl' => '\files\1\demo_canho\3.jpg', 'name' => '3'),
                    array('imageUrl' => '\files\1\demo_canho\4.jpg', 'name' => '4'),
                    array('imageUrl' => '\files\1\demo_canho\5.jpg', 'name' => '5'),
                    array('imageUrl' => '\files\1\demo_canho\6.jpg', 'name' => '6'),
                ),
                $faker->numberBetween(2, 6)
            );
            $project->images()->createMany($arrayImages);
        }
    }
}

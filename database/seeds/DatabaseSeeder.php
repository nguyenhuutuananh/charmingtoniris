<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminSeeder::class);
        $this->call(ConfigSeeder::class);
        $this->call(SliderSeeder::class);
        $this->call(FakerPostData::class);
        $this->call(FakerPageData::class);
        // $this->call(FakeProjectDataSeeder::class);
        
        // $this->call(UsersTableSeeder::class);
    }
}

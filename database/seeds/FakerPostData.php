<?php

use Illuminate\Database\Seeder;
use App\PostCategory;

class FakerPostData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $post_content = <<<EOT
            <!-- AddThis Sharing Buttons above -->
            <p>&#8216;GPR&#8217; &#8211; TNR The GoldView những ngày cuối năm 2017 là những ngày tất bật của đội ngủ thi công &#8211;
                đơn vị Coteccon nhằm gấp rút hoàn thành những hạng mục cuối cùng của dự án. Các đơn vị thầu phụ đang dốc hết sức
                để kiểm tra lại các hệ thống và thiết bị trước khi bàn giao nhà hoàn thiện cao cấp cho khách hàng, cùng cảnh quan
                đẹp lung linh trước khi dự án đón năm mới 2018 bằng vẻ đẹp hoành tráng.</p>
            <p>Hình ảnh mà khách hàng mong đợi nhất là TNR The GoldView khoát lên mình chiếc áo mà toát lên vẻ đẹp của sự sang trọng,
                mang đến sự đẳng cấp cho những cư dân chọn TNR The GoldView là nơi tìm đến bến bờ của hạnh phúc.</p>
            <p>Cập nhật hình ảnh kiểm tra hệ thống đèn led trước dự án ngày 15/12/2017</p>
            <p>
                <img class="alignnone size-medium wp-image-1237" src="http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3500-800x600.jpg"
                    alt="" width="800" height="600" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3500-800x600.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3500-768x576.jpg 768w, http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3500.jpg 1072w"
                    sizes="(max-width: 800px) 100vw, 800px" />
                <img class="alignnone size-large wp-image-1238" src="http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3699-1170x878.jpg"
                    alt="" width="1170" height="878" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3699-1170x878.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3699-800x600.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3699-768x576.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1239" src="http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3727-1170x878.jpg"
                    alt="" width="1170" height="878" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3727-1170x878.jpg 1170w, http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3727-800x600.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3727-768x576.jpg 768w"
                    sizes="(max-width: 1170px) 100vw, 1170px" />
                <img class="alignnone size-large wp-image-1240" src="http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3745.jpg"
                    alt="" width="960" height="1280" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3745.jpg 960w, http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3745-800x1067.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3745-768x1024.jpg 768w"
                    sizes="(max-width: 960px) 100vw, 960px" />
            </p>
            <p>
                <img class="alignnone size-medium wp-image-1242" src="http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3877-800x600.jpg"
                    alt="" width="800" height="600" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3877-800x600.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3877-768x576.jpg 768w, http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3877-1170x878.jpg 1170w"
                    sizes="(max-width: 800px) 100vw, 800px" />
                <img class="alignnone size-medium wp-image-1243" src="http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3879-800x600.jpg"
                    alt="" width="800" height="600" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3879-800x600.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3879-768x576.jpg 768w, http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3879-1170x878.jpg 1170w"
                    sizes="(max-width: 800px) 100vw, 800px" />
                <img class="alignnone size-medium wp-image-1244" src="http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3883-800x796.jpg"
                    alt="" width="800" height="796" srcset="http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3883-800x796.jpg 800w, http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3883-768x764.jpg 768w, http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3883-150x150.jpg 150w, http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3883-266x266.jpg 266w, http://canhothegoldview.com/mucchucnang/uploads/2017/12/IMG_3883.jpg 1080w"
                    sizes="(max-width: 800px) 100vw, 800px" />
            </p>
EOT;
        $post_category = new PostCategory();
        $post_category->title = "Tin tức";
        $post_category->save();
        // post
        for($i=0;$i<10;$i++) {
            $post_category->posts()->create([
                'title' => $faker->sentence(6),
                'description' => $faker->sentence(100),
                'content' => $post_content,
                'imageUrl' => $faker->randomElement(
                    array(
                        '\files\1\demo_tintuc\1.jpg',
                        '\files\1\demo_tintuc\2.jpg',
                        '\files\1\demo_tintuc\3.jpg',
                        '\files\1\demo_tintuc\4.jpg',
                        '\files\1\demo_tintuc\5.jpg',
                        '\files\1\demo_tintuc\6.jpg',
                        '\files\1\demo_tintuc\7.jpg',
                        '\files\1\demo_tintuc\8.jpg',
                    )
                ),
            ]);
        }
    }
}

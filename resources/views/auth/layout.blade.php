<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>@yield('title') | {{ config('app.name', 'Willx') }}</title>

  <!-- Font Awesome -->
  <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

  <!-- Bootstrap -->
  <!-- NProgress -->
  <!-- Animate.css -->
  <!-- Custom Theme Style -->
  <link href="{{ mix('admin-template/css/app.css') }}" rel="stylesheet">

</head>

<body class="login">
  @yield('content')
</body>

</html>
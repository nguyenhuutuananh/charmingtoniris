@extends('frontpage.layout') 
@section('title')Mặt bằng
@endsection
 
@section('content')
<div id='breadcrumbs'>
    <div class="container">
        <strong>Đang xem:</strong>
        <a href="{{url('/')}}" title="{{__c('web_title')}}">{{__c('web_title')}}</a>        / Mặt bằng</div>
    <div class="clear"></div>
    <!--//.container-->
</div>
<div class="matbang">
    <div class="container">
        <div class="box-intro-text text-center">
            Vui lòng click chuột vào Tháp để xem chi tiết mặt bằng tầng.
        </div>
    </div>
    <div id="master_plan">

        <div align="center">
                {{-- height="957" --}}
            <img class="img-responsive map" alt="Mặt bằng căn hộ"  src="{{asset('goldview/mucchucnang/thesis/skins/classic-r/images/mat-bang.jpg')}}"
                usemap="#mymap" width="1920">
        </div>
        <map name="mymap">
                <area coords="704, 666, 766, 760, 780, 754, 813, 801, 821, 798, 836, 815, 852, 807, 912, 879, 913, 897, 941, 885, 971, 924, 980, 929, 986, 929, 991, 925, 994, 922, 999, 880, 1026, 822, 1078, 782, 1139, 761, 1202, 765, 1227, 763, 1232, 752, 1197, 715, 1218, 704, 1277, 337, 1052, 160, 977, 188, 977, 263, 807, 318, 806, 333, 751, 271, 665, 297, 654, 285, 703, 644"
                    data-tooltip="block2_sticky1" href="{{route('mat_bang.a')}}" shape="poly" />
                <area coords="660, 591, 621, 536, 572, 260, 719, 216, 721, 219, 771, 203, 809, 244, 815, 317, 806, 322, 805, 329, 750, 265, 665, 295, 654, 282, 695, 576"
                    data-tooltip="block2_sticky2" href="{{route('mat_bang.b')}}" shape="poly" />
            </map>
    </div>
    <script type="text/javascript" src="{{asset('goldview/mat_bang/template/tint/js/stickytooltip.js')}}"></script>
    <script type="text/javascript" src="{{asset('goldview/mat_bang/template/tint/lib/imgmaps/jquery.imagemapster.min.js')}}"></script>

    <script>
        function loadMaps() {
                jQuery('img[usemap]').mapster({
                    fillColor: 'dcb91c',
                    fillOpacity: 0.4,
                    stroke: false,
                    singleSelect: true,
                    clickNavigate: true
                });
            }
            jQuery(document).ready(function () {
                jQuery(".project-nav-horizontal .project-plan").addClass('active');
                loadMaps();
                jQuery(window).resize(function () {
                    loadMaps();
                });
            });
    </script>
</div>
@endsection
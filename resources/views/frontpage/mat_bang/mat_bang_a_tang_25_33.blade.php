@extends('frontpage.layout') 
@section('title')Mặt bằng tháp A tầng 25-3
@endsection
 
@section('content')
<div id='breadcrumbs'>
    <div class="container">
        <strong>Đang xem:</strong>
        <a href="{{url('/')}}" title="{{__c('web_title')}}">{{__c('web_title')}}</a>        / Mặt bằng tháp A tầng 25- tầng 33</div>
    <div class="clear"></div>
    <!--//.container-->
</div>
<div>
    <div class="page-content page-project-introduction page-project-normal page-project-master-view">

        <div class="container">

            <div class="album-filter-list">
                <ul>
                    <li>
                        <a href='{{ route('mat_bang.a') }}'>Tầng 06</a>
                    </li>
                    <li>
                        <a href='{{route('mat_bang.a.tang_7_24')}}'>Tầng 07-24</a>
                    </li>
                    <li class='active'>
                        <a href='{{route('mat_bang.a.tang_25_33')}}'>Tầng 25-33</a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div id="master_plan">
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                    <p class="title_mb">
                        Mặt bằng căn hộ
                        <br />
                        <span style="font-size: 24px;">Tháp A: tầng 25- tầng 33</span>
                    </p>
                    <img alt="" class="img-responsive" src="{{asset('goldview/mat_bang/assets/uploads/myfiles/images/line_mb.png')}}" />
                    <p style="padding: 20px 20px 20px 0px;">
                        Mặt bằng căn hộ Tháp A: Tầng 25 - Tầng 33 được thiết kế thông thoáng và hợp lý với 10 loại căn hộ diện tích khác nhau.</p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 1])}}">
                                <span style="padding: 5px 12px; border-radius: 40px; margin-right: 15px; background: rgb(242, 175, 116);">&nbsp;</span>&nbsp;1 Phòng ngủ / 50.0m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 1])}}">
                                <span style="padding: 5px 12px; border-radius: 40px; margin-right: 15px; background: rgb(219, 140, 77);">&nbsp;</span>&nbsp;1 Phòng ngủ / 56.1m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 2])}}">
                                <span style="padding: 5px 12px; border-radius: 40px; margin-right: 15px; background: rgb(92, 125, 167);">&nbsp;</span>&nbsp;2 Phòng ngủ / 80.7m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 2])}}">
                                <span style="padding: 5px 12px; border-radius: 40px; margin-right: 15px; background: rgb(92, 125, 167);">&nbsp;</span>&nbsp;2 Phòng ngủ / 83.4m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 2])}}">
                                <span style="padding: 5px 12px; border-radius: 40px; margin-right: 15px; background: rgb(92, 125, 167);">&nbsp;</span>&nbsp;2 Phòng ngủ / 90.2m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 2])}}">
                                <span style="padding: 5px 12px; border-radius: 40px; margin-right: 15px; background: rgb(162, 200, 224);">&nbsp;</span>&nbsp;2 Phòng ngủ / 90.4m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 2])}}">
                                <span style="padding: 5px 12px; border-radius: 40px; margin-right: 15px; background: rgb(85, 185, 212);">&nbsp;</span>&nbsp;2 Phòng ngủ / 91.6m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 2])}}">
                                <span style="padding: 5px 12px; border-radius: 40px; margin-right: 15px; background: rgb(250, 169, 31);">&nbsp;</span>&nbsp;2 Phòng ngủ / 70.75m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 2])}}">
                                <span style=" background:#faa91f;padding:5px 12px;border-radius:40px; margin-right:15px">&nbsp;</span> 2 Phòng ngủ / 69.11m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 2])}}">
                                <span style=" background:#faa91f;padding:5px 12px;border-radius:40px; margin-right:15px">&nbsp;</span> 2 Phòng ngủ / 67.5m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 2])}}">
                                <span style=" background:#faa91f;padding:5px 12px;border-radius:40px; margin-right:15px">&nbsp;</span> 2 Phòng ngủ / 68.28m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 2])}}">
                                <span style=" background:#faa91f;padding:5px 12px;border-radius:40px; margin-right:15px">&nbsp;</span> 2 Phòng ngủ / 68.84m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 2])}}">
                                <span style="padding: 5px 12px; border-radius: 40px; margin-right: 15px; background: rgb(159, 141, 170);">&nbsp;</span>&nbsp;2 Phòng ngủ / 77.45m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 3])}}">
                                <span style="padding: 5px 12px; border-radius: 40px; margin-right: 15px; background: rgb(139, 197, 65);">&nbsp;</span>&nbsp;3 Phòng ngủ / 100.1m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 3])}}">
                                <span style="padding: 5px 12px; border-radius: 40px; margin-right: 15px; background: rgb(121, 150, 60);">&nbsp;</span>&nbsp;3 Phòng ngủ / 116.8m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 3])}}">
                                <span style="padding: 5px 12px; border-radius: 40px; margin-right: 15px; background: rgb(121, 150, 60);">&nbsp;</span>&nbsp;3 Phòng ngủ / 130.4m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 3])}}">
                                <span style="padding: 5px 12px; border-radius: 40px; margin-right: 15px; background: rgb(74, 91, 40);">&nbsp;</span>&nbsp;3 Phòng ngủ / 133.2m²</a>
                    </p>
                </div>
                <div class="col-xs-10 col-sm-9 col-md-6 col-lg-7">
                    <div id="image_map">
                        <img alt="Mặt bằng căn hộ" class="img-responsive img-backgroud" height="810px" src="{{asset('goldview/mat_bang/assets/uploads/myfiles/images/ThapA-tang25-33-hover0.png')}}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="K1 img-responsive img-hover" height="810px" src="{{asset('goldview/mat_bang/assets/uploads/myfiles/images/ThapA-tang25-33-hover1.png')}}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="K11 img-responsive img-hover" height="810px" src="{{asset('goldview/mat_bang/assets/uploads/myfiles/images/ThapA-tang25-33-hover1315.png')}}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="K2 img-responsive img-hover" height="810px" src="{{ asset( 'goldview/mucchucnang/uploads/2017/09/ThapA-tang25-33-hover2.png' ) }}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="K3 img-responsive img-hover" height="810px" src="{{ asset( 'goldview/mucchucnang/uploads/2017/09/ThapA-tang25-33-hover3.png' ) }}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="K307 img-responsive img-hover" height="810px" src="{{asset('goldview/mat_bang/assets/uploads/myfiles/images/ThapA-tang25-33-hover307.png')}}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="K308 img-responsive img-hover" height="810px" src="{{ asset( 'goldview/mucchucnang/uploads/2017/09/ThapA-tang25-33-hover308.png' ) }}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="K315 img-responsive img-hover" height="810px" src="{{ asset( 'goldview/mucchucnang/uploads/2017/09/ThapA-tang25-33-hover315.png' ) }}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="K4 img-responsive img-hover" height="810px" src="{{asset('goldview/mat_bang/assets/uploads/myfiles/images/ThapA-tang25-33-hover4.png')}}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="K5 img-responsive img-hover" height="810px" src="{{asset('goldview/mat_bang/assets/uploads/myfiles/images/ThapA-tang25-33-hover5.png')}}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ " class="K7 img-responsive img-hover" height="810px" src="{{asset('goldview/mat_bang/assets/uploads/myfiles/images/ThapA-tang7-24-hover-7-2.png')}}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ " class="K72 img-responsive img-hover" height="810px" src="{{asset('goldview/mat_bang/assets/uploads/myfiles/images/ThapA-tang7-24-hover7-03.png')}}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ " class="K701 img-responsive img-hover" height="810px" src="{{asset('goldview/mat_bang/assets/uploads/myfiles/images/ThapA-tang7-24-hover7-01.png')}}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ " class="K711 img-responsive img-hover" height="810px" src="{{asset('goldview/mat_bang/assets/uploads/myfiles/images/ThapA-tang7-24-hover7-11.png')}}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="K71 img-responsive img-hover" height="810px" src="{{asset('goldview/mat_bang/assets/uploads/myfiles/images/ThapA-tang25-33-hover711.png.png')}}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="K10 img-responsive img-hover" height="810px" src="{{ asset( 'goldview/mucchucnang/uploads/2017/09/ThapA-tang25-33-hover7.png' ) }}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="K12 img-responsive img-hover" height="810px" src="{{asset('goldview/mat_bang/assets/uploads/myfiles/images/ThapA-tang25-33-hover8.png')}}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="K13 img-responsive img-hover" height="810px" src="{{ asset( 'goldview/mucchucnang/uploads/2017/09/ThapA-tang25-33-hover10.png' ) }}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="img-responsive map" height="810px" src="{{asset('goldview/mat_bang/assets/uploads/myfiles/images/ThapA-tang25-33-hover0-tran.png')}}"
                            usemap="#mymap" width="1240px" />
                    </div>
                    <map name="mymap">
                            <area coords="19, 104, 96, 185" data-key="K1" href="#" shape="rect" />
                            <area coords="604, 23, 681, 105" data-key="K1" href="#" shape="rect" />
                            <area coords="19, 692, 188, 774" data-key="K1" href="#" shape="rect" />
                            <area coords="681, 637, 760, 720" data-key="K1" href="#" shape="rect" />
                            <area coords="96, 91, 188, 172" data-key="K2" href="#" shape="rect" />
                            <area coords="681, 11, 773, 93" data-key="K2" href="#" shape="rect" />
                            <area coords="19, 188, 97, 690" data-key="K3" href="#" shape="rect" />
                            <area coords="109, 186, 188, 249" data-key="K3" href="#" shape="rect" />
                            <area coords="110, 629, 189, 689" data-key="K3" href="#" shape="rect" />
                            <area coords="681, 511, 760, 634" data-key="K3" href="#" shape="rect" />
                            <area coords="696, 233, 773, 423" data-key="K3" href="#" shape="rect" />
                            <area coords="603, 108, 682, 232" data-key="K3" href="#" shape="rect" />
                            <area coords="695, 168, 773, 231" data-key="K308" href="#" shape="rect" />
                            <area coords="772, 511, 859, 634" data-key="K315" href="#" shape="rect" />
                            <area coords="109, 252, 188, 333" data-key="K4" href="#" shape="rect" />
                            <area coords="175, 336, 618, 422" data-key="K5" href="#" shape="rect" />
                            <area coords="238, 435, 301, 498" data-key="K7" href="#" shape="rect" />
                            <area coords="178, 433, 238, 495" data-key="K72" href="#" shape="rect" />
                            <area coords="491, 434, 556, 498" data-key="K701" href="#" shape="rect" />
                            <area coords="301, 433, 365, 494" data-key="K701" href="#" shape="rect" />
                            <area coords="553, 432, 617, 496" data-key="K711" href="#" shape="rect" />
                            <area coords="110, 503, 173, 627" data-key="K71" href="#" shape="rect" />
                            <area coords="618, 433, 694, 496" data-key="K10" href="#" shape="rect" />
                            <area coords="772, 637, 859, 720" data-key="K11" href="#" shape="rect" />
                            <area coords="720, 104, 773, 166" data-key="K307" href="#" shape="rect" />
                            <area coords="629, 235, 682, 336" data-key="K12" href="#" shape="rect" />
                            <area coords="772, 453, 859, 510" data-key="K13" href="#" shape="rect" />
                        </map>
                </div>
                <div class="col-xs-2 col-sm-3 col-md-2 col-lg-2">
                    <a class="bt-block-a" href="{{ route('mat_bang.a') }}">Tháp A</a>
                    <a class="bt-block-b" href="{{ route('mat_bang.b') }}">Tháp B</a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="page-control">
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <a class="bt-back" href="{{route('mat_bang')}}">
                                <span>Quay lại
                                    <br /> mặt bằng</span>
                            </a>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <link rel="stylesheet" href="{{asset('goldview/mat_bang/template/tint/css/tina.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('goldview/mat_bang/template/tint/lib/bootstrap/css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('goldview/mat_bang/template/tint/css/template.css')}}" type="text/css" />
    <script type="text/javascript" src="{{asset('goldview/mat_bang/template/tint/js/stickytooltip.js')}}"></script>
    <script type="text/javascript" src="{{asset('goldview/mat_bang/template/tint/js/jquery.hoverIntent.js')}}"></script>
    <script type="text/javascript" src="{{asset('goldview/mat_bang/template/tint/lib/imgmaps/jquery.imagemapster.min.js')}}"></script>

    <script>
        var tooltip = [];
            jQuery("#master_plan area").each(function () {
                jQuery(this).hover(function () {
                    jQuery("#image_map img." + jQuery(this).attr("data-key")).css("opacity", "1");
                    jQuery("#image_map img.map").stop().animate({
                        opacity: "0"
                    }, 300);
                }, function () {
                    jQuery("#image_map img.map").stop().animate({
                        opacity: "1"
                    }, 600);
                    jQuery("#image_map img." + jQuery(this).attr("data-key")).css("opacity", "0");
                });
            });

            function loadMaps() {
                jQuery('img[usemap]').mapster({
                    fillColor: 'dcb91c',
                    fillOpacity: 0.0,
                    stroke: false,
                    singleSelect: true,
                    clickNavigate: true
                });
            }
            jQuery(document).ready(function () {
                jQuery(".project-nav-horizontal .project-plan").addClass('active');
                jQuery(".nav_column > a.lnk-masterplan").addClass('active');

                jQuery("a.bt-block-a").addClass('active');

                loadMaps();
                jQuery(window).resize(function () {
                    loadMaps();
                });
            });
    </script>
    <div class='clearfix'></div>
</div>
@endsection
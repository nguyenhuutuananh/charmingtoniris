@extends('frontpage.layout') 
@section('title')Mặt bằng tháp B
@endsection
 
@section('content')
<div id='breadcrumbs'>
    <div class="container">
        <strong>Đang xem:</strong>
        <a href="{{url('/')}}" title="{{__c('web_title')}}">{{__c('web_title')}}</a>        / Mặt bằng tháp B</div>
    <div class="clear"></div>
    <!--//.container-->
</div>
<div class="thapb">
    <div class="page-content page-project-introduction page-project-normal page-project-master-view">

        <div class="container">

            <div class="album-filter-list">
                <ul>
                    <li class='active'>
                        <a href='{{route('mat_bang.b')}}'>Mặt bằng căn hộ tháp B</a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div id="master_plan">
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                    <p class="title_mb">
                        Mặt bằng căn hộ
                        <br />
                        <span style="font-size: 24px;">Tháp B: 27 tầng</span>
                    </p>
                    <img alt="" class="img-responsive" src="{{asset('goldview/mat_bang/assets/uploads/myfiles/images/line_mb.png')}}" />
                    <p style="padding: 20px 20px 20px 0px;">
                        Mặt bằng căn hộ Tháp B được thiết kế thông thoáng và&nbsp;hợp lý với 5 loại diện tích căn hộ:</p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 2])}}">
                                <span style="padding: 5px 12px; border-radius: 40px; margin-right: 15px; background: rgb(18, 149, 191);">&nbsp;</span>&nbsp;2 Phòng ngủ / 63.1m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 2])}}">
                                <span style="padding: 5px 12px; border-radius: 40px; margin-right: 15px; background: rgb(18, 149, 191);">&nbsp;</span>&nbsp;2 Phòng ngủ / 65.1m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 2])}}">
                                <span style="padding: 5px 12px; border-radius: 40px; margin-right: 15px; background: rgb(114, 109, 141);">&nbsp;</span>&nbsp;2 Phòng ngủ / 78.8m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 2])}}">
                                <span style="padding: 5px 12px; border-radius: 40px; margin-right: 15px; background: rgb(37, 98, 116);">&nbsp;</span>&nbsp;2 Phòng ngủ / 80.2m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 2])}}">
                                <span style="padding: 5px 12px; border-radius: 40px; margin-right: 15px; background: rgb(37, 98, 116);">&nbsp;</span>&nbsp;2 Phòng ngủ / 80.3m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 2])}}">
                                <span style="padding: 5px 12px; border-radius: 40px; margin-right: 15px; background: rgb(37, 98, 116);">&nbsp;</span>&nbsp;2 Phòng ngủ / 81.9m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 3])}}">
                                <span style="padding: 5px 12px; border-radius: 40px; margin-right: 15px; background: rgb(180, 212, 53);">&nbsp;</span>&nbsp;3 Phòng ngủ / 114.3m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 3])}}">
                                <span style="padding: 5px 12px; border-radius: 40px; margin-right: 15px; background: rgb(180, 212, 53);">&nbsp;</span>&nbsp;3 Phòng ngủ / 117.4m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 3])}}">
                                <span style="padding: 5px 12px; border-radius: 40px; margin-right: 15px; background: rgb(82, 129, 15);">&nbsp;</span>&nbsp;3 Phòng ngủ / 115.6m²</a>
                    </p>
                    <p class="phongngu_rt7">
                        <a href="{{route('project.search', ['phong-ngu' => 3])}}">
                                <span style="padding: 5px 12px; border-radius: 40px; margin-right: 15px; background: rgb(82, 129, 15);">&nbsp;</span>&nbsp;3 Phòng ngủ /119.2m²</a>
                    </p>
                </div>
                <div class="col-xs-10 col-sm-9 col-md-6 col-lg-7">

                    <div id="image_map">
                        <img alt="Mặt bằng căn hộ" class="img-responsive img-backgroud" height="810px" src="{{asset('goldview/mat_bang/assets/uploads/myfiles/images/ThapB-Tang-hover0.png')}}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="K1 img-responsive img-hover" height="810px" src="{{asset('goldview/mat_bang/assets/uploads/myfiles/images/ThapB-Tang-hover1.png')}}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="K11 img-responsive img-hover" height="810px" src="{{asset('goldview/mat_bang/assets/uploads/myfiles/images/ThapB-Tang-hover11.png')}}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="K2 img-responsive img-hover" height="810px" src="{{asset('goldview/mat_bang/assets/uploads/myfiles/images/ThapB-Tang-hover2.png')}}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="K3 img-responsive img-hover" height="810px" src="{{asset('goldview/mat_bang/assets/uploads/myfiles/images/ThapB-Tang-hover3.png')}}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="K31 img-responsive img-hover" height="810px" src="{{asset('goldview/mat_bang/assets/uploads/myfiles/images/ThapB-Tang-hover31.png')}}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="K5 img-responsive img-hover" height="810px" src="{{ asset( 'goldview/mucchucnang/uploads/2017/09/ThapB-Tang-hover5.png' ) }}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="K51 img-responsive img-hover" height="810px" src="{{ asset( 'goldview/mucchucnang/uploads/2017/09/ThapB-Tang-hover51.png' ) }}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="K52 img-responsive img-hover" height="810px" src="{{ asset( 'goldview/mucchucnang/uploads/2017/09/ThapB-Tang-hover52.png' ) }}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="K6 img-responsive img-hover" height="810px" src="{{asset('goldview/mat_bang/assets/uploads/myfiles/images/ThapB-Tang-hover6.png')}}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="K7 img-responsive img-hover" height="810px" src="{{asset('goldview/mat_bang/assets/uploads/myfiles/images/ThapB-Tang-hover7.png')}}"
                            width="1240px" />
                        <img alt="Mặt bằng căn hộ" class="img-responsive map" height="810px" src="{{asset('goldview/mat_bang/assets/uploads/myfiles/images/ThapB-Tang-hover0-tran.png')}}"
                            usemap="#mymap" width="1240px" />
                    </div>
                    <map name="mymap">
                            <area coords="7, 66, 186, 66, 185, 220, 150, 220, 150, 255, 8, 254" data-key="K1" href="#" shape="poly" />
                            <area coords="747, 51, 928, 220" data-key="K11" href="#" shape="rect" />
                            <area coords="186, 93, 209, 93, 210, 51, 324, 53, 323, 221, 186, 221" data-key="K2" href="#" shape="poly" />
                            <area coords="604, 52, 720, 53, 720, 95, 743, 95, 743, 220, 605, 218" data-key="K2" href="#" shape="poly" />
                            <area coords="438, 51, 439, 95, 462, 96, 462, 183, 326, 184, 325, 51" data-key="K3" href="#" shape="poly" />
                            <area coords="486, 95, 486, 50, 598, 52, 601, 184, 466, 184, 465, 95" data-key="K31" href="#" shape="poly" />
                            <area coords="631, 278, 657, 278, 657, 254, 767, 254, 767, 390, 746, 390, 746, 469, 632, 469" data-key="K5" href="#" shape="poly"
                            />
                            <area coords="218, 254, 328, 256, 328, 277, 353, 277, 354, 471, 239, 471, 240, 390, 218, 389" data-key="K5" href="#" shape="poly"
                            />
                            <area coords="601, 255, 601, 277, 626, 278, 626, 469, 515, 471, 514, 390, 496, 390, 498, 255" data-key="K51" href="#" shape="poly"
                            />
                            <area coords="382, 277, 383, 253, 496, 254, 494, 389, 473, 389, 473, 471, 358, 471, 357, 276" data-key="K52" href="#" shape="poly"
                            />
                            <area coords="772, 254, 941, 438" data-key="K6" href="#" shape="rect" />
                            <area coords="8, 257, 181, 437" data-key="K7" href="#" shape="rect" />
                        </map>
                </div>
                <div class="col-xs-2 col-sm-3 col-md-2 col-lg-2">
                    <a class="bt-block-a" href="{{ route('mat_bang.a') }}">Tháp A</a>
                    <a class="bt-block-b" href="{{ route('mat_bang.b') }}">Tháp B</a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="page-control">
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <a class="bt-back" href="{{route('mat_bang')}}">
                                <span>Quay lại
                                    <br /> mặt bằng</span>
                            </a>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <link rel="stylesheet" href="{{asset('goldview/mat_bang/template/tint/css/tina.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('goldview/mat_bang/template/tint/lib/bootstrap/css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('goldview/mat_bang/template/tint/css/template.css')}}" type="text/css" />
    <script type="text/javascript" src="{{asset('goldview/mat_bang/template/tint/js/stickytooltip.js')}}"></script>
    <script type="text/javascript" src="{{asset('goldview/mat_bang/template/tint/js/jquery.hoverIntent.js')}}"></script>
    <script type="text/javascript" src="{{asset('goldview/mat_bang/template/tint/lib/imgmaps/jquery.imagemapster.min.js')}}"></script>

    <script>
        var tooltip = [];
            jQuery("#master_plan area").each(function () {
                jQuery(this).hover(function () {
                    jQuery("#image_map img." + jQuery(this).attr("data-key")).css("opacity", "1");
                    jQuery("#image_map img.map").stop().animate({
                        opacity: "0"
                    }, 300);
                }, function () {
                    jQuery("#image_map img.map").stop().animate({
                        opacity: "1"
                    }, 600);
                    jQuery("#image_map img." + jQuery(this).attr("data-key")).css("opacity", "0");
                });
            });

            function loadMaps() {
                jQuery('img[usemap]').mapster({
                    fillColor: 'dcb91c',
                    fillOpacity: 0.0,
                    stroke: false,
                    singleSelect: true,
                    clickNavigate: true
                });
            }
            jQuery(document).ready(function () {
                jQuery(".project-nav-horizontal .project-plan").addClass('active');
                jQuery(".nav_column > a.lnk-masterplan").addClass('active');

                jQuery("a.bt-block-b").addClass('active');

                loadMaps();
                jQuery(window).resize(function () {
                    loadMaps();
                });
            });
    </script>
    <div class='clearfix'></div>
</div>
@endsection
@extends('frontpage.layout') 
@section('title'){{$post->title}}
@endsection
 
@section('content')
<div id='breadcrumbs'>
    <div class="container">
        <strong>Đang xem:</strong>
        <a href="{{url('/')}}" title="{{__c('web_title')}}">{{__c('web_title')}}</a> /
        <a href="{{route($post->category->slug.'.list')}}" rel="category tag">{{$post->category->title}}</a>
        » <a href="{{route($post->category->slug.'.detail', ['slug' => $post->slug])}}" rel="category tag">{{$post->title}}</a></div>
    <div class="clear"></div>
    <!--//.container-->
</div>
<div class="container">
    <div class="suaanh">
    </div>
    <div class="column">
        <div class="content">
            <div id="{{$post->slug}}" class="post_box top" itemscope itemtype="http://schema.org/Article">
                <meta itemscope itemprop="mainEntityOfPage" itemType="https://schema.org/WebPage" itemid="{{route($post->category->slug.'.detail', ['slug' => $post->slug])}}"
                />
                <h1 class="headline" itemprop="headline">{{$post->title}}</h1>
                <div class="chitietbai">
                </div>
                <div class="post_content" itemprop="articleBody">
                    {!! $post->content !!}

                    <div class="wp_rp_wrap  wp_rp_twocolumns" id="wp_rp_first">
                        <div class="wp_rp_content">
                            <h3 class="related_post_title">Tin khác từ Web</h3>
                            <ul class="related_post wp_rp">
                                @foreach (\App\Post::inRandomOrder()->skip(0)->take(6)->get() as $relatePost)
                                <li data-position="0" data-poid="in-966" data-post-type="none">
                                    <a href="{{route($post->category->slug.'.detail', ['slug' => $relatePost->slug])}}" class="wp_rp_thumbnail">
                                            <img src="{{url($relatePost->imageUrl)}}" alt="{{$relatePost->title}}"
                                                width="150" height="150" />
                                        </a>
                                    <a href="{{route($post->category->slug.'.detail', ['slug' => $relatePost->slug])}}" class="wp_rp_title">{{$relatePost->title}}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- AddThis Sharing Buttons below -->
                    <div class="addthis_toolbox addthis_default_style" addthis:url='{{route($post->category->slug.'.detail', ['slug' => $post->slug])}}'>
                        <a class="addthis_button_facebook_like"></a>
                        <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
                        <a class="addthis_button_pinterest_pinit"></a>
                        <a class="addthis_button_tweet"></a>
                        <a class="addthis_counter addthis_pill_style"></a>
                    </div>
                </div>
                @include('frontpage.lien_he')
                <div class="lienquan">
                </div>
            </div>
        </div>
        {{-- sidebar --}}
        @include('frontpage.sidebar')
        {{-- end sidebar --}}
    </div>
</div>
@endsection
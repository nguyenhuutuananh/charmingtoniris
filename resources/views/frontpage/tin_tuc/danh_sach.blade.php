@extends('frontpage.layout') 
@section('title'){{$post_category->title}}
@endsection
 
@section('content')
<div id='breadcrumbs'>
    <div class="container">
        <strong>Đang xem:</strong>
        <a href="{{url('/')}}" title="{{__c('web_title')}}">{{__c('web_title')}}</a>        /
        <a href="{{route($post_category->slug.'.list')}}" rel="category tag">{{$post_category->title}}</a>
    </div>
    <div class="clear"></div>
    <!--//.container-->
</div>
<div class="container">
    <div class="column">
        <div class="content">
            @foreach (\App\Post::where('category_id', $post_category->id)->orderBy('id', 'desc')->paginate(10) as $post)
            <div id="{{$post->slug}}" class="post_box cmtintuc top" itemscope itemtype="http://schema.org/Article">
                <a class="featured_image_link" href="{{route('news.detail', ['slug' => $post->slug])}}">
                        <img width="200" height="127" src="{{url($post->imageUrl)}}" class="alignleft wp-post-image"
                            alt="" itemprop="image" />
                    </a>
                <h3 class="headline tintuc" itemprop="headline">
                    <a href="{{route('news.detail', ['slug' => $post->slug])}}" rel="bookmark">{{$post->title}}</a>
                </h3>
                <div class="post_content post_excerpt xemthem" itemprop="description">
                    <p>{{str_limit($post->description, 120, " [...]")}}
                        <a class="excerpt_read_more" href="{{route('news.detail', ['slug' => $post->slug])}}">Xem thêm</a>
                    </p>
                </div>
            </div>
            @endforeach
            <style>
                .pagination {
                    list-style: none;
                    display: inline-block;
                }
                .pagination .page-item {
                    padding-right: 10px;
                    display: inline-block;
                }
            </style>
            {{ \App\Post::where('category_id', $post_category->id)->orderBy('id', 'desc')->paginate(10)->links() }}
            {{-- <div class="phantrang">
            </div>
            <div class='wp-pagenavi'>
                <span class='pages'>Trang 1 trên 4</span>
                <span class='current'>1</span>
                <a class="page larger" href="page/2/index.html">2</a>
                <a class="page larger" href="page/3/index.html">3</a>
                <a class="page larger" href="page/4/index.html">4</a>
                <a class="nextpostslink" rel="next" href="page/2/index.html">&raquo;</a>
            </div> --}}
        </div>
        {{-- sidebar --}}
        @include('frontpage.sidebar')
        {{-- end sidebar --}}
    </div>
</div>
@endsection
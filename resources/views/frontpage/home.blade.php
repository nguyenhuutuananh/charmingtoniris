@extends('frontpage.layout')
@section('content')
<div class="sliderhome">
    <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin:0px auto;background-color:#E9E9E9;padding:0px;margin-top:0px;margin-bottom:0px;max-height:626px;">
        <!-- START REVOLUTION SLIDER 4.6.93 fullwidth mode -->
        <div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;max-height:626px;height:626px;">
            <ul>
                @foreach (\App\Slider::find(1)->images as $image)
                <!-- SLIDE  -->
                <li data-transition="random" data-slotamount="7" data-saveperformance="off">
                    <!-- MAIN IMAGE -->
                    <img src="{{ url($image->imageUrl) }}" alt="OFFICETEL-TNR-THEGOLDVIEW-1" data-bgposition="center top" data-bgfit="cover"
                        data-bgrepeat="no-repeat">
                    <!-- LAYERS -->
                </li>
                @endforeach {{--
                <!-- SLIDE  -->
                <li data-transition="random" data-slotamount="7" data-saveperformance="off">
                    <!-- MAIN IMAGE -->
                    <img src="{{ asset('goldview/mucchucnang/uploads/2017/10/tnr-thegoldview1.jpg') }}" alt="tnr-thegoldview1" data-bgposition="center top"
                        data-bgfit="cover" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->
                </li>
                <!-- SLIDE  -->
                <li data-transition="random" data-slotamount="7" data-saveperformance="off">
                    <!-- MAIN IMAGE -->
                    <img src="{{ asset('goldview/mucchucnang/uploads/2017/09/tnr-goldview-quan-4-gia-phuc-real.jpg') }}" alt="tnr-goldview-quan-4-gia-phuc-real"
                        data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->
                </li>
                <!-- SLIDE  -->
                <li data-transition="random" data-slotamount="7" data-saveperformance="off">
                    <!-- MAIN IMAGE -->
                    <img src="{{ asset('goldview/mucchucnang/uploads/2017/05/banner-6.jpg') }}" alt="banner-6" data-bgposition="center top" data-bgfit="cover"
                        data-bgrepeat="no-repeat">
                    <!-- LAYERS -->
                </li> --}}
            </ul>
            <div class="tp-bannertimer"></div>
        </div>


        <style scoped></style>

        <script type="text/javascript">
            /******************************************
                -	PREPARE PLACEHOLDER FOR SLIDER	-
            ******************************************/
            

            var setREVStartSize = function() {
                var	tpopt = new Object();
                    tpopt.startwidth = 1366;
                    tpopt.startheight = 626;
                    tpopt.container = jQuery('#rev_slider_1_1');
                    tpopt.fullScreen = "off";
                    tpopt.forceFullWidth="off";

                tpopt.container.closest(".rev_slider_wrapper").css({height:tpopt.container.height()});tpopt.width=parseInt(tpopt.container.width(),0);tpopt.height=parseInt(tpopt.container.height(),0);tpopt.bw=tpopt.width/tpopt.startwidth;tpopt.bh=tpopt.height/tpopt.startheight;if(tpopt.bh>tpopt.bw)tpopt.bh=tpopt.bw;if(tpopt.bh<tpopt.bw)tpopt.bw=tpopt.bh;if(tpopt.bw<tpopt.bh)tpopt.bh=tpopt.bw;if(tpopt.bh>1){tpopt.bw=1;tpopt.bh=1}if(tpopt.bw>1){tpopt.bw=1;tpopt.bh=1}tpopt.height=Math.round(tpopt.startheight*(tpopt.width/tpopt.startwidth));if(tpopt.height>tpopt.startheight&&tpopt.autoHeight!="on")tpopt.height=tpopt.startheight;if(tpopt.fullScreen=="on"){tpopt.height=tpopt.bw*tpopt.startheight;var cow=tpopt.container.parent().width();var coh=jQuery(window).height();if(tpopt.fullScreenOffsetContainer!=undefined){try{var offcontainers=tpopt.fullScreenOffsetContainer.split(",");jQuery.each(offcontainers,function(e,t){coh=coh-jQuery(t).outerHeight(true);if(coh<tpopt.minFullScreenHeight)coh=tpopt.minFullScreenHeight})}catch(e){}}tpopt.container.parent().height(coh);tpopt.container.height(coh);tpopt.container.closest(".rev_slider_wrapper").height(coh);tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(coh);tpopt.container.css({height:"100%"});tpopt.height=coh;}else{tpopt.container.height(tpopt.height);tpopt.container.closest(".rev_slider_wrapper").height(tpopt.height);tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(tpopt.height);}
            };

            /* CALL PLACEHOLDER */
            setREVStartSize();


            var tpj=jQuery;
            tpj.noConflict();
            var revapi1;

            tpj(document).ready(function() {

            if(tpj('#rev_slider_1_1').revolution == undefined){
                revslider_showDoubleJqueryError('#rev_slider_1_1');
            }else{
               revapi1 = tpj('#rev_slider_1_1').show().revolution(
                {	
                                            dottedOverlay:"none",
                    delay:9000,
                    startwidth:1366,
                    startheight:626,
                    hideThumbs:200,

                    thumbWidth:100,
                    thumbHeight:50,
                    thumbAmount:4,
                    
                                            
                    simplifyAll:"off",

                    navigationType:"bullet",
                    navigationArrows:"solo",
                    navigationStyle:"round",

                    touchenabled:"on",
                    onHoverStop:"on",
                    nextSlideOnWindowFocus:"off",

                    swipe_threshold: 75,
                    swipe_min_touches: 1,
                    drag_block_vertical: false,
                    
                                            
                                            
                    keyboardNavigation:"off",

                    navigationHAlign:"center",
                    navigationVAlign:"bottom",
                    navigationHOffset:0,
                    navigationVOffset:20,

                    soloArrowLeftHalign:"left",
                    soloArrowLeftValign:"center",
                    soloArrowLeftHOffset:20,
                    soloArrowLeftVOffset:0,

                    soloArrowRightHalign:"right",
                    soloArrowRightValign:"center",
                    soloArrowRightHOffset:20,
                    soloArrowRightVOffset:0,

                    shadow:0,
                    fullWidth:"on",
                    fullScreen:"off",

                                            spinner:"spinner2",
                                            
                    stopLoop:"off",
                    stopAfterLoops:-1,
                    stopAtSlide:-1,

                    shuffle:"off",

                    autoHeight:"on",
                    forceFullWidth:"off",
                    
                    
                    
                    hideThumbsOnMobile:"off",
                    hideNavDelayOnMobile:1500,
                    hideBulletsOnMobile:"off",
                    hideArrowsOnMobile:"off",
                    hideThumbsUnderResolution:0,

                                            hideSliderAtLimit:0,
                    hideCaptionAtLimit:0,
                    hideAllCaptionAtLilmit:0,
                    startWithSlide:0					});



                                }
            });	/*ready*/
        </script>


        <style type="text/css">
            #rev_slider_1_1_wrapper .tp-loader.spinner2 {
                background-color: #FFFFFF !important;
            }
        </style>
    </div>
    <!-- END REVOLUTION SLIDER -->
</div>
<div class="container">
    <div class="widget widget_text" id="text-4">
        <div class="textwidget">
            <ul class="list-icons-home">
                @php
                    $vitri = \App\Page::find(7);
                @endphp
                <li class="wow rollIn"><a href="{{route('page.detail', ['slug' => $vitri->slug])}}">
    <img src="{{ asset('goldview/mucchucnang/thesis/skins/classic-r/images/icon-1.png') }}" />
    <span>{{$vitri->title}}</span></a>
                </li>

                <li class="wow rollIn">
                    <a href="{{route('mat_bang')}}">
    <img src="{{ asset('goldview/mucchucnang/thesis/skins/classic-r/images/icon-2.png') }}" />
    <span>Mặt bằng </span></a>
                </li>
            @php
                $tienich = \App\Page::find(6);
            @endphp
                <li class="wow rollIn">
                    <a href="{{route('page.detail', ['slug' => $tienich->slug])}}">
    <img src="{{ asset('goldview/mucchucnang/thesis/skins/classic-r/images/icon-3.png') }}" />
    <span>{{$tienich->title}}</span></a>
                </li>
            @php
                $hinhanh = \App\Page::find(5);
            @endphp
                <li class="wow rollIn">
                    <a href="{{route('page.detail', ['slug' => $hinhanh->slug])}}">
    <img src="{{ asset('goldview/mucchucnang/thesis/skins/classic-r/images/icon-4.png') }}" />
    <span>{{$hinhanh->title}}</span></a>
                </li>
            @php
                $danhgia = \App\PostCategory::find(2);
            @endphp
                <li class="wow rollIn">
                    <a href="{{route('danh-gia.list')}}">
    <img src="{{ asset('goldview/mucchucnang/thesis/skins/classic-r/images/icon-5.png') }}" />
    <span>{{$danhgia->title}}</span></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="query_box noidunghome" itemscope itemtype="http://schema.org/Article">
        <div class="post_content" itemprop="articleBody">
            <!-- AddThis Sharing Buttons above -->
            <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1494646018164">
                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-has-fill">
                    <div class="vc_column-inner vc_custom_1494667010355">
                        <div class="wpb_wrapper">
                            <div class="wpb_text_column wpb_content_element  vc_custom_1501685013736">
                                <div class="wpb_wrapper">
                                    <p style="text-align: justify;"><span style="color: #ff6600;"><strong>Căn hộ TNR THE GOLDVIEW</strong></span> tại 346
                                        Bến Vân Đồn, Phường 1, Quận 4 là một trong những căn hộ cao cấp tiêu biểu tại Việt
                                        Nam do hệ thống giải thưởng danh giá International Property Awards (IPA) 2017 bình
                                        chọn. Dự án sở hữu vị trí vàng ngay mặt tiền đường Bến Văn Đồn, song song với đại
                                        lộ Võ Văn Kiệt và được bao quanh bởi những tuyến đường quan trọng của thành phố như:
                                        Nguyễn Thái Học, Hoàng Diệu, Khánh Hội,…Với vị trí này cư dân dễ dàng di chuyển từ
                                        dự án này đến các quận lân cận như Quận 1, Quận 3, Quận 5, Quận 8,Quận 10,…</p>
                                    <div class="widget widget_text tieudeb2">
                                        <div class="textwidget">
                                            <div class="tieudeb" style="text-align: center;"><span style="color: #ff6600;"><strong>VỊ TRÍ THUẬN LỢI</strong></span></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1494646098375 vc_row-has-fill">
                                <div class="wpb_column vc_column_container vc_col-sm-4 vc_col-has-fill">
                                    <div class="vc_column-inner vc_custom_1494667022395">
                                        <div class="wpb_wrapper">
                                            <div class="uavc-list-icon uavc-list-icon-wrapper ult-adjust-bottom-margin   ">
                                                <ul class="uavc-list">
                                                    <li>
                                                        <div class="uavc-list-content" id="list-icon-wrap-5931">
                                                            <div class="uavc-list-icon  " data-animation="" data-animation-delay="03" style="margin-right:5px;">
                                                                <div class="ult-just-icon-wrapper  ">
                                                                    <div class="align-icon" style="text-align:center;">
                                                                        <div class="aio-icon none " style="color:#dcb91d;font-size:32px;display:inline-block;">
                                                                            <i class="Defaults-check-circle"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div><span data-ultimate-target='#list-icon-wrap-5931 .uavc-list-desc'
                                                                data-responsive-json-new='{"font-size":"","line-height":""}'
                                                                class="uavc-list-desc ult-responsive" style="">Cách bệnh viện Quận 4: 3 phút</span></div>
                                                    </li>
                                                    <li>
                                                        <div class="uavc-list-content" id="list-icon-wrap-3586">
                                                            <div class="uavc-list-icon  " data-animation="" data-animation-delay="03" style="margin-right:5px;">
                                                                <div class="ult-just-icon-wrapper  ">
                                                                    <div class="align-icon" style="text-align:center;">
                                                                        <div class="aio-icon none " style="color:#dcb91d;font-size:32px;display:inline-block;">
                                                                            <i class="Defaults-check-circle"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div><span data-ultimate-target='#list-icon-wrap-3586 .uavc-list-desc'
                                                                data-responsive-json-new='{"font-size":"","line-height":""}'
                                                                class="uavc-list-desc ult-responsive" style="">Cách phố đi bộ Nguyễn Huệ: 3 phút</span></div>
                                                    </li>
                                                    <li>
                                                        <div class="uavc-list-content" id="list-icon-wrap-1671">
                                                            <div class="uavc-list-icon  " data-animation="" data-animation-delay="03" style="margin-right:5px;">
                                                                <div class="ult-just-icon-wrapper  ">
                                                                    <div class="align-icon" style="text-align:center;">
                                                                        <div class="aio-icon none " style="color:#dcb91d;font-size:32px;display:inline-block;">
                                                                            <i class="Defaults-check-circle"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div><span data-ultimate-target='#list-icon-wrap-1671 .uavc-list-desc'
                                                                data-responsive-json-new='{"font-size":"","line-height":""}'
                                                                class="uavc-list-desc ult-responsive" style="">Cách trung tâm văn hóa quận 4: 5 phút</span></div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-4 vc_col-has-fill">
                                    <div class="vc_column-inner vc_custom_1494667030018">
                                        <div class="wpb_wrapper">
                                            <div class="uavc-list-icon uavc-list-icon-wrapper ult-adjust-bottom-margin   ">
                                                <ul class="uavc-list">
                                                    <li>
                                                        <div class="uavc-list-content" id="list-icon-wrap-3783">
                                                            <div class="uavc-list-icon  " data-animation="" data-animation-delay="03" style="margin-right:5px;">
                                                                <div class="ult-just-icon-wrapper  ">
                                                                    <div class="align-icon" style="text-align:center;">
                                                                        <div class="aio-icon none " style="color:#dcb91d;font-size:32px;display:inline-block;">
                                                                            <i class="Defaults-check-circle"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div><span data-ultimate-target='#list-icon-wrap-3783 .uavc-list-desc'
                                                                data-responsive-json-new='{"font-size":"","line-height":""}'
                                                                class="uavc-list-desc ult-responsive" style="">Cách trung tâm TM Lotte quận 7 : 7 phút</span></div>
                                                    </li>
                                                    <li>
                                                        <div class="uavc-list-content" id="list-icon-wrap-2334">
                                                            <div class="uavc-list-icon  " data-animation="" data-animation-delay="03" style="margin-right:5px;">
                                                                <div class="ult-just-icon-wrapper  ">
                                                                    <div class="align-icon" style="text-align:center;">
                                                                        <div class="aio-icon none " style="color:#dcb91d;font-size:32px;display:inline-block;">
                                                                            <i class="Defaults-check-circle"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div><span data-ultimate-target='#list-icon-wrap-2334 .uavc-list-desc'
                                                                data-responsive-json-new='{"font-size":"","line-height":""}'
                                                                class="uavc-list-desc ult-responsive" style="">Cách chợ Bến Thành: 3 phút</span></div>
                                                    </li>
                                                    <li>
                                                        <div class="uavc-list-content" id="list-icon-wrap-8320">
                                                            <div class="uavc-list-icon  " data-animation="" data-animation-delay="03" style="margin-right:5px;">
                                                                <div class="ult-just-icon-wrapper  ">
                                                                    <div class="align-icon" style="text-align:center;">
                                                                        <div class="aio-icon none " style="color:#dcb91d;font-size:32px;display:inline-block;">
                                                                            <i class="Defaults-check-circle"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div><span data-ultimate-target='#list-icon-wrap-8320 .uavc-list-desc'
                                                                data-responsive-json-new='{"font-size":"","line-height":""}'
                                                                class="uavc-list-desc ult-responsive" style="">Cách khu đô thị Phú Mỹ Hưng: 7 phút</span></div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-4 vc_col-has-fill">
                                    <div class="vc_column-inner vc_custom_1494667042505">
                                        <div class="wpb_wrapper">
                                            <div class="uavc-list-icon uavc-list-icon-wrapper ult-adjust-bottom-margin   ">
                                                <ul class="uavc-list">
                                                    <li>
                                                        <div class="uavc-list-content" id="list-icon-wrap-8818">
                                                            <div class="uavc-list-icon  " data-animation="" data-animation-delay="03" style="margin-right:5px;">
                                                                <div class="ult-just-icon-wrapper  ">
                                                                    <div class="align-icon" style="text-align:center;">
                                                                        <div class="aio-icon none " style="color:#dcb91d;font-size:32px;display:inline-block;">
                                                                            <i class="Defaults-check-circle"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div><span data-ultimate-target='#list-icon-wrap-8818 .uavc-list-desc'
                                                                data-responsive-json-new='{"font-size":"","line-height":""}'
                                                                class="uavc-list-desc ult-responsive" style="">Cách trung tâm tài chính TP HCM: 3 phút</span></div>
                                                    </li>
                                                    <li>
                                                        <div class="uavc-list-content" id="list-icon-wrap-2523">
                                                            <div class="uavc-list-icon  " data-animation="" data-animation-delay="03" style="margin-right:5px;">
                                                                <div class="ult-just-icon-wrapper  ">
                                                                    <div class="align-icon" style="text-align:center;">
                                                                        <div class="aio-icon none " style="color:#dcb91d;font-size:32px;display:inline-block;">
                                                                            <i class="Defaults-check-circle"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div><span data-ultimate-target='#list-icon-wrap-2523 .uavc-list-desc'
                                                                data-responsive-json-new='{"font-size":"","line-height":""}'
                                                                class="uavc-list-desc ult-responsive" style="">Cách công viên Hồ Khánh Hội : 3 phút</span></div>
                                                    </li>
                                                    <li>
                                                        <div class="uavc-list-content" id="list-icon-wrap-7729">
                                                            <div class="uavc-list-icon  " data-animation="" data-animation-delay="03" style="margin-right:5px;">
                                                                <div class="ult-just-icon-wrapper  ">
                                                                    <div class="align-icon" style="text-align:center;">
                                                                        <div class="aio-icon none " style="color:#dcb91d;font-size:32px;display:inline-block;">
                                                                            <i class="Defaults-check-circle"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div><span data-ultimate-target='#list-icon-wrap-7729 .uavc-list-desc'
                                                                data-responsive-json-new='{"font-size":"","line-height":""}'
                                                                class="uavc-list-desc ult-responsive" style="">Cách khu đô thị Thủ Thiêm: 5 phút</span></div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row-full-width vc_clearfix"></div>
            @php
                $nhamau = \App\Page::find(3);
                $bangia = \App\Page::find(2);
                $video = \App\Page::find(4);
            @endphp
            <!-- Row Backgrounds -->
            <div class="upb_bg_img" data-ultimate-bg="url(http://www.canhothegoldview.org/wp-content/uploads/2015/07/bg-for-sale.jpg)"
                data-image-id="6181|http://www.canhothegoldview.org/wp-content/uploads/2015/07/bg-for-sale.jpg" data-ultimate-bg-style="vcpb-default"
                data-bg-img-repeat="no-repeat" data-bg-img-size="cover" data-bg-img-position="" data-parallx_sense="30" data-bg-override="full"
                data-bg_img_attach="scroll" data-upb-overlay-color="" data-upb-bg-animation="" data-fadeout="" data-bg-animation="left-animation"
                data-bg-animation-type="h" data-animation-repeat="repeat" data-fadeout-percentage="30" data-parallax-content=""
                data-parallax-content-sense="30" data-row-effect-mobile-disable="true" data-img-parallax-mobile-disable="true"
                data-rtl="false" data-custom-vc-row="" data-vc="4.11.2" data-is_old_vc="" data-theme-support="" data-overlay="false"
                data-overlay-color="" data-overlay-pattern="" data-overlay-pattern-opacity="" data-overlay-pattern-size=""></div>
            <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1494667226066 vc_row-has-fill">
                <div class="wpb_column vc_column_container vc_col-sm-4">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="ult-new-ib ult-ib-effect-style13  ult-ib-resp  " data-min-width="768" data-max-width="900" style="background:#eecb2e;"
                                data-opacity="1" data-hover-opacity="1"><img class="ult-new-ib-img" style="opacity:1;" alt="căn hộ mẫu TNR THE GOLDVIEW" src="{{ asset('goldview/mucchucnang/uploads/2017/05/canhomau.jpg') }}"
                                />
                                <div id="interactive-banner-wrap-3220" class="ult-new-ib-desc" style="">
                                    <h2 class="ult-new-ib-title ult-responsive" data-ultimate-target='#interactive-banner-wrap-3220 .ult-new-ib-title' data-responsive-json-new='{"font-size":"desktop:22px;","line-height":""}'
                                        style="font-weight:bold;">CĂN HỘ MẪU</h2>
                                    <div class="ult-new-ib-content ult-responsive" data-ultimate-target='#interactive-banner-wrap-3220 .ult-new-ib-content' data-responsive-json-new='{"font-size":"desktop:18px;","line-height":""}'
                                        style="font-weight:normal;">
                                        <p>1-3 phòng ngủ diện tích 50-132m2</p>
                                    </div>
                                </div><a class="ult-new-ib-link" href="{{route('page.detail', ['slug' => $nhamau->slug])}}"></a></div>
                            <script type="text/javascript">
                                (function($){
            $(document).ready(function(){
                $(".ult-new-ib").css("opacity","1");
            });
        })(jQuery);
                            </script>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-sm-4">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="ult-new-ib ult-ib-effect-style13  ult-ib-resp  " data-min-width="768" data-max-width="900" style="background:#eecb2e;"
                                data-opacity="1" data-hover-opacity="1"><img class="ult-new-ib-img" style="opacity:1;" alt="null" src="{{ asset('goldview/mucchucnang/uploads/2017/05/goldview-1-1.jpg') }}"
                                />
                                <div id="interactive-banner-wrap-7721" class="ult-new-ib-desc" style="">
                                    <h2 class="ult-new-ib-title ult-responsive" data-ultimate-target='#interactive-banner-wrap-7721 .ult-new-ib-title' data-responsive-json-new='{"font-size":"desktop:22px;","line-height":""}'
                                        style="font-weight:bold;">GIÁ &amp; CHÍNH SÁCH</h2>
                                    <div class="ult-new-ib-content ult-responsive" data-ultimate-target='#interactive-banner-wrap-7721 .ult-new-ib-content' data-responsive-json-new='{"font-size":"desktop:18px;","line-height":""}'
                                        style="font-weight:normal;">
                                        <p>cập nhật bảng giá và phương thức thanh toán sớm nhất</p>
                                    </div>
                                </div><a class="ult-new-ib-link" href="{{route('page.detail', ['slug' => $bangia->slug])}}"></a></div>
                            <script type="text/javascript">
                                (function($){ $(document).ready(function(){ $(".ult-new-ib").css("opacity","1"); }); })(jQuery);
                            </script>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-sm-4">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="ult-new-ib ult-ib-effect-style13  ult-ib-resp  " data-min-width="768" data-max-width="900" style="background:#eecb2e;"
                                data-opacity="1" data-hover-opacity="1"><img class="ult-new-ib-img" style="opacity:1;" alt="video dự án TNR THE GOLDVIEW" src="{{ asset('goldview/mucchucnang/uploads/2017/05/video-goldview-1.jpg') }}"
                                />
                                <div id="interactive-banner-wrap-4977" class="ult-new-ib-desc" style="">
                                    <h2 class="ult-new-ib-title ult-responsive" data-ultimate-target='#interactive-banner-wrap-4977 .ult-new-ib-title' data-responsive-json-new='{"font-size":"desktop:22px;","line-height":""}'
                                        style="font-weight:bold;">VIDEO DỰ ÁN</h2>
                                    <div class="ult-new-ib-content ult-responsive" data-ultimate-target='#interactive-banner-wrap-4977 .ult-new-ib-content' data-responsive-json-new='{"font-size":"desktop:18px;","line-height":""}'
                                        style="font-weight:normal;">
                                        <p>Cái nhìn tổng thể dự án qua video </p>
                                    </div>
                                </div><a class="ult-new-ib-link" href="{{route('page.detail', ['slug' => $video->slug])}}"></a></div>
                            <script type="text/javascript">
                                (function($){ $(document).ready(function(){ $(".ult-new-ib").css("opacity","1"); }); })(jQuery);
                            </script>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row-full-width vc_clearfix"></div>
        </div>
    </div>
    <div class="grid-column-3">
        <div class="widget widget_text" id="text-5">
            <div class="textwidget">
                <h3 class="grid-title-y">CĂN HỘ BÁN</h3>
            </div>
        </div>
        @foreach (\App\Project::where('category_id', 1)->orderBy('id', 'desc')->skip(0)->take(6)->get() as $project)
        <div class="query_box bangoldview item" itemscope itemtype="http://schema.org/Article">
            <div class="thuoctinh">
                <div class="item grid wow fadeInDown">
                    <div class="grid-icon">
                        <a href="{{route('project.detail', ['category_slug' => $project->category->slug, 'slug' => $project->slug])}}">
                            <img width="370" height="235" src="{{ url($project->images()->first()->imageUrl) }}" class="attachment-thumbnail size-thumbnail wp-post-image" alt="" /></a>
                        <span class="grid-price">{{$project->price_text}}</span>
                        <span class="grid-category">{{$project->category->title}}</span>
                    </div>
                    <div class="grid-bottom">
                        <h2 class="headline"><a href="{{route('project.detail', ['category_slug' => $project->category->slug, 'slug' => $project->slug])}}">
                            <span class="title">{{$project->title}}</span></a></h2>
                        <span class="grid-sum f13">{{str_limit($project->description, 120)}}</span>
                        <table class="grid-features">
                            <tr>
                                <th>Diện tích</th>
                                <th>Garage</th>
                                <th>Phòng ngủ</th>
                                <th>Toilet</th>
                            </tr>
                            <tr>
                                <td><img src="{{ asset('goldview/tnr/img/i1.png') }}" />{{$project->acreages}} m<sup>2</sup></td>
                                <td><img src="{{ asset('goldview/tnr/img/i2.png') }}" />{{$project->garages}} chỗ</td>
                                <td><img src="{{ asset('goldview/tnr/img/i3.png') }}" />{{$project->bed_rooms}} PN</td>
                                <td><img src="{{ asset('goldview/tnr/img/i4.png') }}" />{{$project->toilets}} WC</td>
                            </tr>
                        </table>
                    </div>
                    <a class="btn-read-more" href="{{route('project.detail', ['category_slug' => $project->category->slug, 'slug' => $project->slug])}}">Xem chi tiết...</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="grid-column-3">
        <div class="widget widget_text" id="text-6">
            <div class="textwidget">
                <h3 class="grid-title-b mt50">CĂN HỘ CHO THUÊ</h3>
            </div>
        </div>
        @foreach (\App\Project::where('category_id', 2)->orderBy('id', 'desc')->skip(0)->take(6)->get() as $project)
        <div class="query_box bangoldview item" itemscope itemtype="http://schema.org/Article">
            <div class="thuoctinh">
                <div class="item grid wow fadeInDown">
                    <div class="grid-icon">
                        <a href="{{route('project.detail', ['category_slug' => $project->category->slug, 'slug' => $project->slug])}}">
                            <img width="370" height="235" src="{{ url($project->images()->first()->imageUrl) }}" class="attachment-thumbnail size-thumbnail wp-post-image" alt="" /></a>
                        <span class="grid-price">{{$project->price_text}}</span>
                        <span class="grid-category">{{$project->category->title}}</span>
                    </div>
                    <div class="grid-bottom">
                        <h2 class="headline"><a href="{{route('project.detail', ['category_slug' => $project->category->slug, 'slug' => $project->slug])}}">
                            <span class="title">{{$project->title}}</span></a></h2>
                        <span class="grid-sum f13">{{str_limit($project->description, 120)}}</span>
                        <table class="grid-features">
                            <tr>
                                <th>Diện tích</th>
                                <th>Garage</th>
                                <th>Phòng ngủ</th>
                                <th>Toilet</th>
                            </tr>
                            <tr>
                                <td><img src="{{ asset('goldview/tnr/img/i1.png') }}" />{{$project->acreages}} m<sup>2</sup></td>
                                <td><img src="{{ asset('goldview/tnr/img/i2.png') }}" />{{$project->garages}} chỗ</td>
                                <td><img src="{{ asset('goldview/tnr/img/i3.png') }}" />{{$project->bed_rooms}} PN</td>
                                <td><img src="{{ asset('goldview/tnr/img/i4.png') }}" />{{$project->toilets}} WC</td>
                            </tr>
                        </table>
                    </div>
                    <a class="btn-read-more" href="{{route('project.detail', ['category_slug' => $project->category->slug, 'slug' => $project->slug])}}">Xem chi tiết...</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="list-column-2">
        <div class="widget widget_text" id="text-7">
            <div class="textwidget">
                <h3 class="tc mt50 ti3"><a href="{{route('news')}}" title="tin tuc cho thue TNR THE GOLDVIEW">TIN TỨC BẤT ĐỘNG SẢN</a></h3>
            </div>
        </div>
        @foreach (\App\Post::where('category_id', 1)->orderBy('id', 'desc')->skip(0)->take(4)->get() as $post)
        <div class="query_box tintuc item wow bounceInUp" itemscope itemtype="http://schema.org/Article">
            <div>
                <div class="item list">
                    <div class="list-icon">
                        <img width="370" height="235" src="{{ url($post->imageUrl) }}" class="attachment-thumbnail size-thumbnail wp-post-image"
                            alt="" /> </div>
                    <div class="list-bottom">
                        <h3 class="headline"><a href="{{route('news.detail', ['slug' => $post->slug])}}" title="{{ $post->title }}"><span class="item-list-title">{{ $post->title }}</span></a></h3>
                        <div class="list-item-sum f13">{{str_limit($post->description, 100)}}</div>
                    </div>
                    <a class="btn-read-single" href="{{route('news.detail', ['slug' => $post->slug])}}"><img src="{{ asset('goldview/tnr/img/rm.png') }}" />Xem chi tiết...</a>
                </div>

            </div>
        </div>
        @endforeach

    </div>
</div>
@endsection
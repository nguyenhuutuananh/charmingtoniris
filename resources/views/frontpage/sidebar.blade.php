<div class="sidebar">
    <div class="formchung">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.12.4.js"></script>
        <script src="//code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
        <script>
            jQuery(function () {
                        jQuery("#tabs").tabs();
                    });
        </script>
        <div id="tabs">
            <ul>
                <li>
                    <a href="#tabs-1">Bán</a>
                </li>
                <li>
                    <a href="#tabs-2">Cho thuê</a>
                </li>
            </ul>
            <div id="tabs-1">
                <div id="uwpqsf_id">
                    <form id="uwpqsffrom_118" method="get" action="{{route('project.search')}}">
                        <div class="uform_title">TÌM KIẾM CĂN HỘ BÁN</div>
                        <div class="uwpqsf_class " id="cmf-select0">
                            <span class="cmflabel-0">Phòng ngủ</span>
                            <select id="phong-ngu" class="cmfdp-class-0" name="phong-ngu">
                                <option value=""> </option>
                                <option value="1">1 </option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                {{-- <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option> --}}
                            </select>
                        </div>
                        <div class="uwpqsf_class " id="cmf-select1">
                            <span class="cmflabel-1">Giá bán</span>
                            <select id="gia-ban" class="cmfdp-class-1" name="gia-ban">
                                <option value=""> </option>
                                <option value="dưới 2 tỷ">dưới 2 tỷ </option>
                                <option value="2 - 3 tỷ">2 - 3 tỷ </option>
                                <option value="3 - 4 tỷ">3 - 4 tỷ </option>
                                <option value="4 - 5 tỷ">4 - 5 tỷ </option>
                                <option value="trên 5 tỷ">trên 5 tỷ</option>
                            </select>
                        </div>
                        <div class="uwpqsf_class " id="cmf-select2">
                            <span class="cmflabel-2">Nội thất</span>
                            <select id="noi-that" class="cmfdp-class-2" name="noi-that">
                                <option value=""> </option>
                                @foreach (\App\Furniture::all() as $furniture)
                                <option value="{{$furniture->id}}">{{$furniture->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="uwpqsf_class  uwpqsf_submit" id="uwpqsf_btn">
                            <input type="submit" id="uwpqsf_id_btn" value="Tìm kiếm" alt="[Submit]" class="usfbtn " />
                        </div>
                        <div style="clear:both"></div>
                    </form>
                </div>
            </div>
            <div id="tabs-2">
                <div id="uwpqsf_id">
                    <form id="uwpqsffrom_423" method="get" action="{{route('project.search')}}">
                        <div class="uform_title">TÌM KIẾM CĂN HỘ THUÊ</div>
                        <div class="uwpqsf_class " id="cmf-select0">
                            <span class="cmflabel-0">Phòng ngủ</span>
                            <select id="phong-ngu" class="cmfdp-class-0" name="phong-ngu">
                                <option value=""> </option>
                                <option value="1">1 </option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                {{-- <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option> --}}
                            </select>
                        </div>
                        <div class="uwpqsf_class " id="cmf-select1">
                            <span class="cmflabel-1">Giá thuê</span>
                            <select id="gia-thue" class="cmfdp-class-1" name="gia-thue">
                                <option value=""> </option>
                                <option value="Dưới 20 triệu">Dưới 20 triệu</option>
                                <option value="20 - 30 triệu">20 - 30 triệu </option>
                                <option value="30 - 40 triệu">30 - 40 triệu </option>
                                <option value="Trên 50 triệu">Trên 50 triệu</option>
                            </select>
                        </div>
                        <div class="uwpqsf_class " id="cmf-select2">
                            <span class="cmflabel-2">Nội thất</span>
                            <select id="noi-that" class="cmfdp-class-2" name="noi-that">
                                <option value=""> </option>
                                @foreach (\App\Furniture::all() as $furniture)
                                <option value="{{$furniture->id}}">{{$furniture->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="uwpqsf_class  uwpqsf_submit" id="uwpqsf_btn">
                                <input type="submit" id="uwpqsf_id_btn" value="Tìm kiếm" alt="[Submit]" class="usfbtn " />
                        </div>
                        <div style="clear:both"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="widget widget_text tieudetintuc" id="text-12">
        <div class="textwidget">TIN TỨC</div>
    </div>
    @foreach (\App\Post::orderBy('id', 'desc')->skip(0)->take(5)->get() as $post)
    <div class="query_box hoptin" itemscope itemtype="http://schema.org/Article">
        <a class="featured_image_link" href="{{route('news.detail', ['slug' => $post->slug])}}">
                    <img width="115" height="73" src="{{asset($post->imageUrl)}}" class="alignleft wp-post-image" alt=""
                        itemprop="image" />
                </a>
        <h3 class="headline tieudetin" itemprop="headline">
            <a href="{{route('news.detail', ['slug' => $post->slug])}}" rel="bookmark">{{$post->title}}</a>
        </h3>
    </div>
    @endforeach
    <div class="widget widget_text tieudetintuc" id="text-13">
        <div class="textwidget">ĐÁNH GIÁ</div>
    </div>
    @foreach (\App\Post::where('category_id', 2)->orderBy('id', 'desc')->skip(0)->take(4)->get() as $post)
    <div class="query_box tieudedanhgia" itemscope itemtype="http://schema.org/Article">
        <h3 class="headline danhgia" itemprop="headline">
            <a href="{{route('danh-gia.detail', ['slug' => $post->slug])}}" rel="bookmark">{{$post->title}}</a>
        </h3>
    </div>
    @endforeach
</div>
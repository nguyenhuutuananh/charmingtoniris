<div class="lienhe">
    {{__c('sale_contact')}}
    <br> Địa chỉ dự án: {{__c('address')}}
    <br>
    <div class="ht1">Hotline:
        <span class="blue">{{__c('hotline')}}</span>
    </div>
    <div class="ht2">Email: {{__c('email')}}</div>
    <div class="clear"></div>
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <div class='gf_browser_gecko gform_wrapper' id='gform_wrapper_2'>
        <form method='post' enctype='multipart/form-data' id='gform_2' action='#'>
            @csrf
            @if (isset($project_id))
            <input type="hidden" name="project_id" value="{{$project_id}}">
            @endif
            <div class='gform_heading'>
                <span class='gform_description'></span>
            </div>
            <div class='gform_body'>
                <ul id='gform_fields_2' class='gform_fields top_label description_below'>
                    <li id='field_2_1' class='gfield    gf_left_half           gfield_contains_required'>
                        <label class='gfield_label' for='input_2_1'>Họ tên
                                    <span class='gfield_required'>*</span>
                                </label>
                        <div class='ginput_container'>
                            <input name='name' id='input_2_1' type='text' value='' class='medium' tabindex='1' />
                        </div>
                    </li>
                    <li id='field_2_2' class='gfield    gf_right_half'>
                        <label class='gfield_label' for='input_2_2'>Địa chỉ</label>
                        <div class='ginput_container'>
                            <input name='address' id='input_2_2' type='text' value='' class='medium' tabindex='2' />
                        </div>
                    </li>
                    <li id='field_2_6' class='gfield    gf_left_half           gfield_contains_required'>
                        <label class='gfield_label' for='input_2_6'>Điện thoại
                                    <span class='gfield_required'>*</span>
                                </label>
                        <div class='ginput_container'>
                            <input name='phone_number' id='input_2_6' type='text' value='' class='medium' tabindex='3' />
                        </div>
                    </li>
                    <li id='field_2_7' class='gfield    gf_right_half'>
                        <label class='gfield_label' for='input_2_7'>Email</label>
                        <div class='ginput_container'>
                            <input name='email' id='input_2_7' type='text' value='' class='medium' tabindex='4' />
                        </div>
                    </li>
                    <li id='field_2_5' class='gfield'>
                        <label class='gfield_label' for='input_2_5'>Nội dung</label>
                        <div class='ginput_container'>
                            <textarea name='messages' id='input_2_5' class='textarea small' tabindex='5' rows='10' cols='50'></textarea>
                        </div>
                    </li>
                    <li id='field_2_8' class='gfield'>
                        <label class='gfield_label' for='input_2_8'>Captcha</label>
                        <div class="ginput_container">
                            <div class="g-recaptcha" data-sitekey="6Lfv11QUAAAAAPtDwN3dwqv_xgScxFbUfL3PcSDp" data-theme="light"></div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class='gform_footer top_label'>
                <input type='button' id='gform_submit_button_2' class='gform_button button' value='Gửi yêu cầu' tabindex='6' />

            </div>
        </form>
    </div>
    <script type='text/javascript'>
        jQuery(document).ready(function () {
                    $('#gform_submit_button_2').on('click', function(e) {
                        if(window["gf_submitting_2"]){return false;}
                        var response = grecaptcha.getResponse();
                        if(response.length == 0){
                            alert('Vui lòng xác thực captcha!');
                            return;
                        }
                        window["gf_submitting_2"]=true;
                        const form = $(this).closest('form');
                        $.ajax({
                            url: "{{route('contact.save')}}",
                            method: "POST",
                            data: $(form).serialize(),  
                            dataType: 'json'
                        })
                        .done(function(data) {
                            if(data.result) {
                                $(form).find('input[name!="_token"][type!="button"]').val('');
                                $(form).find('textarea').val('');
                                grecaptcha.reset();
                            }
                            else {
                                grecaptcha.reset();
                            }
                            alert(data.message);
                        })
                        .always(function() {
                            window["gf_submitting_2"]=false; 
                        });
                        
                    });
                });
    </script>
    <div class="clear"></div>
</div>
<!DOCTYPE html>
<html dir="ltr" lang="vi">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" type="text/css" href="{{ asset('goldview/mucchucnang/thesis/skins/classic-r/css.css') }}" />
    <title>@hasSection('title'){{trim(View::yieldContent('title'))}} - @endif {{__c('web_title')}}</title>
    <meta name="description" content="TNR THE GOLDVIEW (Gold River View) tại Quận 4 giao nhà tháng 10/2017. ✅ Diện tích từ 50m2 &#8211; 133m2. ✅ Giá từ 2,1 tỷ ✅ Tặng xe 1.7 tỷ ☎ LH: {{ __c('hotline') }}"
    />
    <meta name="robots" content="noodp, noydir" />
    <link rel="canonical" href="{{ url('/') }}" />
    <meta name="google-site-verification" content="FhfXbUDC8zR99PYQx5C9IrAm2r53srdqRKD66CxlMnQ" />
    <link rel="pingback" href="xmlrpc.html" />
    <link rel='dns-prefetch' href='http://s.w.org/' />

    <meta property="og:title" content="Trang chủ" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="{{ url('/') }}" />
    <meta property="og:description" content="" />
    <meta property="og:site_name" content="Bán &amp; Cho thuê căn hộ TNR THE GOLDVIEW" />
    <meta property="fb:admins" content="100008127361030" />
    <meta property="fb:app_id" content="825601000821629" />

    <meta property="og:title" content="Mở bán đợt cuối căn hộ The GoldView giá tốt nhất LH: 0909040965" />
    <meta property="og:image" content="{{ asset('goldview/mucchucnang/uploads/2017/05/thegoldview.jpg') }}" />
    <meta property="og:site_name" content="Bán &amp; Cho thuê căn hộ TNR THE GOLDVIEW" />
    <meta property="fb:admins" content="100008127361030" />
    <meta property="fb:app_id" content="825601000821629" />
    <script type="text/javascript">
        window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/canhothegoldview.com\/baogom\/js\/wp-emoji-release.min.js?ver=4.7.10"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='rs-plugin-settings-css' href="{{ asset('goldview/chucnang/revslider/rs-plugin/css/settings.css') }}" type='text/css' media='all'
    />
    <style id='rs-plugin-settings-inline-css' type='text/css'>
        .tp-caption a {
            color: #ff7302;
            text-shadow: none;
            -webkit-transition: all 0.2s ease-out;
            -moz-transition: all 0.2s ease-out;
            -o-transition: all 0.2s ease-out;
            -ms-transition: all 0.2s ease-out
        }

        .tp-caption a:hover {
            color: #ffa902
        }
    </style>
    <link rel='stylesheet' id='udefault-css' href="{{ asset('goldview/chucnang/ultimate-wp-query-search-filter/themes/default.css') }}" type='text/css'
        media='all' />
    <link rel='stylesheet' id='wp-pagenavi-css' href="{{ asset('goldview/chucnang/wp-pagenavi/pagenavi-css.css') }}" type='text/css' media='all' />
    <link rel='stylesheet' id='newsletter-subscription-css' href="{{ asset('goldview/chucnang/newsletter/subscription/style.css') }}" type='text/css'
        media='all' />
    <link rel='stylesheet' id='js_composer_front-css' href="{{ asset('goldview/chucnang/js_composer/assets/css/js_composer.min.css') }}" type='text/css'
        media='all' />
    <link rel='stylesheet' id='bsf-Defaults-css' href='{{ asset('goldview/mucchucnang/uploads/smile_fonts/Defaults/Defaults.css') }}' type='text/css'
        media='all' />
    <link rel='stylesheet' id='ultimate-style-min-css' href="{{ asset('goldview/chucnang/Ultimate_VC_Addons/assets/min-css/ultimate.min.css') }}" type='text/css'
        media='all' />
    <link rel='stylesheet' id='colorbox-css' href="{{ asset('goldview/chucnang/slideshow-gallery/views/default/css/colorbox.css') }}" type='text/css'
        media='all' />
    <link rel='stylesheet' id='fontawesome-css' href="{{ asset('goldview/chucnang/slideshow-gallery/views/default/css/fontawesome.css') }}" type='text/css'
        media='all' />
    </div>

    <link rel='stylesheet' id='addthis_output-css'  href='{{asset("goldview/chucnang/addthis/css/output.css")}}' type='text/css' media='all' />
    <link rel='stylesheet' id='gforms_reset_css-css'  href='{{asset("goldview/chucnang/gravityforms/css/formreset.css")}}' type='text/css' media='all' />
    <link rel='stylesheet' id='gforms_formsmain_css-css'  href='{{asset("goldview/chucnang/gravityforms/css/formsmain.css")}}' type='text/css' media='all' />
    <link rel='stylesheet' id='gforms_ready_class_css-css'  href='{{asset("goldview/chucnang/gravityforms/css/readyclass.css")}}' type='text/css' media='all' />
    <link rel='stylesheet' id='gforms_browsers_css-css'  href='{{asset("goldview/chucnang/gravityforms/css/browsers.css")}}' type='text/css' media='all' />

    <script type='text/javascript' src=" {{ asset('goldview/baogom/js/jquery/jqueryb8ff.js?ver=1.12.4') }}"></script>
    <script type='text/javascript' src=" {{ asset('goldview/baogom/js/jquery/jquery-migrate.min330a.js?ver=1.4.1') }}"></script>
    <script type='text/javascript' src=" {{ asset('goldview/chucnang/revslider/rs-plugin/js/jquery.themepunch.tools.min9cbc.js?ver=4.6.93') }}"></script>
    <script type='text/javascript' src=" {{ asset('goldview/chucnang/revslider/rs-plugin/js/jquery.themepunch.revolution.min9cbc.js?ver=4.6.93') }}"></script>
    <script type='text/javascript' src=" {{ asset('goldview/baogom/js/jquery/ui/core.mine899.js?ver=1.11.4') }}"></script>
    <script type='text/javascript' src=" {{ asset('goldview/chucnang/Ultimate_VC_Addons/assets/min-js/ultimate.minbcd2.js?ver=3.16.7') }}"></script>
    <script type='text/javascript' src=" {{ asset('goldview/chucnang/slideshow-gallery/views/default/js/gallery5152.js?ver=1.0') }}"></script>
    <script type='text/javascript' src=" {{ asset('goldview/chucnang/slideshow-gallery/views/default/js/colorbox905d.js?ver=1.6.3') }}"></script>
    <script type='text/javascript' src=" {{ asset('goldview/chucnang/slideshow-gallery/views/default/js/jquery-uid714.js?ver=4.7.10') }}"></script>
    <link rel="alternate" type="application/json+oembed" href="wp-json/oembed/1.0/embed8a8f.json?url=http%3A%2F%2Fcanhothegoldview.com%2F"
    />
    <link rel="alternate" type="text/xml+oembed" href="wp-json/oembed/1.0/embedf3ec?url=http%3A%2F%2Fcanhothegoldview.com%2F&amp;format=xml"
    />
    <script type="text/javascript">
        jQuery(document).ready(function() {
				// CUSTOM AJAX CONTENT LOADING FUNCTION
				var ajaxRevslider = function(obj) {
				
					// obj.type : Post Type
					// obj.id : ID of Content to Load
					// obj.aspectratio : The Aspect Ratio of the Container / Media
					// obj.selector : The Container Selector where the Content of Ajax will be injected. It is done via the Essential Grid on Return of Content
					
					var content = "";

					data = {};
					
					data.action = 'revslider_ajax_call_front';
					data.client_action = 'get_slider_html';
					data.token = '20b0062304';
					data.type = obj.type;
					data.id = obj.id;
					data.aspectratio = obj.aspectratio;
					
					// SYNC AJAX REQUEST
					jQuery.ajax({
						type:"post",
						url:"http://canhothegoldview.com/dangnhapweb/admin-ajax.php",
						dataType: 'json',
						data:data,
						async:false,
						success: function(ret, textStatus, XMLHttpRequest) {
							if(ret.success == true)
								content = ret.data;								
						},
						error: function(e) {
							console.log(e);
						}
					});
					
					 // FIRST RETURN THE CONTENT WHEN IT IS LOADED !!
					 return content;						 
				};
				
				// CUSTOM AJAX FUNCTION TO REMOVE THE SLIDER
				var ajaxRemoveRevslider = function(obj) {
					return jQuery(obj.selector+" .rev_slider").revkill();
				};

				// EXTEND THE AJAX CONTENT LOADING TYPES WITH TYPE AND FUNCTION
				var extendessential = setInterval(function() {
					if (jQuery.fn.tpessential != undefined) {
						clearInterval(extendessential);
						if(typeof(jQuery.fn.tpessential.defaults) !== 'undefined') {
							jQuery.fn.tpessential.defaults.ajaxTypes.push({type:"revslider",func:ajaxRevslider,killfunc:ajaxRemoveRevslider,openAnimationSpeed:0.3});   
							// type:  Name of the Post to load via Ajax into the Essential Grid Ajax Container
							// func: the Function Name which is Called once the Item with the Post Type has been clicked
							// killfunc: function to kill in case the Ajax Window going to be removed (before Remove function !
							// openAnimationSpeed: how quick the Ajax Content window should be animated (default is 0.3)
						}
					}
				},30);
			});
    </script>
    <script type="text/javascript">
        window._wp_rp_static_base_url = 'https://wprp.zemanta.com/static/';
	window._wp_rp_wp_ajax_url = "dangnhapweb/admin-ajax.html";
	window._wp_rp_plugin_version = '3.6.3';
	window._wp_rp_post_id = '771';
	window._wp_rp_num_rel_posts = '6';
	window._wp_rp_thumbnails = true;
	window._wp_rp_post_title = 'Trang+ch%E1%BB%A7';
	window._wp_rp_post_tags = [];
	window._wp_rp_promoted_content = true;

    </script>
    <link rel="stylesheet" href="{{ asset('goldview/chucnang/wordpress-23-related-posts-plugin/static/themes/twocolumnseb93.css?version=3.6.3') }}" />

    <link href='https://fonts.googleapis.com/css?family=Roboto&amp;subset=latin,vietnamese' rel='stylesheet' type='text/css'>

    <!--<script type="text/javascript"  src="/js/site.js"></script>-->

    <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="http://canhothegoldview.com/chucnang/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]-->
    <!--[if IE  8]><link rel="stylesheet" type="text/css" href="http://canhothegoldview.com/chucnang/js_composer/assets/css/vc-ie8.min.css" media="screen"><![endif]-->
    <style>
        button#responsive-menu-button,
        #responsive-menu-container {
            display: none;
            -webkit-text-size-adjust: 100%;
        }

        @media screen and (max-width: 1024px) {

            #responsive-menu-container {
                display: block;
            }

            #responsive-menu-container {
                position: fixed;
                top: 0;
                bottom: 0;
                z-index: 99998;
                padding-bottom: 5px;
                margin-bottom: -5px;
                outline: 1px solid transparent;
                overflow-y: auto;
                overflow-x: hidden;
            }

            #responsive-menu-container .responsive-menu-search-box {
                width: 100%;
                padding: 0 2%;
                border-radius: 2px;
                height: 50px;
                -webkit-appearance: none;
            }

            #responsive-menu-container.push-left,
            #responsive-menu-container.slide-left {
                transform: translateX(-100%);
                -ms-transform: translateX(-100%);
                -webkit-transform: translateX(-100%);
                -moz-transform: translateX(-100%);
            }

            .responsive-menu-open #responsive-menu-container.push-left,
            .responsive-menu-open #responsive-menu-container.slide-left {
                transform: translateX(0);
                -ms-transform: translateX(0);
                -webkit-transform: translateX(0);
                -moz-transform: translateX(0);
            }

            #responsive-menu-container.push-top,
            #responsive-menu-container.slide-top {
                transform: translateY(-100%);
                -ms-transform: translateY(-100%);
                -webkit-transform: translateY(-100%);
                -moz-transform: translateY(-100%);
            }

            .responsive-menu-open #responsive-menu-container.push-top,
            .responsive-menu-open #responsive-menu-container.slide-top {
                transform: translateY(0);
                -ms-transform: translateY(0);
                -webkit-transform: translateY(0);
                -moz-transform: translateY(0);
            }

            #responsive-menu-container.push-right,
            #responsive-menu-container.slide-right {
                transform: translateX(100%);
                -ms-transform: translateX(100%);
                -webkit-transform: translateX(100%);
                -moz-transform: translateX(100%);
            }

            .responsive-menu-open #responsive-menu-container.push-right,
            .responsive-menu-open #responsive-menu-container.slide-right {
                transform: translateX(0);
                -ms-transform: translateX(0);
                -webkit-transform: translateX(0);
                -moz-transform: translateX(0);
            }

            #responsive-menu-container.push-bottom,
            #responsive-menu-container.slide-bottom {
                transform: translateY(100%);
                -ms-transform: translateY(100%);
                -webkit-transform: translateY(100%);
                -moz-transform: translateY(100%);
            }

            .responsive-menu-open #responsive-menu-container.push-bottom,
            .responsive-menu-open #responsive-menu-container.slide-bottom {
                transform: translateY(0);
                -ms-transform: translateY(0);
                -webkit-transform: translateY(0);
                -moz-transform: translateY(0);
            }

            #responsive-menu-container,
            #responsive-menu-container:before,
            #responsive-menu-container:after,
            #responsive-menu-container *,
            #responsive-menu-container *:before,
            #responsive-menu-container *:after {
                box-sizing: border-box;
                margin: 0;
                padding: 0;
            }

            #responsive-menu-container #responsive-menu-search-box,
            #responsive-menu-container #responsive-menu-additional-content,
            #responsive-menu-container #responsive-menu-title {
                padding: 25px 5%;
            }

            #responsive-menu-container #responsive-menu,
            #responsive-menu-container #responsive-menu ul {
                width: 100%;
            }
            #responsive-menu-container #responsive-menu ul.responsive-menu-submenu {
                display: none;
            }

            #responsive-menu-container #responsive-menu ul.responsive-menu-submenu.responsive-menu-submenu-open {
                display: block;
            }

            #responsive-menu-container #responsive-menu ul.responsive-menu-submenu-depth-1 a.responsive-menu-item-link {
                padding-left: 10%;
            }

            #responsive-menu-container #responsive-menu ul.responsive-menu-submenu-depth-2 a.responsive-menu-item-link {
                padding-left: 15%;
            }

            #responsive-menu-container #responsive-menu ul.responsive-menu-submenu-depth-3 a.responsive-menu-item-link {
                padding-left: 20%;
            }

            #responsive-menu-container #responsive-menu ul.responsive-menu-submenu-depth-4 a.responsive-menu-item-link {
                padding-left: 25%;
            }

            #responsive-menu-container #responsive-menu ul.responsive-menu-submenu-depth-5 a.responsive-menu-item-link {
                padding-left: 30%;
            }

            #responsive-menu-container #responsive-menu ul.responsive-menu-submenu-depth-6 a.responsive-menu-item-link {
                padding-left: 35%;
            }

            #responsive-menu-container li.responsive-menu-item {
                width: 100%;
                list-style: none;
            }

            #responsive-menu-container li.responsive-menu-item a {
                width: 100%;
                display: block;
                text-decoration: none;
                padding: 0 5%;
                position: relative;
            }

            #responsive-menu-container li.responsive-menu-item a .fa {
                margin-right: 15px;
            }

            #responsive-menu-container li.responsive-menu-item a .responsive-menu-subarrow {
                position: absolute;
                top: 0;
                bottom: 0;
                text-align: center;
                overflow: hidden;
            }

            #responsive-menu-container li.responsive-menu-item a .responsive-menu-subarrow .fa {
                margin-right: 0;
            }

            button#responsive-menu-button .responsive-menu-button-icon-inactive {
                display: none;
            }

            button#responsive-menu-button {
                z-index: 99999;
                display: none;
                overflow: hidden;
                outline: none;
            }

            button#responsive-menu-button img {
                max-width: 100%;
            }

            .responsive-menu-label {
                display: inline-block;
                font-weight: 600;
                margin: 0 5px;
                vertical-align: middle;
            }

            .responsive-menu-accessible {
                display: inline-block;
            }

            .responsive-menu-accessible .responsive-menu-box {
                display: inline-block;
                vertical-align: middle;
            }

            .responsive-menu-label.responsive-menu-label-top,
            .responsive-menu-label.responsive-menu-label-bottom {
                display: block;
                margin: 0 auto;
            }

            button#responsive-menu-button {
                padding: 0 0;
                display: inline-block;
                cursor: pointer;
                transition-property: opacity, filter;
                transition-duration: 0.15s;
                transition-timing-function: linear;
                font: inherit;
                color: inherit;
                text-transform: none;
                background-color: transparent;
                border: 0;
                margin: 0;
                overflow: visible;
            }

            .responsive-menu-box {
                width: 25px;
                height: 19px;
                display: inline-block;
                position: relative;
            }

            .responsive-menu-inner {
                display: block;
                top: 50%;
                margin-top: -1.5px;
            }

            .responsive-menu-inner,
            .responsive-menu-inner::before,
            .responsive-menu-inner::after {
                width: 25px;
                height: 3px;
                background-color: #fff;
                border-radius: 4px;
                position: absolute;
                transition-property: transform;
                transition-duration: 0.15s;
                transition-timing-function: ease;
            }

            .responsive-menu-open .responsive-menu-inner,
            .responsive-menu-open .responsive-menu-inner::before,
            .responsive-menu-open .responsive-menu-inner::after {
                background-color: #fff;
            }

            button#responsive-menu-button:hover .responsive-menu-inner,
            button#responsive-menu-button:hover .responsive-menu-inner::before,
            button#responsive-menu-button:hover .responsive-menu-inner::after,
            button#responsive-menu-button:hover .responsive-menu-open .responsive-menu-inner,
            button#responsive-menu-button:hover .responsive-menu-open .responsive-menu-inner::before,
            button#responsive-menu-button:hover .responsive-menu-open .responsive-menu-inner::after,
            button#responsive-menu-button:focus .responsive-menu-inner,
            button#responsive-menu-button:focus .responsive-menu-inner::before,
            button#responsive-menu-button:focus .responsive-menu-inner::after,
            button#responsive-menu-button:focus .responsive-menu-open .responsive-menu-inner,
            button#responsive-menu-button:focus .responsive-menu-open .responsive-menu-inner::before,
            button#responsive-menu-button:focus .responsive-menu-open .responsive-menu-inner::after {
                background-color: #fff;
            }

            .responsive-menu-inner::before,
            .responsive-menu-inner::after {
                content: "";
                display: block;
            }

            .responsive-menu-inner::before {
                top: -8px;
            }

            .responsive-menu-inner::after {
                bottom: -8px;
            }

            .responsive-menu-boring .responsive-menu-inner,
            .responsive-menu-boring .responsive-menu-inner::before,
            .responsive-menu-boring .responsive-menu-inner::after {
                transition-property: none;
            }

            .responsive-menu-boring.is-active .responsive-menu-inner {
                transform: rotate(45deg);
            }

            .responsive-menu-boring.is-active .responsive-menu-inner::before {
                top: 0;
                opacity: 0;
            }

            .responsive-menu-boring.is-active .responsive-menu-inner::after {
                bottom: 0;
                transform: rotate(-90deg);
            }
            button#responsive-menu-button {
                width: 45px;
                height: 45px;
                position: fixed;
                top: 90px;
                right: 5%;
                background: #000000
            }

            .responsive-menu-open button#responsive-menu-button {
                background: #000000
            }

            .responsive-menu-open button#responsive-menu-button:hover,
            .responsive-menu-open button#responsive-menu-button:focus,
            button#responsive-menu-button:hover,
            button#responsive-menu-button:focus {
                background: #dcb91d
            }

            button#responsive-menu-button .responsive-menu-box {
                color: #fff;
            }

            .responsive-menu-open button#responsive-menu-button .responsive-menu-box {
                color: #fff;
            }

            .responsive-menu-label {
                color: #fff;
                font-size: 14px;
                line-height: 13px;
            }

            button#responsive-menu-button {
                display: inline-block;
                transition: transform 0.5s, background-color 0.5s;
            }



            #responsive-menu-container {
                width: 50%;
                left: 0;
                transition: transform 0.5s;
                text-align: left;
                max-width: 260px;
                background: #dcb91d;
            }

            #responsive-menu-container #responsive-menu-wrapper {
                background: #dcb91d;
            }

            #responsive-menu-container #responsive-menu-additional-content {
                color: #fff;
            }

            #responsive-menu-container .responsive-menu-search-box {
                background: #fff;
                border: 2px solid #dadada;
                color: #333;
            }

            #responsive-menu-container .responsive-menu-search-box:-ms-input-placeholder {
                color: #C7C7CD;
            }

            #responsive-menu-container .responsive-menu-search-box::-webkit-input-placeholder {
                color: #C7C7CD;
            }

            #responsive-menu-container .responsive-menu-search-box:-moz-placeholder {
                color: #C7C7CD;
                opacity: 1;
            }

            #responsive-menu-container .responsive-menu-search-box::-moz-placeholder {
                color: #C7C7CD;
                opacity: 1;
            }

            #responsive-menu-container .responsive-menu-item-link,
            #responsive-menu-container #responsive-menu-title,
            #responsive-menu-container .responsive-menu-subarrow {
                transition: background-color 0.5s, border-color 0.5s, color 0.5s;
            }

            #responsive-menu-container #responsive-menu-title {
                background-color: #dcb91d;
                color: #fff;
                font-size: 14px;
            }

            #responsive-menu-container #responsive-menu-title a {
                color: #fff;
                font-size: 14px;
                text-decoration: none;
            }

            #responsive-menu-container #responsive-menu-title a:hover {
                color: #fff;
            }

            #responsive-menu-container #responsive-menu-title:hover {
                background-color: #dcb91d;
                color: #fff;
            }

            #responsive-menu-container #responsive-menu-title:hover a {
                color: #fff;
            }

            #responsive-menu-container #responsive-menu-title #responsive-menu-title-image {
                display: inline-block;
                vertical-align: middle;
                margin-right: 15px;
            }

            #responsive-menu-container #responsive-menu>li.responsive-menu-item:first-child>a {
                border-top: 1px solid #dcb91d;
            }

            #responsive-menu-container #responsive-menu li.responsive-menu-item .responsive-menu-item-link {
                font-size: 15px;
            }

            #responsive-menu-container #responsive-menu li.responsive-menu-item a {
                line-height: 40px;
                border-bottom: 1px solid #dcb91d;
                color: #fff;
                background-color: #dcb91d;
            }

            #responsive-menu-container #responsive-menu li.responsive-menu-item a:hover {
                color: #fff;
                background-color: #001a4b;
                border-color: #dcb91d;
            }

            #responsive-menu-container #responsive-menu li.responsive-menu-item a:hover .responsive-menu-subarrow {
                color: #fff;
                border-color: #fff;
                background-color: #001a4b;
            }

            #responsive-menu-container #responsive-menu li.responsive-menu-item a .responsive-menu-subarrow {
                right: 0;
                height: 40px;
                line-height: 40px;
                width: 40px;
                color: #fff;
                border-left: 1px solid #dcb91d;
                background-color: #dcb91d;
            }

            #responsive-menu-container #responsive-menu li.responsive-menu-item a .responsive-menu-subarrow.responsive-menu-subarrow-active {
                color: #fff;
                border-color: #212121;
                background-color: #212121;
            }

            #responsive-menu-container #responsive-menu li.responsive-menu-item a .responsive-menu-subarrow.responsive-menu-subarrow-active:hover {
                color: #fff;
                border-color: #3f3f3f;
                background-color: #3f3f3f;
            }

            #responsive-menu-container #responsive-menu li.responsive-menu-item a .responsive-menu-subarrow:hover {
                color: #fff;
                border-color: #fff;
                background-color: #001a4b;
            }

            #responsive-menu-container #responsive-menu li.responsive-menu-current-item>.responsive-menu-item-link {
                background-color: #dcb91d;
                color: #fff;
                border-color: #212121;
            }

            #responsive-menu-container #responsive-menu li.responsive-menu-current-item>.responsive-menu-item-link:hover {
                background-color: #001a4b;
                color: #fff;
                border-color: #3f3f3f;
            }


        }
    </style>
    <script>
        jQuery(document).ready(function($) {

    var ResponsiveMenu = {
        trigger: '#responsive-menu-button',
        animationSpeed: 500,
        breakpoint: 1024,
        pushButton: 'off',
        animationType: 'slide',
        animationSide: 'left',
        pageWrapper: '',
        isOpen: false,
        triggerTypes: 'click',
        activeClass: 'is-active',
        container: '#responsive-menu-container',
        openClass: 'responsive-menu-open',
        accordion: 'off',
        activeArrow: '▲',
        inactiveArrow: '▼',
        wrapper: '#responsive-menu-wrapper',
        closeOnBodyClick: 'off',
        closeOnLinkClick: 'off',
        itemTriggerSubMenu: 'off',
        linkElement: '.responsive-menu-item-link',
        openMenu: function() {
            $(this.trigger).addClass(this.activeClass);
            $('html').addClass(this.openClass);
            $('.responsive-menu-button-icon-active').hide();
            $('.responsive-menu-button-icon-inactive').show();
            this.setWrapperTranslate();
            this.isOpen = true;
        },
        closeMenu: function() {
            $(this.trigger).removeClass(this.activeClass);
            $('html').removeClass(this.openClass);
            $('.responsive-menu-button-icon-inactive').hide();
            $('.responsive-menu-button-icon-active').show();
            this.clearWrapperTranslate();
            this.isOpen = false;
        },
        triggerMenu: function() {
            this.isOpen ? this.closeMenu() : this.openMenu();
        },
        triggerSubArrow: function(subarrow) {
            var sub_menu = $(subarrow).parent().next('.responsive-menu-submenu');
            var self = this;
            if(this.accordion == 'on') {
                /* Get Top Most Parent and the siblings */
                var top_siblings = sub_menu.parents('.responsive-menu-item-has-children').last().siblings('.responsive-menu-item-has-children');
                var first_siblings = sub_menu.parents('.responsive-menu-item-has-children').first().siblings('.responsive-menu-item-has-children');
                /* Close up just the top level parents to key the rest as it was */
                top_siblings.children('.responsive-menu-submenu').slideUp(200, 'linear').removeClass('responsive-menu-submenu-open');
                /* Set each parent arrow to inactive */
                top_siblings.each(function() {
                    $(this).find('.responsive-menu-subarrow').first().html(self.inactiveArrow);
                    $(this).find('.responsive-menu-subarrow').first().removeClass('responsive-menu-subarrow-active');
                });
                /* Now Repeat for the current item siblings */
                first_siblings.children('.responsive-menu-submenu').slideUp(200, 'linear').removeClass('responsive-menu-submenu-open');
                first_siblings.each(function() {
                    $(this).find('.responsive-menu-subarrow').first().html(self.inactiveArrow);
                    $(this).find('.responsive-menu-subarrow').first().removeClass('responsive-menu-subarrow-active');
                });
            }
            if(sub_menu.hasClass('responsive-menu-submenu-open')) {
                sub_menu.slideUp(200, 'linear').removeClass('responsive-menu-submenu-open');
                $(subarrow).html(this.inactiveArrow);
                $(subarrow).removeClass('responsive-menu-subarrow-active');
            } else {
                sub_menu.slideDown(200, 'linear').addClass('responsive-menu-submenu-open');
                $(subarrow).html(this.activeArrow);
                $(subarrow).addClass('responsive-menu-subarrow-active');
            }
        },
        menuHeight: function() {
            return $(this.container).height();
        },
        menuWidth: function() {
            return $(this.container).width();
        },
        wrapperHeight: function() {
            return $(this.wrapper).height();
        },
        setWrapperTranslate: function() {
            switch(this.animationSide) {
                case 'left':
                    translate = 'translateX(' + this.menuWidth() + 'px)'; break;
                case 'right':
                    translate = 'translateX(-' + this.menuWidth() + 'px)'; break;
                case 'top':
                    translate = 'translateY(' + this.wrapperHeight() + 'px)'; break;
                case 'bottom':
                    translate = 'translateY(-' + this.menuHeight() + 'px)'; break;
            }
            if(this.animationType == 'push') {
                $(this.pageWrapper).css({'transform':translate});
                $('html, body').css('overflow-x', 'hidden');
            }
            if(this.pushButton == 'on') {
                $('#responsive-menu-button').css({'transform':translate});
            }
        },
        clearWrapperTranslate: function() {
            var self = this;
            if(this.animationType == 'push') {
                $(this.pageWrapper).css({'transform':''});
                setTimeout(function() {
                    $('html, body').css('overflow-x', '');
                }, self.animationSpeed);
            }
            if(this.pushButton == 'on') {
                $('#responsive-menu-button').css({'transform':''});
            }
        },
        init: function() {
            var self = this;
            $(this.trigger).on(this.triggerTypes, function(e){
                e.stopPropagation();
                self.triggerMenu();
            });
            $(this.trigger).mouseup(function(){
                $(self.trigger).blur();
            });
            $('.responsive-menu-subarrow').on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();
                self.triggerSubArrow(this);
            });
            $(window).resize(function() {
                if($(window).width() > self.breakpoint) {
                    if(self.isOpen){
                        self.closeMenu();
                    }
                } else {
                    if($('.responsive-menu-open').length>0){
                        self.setWrapperTranslate();
                    }
                }
            });
            if(this.closeOnLinkClick == 'on') {
                $(this.linkElement).on('click', function(e) {
                    e.preventDefault();
                    /* Fix for when close menu on parent clicks is on */
                    if(self.itemTriggerSubMenu == 'on' && $(this).is('.responsive-menu-item-has-children > ' + self.linkElement)) {
                        return;
                    }
                    old_href = $(this).attr('href');
                    old_target = typeof $(this).attr('target') == 'undefined' ? '_self' : $(this).attr('target');
                    if(self.isOpen) {
                        if($(e.target).closest('.responsive-menu-subarrow').length) {
                            return;
                        }
                        self.closeMenu();
                        setTimeout(function() {
                            window.open(old_href, old_target);
                        }, self.animationSpeed);
                    }
                });
            }
            if(this.closeOnBodyClick == 'on') {
                $(document).on('click', 'body', function(e) {
                    if(self.isOpen) {
                        if($(e.target).closest('#responsive-menu-container').length || $(e.target).closest('#responsive-menu-button').length) {
                            return;
                        }
                    }
                    self.closeMenu();
                });
            }
            if(this.itemTriggerSubMenu == 'on') {
                $('.responsive-menu-item-has-children > ' + this.linkElement).on('click', function(e) {
                    e.preventDefault();
                    self.triggerSubArrow($(this).children('.responsive-menu-subarrow').first());
                });
            }
        }
    };
    ResponsiveMenu.init();
});
    </script>
    <style type="text/css" data-type="vc_shortcodes-custom-css">
        .vc_custom_1494776950967 {
            margin-top: 10px !important;
            margin-right: 0px !important;
            margin-left: 0px !important;
            border-bottom-width: 10px !important;
            padding-top: 0px !important;
            padding-bottom: 0px !important;
        }

        .vc_custom_1494646018164 {
            margin-bottom: 0px !important;
            padding-top: 0px !important;
            padding-bottom: 0px !important;
        }

        .vc_custom_1494667226066 {
            margin-bottom: 25px !important;
            padding-bottom: 10px !important;
            background-color: #f2f2f2 !important;
        }

        .vc_custom_1494777048228 {
            padding-top: 0px !important;
            padding-bottom: 0px !important;
        }

        .vc_custom_1494667010355 {
            margin-top: 20px !important;
            margin-right: 0px !important;
            margin-bottom: 0px !important;
            margin-left: 0px !important;
            border-top-width: 4px !important;
            border-right-width: 4px !important;
            border-left-width: 4px !important;
            padding-top: 0px !important;
            padding-bottom: 0px !important;
            background-color: #f2f2f2 !important;
            border-left-color: #eecb2e !important;
            border-left-style: solid !important;
            border-right-color: #eecb2e !important;
            border-right-style: solid !important;
            border-top-color: #eecb2e !important;
            border-top-style: solid !important;
        }

        .vc_custom_1501685013736 {
            margin-top: 0px !important;
            margin-bottom: 0px !important;
            padding-top: 7px !important;
        }

        .vc_custom_1494646098375 {
            margin-top: 0px !important;
            margin-right: 0px !important;
            margin-left: 0px !important;
            border-right-width: 4px !important;
            border-left-width: 4px !important;
            background-color: #ffffff !important;
        }

        .vc_custom_1494667022395 {
            background-color: #f2f2f2 !important;
        }

        .vc_custom_1494667030018 {
            background-color: #f2f2f2 !important;
        }

        .vc_custom_1494667042505 {
            background-color: #f2f2f2 !important;
        }
    </style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
    <link rel="shortcut icon"
        href="{{ asset('goldview/mucchucnang/uploads/2016/08/fav-goldview1.png') }}" />
</head>

<body class="template-front">
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-45962768-2', 'auto');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');

    </script>
    <div id="header" class="header">
        <h1 id="site_title">
            <a style="background: url('{{ asset(__c('logo')) }}') center center no-repeat !important;" href="{{ url('/') }}">Bán &#038; Cho thuê căn hộ TNR THE GOLDVIEW</a>
        </h1>
        <p style="background: url('{{ asset(__c('top_banner')) }}') !important;" id="site_tagline"></p>
        <div class="widget widget_text hotline" id="text-3">
            <div class="textwidget">
                <div class="hotline-content">
                    <img src="{{ asset('goldview/mucchucnang/thesis/skins/classic-r/images/icon-phone.png') }}" />
                    <span class="line"></span>
                    <span class="num">HOTLINE: {{ __c('hotline') }}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="khungmenu">
        <span class="menu_control">≡ Menu</span>
        <ul id="menuchinh" class="menu">
            <li id="menu-item-5" class="menu-item menu-item-type-custom menu-item-object-custom @if(url()->current() === url('/')) current-menu-item @endif current_page_item menu-item-home menu-item-5"><a href="{{ url('/') }}">Home</a></li>
            @foreach (\App\ProjectCategory::all() as $category)
            <li id="{{$category->slug}}" class="@if(url()->current() === route('project.list', ['slug' => $category->slug])) current-menu-item @endif menu-item menu-item-type-taxonomy menu-item-object-category {{$category->slug}}">
                <a href="{{route('project.list', ['slug' => $category->slug])}}">{{$category->title}}</a>
            </li>
            @endforeach

            {{-- @php
                $officetel = \App\Page::find(1);
            @endphp
            <li id="{{$officetel->slug}}" class="@if(url()->current() === route('page.detail', ['slug' => $officetel->slug])) current-menu-item @endif menu-item menu-item-type-post_type menu-item-object-page {{$officetel->slug}}">
                <a href="{{route('page.detail', ['slug' => $officetel->slug])}}">Officetel</a>
            </li> --}}

            <li id="menu-item-921" class="@if(url()->current() === route('mat_bang')) current-menu-item @endif menu-item menu-item-type-post_type menu-item-object-page menu-item-921">
                <a href="{{route('mat_bang')}}">Mặt bằng</a>
            </li>

            {{-- @php
                $thietke = \App\PostCategory::find(3);
            @endphp
            <li id="{{$thietke->slug}}" class="@if(url()->current() === route('thiet-ke.list')) current-menu-item @endif menu-item menu-item-type-taxonomy menu-item-object-category {{$thietke->slug}}">
                <a href="{{route('thiet-ke.list')}}">{{$thietke->title}}</a>
            </li>

            @php
                $bangia = \App\Page::find(2);
            @endphp
            <li id="{{$bangia->slug}}" class="@if(url()->current() === route('page.detail', ['slug' => $bangia->slug])) current-menu-item @endif menu-item menu-item-type-post_type menu-item-object-page {{$bangia->slug}}">
                <a href="{{route('page.detail', ['slug' => $bangia->slug])}}">Giá &#038; PTTT</a>
            </li>
            @php
                $nhamau = \App\Page::find(3);
            @endphp
            <li id="{{$nhamau->slug}}" class="@if(url()->current() === route('page.detail', ['slug' => $nhamau->slug])) current-menu-item @endif menu-item menu-item-type-post_type menu-item-object-page {{$nhamau->slug}}">
                <a href="{{route('page.detail', ['slug' => $nhamau->slug])}}">Nhà mẫu</a>
            </li> --}}

            <li id="menu-item-128" class="@if(url()->current() === route('news')) current-menu-item @endif menu-item menu-item-type-taxonomy menu-item-object-category menu-item-128">
                <a href="{{route('news')}}">Tin tức</a>
            </li>
            <li id="menu-item-128" class="@if(url()->current() === route('page.ky_gui')) current-menu-item @endif menu-item menu-item-type-taxonomy menu-item-object-category menu-item-128">
                <a href="{{route('page.ky_gui')}}">Ký gửi</a>
            </li>
            {{-- @php
                $tiendo = \App\PostCategory::find(1);
            @endphp
            <li style="margin-right: 0px;" id="{{$tiendo->slug}}" class="@if(url()->current() === route('tien-do.list')) current-menu-item @endif menu-item menu-item-type-taxonomy menu-item-object-category {{$tiendo->slug}}">
                <a href="{{route('tien-do.list')}}">{{$tiendo->title}}</a>
            </li> --}}
        </ul>
    </div>

    @yield('content')
    
    <div class="footer">
        <div id="s_footer" class="s_footer">
        </div>
        <div style="bottom: -20px; left: -50px" class="nova-phone nova-green nova-show nova-static" id="nova_phone_div">
            <div class="nova-ph-circle"></div>
            <div class="nova-ph-circle-fill"></div>
            <a class="cuocgoi" href="tel:{{str_replace(' ', '', __c('hotline'))}}">
                <div class="nova-ph-img-circle"></div>
            </a>
        </div>

        <script src="{{ asset('goldview/mucchucnang/thesis/skins/classic-r/js/call.js') }}" type="text/javascript"></script>
        <script type="text/javascript">
            jQuery(window).load(function() {
		 var tid = setTimeout(Ring, 5000);
		 var flag = false;

		 function Ring() {
			 if (flag) {
				 flag = !flag;
				 jQuery("#nova_phone_div").addClass("nova-static");
				 tid = setTimeout(Ring, 10000);
			 } else {
				 flag = !flag;
				 jQuery("#nova_phone_div").removeClass("nova-static");
				 tid = setTimeout(Ring, 2000);
			 }
		 }
	 });
        </script>
        <div id='top-buttom-image'>
            <a title='Lên đầu trang'><img class="sc" src="{{ asset('goldview/mucchucnang/thesis/skins/classic-r/images/top3.png') }}" alt="top" width="40"/></a><br/>
        </div>
        <script type="text/javascript">
            jQuery().ready(function(){
            jQuery(document).scroll(function(){
                if(jQuery(this).scrollTop()>200){
                    jQuery('.sc').fadeIn("slow");
                    jQuery('.thongtin').addClass("duoi");	
                }
                else {jQuery('.thongtin').removeClass("duoi");}
            });
            
            jQuery('.sc').click(function () {
                    jQuery('body,html').animate({
                        scrollTop: 0
                    }, 800);});	  
            
            });
        </script>


        <style type="text/css">
            #nova_phone_div {
                display: none
            }

            @media all and (max-width: 767px) {
                #nova_phone_div {
                    display: block;
                }
            }

            .nova-phone.nova-static {
                opacity: 0.9;
            }

            .nova-phone.nova-hover {
                opacity: 1;
            }

            .nova-ph-circle {
                animation: 1.2s ease-in-out 0s normal none infinite running nova-circle-anim;
                background-color: transparent;
                border: 2px solid rgba(30, 30, 30, 0.4);
                border-radius: 100%;
                height: 160px;
                left: 20px;
                opacity: 0.1;
                position: absolute;
                top: 20px;
                transform-origin: 50% 50% 0;
                transition: all 0.5s ease 0s;
                width: 160px;
            }

            .nova-phone.nova-active .nova-ph-circle {
                animation: 1.1s ease-in-out 0s normal none infinite running nova-circle-anim;
            }

            .nova-phone.nova-static .nova-ph-circle {
                animation: 2.2s ease-in-out 0s normal none infinite running nova-circle-anim;
            }

            .nova-phone.nova-hover .nova-ph-circle {
                border-color: rgb(0, 175, 242);
                opacity: 0.5;
            }

            .nova-phone.nova-green.nova-hover .nova-ph-circle {
                border-color: rgb(117, 235, 80);
                opacity: 0.5;
            }

            .nova-phone.nova-green .nova-ph-circle {
                border-color: rgb(117, 238, 35);
                opacity: 0.5;
            }

            .nova-phone.nova-gray.nova-hover .nova-ph-circle {
                border-color: rgb(204, 204, 204);
                opacity: 0.5;
            }

            .nova-phone.nova-gray .nova-ph-circle {
                border-color: rgb(117, 235, 80);
                opacity: 0.5;
            }

            .nova-ph-circle-fill {
                animation: 2.3s ease-in-out 0s normal none infinite running nova-circle-fill-anim;
                background-color: #000;
                border: 2px solid transparent;
                border-radius: 100%;
                height: 100px;
                left: 50px;
                opacity: 0.1;
                position: absolute;
                top: 50px;
                transform-origin: 50% 50% 0;
                transition: all 0.5s ease 0s;
                width: 100px;
            }

            .nova-phone.nova-active .nova-ph-circle-fill {
                animation: 1.7s ease-in-out 0s normal none infinite running nova-circle-fill-anim;
            }

            .nova-phone.nova-static .nova-ph-circle-fill {
                animation: 2.3s ease-in-out 0s normal none infinite running nova-circle-fill-anim;
                opacity: 0;
            }

            .nova-phone.nova-hover .nova-ph-circle-fill {
                background-color: rgba(0, 175, 242, 0.5);
                opacity: 0.75;
            }

            .nova-phone.nova-green.nova-hover .nova-ph-circle-fill {
                background-color: rgba(117, 238, 35, 0.5);
                opacity: 0.75;
            }

            .nova-phone.nova-green .nova-ph-circle-fill {
                background-color: rgba(117, 238, 35, 0.5);
                opacity: 0.75;
            }

            .nova-phone.nova-gray.nova-hover .nova-ph-circle-fill {
                background-color: rgba(204, 204, 204, 0.5);
                opacity: 0.75;
            }

            .nova-phone.nova-gray .nova-ph-circle-fill {
                background-color: rgba(117, 235, 80, 0.5);
                opacity: 0.75;
            }

            .nova-ph-img-circle {
                animation: 1s ease-in-out 0s normal none infinite running nova-circle-img-anim;
                background-color: rgba(30, 30, 30, 0.1);
                background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAAsTAAALEwEAmpwYAAABNmlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjarY6xSsNQFEDPi6LiUCsEcXB4kygotupgxqQtRRCs1SHJ1qShSmkSXl7VfoSjWwcXd7/AyVFwUPwC/0Bx6uAQIYODCJ7p3MPlcsGo2HWnYZRhEGvVbjrS9Xw5+8QMUwDQCbPUbrUOAOIkjvjB5ysC4HnTrjsN/sZ8mCoNTIDtbpSFICpA/0KnGsQYMIN+qkHcAaY6addAPAClXu4vQCnI/Q0oKdfzQXwAZs/1fDDmADPIfQUwdXSpAWpJOlJnvVMtq5ZlSbubBJE8HmU6GmRyPw4TlSaqo6MukP8HwGK+2G46cq1qWXvr/DOu58vc3o8QgFh6LFpBOFTn3yqMnd/n4sZ4GQ5vYXpStN0ruNmAheuirVahvAX34y/Axk/96FpPYgAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAUggAARVYAAA6lwAAF2/XWh+QAAAB/ElEQVR42uya7W3CMBCG31QM4A1aNggTlG6QbpBMkHYC1AloJ4BOABuEDcgGtBOETnD9c1ERCH/lwxeaV8oPFGP86Hy+DxMREW5Bd7gRjSDSNGn4/RiAOvm8C0ZCRD5PSkQVXSr1nK/xE3mcWimA1ZV3JYBZCIO4giQANoYxMwYS6+xKY4lT5dJPreWZY+uspqSCKPYN27GJVBDXheVSQe494ksiEWTuMXcu1dld9SARxDX1OAJ4lgjy4zDnFsC076A4adEiRwAZg4hOUSpNoCsBPDGM+HqkNGynYBCuILuWj+dgWysGsNe8nwL4GsrW0m2fxZBq9rW0rNcX5MOQ9eZD8JFahcG5g/iKT671alGAYQggpYWvpEPYWrU/HDTOfeRIX0q2SL3QN4tGhZJukVobQyXYWw7WtLDKDIuM+ZSzscyCE9PCy5IttCvnZNaeiGLNHKuz8ZVh/MXTVu/1xQKmIqLEAuJ0fNo3iG5B51oSkeKnsBi/4bG9gYB/lCytU5G9DryFW+3Gm+JLwU7ehbJrwTjq4DJU8bHcVbEV9dXXqqP6uqO5e2/QZRYJpqu2IUAA4B3tXvx8hgKp05QZW6dJqrLTNkB6vrRURLRwPHqtYgkC3cLWQAcDQGGKH13FER/NATzi786+BPDNjm1dMkfjn2pGkBHkf4D8DgBJDuDHx9BN+gAAAABJRU5ErkJggg==");
                background-position: center center;
                background-repeat: no-repeat;
                border: 2px solid transparent;
                border-radius: 100%;
                height: 60px;
                left: 70px;
                opacity: 0.8;
                position: absolute;
                top: 70px;
                transform-origin: 50% 50% 0;
                width: 60px;
            }

            .nova-phone.nova-active .nova-ph-img-circle {
                animation: 1s ease-in-out 0s normal none infinite running nova-circle-img-anim;
            }

            .nova-phone.nova-static .nova-ph-img-circle {
                animation: 0s ease-in-out 0s normal none infinite running nova-circle-img-anim;
            }

            .nova-phone.nova-hover .nova-ph-img-circle {
                background-color: rgb(117, 238, 35);
            }

            .nova-phone.nova-green.nova-hover .nova-ph-img-circle {
                background-color: rgb(117, 235, 80);
            }

            .nova-phone.nova-green .nova-ph-img-circle {
                background-color: rgb(117, 238, 35);
            }

            .nova-phone.nova-gray.nova-hover .nova-ph-img-circle {
                background-color: rgb(204, 204, 204);
            }

            .nova-phone.nova-gray .nova-ph-img-circle {
                background-color: rgb(117, 235, 80);
            }

            .nova-phone {
                background-color: transparent;
                cursor: pointer;
                height: 200px;
                position: fixed;
                transition: visibility 0.5s ease 0s;
                visibility: hidden;
                width: 200px;
                z-index: 200000;
            }

            .nova-phone.nova-show {
                visibility: visible;
            }

            @keyframes nova-circle-anim {
                0% {
                    opacity: 0.1;
                    transform: rotate(0deg) scale(0.5) skew(1deg);
                }
                30% {
                    opacity: 0.5;
                    transform: rotate(0deg) scale(0.7) skew(1deg);
                }
                100% {
                    opacity: 0.6;
                    transform: rotate(0deg) scale(1) skew(1deg);
                }
            }

            @keyframes nova-circle-anim {
                0% {
                    opacity: 0.1;
                    transform: rotate(0deg) scale(0.5) skew(1deg);
                }
                30% {
                    opacity: 0.5;
                    transform: rotate(0deg) scale(0.7) skew(1deg);
                }
                100% {
                    opacity: 0.1;
                    transform: rotate(0deg) scale(1) skew(1deg);
                }
            }

            @keyframes nova-circle-fill-anim {
                0% {
                    opacity: 0.2;
                    transform: rotate(0deg) scale(0.7) skew(1deg);
                }
                50% {
                    opacity: 0.2;
                }
                100% {
                    opacity: 0.2;
                    transform: rotate(0deg) scale(0.7) skew(1deg);
                }
            }

            @keyframes nova-circle-fill-anim {
                0% {
                    opacity: 0.2;
                    transform: rotate(0deg) scale(0.7) skew(1deg);
                }
                50% {
                    opacity: 0.2;
                    transform: rotate(0deg) scale(1) skew(1deg);
                }
                100% {
                    opacity: 0.2;
                    transform: rotate(0deg) scale(0.7) skew(1deg);
                }
            }

            @keyframes nova-circle-img-anim {
                0% {
                    transform: rotate(0deg) scale(1) skew(1deg);
                }
                10% {
                    transform: rotate(-25deg) scale(1) skew(1deg);
                }
                20% {
                    transform: rotate(25deg) scale(1) skew(1deg);
                }
                30% {
                    transform: rotate(-25deg) scale(1) skew(1deg);
                }
                40% {
                    transform: rotate(25deg) scale(1) skew(1deg);
                }
                50% {
                    transform: rotate(0deg) scale(1) skew(1deg);
                }
                100% {
                    transform: rotate(0deg) scale(1) skew(1deg);
                }
            }

            @keyframes nova-circle-img-anim {
                0% {
                    transform: rotate(0deg) scale(1) skew(1deg);
                }
                10% {
                    transform: rotate(-25deg) scale(1) skew(1deg);
                }
                20% {
                    transform: rotate(25deg) scale(1) skew(1deg);
                }
                30% {
                    transform: rotate(-25deg) scale(1) skew(1deg);
                }
                40% {
                    transform: rotate(25deg) scale(1) skew(1deg);
                }
                50% {
                    transform: rotate(0deg) scale(1) skew(1deg);
                }
                100% {
                    transform: rotate(0deg) scale(1) skew(1deg);
                }
            }

            .sc {
                display: none;
            }

            #top-buttom-image {
                bottom: 30px;
                clip: inherit;
                cursor: pointer;
                position: fixed;
                right: 15px;
                z-index: 19999;
                display: flex;
            }

            .sc:hover {
                opacity: 0.7
            }
        </style>
        <link rel="stylesheet" href="{{ asset('goldview/mucchucnang/thesis/skins/classic-r/js/animate.css') }}" type="text/css" />
        <script type="text/javascript" src="{{ asset('goldview/mucchucnang/thesis/skins/classic-r/js/wow.min.js') }}"></script>
        <script>
            wow = new WOW(
            {
                animateClass: 'animated',
                offset:       100,
                callback:     function(box) {
                
                }
            }
            );
            wow.init();
        </script>
        <div class="container">
            <div class="f-top">
                <div class="p50">
                    <h3>CHỦ ĐẦU TƯ - CÁC ĐỐI TÁC</h3>
                    <p>
                        <img src="{{ url(__c('investors_image')) }}" />
                    </p>
                </div>
                <div class="p50" style="width:49%;float:right">
                    <h3>BẢN ĐỒ</h3>
                    <script src="http://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyA1SQ7Gn66uIsSOa9kEVqQTIyQJuAZmFcY"></script>
                    <script>
                        function googleMapsSyncer(){var e=document.getElementById("map-canvas2"),a=new google.maps.LatLng({{__c('bottom_web_latitude')}},{{__c('bottom_web_longtitude')}}),o={zoom:15,center:a};map=new google.maps.Map(e,o),markers[0]=new google.maps.Marker({map:map,position:a})}google.maps.event.addDomListener(window,"load",googleMapsSyncer);var map=null,markers=[];
                    </script>
                    <div class="map" width="600" height="450"  style="height:310px;border:0" id="map-canvas2">
        
                    </div>
                  
                    {{-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4661.353072005156!2d106.688435450271!3d10.756427325704141!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f0fc7a15781%3A0xc4507085beab9c80!2zMzQ2IELhur9uIFbDom4gxJDhu5NuLCBwaMaw4budbmcgMSwgUXXhuq1uIDQsIEjhu5MgQ2jDrSBNaW5oLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1494691488633"
                        width="600" height="450" frameborder="0" style="height:310px;border:0" allowfullscreen></iframe> --}}
                </div>
            </div>
            <div style="clear:both;">
                <div class="f-column f1">
                    <a href="{{ url('/') }}"><img src="{{ url(__c('logo_footer')) }}" /></a>
                </div>
                <div class="f-column f2">
                    <p><img src="{{ asset('goldview/tnr/img/f1.png') }}" />Nhà mẫu <a href="{{url('/')}}"><strong>TNR The GoldView</strong></a>: {{__c('address')}}</p>
                    <p><img src="{{ asset('goldview/tnr/img/f2.png') }}" />Điện thoại: <a href="tel:{{str_replace(' ', '', __c('hotline'))}}">{{ __c('hotline') }}</a></p>
                    <p><img src="{{ asset('goldview/tnr/img/f3.png') }}" />Email: {{__c('email')}}</p>
                    <p><img src="{{ asset('goldview/tnr/img/f4.png') }}" />Website: {{Request::getHost()}}</p>
                </div>
                <div class="f-column f3">


                    <script type="text/javascript">
                        //<![CDATA[
                        if (typeof newsletter_check !== "function") {
                            window.newsletter_check = function (f) {
                                var re = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-]{1,})+\.)+([a-zA-Z0-9]{2,})+$/;
                                if (!re.test(f.elements["ne"].value)) {
                                    alert("The email is not correct");
                                    return false;
                                }
                                for (var i=1; i<20; i++) {
                                if (f.elements["np" + i] && f.elements["np" + i].required && f.elements["np" + i].value == "") {
                                    alert("");
                                    return false;
                                }
                                }
                                if (f.elements["ny"] && !f.elements["ny"].checked) {
                                    alert("You must accept the privacy statement");
                                    return false;
                                }
                                return true;
                            }
                        }
                        //]]>

                    </script>

                    <div class="row-1">
                        <form action="{{route('contact.save')}}" method="post">
                            @csrf
                            <input class="newsletter-email" type="email" required
                                name="email" value="Email" onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue"
                            />
                            <button class="newsletter-submit" type="submit" value="Subscribe">Đăng ký</button>
                        </form>
                        <script>
                        jQuery(document).ready(function() {
                            jQuery('.newsletter-submit').on('click', function(e) {
                                e.preventDefault();
                                if(window["gf_submitting_2"]){return false;}
                                
                                window["gf_submitting_2"]=true;
                                const form = jQuery(this).closest('form');
                                jQuery.ajax({
                                    url: "{{route('contact.save')}}",
                                    method: "POST",
                                    data: jQuery(form).serialize(),  
                                    dataType: 'json'
                                })
                                .done(function(data) {
                                    if(data.result) {
                                        jQuery(form).find('input[name!="_token"][type!="button"]').val('');
                                        jQuery(form).find('textarea').val('');
                                    }
                                    alert(data.message);
                                })
                                .always(function() {
                                    window["gf_submitting_2"]=false; 
                                });
                            });
                        });
                        </script>
                    </div>
                    <div class="row-2">
                        <h3>Tìm chúng tôi trên</h3>
                        <ul>
                            <li><a href="https://www.facebook.com/canhothegoldviewq4/" target="_blank"><img src="{{ asset('goldview/tnr/img/s2.png') }}" /></a></li>
                            <li><a href="https://plus.google.com/u/9/b/106654836191381482275/106654836191381482275" target="_blank"><img src="{{ asset('goldview/tnr/img/s3.png') }}" /></a></li>
                            <li><a href="https://www.youtube.com/channel/UCtAJLHI9iforqauSHNGKAqg" target="_blank"><img src="{{ asset('goldview/tnr/img/s5.png') }}" /></a></li>
                            <li><a href="#"><img src="{{ asset('goldview/tnr/img/s1.png') }}" /></a></li>
                            <li><a href="#"><img src="{{ asset('goldview/tnr/img/s4.png') }}" /></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="widget widget_text" id="text-8">
            <div class="textwidget">
                
                <!-- End of LiveChat code -->
            </div>
        </div>
    </div>
    <div style="bottom: -20px; left: -50px" class="nova-phone nova-green nova-show nova-static" id="nova_phone_div">
        <div class="nova-ph-circle"></div>
        <div class="nova-ph-circle-fill"></div>
        <a class="cuocgoi" href="tel:+84909040965">
            <div class="nova-ph-img-circle"></div>
        </a>
    </div>

    <script src="{{ asset('goldview/mucchucnang/thesis/skins/classic-r/js/call.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(window).load(function() {
		 var tid = setTimeout(Ring, 5000);
		 var flag = false;

		 function Ring() {
			 if (flag) {
				 flag = !flag;
				 jQuery("#nova_phone_div").addClass("nova-static");
				 tid = setTimeout(Ring, 10000);
			 } else {
				 flag = !flag;
				 jQuery("#nova_phone_div").removeClass("nova-static");
				 tid = setTimeout(Ring, 2000);
			 }
		 }
	 });
    </script>
    <div id='top-buttom-image'>
        <a title='Lên đầu trang'><img class="sc" src="{{ asset('goldview/mucchucnang/thesis/skins/classic-r/images/top3.png') }}" alt="top" width="40"/></a><br/>
    </div>
    <script type="text/javascript">
        jQuery().ready(function(){
		  jQuery(document).scroll(function(){
			  if(jQuery(this).scrollTop()>200){
				 jQuery('.sc').fadeIn("slow");
				 jQuery('.thongtin').addClass("duoi");	
			  }
			else {jQuery('.thongtin').removeClass("duoi");}
		});
		
		jQuery('.sc').click(function () {
				jQuery('body,html').animate({
					scrollTop: 0
				}, 800);});	  
		  
		});
    </script>


    <style type="text/css">
        #nova_phone_div {
            display: none
        }

        @media all and (max-width: 767px) {
            #nova_phone_div {
                display: block;
            }
        }

        .nova-phone.nova-static {
            opacity: 0.9;
        }

        .nova-phone.nova-hover {
            opacity: 1;
        }

        .nova-ph-circle {
            animation: 1.2s ease-in-out 0s normal none infinite running nova-circle-anim;
            background-color: transparent;
            border: 2px solid rgba(30, 30, 30, 0.4);
            border-radius: 100%;
            height: 160px;
            left: 20px;
            opacity: 0.1;
            position: absolute;
            top: 20px;
            transform-origin: 50% 50% 0;
            transition: all 0.5s ease 0s;
            width: 160px;
        }

        .nova-phone.nova-active .nova-ph-circle {
            animation: 1.1s ease-in-out 0s normal none infinite running nova-circle-anim;
        }

        .nova-phone.nova-static .nova-ph-circle {
            animation: 2.2s ease-in-out 0s normal none infinite running nova-circle-anim;
        }

        .nova-phone.nova-hover .nova-ph-circle {
            border-color: rgb(0, 175, 242);
            opacity: 0.5;
        }

        .nova-phone.nova-green.nova-hover .nova-ph-circle {
            border-color: rgb(117, 235, 80);
            opacity: 0.5;
        }

        .nova-phone.nova-green .nova-ph-circle {
            border-color: rgb(117, 238, 35);
            opacity: 0.5;
        }

        .nova-phone.nova-gray.nova-hover .nova-ph-circle {
            border-color: rgb(204, 204, 204);
            opacity: 0.5;
        }

        .nova-phone.nova-gray .nova-ph-circle {
            border-color: rgb(117, 235, 80);
            opacity: 0.5;
        }

        .nova-ph-circle-fill {
            animation: 2.3s ease-in-out 0s normal none infinite running nova-circle-fill-anim;
            background-color: #000;
            border: 2px solid transparent;
            border-radius: 100%;
            height: 100px;
            left: 50px;
            opacity: 0.1;
            position: absolute;
            top: 50px;
            transform-origin: 50% 50% 0;
            transition: all 0.5s ease 0s;
            width: 100px;
        }

        .nova-phone.nova-active .nova-ph-circle-fill {
            animation: 1.7s ease-in-out 0s normal none infinite running nova-circle-fill-anim;
        }

        .nova-phone.nova-static .nova-ph-circle-fill {
            animation: 2.3s ease-in-out 0s normal none infinite running nova-circle-fill-anim;
            opacity: 0;
        }

        .nova-phone.nova-hover .nova-ph-circle-fill {
            background-color: rgba(0, 175, 242, 0.5);
            opacity: 0.75;
        }

        .nova-phone.nova-green.nova-hover .nova-ph-circle-fill {
            background-color: rgba(117, 238, 35, 0.5);
            opacity: 0.75;
        }

        .nova-phone.nova-green .nova-ph-circle-fill {
            background-color: rgba(117, 238, 35, 0.5);
            opacity: 0.75;
        }

        .nova-phone.nova-gray.nova-hover .nova-ph-circle-fill {
            background-color: rgba(204, 204, 204, 0.5);
            opacity: 0.75;
        }

        .nova-phone.nova-gray .nova-ph-circle-fill {
            background-color: rgba(117, 235, 80, 0.5);
            opacity: 0.75;
        }

        .nova-ph-img-circle {
            animation: 1s ease-in-out 0s normal none infinite running nova-circle-img-anim;
            background-color: rgba(30, 30, 30, 0.1);
            background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAAsTAAALEwEAmpwYAAABNmlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjarY6xSsNQFEDPi6LiUCsEcXB4kygotupgxqQtRRCs1SHJ1qShSmkSXl7VfoSjWwcXd7/AyVFwUPwC/0Bx6uAQIYODCJ7p3MPlcsGo2HWnYZRhEGvVbjrS9Xw5+8QMUwDQCbPUbrUOAOIkjvjB5ysC4HnTrjsN/sZ8mCoNTIDtbpSFICpA/0KnGsQYMIN+qkHcAaY6addAPAClXu4vQCnI/Q0oKdfzQXwAZs/1fDDmADPIfQUwdXSpAWpJOlJnvVMtq5ZlSbubBJE8HmU6GmRyPw4TlSaqo6MukP8HwGK+2G46cq1qWXvr/DOu58vc3o8QgFh6LFpBOFTn3yqMnd/n4sZ4GQ5vYXpStN0ruNmAheuirVahvAX34y/Axk/96FpPYgAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAUggAARVYAAA6lwAAF2/XWh+QAAAB/ElEQVR42uya7W3CMBCG31QM4A1aNggTlG6QbpBMkHYC1AloJ4BOABuEDcgGtBOETnD9c1ERCH/lwxeaV8oPFGP86Hy+DxMREW5Bd7gRjSDSNGn4/RiAOvm8C0ZCRD5PSkQVXSr1nK/xE3mcWimA1ZV3JYBZCIO4giQANoYxMwYS6+xKY4lT5dJPreWZY+uspqSCKPYN27GJVBDXheVSQe494ksiEWTuMXcu1dld9SARxDX1OAJ4lgjy4zDnFsC076A4adEiRwAZg4hOUSpNoCsBPDGM+HqkNGynYBCuILuWj+dgWysGsNe8nwL4GsrW0m2fxZBq9rW0rNcX5MOQ9eZD8JFahcG5g/iKT671alGAYQggpYWvpEPYWrU/HDTOfeRIX0q2SL3QN4tGhZJukVobQyXYWw7WtLDKDIuM+ZSzscyCE9PCy5IttCvnZNaeiGLNHKuz8ZVh/MXTVu/1xQKmIqLEAuJ0fNo3iG5B51oSkeKnsBi/4bG9gYB/lCytU5G9DryFW+3Gm+JLwU7ehbJrwTjq4DJU8bHcVbEV9dXXqqP6uqO5e2/QZRYJpqu2IUAA4B3tXvx8hgKp05QZW6dJqrLTNkB6vrRURLRwPHqtYgkC3cLWQAcDQGGKH13FER/NATzi786+BPDNjm1dMkfjn2pGkBHkf4D8DgBJDuDHx9BN+gAAAABJRU5ErkJggg==");
            background-position: center center;
            background-repeat: no-repeat;
            border: 2px solid transparent;
            border-radius: 100%;
            height: 60px;
            left: 70px;
            opacity: 0.8;
            position: absolute;
            top: 70px;
            transform-origin: 50% 50% 0;
            width: 60px;
        }

        .nova-phone.nova-active .nova-ph-img-circle {
            animation: 1s ease-in-out 0s normal none infinite running nova-circle-img-anim;
        }

        .nova-phone.nova-static .nova-ph-img-circle {
            animation: 0s ease-in-out 0s normal none infinite running nova-circle-img-anim;
        }

        .nova-phone.nova-hover .nova-ph-img-circle {
            background-color: rgb(117, 238, 35);
        }

        .nova-phone.nova-green.nova-hover .nova-ph-img-circle {
            background-color: rgb(117, 235, 80);
        }

        .nova-phone.nova-green .nova-ph-img-circle {
            background-color: rgb(117, 238, 35);
        }

        .nova-phone.nova-gray.nova-hover .nova-ph-img-circle {
            background-color: rgb(204, 204, 204);
        }

        .nova-phone.nova-gray .nova-ph-img-circle {
            background-color: rgb(117, 235, 80);
        }

        .nova-phone {
            background-color: transparent;
            cursor: pointer;
            height: 200px;
            position: fixed;
            transition: visibility 0.5s ease 0s;
            visibility: hidden;
            width: 200px;
            z-index: 200000;
        }

        .nova-phone.nova-show {
            visibility: visible;
        }

        @keyframes nova-circle-anim {
            0% {
                opacity: 0.1;
                transform: rotate(0deg) scale(0.5) skew(1deg);
            }
            30% {
                opacity: 0.5;
                transform: rotate(0deg) scale(0.7) skew(1deg);
            }
            100% {
                opacity: 0.6;
                transform: rotate(0deg) scale(1) skew(1deg);
            }
        }

        @keyframes nova-circle-anim {
            0% {
                opacity: 0.1;
                transform: rotate(0deg) scale(0.5) skew(1deg);
            }
            30% {
                opacity: 0.5;
                transform: rotate(0deg) scale(0.7) skew(1deg);
            }
            100% {
                opacity: 0.1;
                transform: rotate(0deg) scale(1) skew(1deg);
            }
        }

        @keyframes nova-circle-fill-anim {
            0% {
                opacity: 0.2;
                transform: rotate(0deg) scale(0.7) skew(1deg);
            }
            50% {
                opacity: 0.2;
            }
            100% {
                opacity: 0.2;
                transform: rotate(0deg) scale(0.7) skew(1deg);
            }
        }

        @keyframes nova-circle-fill-anim {
            0% {
                opacity: 0.2;
                transform: rotate(0deg) scale(0.7) skew(1deg);
            }
            50% {
                opacity: 0.2;
                transform: rotate(0deg) scale(1) skew(1deg);
            }
            100% {
                opacity: 0.2;
                transform: rotate(0deg) scale(0.7) skew(1deg);
            }
        }

        @keyframes nova-circle-img-anim {
            0% {
                transform: rotate(0deg) scale(1) skew(1deg);
            }
            10% {
                transform: rotate(-25deg) scale(1) skew(1deg);
            }
            20% {
                transform: rotate(25deg) scale(1) skew(1deg);
            }
            30% {
                transform: rotate(-25deg) scale(1) skew(1deg);
            }
            40% {
                transform: rotate(25deg) scale(1) skew(1deg);
            }
            50% {
                transform: rotate(0deg) scale(1) skew(1deg);
            }
            100% {
                transform: rotate(0deg) scale(1) skew(1deg);
            }
        }

        @keyframes nova-circle-img-anim {
            0% {
                transform: rotate(0deg) scale(1) skew(1deg);
            }
            10% {
                transform: rotate(-25deg) scale(1) skew(1deg);
            }
            20% {
                transform: rotate(25deg) scale(1) skew(1deg);
            }
            30% {
                transform: rotate(-25deg) scale(1) skew(1deg);
            }
            40% {
                transform: rotate(25deg) scale(1) skew(1deg);
            }
            50% {
                transform: rotate(0deg) scale(1) skew(1deg);
            }
            100% {
                transform: rotate(0deg) scale(1) skew(1deg);
            }
        }

        .sc {
            display: none;
        }

        #top-buttom-image {
            bottom: 30px;
            clip: inherit;
            cursor: pointer;
            position: fixed;
            right: 15px;
            z-index: 19999;
            display: flex;
        }

        .sc:hover {
            opacity: 0.7
        }
    </style>
    <link rel="stylesheet" href="{{ asset('goldview/mucchucnang/thesis/skins/classic-r/js/animate.css') }}" type="text/css" />
    <script type="text/javascript" src="{{ asset('goldview/mucchucnang/thesis/skins/classic-r/js/wow.min.js') }}"></script>
    <script>
        wow = new WOW(
      {
        animateClass: 'animated',
        offset:       100,
        callback:     function(box) {
          
        }
      }
    );
    wow.init();
    </script>
    <script data-cfasync="false" type="text/javascript">
        var addthis_config = {"data_track_clickback":true,"ui_language":"vi","ui_atversion":300,"ignore_server_config":true};
var addthis_share = {};

    </script>
    <!-- AddThis Settings Begin -->
    <script data-cfasync="false" type="text/javascript">
        var addthis_product = "wpp-5.3.5";
                    var wp_product_version = "wpp-5.3.5";
                    var wp_blog_version = "4.7.10";
                    var addthis_plugin_info = {"info_status":"enabled","cms_name":"WordPress","plugin_name":"Share Buttons by AddThis","plugin_version":"5.3.5","anonymous_profile_id":"wp-6d11d7a98acd8ea4d720fcc5b46bf0b1","plugin_mode":"WordPress","select_prefs":{"addthis_per_post_enabled":true,"addthis_above_enabled":false,"addthis_below_enabled":true,"addthis_sidebar_enabled":false,"addthis_mobile_toolbar_enabled":false,"addthis_above_showon_home":true,"addthis_above_showon_posts":true,"addthis_above_showon_pages":true,"addthis_above_showon_archives":true,"addthis_above_showon_categories":true,"addthis_above_showon_excerpts":true,"addthis_below_showon_home":false,"addthis_below_showon_posts":true,"addthis_below_showon_pages":true,"addthis_below_showon_archives":true,"addthis_below_showon_categories":true,"addthis_below_showon_excerpts":false,"addthis_sidebar_showon_home":true,"addthis_sidebar_showon_posts":true,"addthis_sidebar_showon_pages":true,"addthis_sidebar_showon_archives":true,"addthis_sidebar_showon_categories":true,"addthis_mobile_toolbar_showon_home":true,"addthis_mobile_toolbar_showon_posts":true,"addthis_mobile_toolbar_showon_pages":true,"addthis_mobile_toolbar_showon_archives":true,"addthis_mobile_toolbar_showon_categories":true,"sharing_enabled_on_post_via_metabox":true},"page_info":{"template":"home","post_type":""}};
                    if (typeof(addthis_config) == "undefined") {
                        var addthis_config = {"data_track_clickback":true,"ui_language":"vi","ui_atversion":300,"ignore_server_config":true};
                    }
                    if (typeof(addthis_share) == "undefined") {
                        var addthis_share = {};
                    }
                    if (typeof(addthis_layers) == "undefined") {
                        var addthis_layers = {};
                    }
    </script>
    <script data-cfasync="false" type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=wp-6d11d7a98acd8ea4d720fcc5b46bf0b1 "
        async="async">

    </script>
    <script data-cfasync="false" type="text/javascript">
        (function() {
                        var at_interval = setInterval(function () {
                            if(window.addthis) {
                                clearInterval(at_interval);
                                addthis.layers(addthis_layers);
                            }
                        },1000)
                    }());
    </script>
    <button id="responsive-menu-button" class="responsive-menu-button responsive-menu-boring
         responsive-menu-accessible" type="button" aria-label="Menu">

    
    <span class="responsive-menu-box">
        <span class="responsive-menu-inner"></span>
    </span>

    </button>
    <div id="responsive-menu-container" class="slide-left">
        <div id="responsive-menu-wrapper">
            <ul id="responsive-menu" class="">
                <li id="responsive-menu-item-5" class=" menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home responsive-menu-item responsive-menu-current-item"><a href="{{ url('/') }}" class="responsive-menu-item-link">Home</a></li>
                @foreach (\App\ProjectCategory::all() as $category)
                <li id="responsive-menu-item-7" class="menu-item menu-item-type-taxonomy menu-item-object-category responsive-menu-item">
                    <a href="{{route('project.list', ['slug' => $category->slug])}}" class="responsive-menu-item-link">{{$category->title}}</a>
                </li>
                @endforeach
                {{-- <li id="responsive-menu-item-1209"
                    class=" menu-item menu-item-type-post_type menu-item-object-page responsive-menu-item">
                    <a href="{{route('page.detail', ['slug' => $officetel->slug])}}" class="responsive-menu-item-link">Officetel</a></li> --}}

                <li id="responsive-menu-item-921"
                    class=" menu-item menu-item-type-post_type menu-item-object-page responsive-menu-item">
                    <a href="{{route('mat_bang')}}" class="responsive-menu-item-link">Mặt bằng</a></li>

                {{-- <li id="responsive-menu-item-1142"
                    class=" menu-item menu-item-type-taxonomy menu-item-object-category responsive-menu-item">
                    <a href="{{route('thiet-ke.list')}}" class="responsive-menu-item-link">Thiết kế</a></li>
                    
                <li id="responsive-menu-item-924"
                    class=" menu-item menu-item-type-post_type menu-item-object-page responsive-menu-item">
                    <a href="{{route('page.detail', ['slug' => $bangia->slug])}}" class="responsive-menu-item-link">Giá &#038; PTTT</a></li>
                <li
                    id="responsive-menu-item-920" class=" menu-item menu-item-type-post_type menu-item-object-page responsive-menu-item">
                    <a href="{{route('page.detail', ['slug' => $nhamau->slug])}}" class="responsive-menu-item-link">Nhà mẫu</a></li> --}}

                    <li id="responsive-menu-item-128"
                        class=" menu-item menu-item-type-taxonomy menu-item-object-category responsive-menu-item">
                        <a href="{{route('news')}}" class="responsive-menu-item-link">Tin tức</a></li>
                <li id="responsive-menu-item-128"
                    class=" menu-item menu-item-type-taxonomy menu-item-object-category responsive-menu-item">
                    <a href="{{route('page.ky_gui')}}" class="responsive-menu-item-link">Ký gửi</a></li>
                    {{-- <li id="responsive-menu-item-444"
                        class=" menu-item menu-item-type-taxonomy menu-item-object-category responsive-menu-item">
                        <a href="{{route('tien-do.list')}}" class="responsive-menu-item-link">Tiến độ</a></li> --}}
            </ul>
            <div id="responsive-menu-additional-content"></div>
        </div>
    </div>
    <link rel='stylesheet' id='addthis_output-css' href="{{ asset('goldview/chucnang/addthis/css/output.css') }}" type='text/css' media='all' />
    <script type='text/javascript'>
        /* <![CDATA[ */
var ajax = {"url":"http:\/\/canhothegoldview.com\/dangnhapweb\/admin-ajax.php"};
/* ]]> */

    </script>
    <script type='text/javascript' src=" {{ asset('goldview/chucnang/ultimate-wp-query-search-filter/classes/scripts/uwpqsfscript5152.js?ver=1.0') }}"></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
var newsletter = {"messages":{"email_error":"The email is not correct","name_error":"The name is not correct","surname_error":"The last name is not correct","privacy_error":"You must accept the privacy statement"},"profile_max":"20"};
/* ]]> */

    </script>
    <script type='text/javascript' src=" {{ asset('goldview/chucnang/newsletter/subscription/validated227.js?ver=4.9.0') }}"></script>
    <script type='text/javascript' src=" {{ asset('goldview/baogom/js/wp-embed.mind714.js?ver=4.7.10') }}"></script>
    <script type='text/javascript' src=" {{ asset('goldview/chucnang/js_composer/assets/js/dist/js_composer_front.min2720.js?ver=4.11.2') }}"></script>
    <!--wp_footer-->
    <script type="text/javascript">
        (function(d,g){d[g]||(d[g]=function(g){return this.querySelectorAll("."+g)},Element.prototype[g]=d[g])})(document,"getElementsByClassName");(function(){var classes = document.getElementsByClassName('menu_control');for (i = 0; i < classes.length; i++) {classes[i].onclick = function() {var menu = this.nextElementSibling;if (/show_menu/.test(menu.className))menu.className = menu.className.replace('show_menu', '').trim();else menu.className += ' show_menu';if (/menu_control_triggered/.test(this.className))this.className = this.className.replace('menu_control_triggered', '').trim();else this.className += ' menu_control_triggered';};}})();
    </script>
    <noscript><style type="text/css" scoped>.menu { display: block; }</style></noscript>
    
</body>

</html>
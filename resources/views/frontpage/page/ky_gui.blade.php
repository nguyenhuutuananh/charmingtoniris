@extends('frontpage.layout') 
@section('title')Ký gửi
@endsection
 
@section('content')
<style>
    .medium {
        border-radius: unset !important;
        text-transform: uppercase !important;
        font-weight: bold !important;
        font-size: 14px !important;
        background-color: #efefef !important;
        color: #777777 !important;
        border-color: #777777 !important;
        padding-left: 10px !important;
    }
    .lienhe {
        border: 1px #d8d8d8 solid !important;
        background: #ffffff !important;
        border-radius: 10px;
        padding-top: 0px !important;
    }
    .form-title {
        background-color: #f3f3f3;
        margin-left: -10px !important;
        margin-right: -10px;
        border-top-left-radius: 10px;
        border-top-right-radius: 10px;
        padding: 15px;
        font-size: 18px;
    }
    .form-title input[type="radio"] {
        margin-left: 50px;
    }
    .content .gform_wrapper .gform_footer input.button, .content .gform_wrapper .gform_footer input[type="submit"] {
        padding: 10px 100px !important;
    }


</style>
<script src='https://www.google.com/recaptcha/api.js'></script>
<div id='breadcrumbs'>
    <div class="container">
        <strong>Đang xem:</strong>
        <a href="{{url('/')}}" title="{{__c('web_title')}}">{{__c('web_title')}}</a> / Ký gửi</div>
    <div class="clear">

    </div>
    <!--//.container-->
</div>
<div class="container">
    <div class="column">
        <div class="content">
            <div id="post-26" class="post_box top" itemscope itemtype="http://schema.org/Article">
                <meta itemscope itemprop="mainEntityOfPage" itemType="https://schema.org/WebPage" itemid="http://canhothegoldview.com/tien-ich/"
                />
                <h1 class="headline" itemprop="headline">Ký gửi</h1>
                <div class="post_content" itemprop="articleBody">
                    <div class="lienhe">
                        <div class="form-title">
                                Ký gửi: <input type="radio" name="rent_or_sale" id=""> BÁN <input type="radio" name="rent_or_sale"
                                id=""> THUÊ <input type="radio" name="rent_or_sale" id=""> BÁN & THUÊ
                        </div>
                        
                        <div class="gf_browser_gecko gform_wrapper">
                            <form action="#" method="post">
                                @csrf
                                <div class="gform_body">
                                    <ul class="gform_fields top_label description_below">
                                        <li class="gfield gf_left_half gfield_contains_required">
                                            <label for="input_2_1" class="gfield_label">Loại căn hộ <span class="gfield_required">*</span></label>
                                            <div class="ginput_container">
                                                <select name="project_category" id="" class="medium">
                                                    @foreach (\App\ProjectCategory::all() as $project_category)
                                                    <option value="{{$project_category->title}}">{{$project_category->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </li>
                                        <li class="gfield gf_right_half gfield_contains_required">
                                            <label for="input_2_1" class="gfield_label">Tháp <span class="gfield_required">*</span></label>
                                            <div class="ginput_container">
                                                <select name="thap" id="" class="medium">
                                                    <option value="Tháp A1">Tháp A1</option>
                                                    <option value="Tháp A2">Tháp A2</option>
                                                    <option value="Tháp A3">Tháp A3</option>
                                                    <option value="Tháp B">Tháp B</option>
                                                </select>
                                            </div>
                                        </li>
                                        <li class="gfield gf_left_half gfield_contains_required">
                                            <label for="input_2_1" class="gfield_label">Tầng <span class="gfield_required">*</span></label>
                                            <div class="ginput_container">
                                                <input type="text" name="tang" class="medium" />
                                            </div>
                                        </li>
                                        <li class="gfield gf_right_half gfield_contains_required">
                                            <label for="input_2_1" class="gfield_label">Vị trí căn <span class="gfield_required">*</span></label>
                                            <div class="ginput_container">
                                                <input type="text" name="vi_tri_can" class="medium" />
                                            </div>
                                        </li>
                                        <li class="gfield gf_left_half gfield_contains_required">
                                            <label for="input_2_1" class="gfield_label">Hiện trạng nội thất <span class="gfield_required">*</span></label>
                                            <div class="ginput_container">
                                                <select name="furniture" id="" class="medium">
                                                    @foreach (\App\Furniture::all() as $furniture)
                                                    <option value="{{$furniture->title}}">{{$furniture->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </li>
                                        <li class="gfield gf_right_half gfield_contains_required">
                                            <label for="input_2_1" class="gfield_label">Hiện trạng nhà <span class="gfield_required">*</span></label>
                                            <div class="ginput_container">
                                                <input type="text" name="hien_trang_nha" class="medium" />
                                            </div>
                                        </li>
                                        <li class="gfield gf_left_half gfield_contains_required">
                                            <label for="input_2_1" class="gfield_label">Giá bán - giá thuê (tháng)<span class="gfield_required">*</span></label>
                                            <div class="ginput_container">
                                                <input type="text" name="gia_ban" class="medium" />
                                            </div>
                                        </li>
                                        <li id='field_2_8' class='gfield'>
                                            <label class='gfield_label' for='input_2_8'>Captcha</label>
                                            <div class="ginput_container">
                                                <div class="g-recaptcha" data-sitekey="6Lfv11QUAAAAAPtDwN3dwqv_xgScxFbUfL3PcSDp" data-theme="light"></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="gform_footer top_label">
                                    <input type="button" id="gform_submit_button_2" class="gform_button button" value="Gửi yêu cầu" tabindex="6">
                                </div>
                            </form>

                        </div>
                        <script type='text/javascript'>
                            jQuery(document).ready(function () {
                                $('#gform_submit_button_2').on('click', function(e) {
                                    if(window["gf_submitting_2"]){return false;}
                                    var response = grecaptcha.getResponse();
                                    if(response.length == 0){
                                        alert('Vui lòng xác thực captcha!');
                                        return;
                                    }
                                    window["gf_submitting_2"]=true;
                                    const form = $(this).closest('form');
                                    $.ajax({
                                        url: "{{route('contact.save_special')}}",
                                        method: "POST",
                                        data: $(form).serialize(),  
                                        dataType: 'json'
                                    })
                                    .done(function(data) {
                                        if(data.result) {
                                            $(form).find('input[name!="_token"][type!="button"]').val('');
                                            $(form).find('textarea').val('');
                                            grecaptcha.reset();
                                        }
                                        else {
                                            grecaptcha.reset();
                                        }
                                        alert(data.message);
                                    })
                                    .always(function() {
                                        window["gf_submitting_2"]=false; 
                                    });
                                    
                                });
                            });
                        </script>
                        <div class="clear"></div>
                    </div>
                    <p style="text-align: justify;">
                        <!-- AddThis Sharing Buttons below -->
                        <div class="addthis_toolbox addthis_default_style" addthis:url='{{url()->current()}}'>
                            <a class="addthis_button_facebook_like"></a>
                            <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
                            <a class="addthis_button_pinterest_pinit"></a>
                            <a class="addthis_button_tweet"></a>
                            <a class="addthis_counter addthis_pill_style"></a>
                        </div>
                    </p>
                </div>
            </div>
        </div>
        {{-- sidebar --}}
    @include('frontpage.sidebar') {{-- end sidebar --}}
    </div>
</div>
@endsection
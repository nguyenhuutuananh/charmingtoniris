@extends('frontpage.layout')
@section('title'){{$page->title}}
@endsection
@section('content')
<div id='breadcrumbs'>
    <div class="container">
        <strong>Đang xem:</strong>
        <a href="{{url('/')}}" title="{{__c('web_title')}}">{{__c('web_title')}}</a> 
        / {{$page->title}}</div>
    <div class="clear">

    </div>
    <!--//.container-->
</div>
<div class="container">
    <div class="column">
        <div class="content">
            <div id="post-26" class="post_box top" itemscope itemtype="http://schema.org/Article">
                <meta itemscope itemprop="mainEntityOfPage" itemType="https://schema.org/WebPage" itemid="http://canhothegoldview.com/tien-ich/"
                />
                <h1 class="headline" itemprop="headline">{{$page->title}}</h1>
                <div class="post_content" itemprop="articleBody">
                    {!! $page->content !!}
                    <p style="text-align: justify;">
                        <!-- AddThis Sharing Buttons below -->
                        <div class="addthis_toolbox addthis_default_style" addthis:url='{{url()->current()}}'>
                            <a class="addthis_button_facebook_like"></a>
                            <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
                            <a class="addthis_button_pinterest_pinit"></a>
                            <a class="addthis_button_tweet"></a>
                            <a class="addthis_counter addthis_pill_style"></a>
                        </div>
                    </p>
                </div>
            </div>
        </div>
        {{-- sidebar --}}
        @include('frontpage.sidebar')
        {{-- end sidebar --}}
    </div>
</div>
@endsection
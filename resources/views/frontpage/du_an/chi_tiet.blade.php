@extends('frontpage.layout') 
@section('title'){{$project->title}}
@endsection
 
@section('content')
<div id='breadcrumbs'>
    <div class="container">
        <strong>Đang xem:</strong>
        <a href="{{url('/')}}" title="{{__c('web_title')}}">{{__c('web_title')}}</a>        /
        <a href="{{route('project.list', ['slug' => $project->category->slug])}}" rel="category tag">
            {{$project->category->title}}</a> 
        » {{$project->title}}</div>
    <div class="clear"></div>
    <!--//.container-->
</div>
<div class="container">
    <div class="suaanh">
    </div>
    <div class="column">
        <div class="content">
            <div id="{{$project->slug}}" class="post_box top" itemscope itemtype="http://schema.org/Article">
                <meta itemscope itemprop="mainEntityOfPage" itemType="https://schema.org/WebPage" itemid="http://canhothegoldview.com/ban-can-ho-the-goldview-3pn2wc-gia-tot/"
                />
                <h1 class="headline" itemprop="headline">{{$project->title}}</h1>
                <div class="chitietbai">
                    <div class="col1">
                        <ul id="slideshow340" class="slideshow340" style="display:none;">
                            <!-- From a WordPress post/page -->
                            @foreach ($project->images as $project_image)
                            <li>
                                <h3 style="opacity:70;">{{$project_image->name}}</h3>

                                <span data-alt="{{$project_image->name}}">{{url($project_image->imageUrl)}}</span>
                                <p>
                                    <!-- AddThis Sharing Buttons above -->
                                    <!-- AddThis Sharing Buttons below -->
                                </p>
                                <a href="{{url($project_image->imageUrl)}}" id="project_img_{{$project_image->id}}" class="colorboxslideshow340"
                                    data-rel="slideshowgroup340" title="{{$project_image->name}}">
                                        <img src="{{url($project_image->imageUrl)}}" alt="{{$project_image->name}}"
                                        />
                                    </a>
                            </li>
                            @endforeach
                        </ul>

                        <div id="slideshow-wrapper340" class="slideshow-wrapper">

                            <div class="slideshow-fullsize" id="fullsize340">
                                <div id="imgprev340" class="slideshow-imgprev imgnav" title="Previous Image">Previous Image</div>
                                <a id="imglink340" class="slideshow-imglink imglink">
                                    <!-- link -->
                                </a>
                                <div id="imgnext340" class="slideshow-imgnext imgnav" title="Next Image">Next Image</div>
                                <div id="image340" class="slideshow-image"></div>
                                <div class="slideshow-information infobottom" id="information340">
                                    <h3 class="slideshow-info-heading">info heading</h3>
                                    <p class="slideshow-info-content">info content</p>
                                </div>
                            </div>

                            <div id="thumbnails340" class="slideshow-thumbnails thumbsbot">
                                <div class="slideshow-slideleft" id="slideleft340" title="Slide Left"></div>
                                <div class="slideshow-slidearea" id="slidearea340">
                                    <div class="slideshow-slider" id="slider340"></div>
                                </div>
                                <div class="slideshow-slideright" id="slideright340" title="Slide Right"></div>
                                <br style="clear:both; visibility:hidden; height:1px;" />
                            </div>
                        </div>

                        <!-- Slideshow Gallery Javascript BEG -->
                        <script type="text/javascript">
                            jQuery.noConflict();
                                tid('slideshow340').style.display = "none";
                                tid('slideshow-wrapper340').style.display = 'block';
                                tid('slideshow-wrapper340').style.visibility = 'hidden';
                                jQuery("#fullsize340").append(
                                    '<div id="spinner340"><i class="fa fa-cog fa-spin"></i></div>');
                                tid('spinner340').style.visibility = 'visible';

                                var slideshow340 = new TINY.slideshow("slideshow340");
                                jQuery(document).ready(function () {
                                    slideshow340.auto = true;
                                    slideshow340.speed = 4;
                                    slideshow340.effect = "slide";
                                    slideshow340.slide_direction = "lr";
                                    slideshow340.easing = "swing";
                                    slideshow340.alwaysauto = true;
                                    slideshow340.autoheight = true;
                                    slideshow340.autoheight_max = false;
                                    slideshow340.imgSpeed = 8;
                                    slideshow340.navOpacity = 89;
                                    slideshow340.navHover = 90;
                                    slideshow340.letterbox = "#000000";
                                    slideshow340.linkclass = "linkhover";
                                    slideshow340.imagesid = "images340";
                                    slideshow340.info = "";
                                    slideshow340.infoonhover = 0;
                                    slideshow340.infoSpeed = 10;
                                    slideshow340.infodelay = 0;
                                    slideshow340.infofade = 0;
                                    slideshow340.infofadedelay = 0;
                                    slideshow340.thumbs = "slider340";
                                    slideshow340.thumbOpacity = 70;
                                    slideshow340.left = "slideleft340";
                                    slideshow340.right = "slideright340";
                                    slideshow340.scrollSpeed = 5;
                                    slideshow340.spacing = 5;
                                    slideshow340.active = "";
                                    slideshow340.imagesthickbox = "false";
                                    jQuery("#spinner340").remove();
                                    slideshow340.init("slideshow340", "image340", "imgprev340", "imgnext340",
                                        "imglink340");
                                    tid('slideshow-wrapper340').style.visibility = 'visible';
                                    jQuery(window).trigger('resize');

                                });

                                jQuery(window).resize(function () {
                                    var width = jQuery('#slideshow-wrapper340').width();
                                    var resheight = 50;
                                    var height = Math.round(((resheight / 100) * width));
                                    jQuery('#fullsize340').height(height);
                                });
                        </script>

                        <!-- Slideshow Gallery Javascript END -->
                        <!-- Slideshow Gallery CSS BEG -->
                        <link rel="stylesheet" property="stylesheet" href="{{asset('goldview/chucnang/slideshow-gallery/views/default/css-responsive784c.css?id=340')}}"
                            type="text/css" media="all" />

                        <!--[if IE 6]>
            <style type="text/css">
            .imglink, #imglink { display: none !important; }
            .linkhover { display: none !important; }
            </style>
            <![endif]-->

                        <!-- Slideshow Gallery CSS END -->
                    </div>
                    <div class="col2">
                        <div class="ct1">GIÁ</div>
                        <div class="ct2">{{$project->price_text}}</div>
                        <div class="ct3"></div>
                        <div class="clear"></div>
                        <table>
                            <tr>
                                <td class="fl">Diện tích</td>
                                <td class="fr">{{$project->acreages}}m
                                        <sup>2</sup></td>
                            </tr>
                            <tr>
                                <td class="fl">Phòng ngủ</td>
                                <td class="fr">{{$project->bed_rooms}}</td>
                            </tr>
                            <tr>
                                <td class="fl">WC</td>
                                <td class="fr">{{$project->toilets}}</td>
                            </tr>
                            <tr>
                                <td class="fl">Garage</td>
                                <td class="fr">{{$project->garages}} chỗ</td>
                            </tr>
                            <tr>
                                <td class="fl">Nội thất</td>
                                <td class="fr">{{$project->furniture->title}}</td>
                            </tr>

                        </table>
                    </div>
                    <div class="clear"></div>
                    <div class="ttct">
                        <span>THÔNG TIN CHI TIẾT</span>
                    </div>
                </div>
                <div class="post_content" itemprop="articleBody">
                    {!! $project->content !!}
                    <!-- AddThis Sharing Buttons below -->
                    <div class="addthis_toolbox addthis_default_style" addthis:url='{{url('/')}}'>
                        <a class="addthis_button_facebook_like"></a>
                        <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
                        <a class="addthis_button_pinterest_pinit"></a>
                        <a class="addthis_button_tweet"></a>
                        <a class="addthis_counter addthis_pill_style"></a>
                    </div>
                </div>
                @include('frontpage.lien_he', ['project_id' => $project->id])
                <div class="lienquan">
                    <div class="block-related">
                        <h3>Các căn hộ khác</h3>

                        <div class="grid-column-2">
                            @foreach (\App\Project::where('category_id', $project->category_id)->inRandomOrder()->skip(0)->take(4)->get() as $relateProject)
                            <div class="item grid wow fadeInDown">
                                <div class="grid-icon">
                                    <a href="{{route('project.detail', ['category_slug' => $relateProject->category->slug, 'slug' => $relateProject->slug])}}">
                                            <img width="370" height="235" src="{{url($relateProject->images()->first()->imageUrl)}}"
                                                class="attachment-thumbnail size-thumbnail wp-post-image" alt="" />
                                        </a>
                                    <span class="grid-price">{{$relateProject->price_text}}</span>
                                    <span class="grid-category">{{$relateProject->category->title}}</span>
                                </div>
                                <div class="grid-bottom">
                                    <h2 class="headline">
                                        <a href="{{route('project.detail', ['category_slug' => $relateProject->category->slug, 'slug' => $relateProject->slug])}}">
                                                <span class="title">{{$relateProject->title}}</span>
                                            </a>
                                    </h2>
                                    <span class="grid-sum f13">
                                        {{str_limit($relateProject->description, 120)}}
                                    </span>
                                    <table class="grid-features">
                                        <tr>
                                            <th>Diện tích</th>
                                            <th>Garage</th>
                                            <th>Phòng ngủ</th>
                                            <th>Toilet</th>
                                        </tr>
                                        <tr>
                                            <td><img src="{{ asset('goldview/tnr/img/i1.png') }}" />{{$relateProject->acreages}} m<sup>2</sup></td>
                                            <td><img src="{{ asset('goldview/tnr/img/i2.png') }}" />{{$relateProject->garages}} chỗ</td>
                                            <td><img src="{{ asset('goldview/tnr/img/i3.png') }}" />{{$relateProject->bed_rooms}} PN</td>
                                            <td><img src="{{ asset('goldview/tnr/img/i4.png') }}" />{{$relateProject->toilets}} WC</td>
                                        </tr>
                                    </table>
                                </div>
                                <a class="btn-read-more" href="{{route('project.detail', ['category_slug' => $relateProject->category->slug, 'slug' => $relateProject->slug])}}">Xem chi tiết...</a>
                            </div>
                            @endforeach
                        </div>

                        <div class="text-center">
                            <a class="btn-clear-black" href="{{route('project.list', ['slug' => $project->category->slug])}}">Xem thêm + </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- sidebar --}}
        @include('frontpage.sidebar')
        {{-- end sidebar --}}
    </div>
</div>
@endsection
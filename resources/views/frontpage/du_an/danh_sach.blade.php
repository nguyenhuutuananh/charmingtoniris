@extends('frontpage.layout') 
@section('title'){{$project_category->title}}
@endsection
 
@section('content')
<div id='breadcrumbs'>
    <div class="container">
        <strong>Đang xem:</strong>
        <a href="{{url('/')}}" title="{{__c('web_title')}}">{{__c('web_title')}}</a>        /
        <a href="{{route('project.list', ['slug' => $project_category->slug])}}" rel="category tag">{{$project_category->title}}</a>
    </div>
    <div class="clear"></div>
    <!--//.container-->
</div>
<div class="container">
    <div class="column">
        <div class="content">
            @foreach ($project_category->projects()->paginate(10) as $project)
            <div id="{{$project->slug}}" class="post_box top" itemscope itemtype="http://schema.org/Article">
                <div class="thuoctinhchuyenmuc">
                    <div class="item grid wow fadeInDown">
                        <div class="grid-icon">
                            <a href="{{route('project.detail', ['category_slug' => $project->category->slug, 'slug' => $project->slug])}}">
                                    <img width="370" height="235" src="{{url($project->images()->first()->imageUrl)}}"
                                        class="attachment-thumbnail size-thumbnail wp-post-image" alt="" />
                                </a>
                            <span class="grid-price">{{$project->price_text}}</span>
                            <span class="grid-category">
                                    <a href="#" title="{{$project->category->title}}">{{$project->category->title}}</a>
                                </span>
                        </div>
                        <div class="grid-bottom">
                            <h2 class="headline">
                                <a href="{{route('project.detail', ['category_slug' => $project->category->slug, 'slug' => $project->slug])}}">
                                        <span class="title">{{$project->title}}</span>
                                    </a>
                            </h2>
                            <span class="grid-sum f13">{{str_limit($project->description, 150)}}</span>
                            <table class="grid-features">
                                <tr>
                                    <th>Diện tích</th>
                                    <th>Garage</th>
                                    <th>Phòng ngủ</th>
                                    <th>Toilet</th>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="{{asset('goldview/tnr/img/i1.png')}}" />{{$project->acreages}} m
                                        <sup>2</sup>
                                    </td>
                                    <td>
                                        <img src="{{asset('goldview/tnr/img/i2.png')}}" />{{$project->garages}} chỗ</td>
                                    <td>
                                        <img src="{{asset('goldview/tnr/img/i3.png')}}" />{{$project->bed_rooms}} PN</td>
                                    <td>
                                        <img src="{{asset('goldview/tnr/img/i4.png')}}" />{{$project->toilets}} WC</td>
                                </tr>
                            </table>
                        </div>
                        <a class="btn-read-more" href="{{route('project.detail', ['category_slug' => $project->category->slug, 'slug' => $project->slug])}}">Xem chi tiết...</a>
                    </div>
                </div>
            </div>
            @endforeach
            {{-- <div class="phantrang">
            </div> --}}
            <style>
                .pagination {
                    list-style: none;
                    display: inline-block;
                }
                .pagination .page-item {
                    padding-right: 10px;
                    display: inline-block;
                }
            </style>
            {{ $project_category->projects()->paginate(10)->links() }}
        </div>
        {{-- sidebar --}}
        @include('frontpage.sidebar')
        {{-- end sidebar --}}
    </div>
</div>
@endsection
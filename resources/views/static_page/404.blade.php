<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{__c('web_title')}}</title>
</head>
<body>
    <h3>Ôi! Trang này không tồn tại!</h3>
    @if (isset($suggestPage))
        Trang gợi ý: <a href="{{$suggestPage}}">{{$suggestPage}}</a>
    @endif
</body>
</html>
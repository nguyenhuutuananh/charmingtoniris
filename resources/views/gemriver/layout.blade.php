<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns:fb="http://ogp.me/ns/fb#">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="format-detection" content="telephone=no" />
    <link rel="shortcut icon" type="image/x-icon" href="{{url(__c('favicon'))}}" />
    <link rel="stylesheet" href="{{ asset('gem/template/tint/lib/bootstrap/css/bootstrap.css') }}" type="text/css" />
    <!-- Custom fonts for this template -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('gem/template/tint/css/animate.min.css') }}" type="text/css" />
    <!-- <link href="https://fonts.googleapis.com/css?family=Athiti:400,500,600,700&amp;subset=vietnamese" rel="stylesheet">-->




    <link rel="stylesheet" href="{{ asset('gem/template/tint/css/template.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('gem/template/tint/css/gem-mobile.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('gem/template/tint/css/gem-tablet.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('gem/template/tint/css/gem-pc.css') }}" type="text/css" />

    <link rel="stylesheet" href="{{ asset('gem/template/tint/css/jquery.fullPage.css') }}" type="text/css" />

    <link rel="stylesheet" href="{{ asset('gem/template/tint/lib/ionicons/css/ionicons.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('gem/template/tint/css/animations.css') }}" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('gem/template/tint/lib/slick/slick.css') }}" />

    <link rel="stylesheet" href="{{ asset('gem/template/tint/lib/fancybox/jquery.fancybox.css') }}" type="text/css" />

    <script type="text/javascript" src="{{ asset('gem/template/tint/js/jquery-1.10.2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('gem/template/tint/js/jquery.tint.js') }}"></script>
    {!! SEO::generate() !!}
    
</head>

<body class=" lang-vi-VN homepage ">
    <form name="tqForm" method="post" action="{{url('/')}}" id="tqForm" class="tqForm">
        <div id="divMsg" class="divMsg">

        </div>

        <header class="menu-sticky" id="header">
            <nav id="main-menu" class="navbar" role="navigation">
                <div class="container">
                    <div class="row">
                        <div class="navbar-header">
                            <a href="{{route('home')}}" id="logo">
                                <img src="{{ url(__c('logo')) }}" class="logo" alt="Gem Reiverside" title="Gem Reiverside" />
                            </a>
                            <div class="navbar-gem" aria-expanded="false">
                                <ul class="nav navbar-nav">
                                    <li class="home"><a href="{{route('home')}}"></a></li>
                                    <!-- <li><a href="/gioi-thieu.html">Giới thiệu</a>-->
                                    <li><a>Giới thiệu</a>
                                        <ul id="menuAbout" class="sub-menu">
                                            <li><a href='{{route('tong_quan')}}'>Tổng quan dự án</a></li>
                                            <li><a href='{{route('chu_dau_tu')}}'>Chủ đầu tư</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="{{route('vi_tri')}}">Vị trí</a></li>
                                    <li><a href="{{route('mat_bang')}}">MẶT BẰNG CĂN HỘ</a></li>
                                    <li><a href="{{route('tien_ich')}}">TIỆN ÍCH</a></li>
                                    <li><a href="{{route('news')}}">TIN TỨC</a></li>
                                    <li><a href="{{route('thu_vien')}}">THƯ VIỆN</a></li>
                                    <li><a href="{{route('contact')}}">LIÊN HỆ</a></li>
                                </ul>

                                <div class="menu-close visible-xs-block visible-sm-block">
                                    <div class="line line-1"></div>
                                    <div class="line line-2"></div>
                                </div>
                            </div>
                            <a target="_blank" href="tel:{{__c('hotline')}}
                            " id="hotline">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <span>{{__c('hotline')}}</span>
                            </a>
                            
                            <div class="menu-gem-open">
                                <div class="menu-open visible-xs-block visible-sm-block">
                                    <div class="line"></div>
                                    <div class="line"></div>
                                    <div class="line"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>

        </header>
        <a href="#" class="register">
            <img src='{{ asset('gem/template/tint/images/dk.gif') }}' class='img-responsive'></a>


        
        @yield('content')
        
        
        <div id="vinhhalong_chitiet" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">VỊNH HẠ LONG GIỮA LÒNG SÀI GÒN</h4>
                    </div>
                    <div class="modal-body scrollbar-inner">
                        <p style="text-align: justify;">Với lịch sử hình thành từ lâu đời và truyền thuyết về nguồn cội dòng dõi “con rồng cháu tiên”, Vịnh
                            Hạ Long trở thành niềm tự hào của đất nước Việt Nam khi thắng cảnh thiên nhiền này được UNESCO
                            công nhận là di sản thiên nhiên thế giới vào năm 1994, và được tổ chức New7Wonder công nhận là
                            “1 trong 7 kỳ quan thiên nhiên thế giới mới” vào năm 2011. Quả thật, từ hơn 500 năm về trước,
                            vẻ đẹp của vịnh Hạ Long đã được thi hào Nguyễn Trãi ca tụng và ví von là "kỳ quan đá dựng giữa
                            trời cao"trong những áng văn chương lưu truyền.</p>
                        <p style="text-align: justify;">Không chỉ có vậy, ngược dòng chảy lịch sử trở về tìm hiểu về thời cha ông xa xưa, Vịnh Hạ Long không
                            chỉ mang trong mình vẻ đẹp non nước hùng vĩ mà còn chứa đựng trong mình những truyền thuyết hào
                            hùng về quá trình chống giặc ngoại xâm lăng. Tương truyền rằng con dân Đại Việt với nguồn cội
                            thanh cao “con Rồng cháu Tiên” nên đã được Ngọc Hoàng cử đàn rồng hạ giới giúp con dân Đại Việt
                            đánh đuổi giặc ra khỏi bờ cõi. Sau khi giặc tan, thấy cảnh mặt đất thanh bình, cây cối tươi tốt,
                            đức tính con người cần cù, chịu khó, đoàn kết giúp đỡ nhau, đàn rồng đã không trở về trời mà
                            quyết định ở lại hạ giới, nơi vừa diễn ra trận chiến để muôn đời trấn giữ bờ cõi Đại Việt. Vị
                            trí Rồng đáp xuống là vịnh nước xanh tươi, núi non hùng vĩ, từ đó được gọi tên là vịnh Hạ Long.</p>
                        <p style="text-align: justify;">Vẻ đẹp kỳ quan thiên nhiên Vịnh Hạ Long hiện ra sống động như một bức tranh thủy mặc mà tạo hóa đã
                            ưu ái ban tặng cho Việt Nam. Dưới bàn tay của mẹ thiên nhiên, những khối núi đá vôi quần tụ với
                            nhau, nổi bật trên mặt nước xanh biếc đặc trưng, đã trở thành thương hiệu du lịch, là biểu tượng
                            cảnh sắc thiên nhiên độc đáo mà bất cứ ai cùng đều mong ước một lần được đặt chân đến, tận mắt
                            nhìn ngắm cho thỏa lòng mê say.</p>
                        <p style="text-align: justify;">Và với quyết tâm mang vẻ đẹp rất riêng của Vịnh Hạ Long vào công trình hạ tầng hiện đại, những kiến
                            trúc sư xuất sắc của tập đoàn CPG Consultants hàng đầu tại đất nước Singapore đã dành tâm huyết,
                            mang thần thái vẻ đẹp kỳ quan này vào công trình Gem Riverside với thông điệp “Vịnh Hạ Long giữa
                            lòng Sài Gòn”!</p>
                        <p style="text-align: justify;">Giữa Sài Gòn hối hả, người người tấp nập, Gem Riverside trở thành chốn bình yên sau chuỗi ngày dài
                            lao động hăng say. Tọa lạc tại Quận 2 - vị trí trọng yếu phát triển nhất của khu Đông thành phố,
                            trên trục đường song hành cao tốc Long Thành- Dầu Giây, một kỳ quan “Vịnh Hạ Long” rất riêng
                            với tên gọi Gem Riverside hiên ngang nổi bật giữa lòng phố thị Sài Gòn với 12 block cao tầng
                            sắp xếp vị trí uốn lượn như đàn mãnh rồng nằm phục trên mặt sông xanh. Gem Riverside nổi bật
                            với hai mặt tiền hướng sông thoáng mát, hội tủ hơn 45 tiện ích nội khu cao cấp và hơn 60 tiện
                            ích xung quanh. Bên cạnh đó, hệ thống giao thông thuận lợi, cũng như uy tín lâu năm từ chủ đầu
                            tư Tập đoàn Đất Xanh, cư dân tương lai của Gem riverside sẽ hoàn toàn an tâm với sự chọn lựa
                            thông minh này. Có thể khẳng định, Gem Riverside chính là lựa chọn sống đẳng cấp, sang trọng
                            và tinh tế hàng đầu dành thế hệ cư dân hiện đại ngày nay.</p>
                    </div>
                </div>
            </div>
        </div>



        <link rel="stylesheet" href="{{ asset('gem/template/tint/lib/vegas/vegas.css') }}" type="text/css" />
        <script type="text/javascript" src="{{ asset('gem/template/tint/lib/vegas/vegas.min.js') }}"></script>

        @if(url('/') == url()->current())
        <script type="text/javascript" src="{{ asset('gem/template/tint/js/gem-index.js') }}"></script>
        @endif
        <link href="{{ asset('gem/template/tint/lib/scrollbar/jquery.scrollbar.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('gem/template/tint/lib/scrollbar/scroll-custom.css') }}" rel="stylesheet" type="text/css" />
        <script src="{{ asset('gem/template/tint/lib/scrollbar/jquery.scrollbar.js') }}" type="text/javascript"></script>
        <script>
            var slide = [];
        @php
            $slider = \App\Slider::find(1);
        @endphp

        @foreach ($slider->images as $detail)
        slide.push({ src: "{{url($detail->imageUrl)}}" });
        @endforeach
        
    $("#slider").vegas({
        slides: slide,
        timer: false,
        animationDuration: 10000,
        animation: ['kenburns'],
    });
    $('#slider .title').addClass('fadeInLeft delay-500 animated');
    $('#slider .description').addClass('fadeInLeft delay-750 animated');
	$('.scrollbar-inner').scrollbar();
	//$('.modal-dialog').dialog('option', 'position', 'center');
	$("#news-blocks .row .col-sm-6:nth-child(2n)").after("<div class='clearfix'></div>");
        </script>


        <div class="hidden">
            <div id="startpopup" class="startpopup">
                <div class="content">
                    <a href="gioi-thieu/tong-quan-du-an.html"><img alt="" src="{{ asset('gem/assets/uploads/myfiles/images/gioi-thieu/gioi-thieu.jpg') }}" /></a>
                </div>
            </div>
        </div>
        <div class="register-price">
            <div class="register-price-form">
                <div class="wrap-border"></div>
                <div class="wrap-form">
                    <div class="page-branch-title bg-w">
                        <div class="title color-w">Đặt lịch xem nhà mẫu</div>
                    </div>
                    <p>Quý khách vui lòng điền đầy đủ thông tin bên dưới để đặt lịch xem nhà mẫu và nhận được thông tin chính
                        thức của chủ đầu tư về dự án GEM và các chương trình khuyến mãi.</p>
                    <div class="row">
                        <div class="col-xs-12">
                            <input type="text" id="name" name="name" class="textbox name" placeholder="Họ &amp; Tên (*)" />
                        </div>
                        <div class="col-xs-12">
                            <input type="text" id="txtEmailP" name="txtEmailP" class="textbox txtEmailP" placeholder="Email (*)" />
                        </div>
                        <div class="col-xs-6">
                            <input type="number" id="phone_number" name="phone_number" class="textbox phone_number" placeholder="Điện thoại (*)" />
                        </div>
                        <div class="col-xs-6">
                            <input type="text" id="txtDateP" name="txtDateP" class="textbox datepicker txtDateP hasDatepicker" placeholder="Ngày xem nhà (*)"
                                readonly />
                        </div>

                        <div class="col-xs-6">
                            <input data-key="CaptchaPopup" type="text" id="txtCaptchaP" name="txtCaptchaP" class="textbox captcha txtCaptchaP block-left"
                                placeholder="Mã xác nhận (*)" />
                        </div>
                        <div class="col-xs-6 text-left">
                            <span class="captcha-image">
                                <img class=" middle img-captcha-popup" alt="" src="#" /></span>
                        </div>

                        <div class="col-xs-12">
                            <button class="bt-register bt-re-price">Đặt lịch</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <a class="bt-close-register-price"><i class="fa fa-times" aria-hidden="true"></i></a>
            </div>
        </div>
        <!-- Modal -->
        <div id="advisoryModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-md">
                <!-- Modal content-->
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="modal-body">
                        <div class="row">
                            {{-- <div class="col-md-6">
                                <img src="{{ asset('gem/template/tint/images/sh.png') }}" alt="" class="img-responsive form-thumb" />
                            </div> --}}
                            <div class="col-md-12">
                                <form action="#" method="post">
                                    <div class="page-branch-title bg-w">
                                        <div class="title color-w">Tư vấn miễn phí</div>
                                    </div>
                                    <p class="description">ĐĂNG KÝ NGAY ĐỂ NHẬN BẢNG GIÁ CỰC HẤP DẪN</p>
                                    <div class="form-group first">
                                        @csrf
                                        <input type="text" class="textbox name form-control" placeholder="Họ &amp; Tên (*)" />
                                    </div>
                                    <div class="form-group">
                                        <input type="number" class="textbox phone_number form-control" placeholder="Điện thoại (*)" />
                                    </div>
                                    <div class="form-group">
                                        <div class="text-center text-info">Bạn quan tâm căn hộ nào?</div>
                                        <label class="radio-inline">
                                            <input type="radio" name="choices" value="1 Phòng ngủ">1 Phòng ngủ
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="choices" value="2 Phòng ngủ" checked>2 Phòng ngủ
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="choices" value="3 Phòng ngủ" >3 Phòng ngủ
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="choices" value="Shophouse">Shophouse
                                        </label>
                                    </div>
                                    <div class="form-group text-center">
                                        <button class="bt-submit bt-re-price btn btn-info">Gửi</button>
                                    </div>
                                </form>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </form>

    <script src="{{ asset('gem/template/tint/js/jquery.easings.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('gem/template/tint/js/jquery.fullPage.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('gem/template/tint/js/jquery.matchHeight-min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('gem/template/tint/lib/validate/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('gem/template/tint/lib/validate/messages_vi.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('gem/template/tint/lib/fancybox/jquery.fancybox.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('gem/template/tint/js/jquery.nicescroll.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('gem/template/tint/js/default.js') }}"></script>
    <script type="text/javascript" src="{{ asset('gem/template/tint/lib/bootstrap/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('gem/template/tint/lib/bootstrap/js/bootbox.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('gem/template/tint/lib/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('gem/template/tint/lib/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}"
        type="text/css" />

    <script type="text/javascript" src="{{ asset('gem/template/tint/lib/slick/slick.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".bt-register").click(function (event) {
                var ethis = $(this);
                var dText = $(this).html();
                event.preventDefault();
                var parent = $(this).parent().parent().parent();
                var email = $(".txtEmailP", parent);
                var name = $(".name", parent);
                var mobile = $(".phone_number", parent);
                var date = $(".txtDateP", parent);
                if (!Valid_Register(
                   name, 'Vui lòng nhập họ & tên',
                  mobile, 'Vui lòng nhập số điện thoại',
                   email, 'Vui lòng nhập email hoặc email của Quý khách không đúng định dạng',
                   date, 'Vui lòng chọn ngày xem nhà'
                )) {
                    return false;
                }
                else {
                    $(this).prop("disabled", true).html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i>');
                    $.ajax({
                        type: "POST",
                        //cache: false,
                        url: "{{route('contact.save')}}",
                        //async: false,
                        data: "func=POSTREGISTER&email=" + $(email).val() + "&name=" + $(name).val() + "&mobile=" + $(mobile).val() + "&date=" + $(date).val(),
                        dataType: "html",
                        success: function (msg) {
                            if (msg == 1) {
                                bootbox.alert("Thông tin đăng ký của quý vị đã được gửi thành công. Chúng tôi sẽ liên hệ quý vị trong thời gian sớm nhất.");
                            }
                        },
                        complete: function () {
                            $(".textbox", parent).val("");
                            $(ethis).prop("disabled", false).html(dText);
                        }
                    });
                }
                return false;
            });
            $('.hasDatepicker').datepicker({
                format: 'dd/mm/yyyy',
                startDate: '0'
            });
            $('a.register').click(function (e) {
                e.preventDefault();
                //$('.register-price').addClass('open');
                $('#advisoryModal').modal({
                    backdrop: false
                });
                return false;
            });
            $('.bt-close-register-price').click(function (e) {
                e.preventDefault(); $('.register-price').removeClass('open');
            });

            
            $("#advisoryModal .bt-submit").click(function (e) {
                e.preventDefault();
                var ethis = $(this);
                var dText = $(this).html();
                var parent = $(this).parent().parent();
                var token = $(parent).find('input[name="_token"]').val();
                var name = $(".name", parent);
                var mobile = $(".phone_number", parent);
                var content = "Căn hộ quan tâm: " + $("input:radio[name='choices']:checked", parent).val();
                
                if (!Valid_Consultant(
                   name, 'Vui lòng nhập họ & tên',
                    mobile, 'Vui lòng nhập số điện thoại'
                )) {
                    return false;
                }
                else {
                    $(this).prop("disabled", true).html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i>');
                    $.ajax({
                        type: "POST",
                        //cache: false,
                        url: "{{route('contact.save')}}",
                        //async: false,
                        data: "name=" + $(name).val() + "&phone_number=" + $(mobile).val() + "&messages=" + content + "&_token=" + token,
                        dataType: "json",
                        success: function (msg) {
                            if(msg.result) {
                                bootbox.alert("Thông tin của quý vị đã được gửi thành công. Chúng tôi sẽ liên hệ quý vị trong thời gian sớm nhất.", function () {
                                    $("#advisoryModal").modal("hide");
                                });
                            } else {
                                bootbox.alert(msg.message);
                            }
                        },
                        complete: function () {
                            $(".textbox", parent).val("");
                            $(ethis).prop("disabled", false).html(dText);
                        }
                    });
                }
                return false;
            });
            // setTimeout(function () {
            //     $('#advisoryModal').modal({
            //         backdrop: false
            //     });
            // }, 30000);

            

        });
    </script>

</body>

</html>
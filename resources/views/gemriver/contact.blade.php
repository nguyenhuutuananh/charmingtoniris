@extends('gemriver.layout') 
@section('content')
<section id="contact">
    <div class="container">
        <div class="bread-crumb">
            <a href="#" class="home">Trang chủ /</a>
            <a href="#">Liên hệ</a>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h2>Liên hệ</h2>
            </div>
            <div class="col-lg-12">
                <style>
                    #content {
                        color: black;
                    }
                </style>
                <div id="shortDesc" class="google-maps">

                </div>
                <script>
                var map;
                function initMap() {
                    // map = new google.maps.Map(document.getElementById('shortDesc'), {
                    // center: {lat: {{__c('bottom_web_latitude')}}, lng: {{__c('bottom_web_longtitude')}} },
                    // zoom: 8
                    // });
                    // The location of Uluru
                    var uluru = {lat: {{__c('bottom_web_latitude')}}, lng: {{__c('bottom_web_longtitude')}} };
                    // The map, centered at Uluru
                    var map = new google.maps.Map(
                        document.getElementById('shortDesc'), {zoom: 18, center: uluru});
                    // The marker, positioned at Uluru
                    var infowindow = new google.maps.InfoWindow({
                        content: `<div id="content">
                        <div id="siteNotice">
                        </div>
                        <h5 id="firstHeading" class="firstHeading">{{__c('web_title')}}</h5>
                        <div id="bodyContent">
                            <p>
                                Địa chỉ: {{__c('address')}}
                            </p>
                        </div>
                        `
                    });
                    var marker = new google.maps.Marker({position: uluru, map: map, title: "{{__c('web_title')}}"});
                    infowindow.open(map, marker);
                }
                </script>
                <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA6nXqysYPjvEPsmvdAzG5r9W1JUmTu2cM&callback=initMap"
                async defer></script>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="contact-title">Thông tin liên hệ</div>
                        <div id="longDesc" class="contact-content">
                            <p class="t">
                                DỰ ÁN CHARMINGTON IRIS</p>
                            <ul>
                                <li class="address">
                                    {{__c('address')}}
                                </li>
                                <li class="phone">
                                    {{__c('hotline')}}
                                </li>
                                
                                <li class="email">
                                    <a href="mailto:{{__c('email')}}" style="color:#0fb9ed;">{{__c('email')}}</a>
                                </li>
                                <li class="website">
                                    @php
                                    if (starts_with(Request::root(), 'http://'))
                                    {
                                        $domain = substr (Request::root(), 7); // $domain is now 'www.example.com'
                                    }
                                    else if (starts_with(Request::root(), 'https://'))
                                    {
                                        $domain = substr (Request::root(), 8); // $domain is now 'www.example.com'
                                    }
                                    @endphp
                                    <a href="{{url('/')}}" style="color:#0fb9ed;" target="_blank">{{$domain}}</a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-8 col-lg-8 contact_f">
                        <div class="contact-title">Gửi liên hệ</div>
                        <div class="contact-content">
                            <div class="row">
                                <div>
                                    <form action="#" method="post">
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                            @csrf
                                            <input type="text" id="name" name="name" class="textbox name" placeholder="Họ tên (*)" required />
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                            <input type="email" id="email" name="email" class="textbox email" placeholder="Email (*)" required />
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                            <input type="text" id="phone_number" name="phone_number" class="textbox phone_number" placeholder="Điện thoại (*)" required
                                            />
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                            <input type="text" id="address" name="address" class="textbox address" placeholder="Địa chỉ" />
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <textarea id="messages" name="messages" class="textbox textarea messages" placeholder="Nội dung (*)" rows="3"
                                                required></textarea>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <input type="button" value="Gửi" id="btnSubmit" class="cmdContactSend" />
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function (e) {
        $('.textbox').attr('autocomplete', 'off');
    });

</script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".top-head").addClass("islight");
        $('.textbox').attr('autocomplete', 'off');
        $('.cmdContactSend').click(function () {
            const button = $(this);
            const dText = $(this).html();
            const form = $(this).closest('form');
            $(this).prop("disabled", true).html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i>');
            $.ajax({
                url: "{{route('contact.save')}}",
                method: "POST",
                data: $(form).serialize(),  
                dataType: 'json'
            })
            .done(function(data) {
                if(data.result) {
                    $(form).find('input[name!="_token"][type!="button"]').val('');
                    $(form).find('textarea').val('');
                }
                
                bootbox.alert(data.message);
            })
            .always(function() {
                $(button).prop("disabled", false).html(dText);
            });

        });
        $('.bt-clear').click(function () {
            $(".textbox").removeClass('error');
            $(".textbox").val('');
        });
    });

</script>
    @include('gemriver.footer')
@endsection
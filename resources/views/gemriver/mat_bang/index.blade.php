@extends('gemriver.layout') 
@section('content')
<link rel="stylesheet" href="{{ asset('gem/template/tint/css/style-masterplans.css') }}">
<section id="house">
    <div class="container">
        <div class="bread-crumb">
            <a href='{{route('home')}}' class='home'>Trang chủ /</a>
        </div>
        {{--
        <div class="row">
            <div class="col-sm-12">
                <h1 class="page-title">Mặt bằng căn hộ</h1>
            </div>

            <div class="col-sm-6">
                <div class="sub pull-right">
                    <span class="btn-dropdown-toggle fix-title">Vui lòng chọn Block</span>
                    <ul class="select pull-right">
                        <li> <a href='mat-bang/block1-dien-hinh-3-24-27-31.html'>Block 1</a></li>
                        <li> <a href='mat-bang/block2-dien-hinh-3-24-27-31.html'>Block 2</a></li>
                        <li> <a href='mat-bang/block3-dien-hinh-3-24-27-31.html'>Block 3</a></li>
                        <li> <a href='mat-bang/block4-dien-hinh-3-24-27-31.html'>Block 4</a></li>
                        <li> <a href='mat-bang/block5-dien-hinh-3-24-27-31.html'>Block 5</a></li>
                        <li> <a href='mat-bang/block6-dien-hinh-3-24-27-31.html'>Block 6</a></li>
                        <li> <a href='mat-bang/block7-dien-hinh-3-24-27-31.html'>Block 7</a></li>
                        <li> <a href='mat-bang/block8-dien-hinh-3-24-27-30.html'>Block 8</a></li>
                    </ul>
                </div>
            </div>
            <div class="clearfix"></div>
        </div> --}} {{--
        <div class="note">
            <div id="lblShortDesc" class="box-intro-text">Vui lòng click chuột vào Tháp / Block trong hình bên dưới để xem mặt bằng tầng.</div>
        </div> --}}
        <div id="master_plan" class="imagemapster ">
            <div class="page-masterplans masterplans-home page-different" id="page-masterplans">
                <div class="parallax page-inside">
                    <div class="container page-content">
                        <h2>MẶT BẰNG TỔNG QUAN</h2>
                        <div class="box-content">
                            <div class="list-masterplan">
                                <p>Vui lòng click vào hình để xem mặt bằng tầng hoặc tuỳ chọn tầng để xem:</p>
                                <div class="sub">
                                    <span class="btn-dropdown-toggle fix-title">Chọn tầng</span>
                                    <ul class="select pull-right">
                                        <li>
                                            <a href='{{route('mat_bang_tang_1')}}'>Tầng 1</a>
                                        </li>
                                        <li>
                                            <a href='{{route('mat_bang_tang_2')}}'>Tầng 2</a>
                                        </li>
                                        <li>
                                            <a href='{{route('mat_bang_tang_3')}}'>Tầng 3</a>
                                        </li>
                                        <li>
                                            <a href='{{route('mat_bang_tang_5')}}'>Tầng 5</a>
                                        </li>
                                        <li>
                                            <a href='{{route('mat_bang_tang_6_32')}}'>Tầng 6 - 32</a>
                                        </li>
                                        <li>
                                            <a href='{{route('mat_bang_tang_33_34')}}'>Tầng 33 - 34</a>
                                        </li>

                                    </ul>
                                </div>
                            </div>

                            <div id="master_plan" class="imagemapster" data-opacity-s=".35">
                                <div class="map-hover">
                                    <div class="img-container">
                                        <div class="position-relative inline-block">
                                            <img alt="" class="main-img img-responsive inline-block img-foor" src="{{ asset('gem/assets/uploads/myfiles/images/masterplans/img-masterplan.png') }}"
                                                usemap="#can-ho" />
                                            <div class="hover-imgs">
                                                <img alt="" class="img-responsive inline-block " data-id="tang-3" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/Tang3-hover.png') }}"
                                                />
                                                <img alt="" class="img-responsive inline-block " data-id="tang-5" src="{{ asset('gem/assets/uploads/myfiles/images/masterplans/tang1-hover.png') }}"
                                                />
                                                <img alt="" class="img-responsive inline-block " data-id="tang-6-32" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/Tang6-32-hover.png') }}"
                                                />
                                                <img alt="" class="img-responsive inline-block " data-id="tang-33-34" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/Tang34-hover.png') }}"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <map class="can-ho" name="can-ho">
                                                <area coords="318, 765, 1087, 785" data-id="tang-5" href="mat-bang/tang-5.html" shape="rect" />
                                                <area coords="60, 750, 196, 774" data-id="tang-5" href="mat-bang/tang-5.html" shape="rect" />
                                                <area coords="59, 775, 260, 802" data-id="tang-3" href="mat-bang/tang-3.html" shape="rect" />
                                                <area coords="318, 788, 1088, 811" data-id="tang-3" href="mat-bang/tang-3.html" shape="rect" />
                                                <area coords="318, 88, 1088, 788" data-id="tang-6-32" href="mat-bang/tang-6-32.html" shape="rect" />
                                                <area coords="59, 77, 195, 775" data-id="tang-6-32" href="mat-bang/tang-6-32.html" shape="rect" />
                                                <area coords="58, 51, 195, 78" data-id="tang-33-34" href="mat-bang/tang-33-34.html" shape="rect" />
                                            </map>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="{{ asset('gem/template/tint/lib/imgmaps/jquery.rwdImageMaps.min.js') }}"></script>
            <script type="text/javascript" src="{{ asset('gem/template/tint/js/jquery.maphilight.js') }}"></script>
            <script type="text/javascript">
                function loadMaps() {
                            $('img[usemap]').rwdImageMaps();
                        }
            
                        $(document).ready(function () {
            
                            $(".divphoicanh a.btn-2D").addClass("active");
            
                            $("img.3D").hide();
                            $(".divphoicanh a.btn-3D").click(function () {
                                $(".divphoicanh a").removeClass("active");
                                $(this).addClass("active");
                                $("img.2D").hide();
                                $("img.3D").show();
                                return false;
                            })
                            $(".divphoicanh a.btn-2D").click(function () {
                                $(".divphoicanh a").removeClass("active");
                                $(this).addClass("active");
                                $("img.2D").show();
                                $("img.3D").hide();
                                return false;
                            })
            
                            loadMaps();
            
                            /******************* Map hover *******************/
                            var imagemapster = $(".imagemapster");
                            if (imagemapster.length >= 1) {
                                imagemapster.find("area").hover(function () {
                                    var opacity = imagemapster.data("opacity-s");
                                    imagemapster.find(".mainimg").css("opacity", opacity + "");
                                    var id = $(this).data("id");
                                    var selecter = ".hover-imgs img[data-id='" + id + "']";
                                    imagemapster.find(selecter).addClass("active");
                                }, function () {
                                    var opacity = imagemapster.data("opacity-e");
                                    imagemapster.find(".mainimg").css("opacity", opacity + "");
                                    var id = $(this).data("id");
                                    var selecter = ".hover-imgs img[data-id='" + id + "']";
                                    imagemapster.find(selecter).removeClass("active");
                                })
                            }
                        });
            </script>



        </div>

    </div>
</section>
    @include('gemriver.footer')
@endsection
@extends('gemriver.layout') 
@section('content')
<link rel="stylesheet" href="{{ asset('gem/template/tint/css/style-masterplans.css') }}">
<section id="house">
    <div class="container">
        <div class="bread-crumb">
            <a href='{{route('home')}}' class='home'>Trang chủ /</a>
        </div>
        <div class="page-masterplans masterplans-detail page-different" id="page-masterplans">
            <div class="parallax page-inside">
                <div class="container page-content">
                    <h2 id="lblTitle">CHI TIẾT Căn hộ B8</h2>
                    <div class="box-content">
                        <div class="list-masterplan">
                            <p></p>
                            <div class="sub">
                                <span class="btn-dropdown-toggle fix-title">Chọn căn hộ</span>
                                <ul class="select pull-right">
                                    <li> <a href='{{route('can_ho_a')}}'>Căn hộ A</a></li>
                                    <li> <a href='{{route('can_ho_b1')}}'>Căn hộ B1</a></li>
                                    <li> <a href='{{route('can_ho_b2')}}'>Căn hộ B2</a></li>
                                    <li> <a href='{{route('can_ho_b3')}}'>Căn hộ B3</a></li>
                                    <li> <a href='{{route('can_ho_b4')}}'>Căn hộ B4</a></li>
                                    <li> <a href='{{route('can_ho_b5')}}'>Căn hộ B5</a></li>
                                    <li> <a href='{{route('can_ho_b6')}}'>Căn hộ B6</a></li>
                                    <li> <a href='{{route('can_ho_b7')}}'>Căn hộ B7</a></li>
                                    <li> <a href='{{route('can_ho_b8')}}'>Căn hộ B8</a></li>
                                    <li> <a href='{{route('can_ho_c1')}}'>Căn hộ C1</a></li>
                                    <li> <a href='{{route('can_ho_c2')}}'>Căn hộ C2</a></li>
                                    <li> <a href='{{route('can_ho_c3')}}'>Căn hộ C3</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="row thongso-deatail">
                            <div class="col-xs-12 col-sm-12 col-md-7">
                                <div id="longdesc" style="text-align: center; margin-bottom: 30px"><img alt="" class="2D" src="{{ asset('/gem/assets/uploads/myfiles/images/Mat-Bang/MB-Can-Ho_2D/canho-B8-2D.png') }}"
                                    /></div>

                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-5">
                                <div class="box-homeinfo">
                                    <div id="shortdesc">
                                        <p>
                                            DT tim tường:&nbsp; &nbsp;69.51m² -&nbsp;70.49m²</p>
                                        <div class="shortDesc" id="shortdesc">
                                            <ul class="homeinfo">
                                                <li>
                                                    DT thông thủy<span>63.58m² - 65.62m²</span></li>
                                                <li>
                                                    Phòng khách<span>14.53m²</span></li>
                                                <li>
                                                    Bếp<span>4.99m²</span></li>
                                                <li>
                                                    Lô gia<span>2.29m²</span></li>
                                                <li>
                                                    Sân phơi<span>1.08m²</span></li>
                                                <li>
                                                    Phòng ngủ 1<span>9.64m²</span></li>
                                                <li>
                                                    Phòng ngủ 2<span>15.00m²</span></li>
                                                <li>
                                                    Vệ sinh 1<span>4.31m²</span></li>
                                                <li>
                                                    Vệ sinh 2<span>3.64m²</span></li>
                                                <li>
                                                    DT phụ<span>8.10m²</span></li>
                                            </ul>
                                        </div>
                                        <br />
                                    </div>
                                </div>

                                <img src="{{ asset('/gem/assets/uploads/images/post/mb_can_ho_b8_185201812539.png') }}" id="postImage" class="img-responsive img-mat-bang-tang"
                                />

                            </div>
                        </div>

                        <div class="row thongso-deatail">
                            <ul class="list-controls text-center">
                                <li class="item item-2d active">
                                    <a class="bt-2d">Phối cảnh 2D</a></li>
                                <li class="item item-3d hidden">
                                    <a class="bt-3d">Phối cảnh 3D</a></li>
                                <li class="item canhomau">
                                    <a>Căn hộ mẫu</a></li>
                                <li class="item">
                                    <a href="{{route('mat_bang')}}">Quay lại</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
    @include('gemriver.footer')
@endsection
@extends('gemriver.layout') 
@section('content')
<link rel="stylesheet" href="{{ asset('gem/template/tint/css/style-masterplans.css') }}">
<section id="house">
    <div class="container">
        <div class="bread-crumb">
            <a href='{{route('home')}}' class='home'>Trang chủ /</a>
        </div>
        {{--
        <div class="row">
            <div class="col-sm-12">
                <h1 class="page-title">Mặt bằng căn hộ</h1>
            </div>

            <div class="col-sm-6">
                <div class="sub pull-right">
                    <span class="btn-dropdown-toggle fix-title">Vui lòng chọn Block</span>
                    <ul class="select pull-right">
                        <li> <a href='mat-bang/block1-dien-hinh-3-24-27-31.html'>Block 1</a></li>
                        <li> <a href='mat-bang/block2-dien-hinh-3-24-27-31.html'>Block 2</a></li>
                        <li> <a href='mat-bang/block3-dien-hinh-3-24-27-31.html'>Block 3</a></li>
                        <li> <a href='mat-bang/block4-dien-hinh-3-24-27-31.html'>Block 4</a></li>
                        <li> <a href='mat-bang/block5-dien-hinh-3-24-27-31.html'>Block 5</a></li>
                        <li> <a href='mat-bang/block6-dien-hinh-3-24-27-31.html'>Block 6</a></li>
                        <li> <a href='mat-bang/block7-dien-hinh-3-24-27-31.html'>Block 7</a></li>
                        <li> <a href='mat-bang/block8-dien-hinh-3-24-27-30.html'>Block 8</a></li>
                    </ul>
                </div>
            </div>
            <div class="clearfix"></div>
        </div> --}} {{--
        <div class="note">
            <div id="lblShortDesc" class="box-intro-text">Vui lòng click chuột vào Tháp / Block trong hình bên dưới để xem mặt bằng tầng.</div>
        </div> --}}
        <div id="master_plan" class="">
            <div class="page-masterplans masterplans-home page-different" id="page-masterplans">
                <div class="parallax page-inside">
                    <div class="container page-content">
                        <h2>MẶT BẰNG TẦNG 5</h2>
                        <div class="box-content">
                            <div class="list-masterplan">
                                <p>Vui lòng click vào hình để xem mặt bằng tầng hoặc tuỳ chọn tầng để xem:</p>
                                <div class="sub">
                                    <span class="btn-dropdown-toggle fix-title">Chọn tầng</span>
                                    <ul class="select pull-right">
                                        <li>
                                            <a href='{{route('mat_bang_tang_1')}}'>Tầng 1</a>
                                        </li>
                                        <li>
                                            <a href='{{route('mat_bang_tang_2')}}'>Tầng 2</a>
                                        </li>
                                        <li>
                                            <a href='{{route('mat_bang_tang_3')}}'>Tầng 3</a>
                                        </li>
                                        <li>
                                            <a href='{{route('mat_bang_tang_5')}}'>Tầng 5</a>
                                        </li>
                                        <li>
                                            <a href='{{route('mat_bang_tang_6_32')}}'>Tầng 6 - 32</a>
                                        </li>
                                        <li>
                                            <a href='{{route('mat_bang_tang_33_34')}}'>Tầng 33 - 34</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="sub">
                                    <span class="btn-dropdown-toggle fix-title">Chọn căn hộ</span>
                                    <ul class="select pull-right">
                                        <li> <a href='{{route('can_ho_a')}}'>Căn hộ A</a></li>
                                        <li> <a href='{{route('can_ho_b1')}}'>Căn hộ B1</a></li>
                                        <li> <a href='{{route('can_ho_b2')}}'>Căn hộ B2</a></li>
                                        <li> <a href='{{route('can_ho_b3')}}'>Căn hộ B3</a></li>
                                        <li> <a href='{{route('can_ho_b4')}}'>Căn hộ B4</a></li>
                                        <li> <a href='{{route('can_ho_b5')}}'>Căn hộ B5</a></li>
                                        <li> <a href='{{route('can_ho_b6')}}'>Căn hộ B6</a></li>
                                        <li> <a href='{{route('can_ho_b7')}}'>Căn hộ B7</a></li>
                                        <li> <a href='{{route('can_ho_b8')}}'>Căn hộ B8</a></li>
                                        <li> <a href='{{route('can_ho_c1')}}'>Căn hộ C1</a></li>
                                        <li> <a href='{{route('can_ho_c2')}}'>Căn hộ C2</a></li>
                                        <li> <a href='{{route('can_ho_c3')}}'>Căn hộ C3</a></li>
                                    </ul>
                                </div>
                            </div>
                            

                            <div class="imagemapster">
                                <div id="master_plan" class="imagemapster" data-opacity-s=".35" data-opacity-e="1">
                                    <div class="map-hover">
                                        <div class="img-container">
                                            <div class="position-relative inline-block">
                                                <img alt="" class="main-img img-responsive inline-block" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/mb-tang-5.png') }}"
                                                    usemap="#can-ho" />
                                                <div class="hover-imgs">
                                                    <img alt="" class="img-responsive inline-block" data-id="can-ho-a" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/CH_A_t5.png') }}"
                                                    />
                                                    <img alt="" class="img-responsive inline-block" data-id="can-ho-b1" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/CH_B1.png') }}"
                                                    />
                                                    <img alt="" class="img-responsive inline-block" data-id="can-ho-b2" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/CH_B2.png') }}"
                                                    />
                                                    <img alt="" class="img-responsive inline-block" data-id="can-ho-b3" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/CH_B3_t5.png') }}"
                                                    />
                                                    <img alt="" class="img-responsive inline-block" data-id="can-ho-b4" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/CH_B4.png') }}"
                                                    />
                                                    <img alt="" class="img-responsive inline-block" data-id="can-ho-b5" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/CH_B5.png') }}"
                                                    />
                                                    <img alt="" class="img-responsive inline-block" data-id="can-ho-b6" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/CH_B6.png') }}"
                                                    />
                                                    <img alt="" class="img-responsive inline-block" data-id="can-ho-b7" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/CH_B7.png') }}"
                                                    />
                                                    <img alt="" class="img-responsive inline-block" data-id="can-ho-b8" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/CH_B8.png') }}"
                                                    />
                                                    <img alt="" class="img-responsive inline-block" data-id="can-ho-c1" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/CH_C1.png') }}"
                                                    />
                                                    <img alt="" class="img-responsive inline-block" data-id="can-ho-c2" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/CH_C2.png') }}"
                                                    />
                                                    <img alt="" class="img-responsive inline-block" data-id="can-ho-c3" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/CH_C3.png') }}"
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <map class="can-ho" id="can-ho" name="can-ho">
                                            <area coords="192, 267, 195, 448, 245, 448, 243, 268" data-id="can-ho-b1" href="can-ho-b1.html" shape="poly" />
                                            <area coords="610, 722, 612, 773, 843, 774, 845, 720" data-id="can-ho-b1" href="can-ho-b1.html" shape="poly" />
                                            <area coords="668, 657, 668, 709, 840, 709, 841, 658" data-id="can-ho-b1" href="can-ho-b1.html" shape="poly" />
                                            <area coords="398, 722, 513, 721, 514, 837, 400, 839" data-id="can-ho-b1" href="can-ho-b1.html" shape="poly" />
                                            <area coords="71, 608, 181, 606, 183, 727, 68, 730" data-id="can-ho-b1" href="can-ho-b1.html" shape="poly" />
                                            <area coords="131, 266, 186, 267, 186, 310, 183, 448, 184, 507, 128, 509" data-id="can-ho-b1" href="can-ho-b1.html" shape="poly"
                                            />
                                            <area coords="839, 651, 881, 708" data-id="can-ho-a" href="{{route('can_ho_a')}}" shape="rect" />
                                            <area coords="627, 652, 669, 705" data-id="can-ho-a" href="{{route('can_ho_a')}}" shape="rect" />
                                            <area coords="515, 778, 555, 831" data-id="can-ho-a" href="{{route('can_ho_a')}}" shape="rect" />
                                            <area coords="74, 564, 129, 603" data-id="can-ho-a" href="{{route('can_ho_a')}}" shape="rect" />
                                            <area coords="200, 226, 250, 269" data-id="can-ho-a" href="{{route('can_ho_a')}}" shape="rect" />
                                            <area coords="294, 715, 350, 835" data-id="can-ho-b2" href="{{route('can_ho_b2')}}" shape="rect" />
                                            <area coords="75, 780, 191, 836" data-id="can-ho-b2" href="{{route('can_ho_b2')}}" shape="rect" />
                                            <area coords="570, 653, 630, 705" data-id="can-ho-b3" href="{{route('can_ho_b3')}}" shape="rect" />
                                            <area coords="892, 717, 934, 785" data-id="can-ho-b4" href="{{route('can_ho_b4')}}" shape="rect" />
                                            <area coords="121, 169, 196, 217" data-id="can-ho-b4" href="{{route('can_ho_b4')}}" shape="rect" />
                                            <area coords="349, 780, 400, 833" data-id="can-ho-b5" href="{{route('can_ho_b5')}}" shape="rect" />
                                            <area coords="75, 723, 130, 779" data-id="can-ho-b5" href="{{route('can_ho_b5')}}" shape="rect" />
                                            <area coords="839, 786, 894, 834" data-id="can-ho-b6" href="{{route('can_ho_b6')}}" shape="rect" />
                                            <area coords="71, 214, 123, 270" data-id="can-ho-b6" href="{{route('can_ho_b6')}}" shape="rect" />
                                            <area coords="555, 780, 614, 833" data-id="can-ho-b7" href="{{route('can_ho_b7')}}" shape="rect" />
                                            <area coords="79, 504, 128, 566" data-id="can-ho-b7" href="{{route('can_ho_b7')}}" shape="rect" />
                                            <area coords="513, 717, 571, 768" data-id="can-ho-b8" href="{{route('can_ho_b8')}}" shape="rect" />
                                            <area coords="143, 543, 189, 603" data-id="can-ho-b8" href="{{route('can_ho_b8')}}" shape="rect" />
                                            <area coords="879, 652, 956, 704" data-id="can-ho-c1" href="{{route('can_ho_c1')}}" shape="rect" />
                                            <area coords="200, 148, 250, 230" data-id="can-ho-c1" href="{{route('can_ho_c1')}}" shape="rect" />
                                            <area coords="895, 785, 966, 837" data-id="can-ho-c2" href="{{route('can_ho_c2')}}" shape="rect" />
                                            <area coords="75, 140, 124, 211" data-id="can-ho-c2" href="{{route('can_ho_c2')}}" shape="rect" />
                                            <area coords="195, 448, 244, 545" data-id="can-ho-c3" href="{{route('can_ho_c2')}}" shape="rect" />
                                        </map>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="{{ asset('gem/template/tint/lib/imgmaps/jquery.rwdImageMaps.min.js') }}"></script>
            <script type="text/javascript" src="{{ asset('gem/template/tint/js/jquery.maphilight.js') }}"></script>
            <script type="text/javascript">
                function loadMaps() {
                    $('img[usemap]').rwdImageMaps();
                }

                $(document).ready(function () {
                    //$("#page-masterplans .sub:last-child").each(function () {
                    //    if ($(this).find("li").length < 1) {
                    //        $(this).hide();
                    //    }

                    //});
                    var tang1 = $(document).attr('title').search('tầng 1');
                    var tang2 = $(document).attr('title').search('tầng 2');
                
                    if (tang1 != -1 || tang2 != -1) {
                        $("#page-masterplans .sub:last-child").hide();
                    }
                    else {
                        $("#page-masterplans .sub:last-child").show();
                    }
                    $(".divphoicanh a.btn-2D").addClass("active");

                    $("img.3D").hide();
                    $(".divphoicanh a.btn-3D").click(function () {
                        $(".divphoicanh a").removeClass("active");
                        $(this).addClass("active");
                        $("img.2D").hide();
                        $("img.3D").show();
                        return false;
                    })
                    $(".divphoicanh a.btn-2D").click(function () {
                        $(".divphoicanh a").removeClass("active");
                        $(this).addClass("active");
                        $("img.2D").show();
                        $("img.3D").hide();
                        return false;
                    })
                    //$(window).resize(function () {
                    //    if ($(window).width() > 1285) {
                    //        var chieucao = $(".danhsach-canho ul.list-canho").height();
                    //        $(".danhsach-canho").css("top", "calc(93% - " + chieucao + "px)");
                    //        $("section.chitiet-canho .danhsach-canho").css("top", "calc(80% - " + chieucao + "px");
                    //    }
                    //});
                    loadMaps();

                    /******************* Map hover *******************/
                    var imagemapster = $(".imagemapster:eq(1)");
                    console.log(imagemapster.data("opacity-s"));
                    if (imagemapster.length >= 1) {
                        imagemapster.find("area").hover(function () {
                            var opacity = imagemapster.data("opacity-s");
                            imagemapster.find(".main-img").css("opacity", opacity + "");
                            var id = $(this).data("id");
                            var selecter = ".hover-imgs img[data-id='" + id + "']";
                            imagemapster.find(selecter).addClass("active");
                        }, function () {
                            var opacity = imagemapster.data("opacity-e");
                            imagemapster.find(".main-img").css("opacity", opacity + "");
                            var id = $(this).data("id");
                            var selecter = ".hover-imgs img[data-id='" + id + "']";
                            imagemapster.find(selecter).removeClass("active");
                        })
                    }
                });
            </script>



        </div>

    </div>
</section>
    @include('gemriver.footer')
@endsection
@extends('gemriver.layout') 
@section('content')
<link rel="stylesheet" href="{{ asset('gem/template/tint/css/style-masterplans.css') }}">
<section id="house">
    <div class="container">
        <div class="bread-crumb">
            <a href='{{route('home')}}' class='home'>Trang chủ /</a>
        </div>
        <div id="master_plan" class="imagemapster ">
            <div class="page-masterplans masterplans-home page-different" id="page-masterplans">
                <div class="parallax page-inside">
                    <div class="container page-content">
                        <h2>MẶT BẰNG TẦNG 1</h2>
                        <div class="box-content">
                            <div class="list-masterplan">
                                <p>Vui lòng click vào hình để xem mặt bằng tầng hoặc tuỳ chọn tầng để xem:</p>
                                <div class="sub">
                                    <span class="btn-dropdown-toggle fix-title">Chọn tầng</span>
                                    <ul class="select pull-right">
                                        <li>
                                            <a href='{{route('mat_bang_tang_1')}}'>Tầng 1</a>
                                        </li>
                                        <li>
                                            <a href='{{route('mat_bang_tang_2')}}'>Tầng 2</a>
                                        </li>
                                        <li>
                                            <a href='{{route('mat_bang_tang_3')}}'>Tầng 3</a>
                                        </li>
                                        <li>
                                            <a href='{{route('mat_bang_tang_5')}}'>Tầng 5</a>
                                        </li>
                                        <li>
                                            <a href='{{route('mat_bang_tang_6_32')}}'>Tầng 6 - 32</a>
                                        </li>
                                        <li>
                                            <a href='{{route('mat_bang_tang_33_34')}}'>Tầng 33 - 34</a>
                                        </li>

                                    </ul>
                                </div>
                            </div>

                            <div id="master_plan" class="imagemapster" data-opacity-s=".35">
                                <img alt="" src="{{ asset('gem/assets//uploads/myfiles/images/Mat-Bang/MB-Tang/tang-1.png') }}">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="{{ asset('gem/template/tint/lib/imgmaps/jquery.rwdImageMaps.min.js') }}"></script>
            <script type="text/javascript" src="{{ asset('gem/template/tint/js/jquery.maphilight.js') }}"></script>
            <script type="text/javascript">
                function loadMaps() {
                            $('img[usemap]').rwdImageMaps();
                        }
            
                        $(document).ready(function () {
            
                            $(".divphoicanh a.btn-2D").addClass("active");
            
                            $("img.3D").hide();
                            $(".divphoicanh a.btn-3D").click(function () {
                                $(".divphoicanh a").removeClass("active");
                                $(this).addClass("active");
                                $("img.2D").hide();
                                $("img.3D").show();
                                return false;
                            })
                            $(".divphoicanh a.btn-2D").click(function () {
                                $(".divphoicanh a").removeClass("active");
                                $(this).addClass("active");
                                $("img.2D").show();
                                $("img.3D").hide();
                                return false;
                            })
            
                            loadMaps();
            
                            /******************* Map hover *******************/
                            var imagemapster = $(".imagemapster");
                            if (imagemapster.length >= 1) {
                                imagemapster.find("area").hover(function () {
                                    var opacity = imagemapster.data("opacity-s");
                                    imagemapster.find(".mainimg").css("opacity", opacity + "");
                                    var id = $(this).data("id");
                                    var selecter = ".hover-imgs img[data-id='" + id + "']";
                                    imagemapster.find(selecter).addClass("active");
                                }, function () {
                                    var opacity = imagemapster.data("opacity-e");
                                    imagemapster.find(".mainimg").css("opacity", opacity + "");
                                    var id = $(this).data("id");
                                    var selecter = ".hover-imgs img[data-id='" + id + "']";
                                    imagemapster.find(selecter).removeClass("active");
                                })
                            }
                        });
            </script>



        </div>

    </div>
</section>
    @include('gemriver.footer')
@endsection
@extends('gemriver.layout') 
@section('content')
<link rel="stylesheet" href="{{ asset('gem/template/tint/css/style-masterplans.css') }}">
<section id="house">
    <div class="container">
        <div class="bread-crumb">
            <a href='{{route('home')}}' class='home'>Trang chủ /</a>
        </div>
        {{--
        <div class="row">
            <div class="col-sm-12">
                <h1 class="page-title">Mặt bằng căn hộ</h1>
            </div>

            <div class="col-sm-6">
                <div class="sub pull-right">
                    <span class="btn-dropdown-toggle fix-title">Vui lòng chọn Block</span>
                    <ul class="select pull-right">
                        <li> <a href='mat-bang/block1-dien-hinh-3-24-27-31.html'>Block 1</a></li>
                        <li> <a href='mat-bang/block2-dien-hinh-3-24-27-31.html'>Block 2</a></li>
                        <li> <a href='mat-bang/block3-dien-hinh-3-24-27-31.html'>Block 3</a></li>
                        <li> <a href='mat-bang/block4-dien-hinh-3-24-27-31.html'>Block 4</a></li>
                        <li> <a href='mat-bang/block5-dien-hinh-3-24-27-31.html'>Block 5</a></li>
                        <li> <a href='mat-bang/block6-dien-hinh-3-24-27-31.html'>Block 6</a></li>
                        <li> <a href='mat-bang/block7-dien-hinh-3-24-27-31.html'>Block 7</a></li>
                        <li> <a href='mat-bang/block8-dien-hinh-3-24-27-30.html'>Block 8</a></li>
                    </ul>
                </div>
            </div>
            <div class="clearfix"></div>
        </div> --}} {{--
        <div class="note">
            <div id="lblShortDesc" class="box-intro-text">Vui lòng click chuột vào Tháp / Block trong hình bên dưới để xem mặt bằng tầng.</div>
        </div> --}}
        <div id="master_plan" class="">
            <div class="page-masterplans masterplans-home page-different" id="page-masterplans">
                <div class="parallax page-inside">
                    <div class="container page-content">
                        <h2>MẶT BẰNG TẦNG 33 - 34</h2>
                        <div class="box-content">
                            <div class="list-masterplan">
                                <p>Vui lòng click vào hình để xem mặt bằng tầng hoặc tuỳ chọn tầng để xem:</p>
                                <div class="sub">
                                    <span class="btn-dropdown-toggle fix-title">Chọn tầng</span>
                                    <ul class="select pull-right">
                                        <li>
                                            <a href='{{route('mat_bang_tang_1')}}'>Tầng 1</a>
                                        </li>
                                        <li>
                                            <a href='{{route('mat_bang_tang_2')}}'>Tầng 2</a>
                                        </li>
                                        <li>
                                            <a href='{{route('mat_bang_tang_3')}}'>Tầng 3</a>
                                        </li>
                                        <li>
                                            <a href='{{route('mat_bang_tang_5')}}'>Tầng 5</a>
                                        </li>
                                        <li>
                                            <a href='{{route('mat_bang_tang_6_32')}}'>Tầng 6 - 32</a>
                                        </li>
                                        <li>
                                            <a href='{{route('mat_bang_tang_33_34')}}'>Tầng 33 - 34</a>
                                        </li>

                                    </ul>
                                </div>
                                <div class="sub">
                                    <span class="btn-dropdown-toggle fix-title">Chọn căn hộ</span>
                                    <ul class="select pull-right">
                                        <li> <a href='{{route('can_ho_a')}}'>Căn hộ A</a></li>
                                        <li> <a href='{{route('can_ho_b1')}}'>Căn hộ B1</a></li>
                                        <li> <a href='{{route('can_ho_b2')}}'>Căn hộ B2</a></li>
                                        <li> <a href='{{route('can_ho_b3')}}'>Căn hộ B3</a></li>
                                        <li> <a href='{{route('can_ho_b4')}}'>Căn hộ B4</a></li>
                                        <li> <a href='{{route('can_ho_b5')}}'>Căn hộ B5</a></li>
                                        <li> <a href='{{route('can_ho_b6')}}'>Căn hộ B6</a></li>
                                        <li> <a href='{{route('can_ho_b7')}}'>Căn hộ B7</a></li>
                                        <li> <a href='{{route('can_ho_b8')}}'>Căn hộ B8</a></li>
                                        <li> <a href='{{route('can_ho_c1')}}'>Căn hộ C1</a></li>
                                        <li> <a href='{{route('can_ho_c2')}}'>Căn hộ C2</a></li>
                                        <li> <a href='{{route('can_ho_c3')}}'>Căn hộ C3</a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="imagemapster">
                                <div id="master_plan" class="imagemapster" data-opacity-s=".35">
                                    <div class="map-hover">
                                        <div class="img-container">
                                            <div class="position-relative inline-block">
                                                <img alt="" class="main-img img-responsive inline-block" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/tang-34.png') }}"
                                                    usemap="#can-ho" />
                                                <div class="hover-imgs">
                                                    <img alt="" class="img-responsive inline-block" data-id="can-ho-a" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/CH_A_t34.png') }}"
                                                    />
                                                    <img alt="" class="img-responsive inline-block" data-id="can-ho-b1" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/CH_B1_t34.png') }}"
                                                    />
                                                    <img alt="" class="img-responsive inline-block" data-id="can-ho-b2" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/CH_B2_t34.png') }}"
                                                    />
                                                    <img alt="" class="img-responsive inline-block" data-id="can-ho-b3" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/CH_B3_t34.png') }}"
                                                    />
                                                    <img alt="" class="img-responsive inline-block" data-id="can-ho-b4" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/CH_B4_t34.png') }}"
                                                    />
                                                    <img alt="" class="img-responsive inline-block" data-id="can-ho-b5" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/CH_B5_t34.png') }}"
                                                    />
                                                    <img alt="" class="img-responsive inline-block" data-id="can-ho-b6" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/CH_B6_t34.png') }}"
                                                    />
                                                    <img alt="" class="img-responsive inline-block" data-id="can-ho-b7" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/CH_B7_t34.png') }}"
                                                    />
                                                    <img alt="" class="img-responsive inline-block" data-id="can-ho-b8" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/CH_B8_t34.png') }}"
                                                    />
                                                    <img alt="" class="img-responsive inline-block" data-id="can-ho-c1" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/CH_C1_t34.png') }}"
                                                    />
                                                    <img alt="" class="img-responsive inline-block" data-id="can-ho-c2" src="{{ asset('gem/assets/uploads/myfiles/images/Mat-Bang/MB-Tang/CH_C2_t34.png') }}"
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <map class="can-ho" id="can-ho" name="can-ho">
                                            <area coords="80, 559, 133, 602" data-id="can-ho-a" href="{{route('can_ho_a')}}" shape="rect" />
                                            <area coords="200, 445, 253, 486" data-id="can-ho-a" href="{{route('can_ho_a')}}" shape="rect" />
                                            <area coords="200, 226, 253, 270" data-id="can-ho-a" href="{{route('can_ho_a')}}" shape="rect" />
                                            <area coords="202, 267, 251, 445" data-id="can-ho-b1" href="{{route('can_ho_a')}}" shape="rect" />
                                            <area coords="141, 270, 193, 503" data-id="can-ho-b1" href="{{route('can_ho_a')}}" shape="rect" />
                                            <area coords="80, 599, 192, 717" data-id="can-ho-b1" href="{{route('can_ho_a')}}" shape="rect" />
                                            <area coords="84, 770, 192, 826" data-id="can-ho-b2" href="{{route('can_ho_b2')}}" shape="rect" />
                                            <area coords="200, 487, 252, 545" data-id="can-ho-b3" href="{{route('can_ho_b3')}}" shape="rect" />
                                            <area coords="126, 172, 194, 215" data-id="can-ho-b4" href="{{route('can_ho_b4')}}" shape="rect" />
                                            <area coords="78, 718, 135, 772" data-id="can-ho-b5" href="{{route('can_ho_b5')}}" shape="rect" />
                                            <area coords="78, 216, 125, 268" data-id="can-ho-b6" href="{{route('can_ho_b6')}}" shape="rect" />
                                            <area coords="81, 500, 132, 560" data-id="can-ho-b7" href="{{route('can_ho_b7')}}" shape="rect" />
                                            <area coords="146, 539, 190, 597" data-id="can-ho-b8" href="{{route('can_ho_b8')}}" shape="rect" />
                                            <area coords="203, 150, 254, 227" data-id="can-ho-c1" href="{{route('can_ho_c1')}}" shape="rect" />
                                            <area coords="78, 143, 125, 215" data-id="can-ho-c2" href="{{route('can_ho_c2')}}" shape="rect" />
                                        </map>
                                    </div>
                                </div>
    
    
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="{{ asset('gem/template/tint/lib/imgmaps/jquery.rwdImageMaps.min.js') }}"></script>
            <script type="text/javascript" src="{{ asset('gem/template/tint/js/jquery.maphilight.js') }}"></script>
            <script type="text/javascript">
                function loadMaps() {
                    $('img[usemap]').rwdImageMaps();
                }

                $(document).ready(function () {
                    //$("#page-masterplans .sub:last-child").each(function () {
                    //    if ($(this).find("li").length < 1) {
                    //        $(this).hide();
                    //    }

                    //});
                    var tang1 = $(document).attr('title').search('tầng 1');
                    var tang2 = $(document).attr('title').search('tầng 2');
                
                    if (tang1 != -1 || tang2 != -1) {
                        $("#page-masterplans .sub:last-child").hide();
                    }
                    else {
                        $("#page-masterplans .sub:last-child").show();
                    }
                    $(".divphoicanh a.btn-2D").addClass("active");

                    $("img.3D").hide();
                    $(".divphoicanh a.btn-3D").click(function () {
                        $(".divphoicanh a").removeClass("active");
                        $(this).addClass("active");
                        $("img.2D").hide();
                        $("img.3D").show();
                        return false;
                    })
                    $(".divphoicanh a.btn-2D").click(function () {
                        $(".divphoicanh a").removeClass("active");
                        $(this).addClass("active");
                        $("img.2D").show();
                        $("img.3D").hide();
                        return false;
                    })
                    //$(window).resize(function () {
                    //    if ($(window).width() > 1285) {
                    //        var chieucao = $(".danhsach-canho ul.list-canho").height();
                    //        $(".danhsach-canho").css("top", "calc(93% - " + chieucao + "px)");
                    //        $("section.chitiet-canho .danhsach-canho").css("top", "calc(80% - " + chieucao + "px");
                    //    }
                    //});
                    loadMaps();

                    /******************* Map hover *******************/
                    var imagemapster = $(".imagemapster:eq(1)");
                    console.log(imagemapster.data("opacity-s"));
                    if (imagemapster.length >= 1) {
                        imagemapster.find("area").hover(function () {
                            var opacity = imagemapster.data("opacity-s");
                            imagemapster.find(".main-img").css("opacity", opacity + "");
                            var id = $(this).data("id");
                            var selecter = ".hover-imgs img[data-id='" + id + "']";
                            imagemapster.find(selecter).addClass("active");
                        }, function () {
                            imagemapster.find(".main-img").css("opacity", 1 + "");
                            var id = $(this).data("id");
                            var selecter = ".hover-imgs img[data-id='" + id + "']";
                            imagemapster.find(selecter).removeClass("active");
                        })
                    }
                });
            </script>



        </div>

    </div>
</section>
    @include('gemriver.footer')
@endsection
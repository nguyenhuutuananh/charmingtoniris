@extends('gemriver.layout')
@section('content')

<section id="position-introdution">
    <div class="container">
        <div class="bread-crumb">
            <a href='index.html' class='home'>Trang chủ /</a>
            Vị trí
        </div>
        <div class="row">

            <div class="col-sm-12 col-md-4 col-lg-4">
                <h2>Vị trí chiến lược</h2>
                <div class="description">
                    {!! __c('vi_tri_mo_ta') !!}
                </div>
            </div>
            <div class="col-sm-12 col-md-8 col-lg-8">
                <div class="photo">
                    <img class="img-responsive" src="{{ url(__c('vi_tri_hinh_anh')) }}">
                </div>
            </div>
        </div>
    </div>
</section>

<div class="clearfix"></div>

<section id="introdution">
    <div class="container">
        <div class="row position-photo vertical-space2">
            <div class="col-lg-12">
                    {!! __c('vi_tri_mo_ta_chung') !!}
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>

@include('gemriver.footer')

@endsection
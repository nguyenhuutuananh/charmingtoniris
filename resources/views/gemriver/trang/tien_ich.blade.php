@extends('gemriver.layout') 
@section('content')

<section id="util" class="detail">
    <div class="container">
        <div class="bread-crumb">
            <a href='{{route('home')}}' class='home'>Trang chủ /</a>
        </div>
        <div class="row">
            <div class="advange-block">
                <div class="description">
                    <div id="shortDesc" class="col-lg-11 col-lg-offset-1"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="photo-items">
        <div id="utilitiesslide" class="photo-item">
            @php
            $slider = \App\Slider::find(3);
            @endphp
            @foreach ($slider->images as $detail)
            <div class='item photo-item-left'><img src='{{url($detail->imageUrl)}}' class='img-responsive'>
                <div class='p'>
                    <p class='top'>{{$detail->title}}</p>
                    <p class='bottom'></p>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    <div id="longDesc">
        <div class="container">
            <div class="row">
                <div class="a-block">
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        {!! __c('cot_tien_ich_1') !!}
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            {!! __c('cot_tien_ich_2') !!}
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            {!! __c('cot_tien_ich_3') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    (function ($) {
        $(document).ready(function () {
            $('.photo-item').slick({
                infinite: true,
                slidesToShow: 2,
                slidesToScroll: 2,
                responsive: [
                {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
                }
            ]
            });
        });

    })(jQuery);

</script>
    @include('gemriver.footer')
@endsection
@extends('gemriver.layout')
@section('content')
<section id="host">
    <div class="container">
        <div class="bread-crumb">
            <a href='{{url('/')}}' class='home'>Trang chủ /</a><a href='{{route('tong_quan')}}' class='home'>Giới thiệu /</a>
            <a class=''>Chủ đầu tư</a>
        </div>
        <div class="row">
            <div class="host-block">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <h2>Nhà phát triển dự án</h2>
                </div>
                <div class='item'>
                    <div class='col-sm-3'><a href='{{__c('chu_dau_tu_link')}}' target='_blank'><img class='img-responsive' src='{{url(__c('chu_dau_tu_hinh_anh'))}}'></a></div>
                    <div class='col-sm-9'>
                        <div style="text-align: justify;">
                            {!! __c('chu_dau_tu_noi_dung') !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="host-block">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <h2>Đối tác chiến lược</h2>
                </div>
                @php
                $partners = \App\Partner::all();
                @endphp
                @foreach ($partners as $partner)
                <div class='item'>
                    <div class='col-sm-3'>
                        <h4>{{$partner->sub_title}}</h4><a href='{{$partner->link}}' target='_blank'>
                            <img class='img-responsive' src='{{url($partner->imageUrl)}}'></a></div>
                    <div class='col-sm-9'>
                        <p style="text-align: justify;">
                            {!! $partner->description !!}
                        </p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>

@include('gemriver.footer')
@endsection
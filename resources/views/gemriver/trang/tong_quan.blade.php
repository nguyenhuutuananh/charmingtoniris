@extends('gemriver.layout')
@section('content')

<div class="page-info page-about">
    <div class="fullpage" id="fullpage">
        <div class="content-main content-dynamic-section">

            <section id="about" class="detail">
                <div class="container">
                    <div class="bread-crumb">
                        <a href='{{url('/')}}' class='home'>Trang chủ /</a><a href='{{route('tong_quan')}}' class='home'>Giới thiệu /</a>
                        <a
                            class=''>Tổng quan dự án</a>
                    </div>
                    <div class="row ab-block">
                        <div class="col-sm-12 col-md-6">
                            <h5 class="title">Tổng quan dự án</h5>
                            <h2>{!! __c('tong_quan_tieu_de') !!}</h2>
                            <div class="description">
                                {!! __c('tong_quan_mo_ta') !!}
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="photo">
                                <img src="{{url(__c('tong_quan_hinh_anh'))}}" class="img-responsive-xs " />
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <section id="total" class="detail">
                    <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 total-item total-1">
                        <p class="top flipInX delay-250 animated">{{ __c('dien_tich') }}</p>
                        <p class="bottom fadeInUp delay-250 animated">Tổng diện tích</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 total-item total-2">
                        <p class="top flipInX delay-500 animated">{{ __c('so_block') }}</p>
                        <p class="bottom fadeInUp delay-500 animated">Tổng số Block</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 total-item total-3">
                        <p class="top flipInX delay-750 animated">{{ __c('tong_can_ho') }}</p>
                        <p class="bottom fadeInUp delay-750 animated">Tổng căn hộ</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 total-item total-4">
                        <p class="top flipInX delay-1000 animated">{{ __c('so_tang') }}</p>
                        <p class="bottom fadeInUp delay-1000 animated">Số tầng của từng block</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 total-item total-5">
                        <p class="top flipInX delay-1250 animated">{{ __c('mat_do_xay_dung_khoi_de') }}</p>
                        <p class="bottom fadeInUp delay-1250 animated">Mật độ xây dựng khối đế</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 total-item total-6">
                        <p class="top flipInX delay-1500 animated">{{ __c('dien_tich_xay_dung') }}</p>
                        <p class="bottom fadeInUp delay-1500 animated">Bàn giao nhà dự kiến</p>
                    </div>
                </section>
            </section>



            <div class="clearfix"></div>
            <section id="reason" class="detail">
                <div class="photo zoomInUp animated">
                    {{-- <h3><span>5 lý do </span>để khách hàng chọn {{__c('web_title')}}</h3> --}}
                    <img style="width: calc(100vh);" class="hidden-xs hidden-sm  img-responsive" src="{{ url(__c('ly_do_khach_hang_chon')) }}" />
                    <img src='{{ url(__c('ly_do_khach_hang_chon_small_device')) }}' class='visible-sm img-responsive'>
                    <img src='{{ url(__c('ly_do_khach_hang_chon_mobile')) }}' class='visible-xs  img-responsive'>
                </div>
            </section>
        </div>
    </div>
</div>

@include('gemriver.footer')

@endsection
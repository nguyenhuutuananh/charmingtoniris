@extends('gemriver.layout') 
@section('content')

<section id="library">
    <div class="container">
        <div class="bread-crumb">
            <a href='index.html' class='home'>Trang chủ /</a>
            Thư viện
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h2>Thư viện</h2>
            </div>
        </div>
    </div>
    <div id="gallery" class="photo-gallery">
        @php
        $gallery = \App\Slider::find(2);
        @endphp
        
        @foreach ($gallery->images as $detail)
        <a data-id='{{$detail->title}}' class='photo-gallery-items'>
            <div class='title'>{{$detail->title}}</div>
            <div class='mask'></div><img src='{{url($detail->imageUrl)}}' alt='{{$detail->title}}' title='{{$detail->title}}'>
        </a>
        @endforeach

    </div>
    <div id="galleryDetail">
        @foreach ($gallery->images as $detail)
            <a href='{{url($detail->imageUrl)}}' class='popup-items' data-lightbox='gallery-{{$detail->title}}'
                data-title='<div>{{$detail->title}}</div><p></p>'></a>
            {{-- Nhiều ảnh đc  --}}
            {{-- <a href='{{ asset('gem/assets/uploads/myfiles/images/thuvienhinh/Phoi-canh-du-an-2.jpg') }}'
            class='popup-items' data-lightbox='gallery-93' data-title='<div>Phối cảnh dự án</div><p></p>'></a> --}}
        @endforeach
    </div>
</section>


<link rel="stylesheet" href="{{ asset('gem/template/tint/lib/lightbox/lightbox.css') }}" type="text/css" />
<script type="text/javascript" src="{{ asset('gem/template/tint/lib/lightbox/lightbox.min.js') }}"></script>
<script type="text/javascript">
    (function ($) {
            $(document).ready(function () {
                lightbox.option({
                    'showImageNumberLabel': true,
                    'wrapAround': true,
                    'fitImagesInViewport': true
                });

                $(".photo-gallery-items").click(function () {
                    var id = $(this).data("id");
                    if ($("#galleryDetail a[data-lightbox='gallery-" + id + "']").length > 0) {
                        $("#galleryDetail a[data-lightbox='gallery-" + id + "']").first().trigger("click");
                    }
                    else {
                        bootbox.alert("Album đang cập nhật");
                    }
                });

                $('.photo-gallery').slick({
                    centerMode: true,
                    centerPadding: '487px',
                    slidesToShow: 1,
                    responsive: [
                    {
                        breakpoint: 1600,
                        settings: {
                            arrows: true,
                            centerMode: true,
                            centerPadding: '350px',
                            slidesToShow: 1
                        }
                    },
                    {
                        breakpoint: 1400,
                        settings: {
                            arrows: true,
                            centerMode: true,
                            centerPadding: '350px',
                            slidesToShow: 1
                        }
                    },
                    {
                        breakpoint: 1100,
                        settings: {
                            centerMode: true,
                            centerPadding: '200px',
                            slidesToShow: 1
                        }
                    },
                    {
                        breakpoint: 992,
                        settings: {
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 800,
                        settings: {
                            centerMode: true,
                            centerPadding: '70px',
                            slidesToShow: 1
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            centerMode: false,
                            centerPadding: '40px',
                            slidesToShow: 1
                        }
                    }
                    ]
                });
            });
        })(jQuery);

</script>

@include('gemriver.footer')
@endsection
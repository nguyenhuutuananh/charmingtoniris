@extends('gemriver.layout')
@section('content')
<section id="news-blocks" class="detail">
    <div class="container">
        <div class="bread-crumb">
            <a href='{{url('/')}}' class='home'>Trang chủ /</a>
        </div>
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <h2 id="lblTitle" class="page-title">Tin dự &#225;n</h2>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <ul class="tabs pull-right">
                    <li class='active'><a href='{{route('news')}}'>Tin dự án</a></li>
                </ul>
            </div>
        </div>
        <div class="">
            <div class="row">
                @php
                $posts = \App\Post::all();
                @endphp
                @foreach ($posts as $post)
                <div class='col-xs-12 col-sm-6'>
                    <div class='news-block'>
                        <div class='photo'><a href='{{route('news.detail', ['slug' => $post->slug])}}'></a>
                            <img class='img-responsive' src='{{url($post->imageUrl)}}'
                                alt='{{$post->title}}' title='{{$post->title}}'>
                            <div class='date'>Ngày {{str_before($post->created_at, ' ')}}</div>
                        </div>
                        <div class='title'><a href='{{route('news.detail', ['slug' => $post->slug])}}'>{{$post->title}}</a></div>
                        <div class='description'><a href='{{route('news.detail', ['slug' => $post->slug])}}'>{{$post->description}}</a></div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        {{-- <div class="page-bottom tin-tuc-nav vertical-space2">
            <div class="paging button-nav-group">
                <a class="pFirst">page_first - undefined</a><a class="pPrev">page_prev - undefined</a><a class=" current"
                    href="javascript:void(0);">1</a><a class="pageNumer" href="tin-du-an/2.html">2</a><a class="pageNumer" href="tin-du-an/3.html">3</a>
                <a
                    class="Haslink pNext" href="tin-du-an/2.html">page_next - undefined</a>
                    <a class="Haslink pLast" href="tin-du-an/3.html">page_last - undefined</a>
            </div>
        </div> --}}
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $(".post-list .row .col-xs-12:nth-child(2n)").after("<div class='clearfix'></div>");
    });
</script>

@include('gemriver.footer')

@endsection
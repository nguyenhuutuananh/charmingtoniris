@extends('gemriver.layout') 
@section('content')
<section id="news-blocks-detail" class="detail">
    <div class="container">
        <div class="bread-crumb">
            <a href='{{url('/')}}' class='home'>Trang chủ /</a>
        </div>
        <div class="row ">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <h2 id="lblCateName">Tin dự án</h2>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <ul class="tabs pull-right">
                    <li class='active'><a href='{{route('news')}}'>Tin dự án</a></li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="block-content ">
            <h1 id="lblTitle" class="new-title">{{$post->title}}</h1>
            <div id="lblDate" class="top-date pull-left">{{$post->created_at}}</div>
            <div class="pull-right ">

                <ul class="share">
                    <li><a target="_blank" href="http://www.facebook.com/share.php?u={{url()->current()}}">
                <img src="{{ asset('gem/template/tint/images/icon-facebook.png') }}"></a></li>
                    <li><a target="_blank" href="http://twitter.com/share?url={{url()->current()}}">
                <img src="{{ asset('gem/template/tint/images/icon-twiter.png') }}"></a></li>
                    <li><a target="_blank" href="https://plus.google.com/share?url={{url()->current()}}">
                <img src="{{ asset('gem/template/tint/images/icon-google.png') }}"></a></li>
                </ul>
            </div>
            <div class="clearfix"></div>
            <div id="divPostContent" class="post-content content editor-content">
                <div style="text-align: justify;">
                    <div>
                        <em>
                            {{$post->description}}
                        </em>
                    </div>
                    <br />
                    <div>
                        {!! $post->content !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="block-other vertical-space2  ">
            <h2>Tin liên quan</h2>
            <div class="post-list">
                <div class="row">
                    @php
                    $random_posts = \App\Post::inRandomOrder()->skip(0)->take(4)->get();
                    @endphp
                    @foreach ($random_posts as $post)
                    <div class='col-xs-12 col-sm-6'>
                        <div class='news-block'>
                            <div class='photo'><a href='{{route('news.detail', ['slug' => $post->slug])}}'></a>
                                <img class='img-responsive' src='{{url($post->imageUrl)}}'
                                    alt='{{$post->title}}' title='{{$post->title}}'>
                                <div class='date'>Ngày {{str_before($post->created_at, ' ')}}</div>
                            </div>
                            <div class='title'><a href='{{route('news.detail', ['slug' => $post->slug])}}'>{{$post->title}}</a></div>
                            <div class='description'><a href='{{route('news.detail', ['slug' => $post->slug])}}'>{{$post->description}}</a></div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $(".post-list .row .col-xs-12:nth-child(2n)").after("<div class='clearfix'></div>");
    });

</script>
    @include('gemriver.footer')
@endsection
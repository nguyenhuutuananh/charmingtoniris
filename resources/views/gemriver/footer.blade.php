<div class="page-footer">
    <footer>
        <div id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-8 col-md-6 col-lg-8 tab_left_f">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 device-col1">
                                <h4>Chủ đầu tư</h4>
                                <div class="icon">
                                    <a href="{{__c('chu_dau_tu_link')}}" target="_blank"><img class="img-responsive" src="{{url(__c('chu_dau_tu_hinh_anh'))}}"></a>
                                </div>
                                <div class="description">
                                    {{__c('chu_dau_tu_mo_ta')}}
                                </div>
                            </div>
                            
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 device-col2">
                                <h4>Đối tác chiến lược</h4>
                                @php
                                $partners = \App\Partner::all();
                                @endphp
                                @foreach ($partners as $partner)
                                <div class="icon">
                                    <a href="{{$partner->link}}" target="_blank"><img class="img-responsive" src="{{url($partner->imageUrl)}}"></a>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-6 col-lg-4 tab_right_f">
                        <div class="footer-top">
                            <div class="tab_rnone2">
                                <h4>Nhận thông tin dự án</h4>
                                <div>
                                    <form action="" method="post">
                                        @csrf
                                        <input name="name" type="text" class="textbox name" placeholder="Họ &amp; Tên (*)" />
                                        <input name="phone_number" type="text" class="textbox phone_number" placeholder="Điện thoại (*)" />
                                        <textarea rows="3" name="messages" class="textbox messages" placeholder="Nội dung tư vấn"></textarea>
                                        <div class="row">
                                            
                                            <div class="col-xs-12 btn-form text-center">
                                                <button class="bt-register-support bt-re-price">Gửi</button>
                                            </div>
                                        </div>   
                                    </form>                             
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 footer-bottom">
                    <ul class="social">
                        <li><a href="{{__c('facebook_link')}}" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="{{__c('gplus_link')}}" target="_blank"><i class="ion-social-googleplus-outline" aria-hidden="true"></i></a></li>
                        <li><a href="{{__c('youtube_link')}}" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                    </ul>
                    <p>© 2018 {{__c('web_title')}}. All rights reserved.</p>
                </div>
            </div>
        </div>
    </footer>
    
    <script type="text/javascript">
        $(document).ready(function () {
            $(".bt-register-support").click(function (event) {
                var ethis = $(this);
                var dText = $(this).html();
                event.preventDefault();
                var parent = $(this).parent().parent().parent();
                var name = $(".name", parent);
                var mobile = $(".phone_number", parent);
                var content = $(".messages", parent);
                if (!Valid_Consultant(
                name, 'Vui lòng nhập họ & tên',
                    mobile, 'Vui lòng nhập số điện thoại'
                )) {
                    return false;
                }
                else {
                    $(this).prop("disabled", true).html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i>');
                    const form = $(this).closest('form')[0];
                    $.ajax({
                        type: "POST",
                        url: "{{route('contact.save')}}",
                        data: $(form).serialize(),
                        dataType: "json",
                        success: function (msg) {
                            bootbox.alert(msg.message);
                            if(msg.result) {
                                $(".textbox", parent).val(""); 
                            }
                        },
                        complete: function () {
                            $(ethis).prop("disabled", false).html(dText);
                        }
                    });
                }
                return false;
            });
        });
    </script>
</div>
@extends('gemriver.layout')
@section('content')
<div id="fullpage">
    <section id="slider" class="vertical-scrolling">
        <div class="slider-item">
            <div class='description'>
                {{__c('web_sub_title')}}
            </div>
        </div>
        <ul class="box">
            <!--<li class="note hoverScale"><a href="#">note</a></li>-->
            <li class="play hoverScale"><a data-fancybox href="https://www.youtube.com/watch?v={{__c('youtube_id_introduction')}}?rel=0&amp;autoplay=1">play</a></li>
        </ul>
        <a href="#section-2" class="scroll-down"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
    </section>

    <section id="about" class="vertical-scrolling section">
        <h3>Giới thiệu tổng quan</h3>
        <div class="container">
            <div class="row ab-block">
                <div class="col-sm-6 col-md-6 col-lg-6">
                    <h2>{!! __c('tong_quan_tieu_de') !!}</h2>
                    <div class="description">
                        {!! __c('tong_quan_mo_ta') !!}
                    </div>
                    <a href="{{route('tong_quan')}}" class="readmore">Xem chi tiết</a>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="photo">
                        <img class="img-responsive" src="{{url(__c('tong_quan_hinh_anh'))}}" />
                    </div>
                </div>
            </div>
        </div>
        <section id="total">
            <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 total-item total-1">
                <p class="top">{{ __c('dien_tich') }}</p>
                <p class="bottom">Tổng diện tích</p>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 total-item total-2">
                <p class="top">{{ __c('so_block') }}</p>
                <p class="bottom">Tổng số Block</p>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 total-item total-3">
                <p class="top">{{ __c('tong_can_ho') }}</p>
                <p class="bottom">Tổng căn hộ</p>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 total-item total-4">
                <p class="top">{{ __c('so_tang') }}</p>
                <p class="bottom">Số tầng của từng block</p>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 total-item total-5">
                <p class="top">{{ __c('mat_do_xay_dung_khoi_de') }}</p>
                <p class="bottom">Mật độ xây dựng khối đế</p>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 total-item total-6">
                <p class="top">{{ __c('dien_tich_xay_dung') }}</p>
                <p class="bottom">Bàn giao nhà dự kiến</p>
            </div>
        </section>
        <div class="clearfix"></div>

    </section>

    <section id="position" class="vertical-scrolling section">
        <h3>Vị trí chiến lược</h3>
        <div class="container">
            <div class="">
                <div class="position-photo">
                    <img class="img-responsive" src="{{ url(__c('vi_tri_hinh_anh_trang_chu')) }}">
                </div>
            </div>
        </div>
    </section>

    <section id="util" class="vertical-scrolling section">
        <div class="photo-items">
            <div class="title">
                <h3>Tiện ích xung quanh</h3>
            </div>
            <div id="utilitiesslide" class="photo-item">
                @php
                $slider = \App\Slider::find(3);
                @endphp
                @foreach ($slider->images as $detail)
                <div class='item photo-item-left'><img src="{{url($detail->imageUrl)}}"
                        class='img-responsive'>
                    <div class='p'>
                        <p class='top'>{{$detail->title}}</p>
                        <p class='bottom'></p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>

    <section id="furniture" class="vertical-scrolling section">
        <div class="container">
            <div class="row">
                <div class="block">
                    <div class="title">{{__c('trang_chu_section_5')}}</div>
                    <div class="content scrollbar-inner">
                        {!! __c('trang_chu_section_5_content') !!}
                        <div class="clearfix"></div>
                        <a data-toggle="modal" data-target="#vinhhalong_chitiet" class="read-more hidden">Xem chi tiết</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="news" class="vertical-scrolling section">
        <h3>{{__c('trang_chu_section_6')}}</h3>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="new-b">
                        {!! __c('trang_chu_section_6_content') !!}
                        <div class="photo">
                            <img class="img-responsive" src="{{ url(__c('trang_chu_section_6_image')) }}" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="reason" class="vertical-scrolling section">
        <div class="photo">
            {{-- <h3><span>5 lý do </span>để khách hàng chọn {{__c('web_title')}}</h3> --}}
            <img style="width: calc(100vh);" class="hidden-xs hidden-sm  img-responsive" src="{{ url(__c('ly_do_khach_hang_chon')) }}" />
            <img src='{{ url(__c('ly_do_khach_hang_chon_small_device')) }}' class='visible-sm img-responsive'>
            <img src='{{ url(__c('ly_do_khach_hang_chon_mobile')) }}' class='visible-xs  img-responsive'>
        </div>
    </section>

    <section id="news-blocks" class="vertical-scrolling section">
        <h3>Tin tức nổi bật</h3>
        <div class="container">
            <div class="row">
                @php
                $posts = \App\Post::skip(0)->take(4)->get();
                @endphp
                @foreach ($posts as $post)
                <div class='col-sm-6 col-md-6 col-lg-6'>
                        <div class='news-block n1'>
                            <div class='photo'><a href='{{route('news.detail', ['slug' => $post->slug])}}'></a>
                                <img class='img-responsive' src='{{url($post->imageUrl)}}'
                                    alt='{{$post->title}}' title='{{$post->title}}'>
                                <div class='date'>Ngày {{str_before($post->created_at, ' ')}}</div>
                            </div>
                            <div class='title'><a href='{{route('news.detail', ['slug' => $post->slug])}}'>{{$post->title}}</a></div>
                            <div class='description'><a href='{{route('news.detail', ['slug' => $post->slug])}}'>{{$post->description}}</a></div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="text-center vertical-space2">
                <a href="{{route('news')}}" class="read-more">Xem thêm</a>
            </div>
        </div>
    </section>
    <section class="vertical-scrolling fp-auto-height">
        @include('gemriver.footer')
    </section>
</div>
@endsection
@extends('admin.layout') 
@section('content')
<!-- page content -->
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Thiết lập trang</h3>
        </div>

        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                &nbsp;
                
            </div>
        </div>
        
    </div>
    <div class="clearfix"></div>
    <br />
    {!! Form::open(['url' => route('admin_config_page'), 'class' => 'form-horizontal form-label-left']) !!}
    <div class="row">
        <div class="col-md-8 col-xs-8 col-md-offset-2 col-xs-offset-2">
            <div class="x_panel">
                <div class="x_title">
                    <h2>SEO
                        <small>&nbsp;</small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" style="display: none;">
                    <br />
                        <div class="form-group">
                            {!! Form::label('keywords', 'Keywords', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('keywords', $configs->keywords, ['class' => 'form-control', 'placeholder' => "Keywords"]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('descriptions', 'Descriptions', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('descriptions', $configs->descriptions, ['class' => 'form-control', 'placeholder' => "Descriptions"]) !!}
                            </div>
                        </div>
                        
                </div>
            </div>
        </div>
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Tổng quan
                        <small>&nbsp;</small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                        <div class="form-group">
                            {!! Form::label('web_title', 'Tiêu đề web', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('web_title', $configs->web_title, ['class' => 'form-control', 'placeholder' => "Tiêu đề web"]) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('favicon', __ctitle('favicon'), ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                <div class="input-group">
                                    {!! Form::text('favicon', $configs->favicon, ['id' => 'favicon', 'readonly' => 'readonly', 'class' => 'form-control', 'placeholder' => __ctitle('favicon')]) !!}
                                    <span class="input-group-btn">
                                        <a id="lfm-favicon" data-input="favicon" data-preview="favicon_holder" class="btn btn-primary">
                                            <i class="fa fa-picture-o"></i> Chọn hình
                                        </a>
                                    </span>
                                </div>
                                <img id="favicon_holder" style="margin-top:15px;max-height:100px;">
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('web_sub_title', __ctitle('web_sub_title'), ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('web_sub_title', $configs->web_sub_title, ['class' => 'form-control', 'placeholder' => __ctitle('web_sub_title')]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('logo', 'Logo', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                <div class="input-group">
                                    {!! Form::text('logo', $configs->logo, ['id' => 'logo', 'readonly' => 'readonly', 'class' => 'form-control', 'placeholder' => "Logo"]) !!}
                                    <span class="input-group-btn">
                                        <a id="lfm-logo" data-input="logo" data-preview="logo_holder" class="btn btn-primary">
                                            <i class="fa fa-picture-o"></i> Chọn hình
                                        </a>
                                    </span>
                                </div>
                                <img id="logo_holder" style="margin-top:15px;max-height:100px;">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            {!! Form::label('vi_tri_hinh_anh_trang_chu', __ctitle('vi_tri_hinh_anh_trang_chu'), ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                <div class="input-group">
                                    {!! Form::text('vi_tri_hinh_anh_trang_chu', $configs->vi_tri_hinh_anh_trang_chu, ['id' => 'vi_tri_hinh_anh_trang_chu', 'readonly' => 'readonly', 'class' => 'form-control', 'placeholder' => __ctitle('vi_tri_hinh_anh_trang_chu')]) !!}
                                    <span class="input-group-btn">
                                        <a id="lfm-vi_tri_hinh_anh_trang_chu" data-input="vi_tri_hinh_anh_trang_chu" data-preview="vi_tri_hinh_anh_trang_chu_holder" class="btn btn-primary">
                                            <i class="fa fa-picture-o"></i> Chọn hình
                                        </a>
                                    </span>
                                </div>
                                <img id="vi_tri_hinh_anh_trang_chu_holder" style="margin-top:15px;max-height:100px;">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            {!! Form::label('bottom_web_latitude', 'Latitude', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('bottom_web_latitude', $configs->bottom_web_latitude, ['class' => 'form-control', 'placeholder' => "Latitude"]) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('bottom_web_longtitude', 'Nội dung dòng copyright', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('bottom_web_longtitude', $configs->bottom_web_longtitude, ['class' => 'form-control', 'placeholder' => "Longtitude"]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('youtube_id_introduction', __ctitle('youtube_id_introduction'), ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('youtube_id_introduction', $configs->youtube_id_introduction, ['class' => 'form-control', 'placeholder' => __ctitle('youtube_id_introduction')]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('trang_chu_section_5', __ctitle('trang_chu_section_5'), ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('trang_chu_section_5', $configs->trang_chu_section_5, ['class' => 'form-control', 'placeholder' => __ctitle('trang_chu_section_5')]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('trang_chu_section_5_content', __ctitle('trang_chu_section_5_content'), ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::textarea('trang_chu_section_5_content', $configs->trang_chu_section_5_content, ['class' => 'form-control ckeditor-all', 'placeholder' => __ctitle('trang_chu_section_5_content')]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('trang_chu_section_6', __ctitle('trang_chu_section_6'), ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('trang_chu_section_6', $configs->trang_chu_section_6, ['class' => 'form-control', 'placeholder' => __ctitle('trang_chu_section_6')]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('trang_chu_section_6_content', __ctitle('trang_chu_section_6_content'), ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::textarea('trang_chu_section_6_content', $configs->trang_chu_section_6_content, ['class' => 'form-control ckeditor-all', 'placeholder' => __ctitle('trang_chu_section_6_content')]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('trang_chu_section_6_image', __ctitle('trang_chu_section_6_image'), ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                <div class="input-group">
                                    {!! Form::text('trang_chu_section_6_image', $configs->trang_chu_section_6_image, ['id' => 'trang_chu_section_6_image', 'readonly' => 'readonly', 'class' => 'form-control', 'placeholder' => __ctitle('trang_chu_section_6_image')]) !!}
                                    <span class="input-group-btn">
                                        <a id="lfm-trang_chu_section_6_image" data-input="trang_chu_section_6_image" data-preview="trang_chu_section_6_image_holder" class="btn btn-primary">
                                            <i class="fa fa-picture-o"></i> Chọn hình
                                        </a>
                                    </span>
                                </div>
                                <img id="trang_chu_section_6_image_holder" style="margin-top:15px;max-height:100px;">
                            </div>
                        </div>

                </div>
            </div>
        </div>
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Thông tin liên hệ
                        <small>&nbsp;</small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" style="display: none">
                    <br />  
                        <div class="form-group">
                            {!! Form::label('address', 'Địa chỉ liên hệ', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('address', $configs->address, ['class' => 'form-control', 'placeholder' => "Địa chỉ liên hệ"]) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', 'Email', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('email', $configs->email, ['class' => 'form-control', 'placeholder' => "Email"]) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('hotline', 'Hotline', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('hotline', $configs->hotline, ['class' => 'form-control', 'placeholder' => "Hotline"]) !!}
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-md-offset-6">
            <div class="form-group">
                <button type="submit" class="btn btn-success">Cập nhật</button>
                <a class="btn btn-danger" href="{{ route('admin_post_list') }}">Trở lại</a>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection

@section('page_scripts')
<script type="text/javascript">
    var route_prefix = "{{ url(config('lfm.url_prefix')) }}";
    // Tags
    $('.tags').tagsInput({
        width: 'auto'
    });

    // Ckeditor
    $('#content, .ckeditor-sign').ckeditor({
        filebrowserImageBrowseUrl: route_prefix + '?type=Images',
        filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
        filebrowserBrowseUrl: route_prefix + '?type=Files',
        filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
    });
    (function( $ ){

        $.fn.filemanager = function(type, options) {
            type = type || 'file';
        
            this.on('click', function(e) {
            var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
            localStorage.setItem('target_input', $(this).find('a').attr('data-input'));
            localStorage.setItem('target_preview', $(this).find('a').attr('data-preview'));

            window.open(route_prefix + '?type=' + type, 'FileManager', 'width=900,height=600');
            return false;
            });
        }
      
      })(jQuery);
    function SetUrl(url, file_path){
        console.log(url, file_path);
        //set the value of the desired input to image url
        var target_input = $('#' + localStorage.getItem('target_input'));
        console.log(localStorage.getItem('target_input'));
        target_input.val(file_path);
      
        //set or change the preview image src
        var target_preview = $('#' + localStorage.getItem('target_preview'));
        target_preview.attr('src', url);
      }

    {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
    
    $('#favicon, #lfm-trang_chu_section_6_image, #lfm-vi_tri_hinh_anh_trang_chu, #lfm-logo,#lfm-logo_footer, #lfm-top_banner, #lfm-investors_image, #lfm-home_banner').filemanager('image', {prefix: route_prefix});
    
    if($('#favicon').val() !== '') {
        $('#' + $('#lfm-favicon').attr('data-preview')).attr('src', $('#favicon').val());
    }
    if($('#trang_chu_section_6_image').val() !== '') {
        $('#' + $('#lfm-trang_chu_section_6_image').attr('data-preview')).attr('src', $('#trang_chu_section_6_image').val());
    }
    if($('#vi_tri_hinh_anh_trang_chu').val() !== '') {
        $('#' + $('#lfm-vi_tri_hinh_anh_trang_chu').attr('data-preview')).attr('src', $('#vi_tri_hinh_anh_trang_chu').val());
    }
    if($('#logo').val() !== '') {
        $('#' + $('#lfm-logo').attr('data-preview')).attr('src', $('#logo').val());
    }
    if($('#logo_footer').val() !== '') {
        $('#' + $('#lfm-logo_footer').attr('data-preview')).attr('src', $('#logo_footer').val());
    }
    if($('#top_banner').val() !== '') {
        $('#' + $('#lfm-top_banner').attr('data-preview')).attr('src', $('#top_banner').val());
    }
    if($('#investors_image').val() !== '') {
        $('#' + $('#lfm-investors_image').attr('data-preview')).attr('src', $('#investors_image').val());
    }
    if($('#home_banner').val() !== '') {
        $('#' + $('#lfm-home_banner').attr('data-preview')).attr('src', $('#home_banner').val());
    }
    
    
    @if ($errors->any())
    let errorMessages = `
    @foreach ($errors->all() as $error)
        {{$error}} \n
    @endforeach
    `;
    new PNotify({
        title: 'Lỗi nhập liệu',
        type: 'error',
        text: errorMessages,
        nonblock: {
            nonblock: true
        },
        styling: 'bootstrap3',
        hide: false
    });
    @endif
    
    @if(Session::has('success_message'))
    let successMessages = `{{session('success_message')}}`;
    new PNotify({
        title: 'Success',
        type: 'success',
        text: successMessages,
        nonblock: {
            // nonblock: true
        },
        styling: 'bootstrap3',
    });
    @endif
</script>
@endsection
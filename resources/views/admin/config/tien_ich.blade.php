@extends('admin.layout') 
@section('content')
<!-- page content -->
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Thiết lập trang</h3>
        </div>

        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                &nbsp;
                
            </div>
        </div>
        
    </div>
    <div class="clearfix"></div>
    <br />
    {!! Form::open(['url' => route('admin_config_page'), 'class' => 'form-horizontal form-label-left']) !!}
    <div class="row">
        
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Tổng quan
                        <small>&nbsp;</small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />


                    <div class="form-group">
                        {!! Form::label('cot_tien_ich_1', __ctitle('cot_tien_ich_1'), ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            {!! Form::textarea('cot_tien_ich_1', $configs->cot_tien_ich_1, ['class' => 'form-control ckeditor-all', 'placeholder' => __ctitle('cot_tien_ich_1')]) !!}
                        </div>
                    </div>
                    
                    <div class="form-group">
                        {!! Form::label('cot_tien_ich_2', __ctitle('cot_tien_ich_2'), ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            {!! Form::textarea('cot_tien_ich_2', $configs->cot_tien_ich_2, ['class' => 'form-control ckeditor-all', 'placeholder' => __ctitle('cot_tien_ich_2')]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('cot_tien_ich_3', __ctitle('cot_tien_ich_3'), ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            {!! Form::textarea('cot_tien_ich_3', $configs->cot_tien_ich_3, ['class' => 'form-control ckeditor-all', 'placeholder' => __ctitle('cot_tien_ich_3')]) !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-md-offset-6">
            <div class="form-group">
                <button type="submit" class="btn btn-success">Cập nhật</button>
                <a class="btn btn-danger" href="{{ route('admin_post_list') }}">Trở lại</a>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection

@section('page_scripts')
<script type="text/javascript">
    var route_prefix = "{{ url(config('lfm.url_prefix')) }}";
    // Tags
    $('.tags').tagsInput({
        width: 'auto'
    });

    // Ckeditor
    $('#content, .ckeditor-sign').ckeditor({
        filebrowserImageBrowseUrl: route_prefix + '?type=Images',
        filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
        filebrowserBrowseUrl: route_prefix + '?type=Files',
        filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
    });
    (function( $ ){

        $.fn.filemanager = function(type, options) {
            type = type || 'file';
        
            this.on('click', function(e) {
            var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
            localStorage.setItem('target_input', $(this).find('a').attr('data-input'));
            localStorage.setItem('target_preview', $(this).find('a').attr('data-preview'));

            window.open(route_prefix + '?type=' + type, 'FileManager', 'width=900,height=600');
            return false;
            });
        }
      
      })(jQuery);
    function SetUrl(url, file_path){
        console.log(url, file_path);
        //set the value of the desired input to image url
        var target_input = $('#' + localStorage.getItem('target_input'));
        console.log(localStorage.getItem('target_input'));
        target_input.val(file_path);
      
        //set or change the preview image src
        var target_preview = $('#' + localStorage.getItem('target_preview'));
        target_preview.attr('src', url);
      }

    {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
    
    $('#lfm-chu_dau_tu_hinh_anh, #lfm-ly_do_khach_hang_chon, #lfm-ly_do_khach_hang_chon_mobile,#lfm-ly_do_khach_hang_chon_small_device, #lfm-top_banner, #lfm-investors_image, #lfm-home_banner').filemanager('image', {prefix: route_prefix});
    if($('#chu_dau_tu_hinh_anh').val() !== '') {
        $('#' + $('#lfm-chu_dau_tu_hinh_anh').attr('data-preview')).attr('src', $('#chu_dau_tu_hinh_anh').val());
    }
    if($('#vi_tri_hinh_anh_trang_chu').val() !== '') {
        $('#' + $('#lfm-vi_tri_hinh_anh_trang_chu').attr('data-preview')).attr('src', $('#vi_tri_hinh_anh_trang_chu').val());
    }
    if($('#ly_do_khach_hang_chon_mobile').val() !== '') {
        $('#' + $('#lfm-ly_do_khach_hang_chon_mobile').attr('data-preview')).attr('src', $('#ly_do_khach_hang_chon_mobile').val());
    }
    if($('#ly_do_khach_hang_chon_small_device').val() !== '') {
        $('#' + $('#lfm-ly_do_khach_hang_chon_small_device').attr('data-preview')).attr('src', $('#ly_do_khach_hang_chon_small_device').val());
    }
    if($('#top_banner').val() !== '') {
        $('#' + $('#lfm-top_banner').attr('data-preview')).attr('src', $('#top_banner').val());
    }
    if($('#investors_image').val() !== '') {
        $('#' + $('#lfm-investors_image').attr('data-preview')).attr('src', $('#investors_image').val());
    }
    if($('#home_banner').val() !== '') {
        $('#' + $('#lfm-home_banner').attr('data-preview')).attr('src', $('#home_banner').val());
    }
    
    
    @if ($errors->any())
    let errorMessages = `
    @foreach ($errors->all() as $error)
        {{$error}} \n
    @endforeach
    `;
    new PNotify({
        title: 'Lỗi nhập liệu',
        type: 'error',
        text: errorMessages,
        nonblock: {
            nonblock: true
        },
        styling: 'bootstrap3',
        hide: false
    });
    @endif
    
    @if(Session::has('success_message'))
    let successMessages = `{{session('success_message')}}`;
    new PNotify({
        title: 'Success',
        type: 'success',
        text: successMessages,
        nonblock: {
            // nonblock: true
        },
        styling: 'bootstrap3',
    });
    @endif
</script>
@endsection
<a href="{{ route('admin_post_category_edit', ['post_category' => $id]) }}" class="btn btn-success btn-sm">
    Chỉnh sửa
</a>
<a data-id="{{$id}}" data-title="{{$title}}" href="{{ route('admin_post_category_delete', ['post_category' => $id]) }}" class="btn btn-danger btn-sm btn-delete">
    Xóa
</a>
@extends('admin.layout') 
@section('content')
<!-- page content -->
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Tạo danh mục bài viết mới</h3>
        </div>

        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                &nbsp;
                
            </div>
        </div>
        
    </div>
    <div class="clearfix"></div>
    <br />
    {!! Form::open(['url' => route('admin_post_category_create'), 'class' => 'form-horizontal form-label-left']) !!}
    <div class="row">
        <div class="col-md-8 col-xs-8 col-md-offset-2 col-xs-offset-2">
            <div class="x_panel">
                <div class="x_title">
                    <h2>SEO
                        <small>&nbsp;</small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" style="display: none;">
                    <br />
                        <div class="form-group">
                            {!! Form::label('seo_keywords', 'Keywords', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('seo_keywords', '', ['class' => 'form-control', 'placeholder' => "Keywords"]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('seo_description', 'Descriptions', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('seo_description', '', ['class' => 'form-control', 'placeholder' => "Descriptions"]) !!}
                            </div>
                        </div>
                        
                </div>
            </div>
        </div>
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Tổng quan
                        <small>&nbsp;</small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                        <div class="form-group">
                            {!! Form::label('title', 'Tên danh mục bài viết', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('title', '', ['class' => 'form-control', 'placeholder' => "Tên danh mục bài viết"]) !!}
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-md-offset-6">
            <div class="form-group">
                <button type="submit" class="btn btn-success">Tạo</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection

@section('page_scripts')
<script type="text/javascript">
    var route_prefix = "{{ url(config('lfm.url_prefix')) }}";
    // Tags
    $('.tags').tagsInput({
        width: 'auto'
    });

    // Ckeditor
    {{--  $('#introduction').ckeditor({
        filebrowserImageBrowseUrl: route_prefix + '?type=Images',
        filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
        filebrowserBrowseUrl: route_prefix + '?type=Files',
        filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
    });  --}}
    (function( $ ){

        $.fn.filemanager = function(type, options) {
            type = type || 'file';
        
            this.on('click', function(e) {
            var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
            localStorage.setItem('target_input', $(this).find('a').attr('data-input'));
            localStorage.setItem('target_preview', $(this).find('a').attr('data-preview'));

            window.open(route_prefix + '?type=' + type, 'FileManager', 'width=900,height=600');
            return false;
            });
        }
      
      })(jQuery);
    function SetUrl(url, file_path){
        console.log(url, file_path);
        //set the value of the desired input to image url
        var target_input = $('#' + localStorage.getItem('target_input'));
        console.log(localStorage.getItem('target_input'));
        target_input.val(file_path);
      
        //set or change the preview image src
        var target_preview = $('#' + localStorage.getItem('target_preview'));
        target_preview.attr('src', url);
      }

    {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
    
    $('#lfm-location').filemanager('image', {prefix: route_prefix});
    $('#lfm-project_images\\[\\]').each((index, value) => {
        $(value).filemanager('image', {prefix: route_prefix});
    });
    // Default images
    $(document).ready(function() {
        if($('#location').val() !== '') {
            $('#' + $('#lfm-location').attr('data-preview')).attr('src', $('#location').val());
        }
        $('.project_images').each((index, value) => {
            $('#' + $(value).find('a').attr('data-preview')).attr('src', $(value).find('input').val());
        });
    });
    @if (old('project_images') && count(old('project_images')) > 0)
    let numImages = {{ count(old('project_images')) }};
    @else
    let numImages = 1;
    @endif

    // Add Image Input
    $('#addProjectImage').on('click', function(e) {
        let clonedProjectImages = $(`<div class="form-group project_images">
            {!! Form::label('project_images[]', 'Ảnh danh mục bài viết', ['class'=> 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
            <div class="col-md-9 col-sm-9 col-xs-12">
                <div class="input-group">
                    {!! Form::text('project_images[]', '', ['id' => 'project_images_0', 'readonly' => 'readonly', 'class' => 'form-control', 'placeholder' => "Ảnh danh mục bài viết"]) !!}
                    <span class="input-group-btn">
                        <a id="lfm-project_images[]" data-input="project_images_0" data-preview="project_image_holder_0" class="btn btn-primary">
                            <i class="fa fa-picture-o"></i> Chọn hình
                        </a>
                        <button type="button" class="removeProjectImage btn btn-danger"><i class="fa fa-times"></i>Bỏ chọn</button>
                    </span>
                </div>
                <img id="project_image_holder_0" style="margin-top:15px;max-height:100px;">
            </div>
        </div>`);
        clonedProjectImages.find('input').attr('id', 'project_images_' + (numImages+1));
        clonedProjectImages.find('img').attr('id', 'project_image_holder_' + (numImages+1));
        clonedProjectImages.find('a').attr('data-input', 'project_images_' + (numImages+1))
                                    .attr('data-preview', 'project_image_holder_' + (numImages+1));
        
        clonedProjectImages.filemanager = function(type, options) {
            type = type || 'file';
        
            this.on('click', function(e) {
            var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
            localStorage.setItem('target_input', clonedProjectImages.find('a').attr('data-input'));
            localStorage.setItem('target_preview', clonedProjectImages.find('a').attr('data-preview'));

            window.open(route_prefix + '?type=' + type, 'FileManager', 'width=900,height=600');
            return false;
            });
        };
        $('.project_images:last').after(clonedProjectImages);
        
        clonedProjectImages.filemanager('image', {prefix: route_prefix});
        numImages++;
    });
    $(document).on('click', '.removeProjectImage', function(e) {
        if($('.project_images').length > 1) {
            $(this).parents('.project_images').remove();
        }
    });
    @if ($errors->any())
    let errorMessages = `
    @foreach ($errors->all() as $error)
        {{$error}} \n
    @endforeach
    `;
    new PNotify({
        title: 'Lỗi nhập liệu',
        type: 'error',
        text: errorMessages,
        nonblock: {
            nonblock: true
        },
        styling: 'bootstrap3',
        hide: false
    });
    @endif
    @if(Session::has('success_message'))
    let successMessages = `{{session('success_message')}}`;
    new PNotify({
        title: 'Success',
        type: 'success',
        text: successMessages,
        nonblock: {
            // nonblock: true
        },
        styling: 'bootstrap3',
    });
    @endif
</script>
@endsection
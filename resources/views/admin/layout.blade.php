<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="images/favicon.ico" type="image/ico" />

<title>@yield('title', 'Admin Panel') - {{$public_configs->web_title}}</title>

    <!-- Font Awesome -->
    <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

    <!-- Bootstrap -->
    <!-- NProgress -->
    <!-- iCheck -->
    <!-- bootstrap-progressbar -->
    <!-- JQVMap -->
    <!-- bootstrap-daterangepicker -->
    <!-- Custom Theme Style -->
    <link href="{{ mix('admin-template/css/app.css') }}" rel="stylesheet">
    {{--  <link href="{{ asset('admin-template/css/app.css') }}" rel="stylesheet">  --}}
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="{{ route('admin_home') }}" class="site_title">
                            <i class="fa fa-paw"></i>
                            <span>{{$public_configs->web_title}}</span>
                        </a>
                    </div>

                    <div class="clearfix"></div>

                    <!-- menu profile quick info -->
                    <div class="profile clearfix">
                        <div class="profile_pic">
                            <img src="{{ url('images/user.png') }}" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2>{{ Auth::user()->name }}</h2>
                        </div>
                    </div>
                    <!-- /menu profile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    @include('admin.left_sidebar_menu')
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    {{--  <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>  --}}
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            @include('admin.top_navbar')
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                @yield('content')
                <br />
            </div>
            <!-- /page content -->

            <!-- footer content -->
            <footer>
                <div class="pull-right">
                    &copy; Willx
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
        </div>
    </div>

    <!-- jQuery -->
    <!-- Bootstrap -->
    <!-- FastClick -->
    <!-- NProgress -->
    <!-- Chart.js -->
    <!-- gauge.js -->
    <!-- bootstrap-progressbar -->
    <!-- iCheck -->
    <!-- Skycons -->
    <!-- Flot -->
    <!-- Flot plugins -->
    <!-- DateJS -->
    <!-- JQVMap -->
    <!-- bootstrap-daterangepicker -->
    <!-- Custom Theme Scripts -->
    <script src="{{ mix('admin-template/js/app.js') }}"></script>
    <script src="{{ asset('admin-template/statics/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('admin-template/statics/ckeditor/adapters/jquery.js') }}"></script>
    
    @yield('page_scripts', '// Page doesnt have any scripts')
    
</body>

</html>
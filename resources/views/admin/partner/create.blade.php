@extends('admin.layout') 
@section('content')
<!-- page content -->
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Tạo đối tác mới</h3>
        </div>

        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                &nbsp;
                
            </div>
        </div>
        
    </div>
    <div class="clearfix"></div>
    <br />
    {!! Form::open(['url' => route('admin_partner_create'), 'class' => 'form-horizontal form-label-left']) !!}
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Tổng quan
                        <small>&nbsp;</small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                        <div class="form-group">
                            {!! Form::label('title', 'Tên đối tác', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('title', '', ['class' => 'form-control', 'placeholder' => "Tên đối tác"]) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('sub_title', __ctitle('sub_title'), ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('sub_title', '', ['class' => 'form-control', 'placeholder' => __ctitle('sub_title')]) !!}
                            </div>
                        </div>
                        
        

                        <div class="form-group">
                            {!! Form::label('imageUrl', 'Ảnh đối tác', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                <div class="input-group">
                                    {!! Form::text('imageUrl', '', ['id' => 'imageUrl', 'readonly' => 'readonly', 'class' => 'form-control', 'placeholder' => "Ảnh đối tác"]) !!}
                                    <span class="input-group-btn">
                                        <a id="lfm-imageUrl" data-input="imageUrl" data-preview="partnerimage_holder" class="btn btn-primary">
                                            <i class="fa fa-picture-o"></i> Chọn hình
                                        </a>
                                    </span>
                                </div>
                                <img id="partnerimage_holder" style="margin-top:15px;max-height:100px;">
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('description', __ctitle('description'), ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::textarea('description', '', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('link', __ctitle('link'), ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('link', '', ['class' => 'form-control', 'placeholder' => __ctitle('link')]) !!}
                            </div>
                        </div>
                        {{--  <div class="ln_solid"></div>  --}}
                        
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-md-offset-6">
            <div class="form-group">
                <button type="submit" class="btn btn-success">Tạo</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection

@section('page_scripts')
<script type="text/javascript">
    var route_prefix = "{{ url(config('lfm.url_prefix')) }}";
    // Tags
    $('.tags').tagsInput({
        width: 'auto'
    });

    // Ckeditor
    $('#description').ckeditor({
        filebrowserImageBrowseUrl: route_prefix + '?type=Images',
        filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
        filebrowserBrowseUrl: route_prefix + '?type=Files',
        filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
    });
    (function( $ ){

        $.fn.filemanager = function(type, options) {
            type = type || 'file';
        
            this.on('click', function(e) {
            var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
            localStorage.setItem('target_input', $(this).find('a').attr('data-input'));
            localStorage.setItem('target_preview', $(this).find('a').attr('data-preview'));

            window.open(route_prefix + '?type=' + type, 'FileManager', 'width=900,height=600');
            return false;
            });
        }
      
      })(jQuery);
    function SetUrl(url, file_path){
        console.log(url, file_path);
        //set the value of the desired input to image url
        var target_input = $('#' + localStorage.getItem('target_input'));
        console.log(localStorage.getItem('target_input'));
        target_input.val(file_path);
      
        //set or change the preview image src
        var target_preview = $('#' + localStorage.getItem('target_preview'));
        target_preview.attr('src', url);
      }

    {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
    
    $('#lfm-imageUrl').filemanager('image', {prefix: route_prefix});

    if($('#imageUrl').val() !== '') {
        $('#' + $('#lfm-imageUrl').attr('data-preview')).attr('src', $('#imageUrl').val());
    }

    @if ($errors->any())
    let errorMessages = `
    @foreach ($errors->all() as $error)
        {{$error}} \n
    @endforeach
    `;
    new PNotify({
        title: 'Lỗi nhập liệu',
        type: 'error',
        text: errorMessages,
        nonblock: {
            nonblock: true
        },
        styling: 'bootstrap3',
        hide: false
    });
    @endif
    @if(Session::has('success_message'))
    let successMessages = `{{session('success_message')}}`;
    new PNotify({
        title: 'Success',
        type: 'success',
        text: successMessages,
        nonblock: {
            // nonblock: true
        },
        styling: 'bootstrap3',
    });
    @endif
</script>
@endsection
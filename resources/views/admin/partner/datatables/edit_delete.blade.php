<a href="{{ route('admin_partner_edit', ['partner' => $id]) }}" class="btn btn-success btn-sm">
    Chỉnh sửa
</a>
<a data-id="{{$id}}" data-title="{{$title}}" href="{{ route('admin_partner_delete', ['partner' => $id]) }}" class="btn btn-danger btn-sm btn-delete">
    Xóa
</a>
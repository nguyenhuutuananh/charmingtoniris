@extends('admin.layout')
@section('content')
<!-- top tiles -->
<div class="row tile_count">
    <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top">
            <i class="fa fa-user"></i> Tổng số dự án</span>
        <div class="count">{{number_format(App\Project::count())}}</div>
        {{--  <span class="count_bottom">
            <i class="green">4% </i> From last Week</span>  --}}
    </div>
    <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top">
            <i class="fa fa-clock-o"></i> Tổng số người gửi liên hệ</span>
        <div class="count green">{{number_format(App\Contact::count())}}</div>
        {{--  <span class="count_bottom">
            <i class="green">
                <i class="fa fa-sort-asc"></i>3% </i> From last Week</span>  --}}
    </div>
    <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top">
            <i class="fa fa-user"></i> Tổng số danh mục bài viết</span>
        <div class="count">{{number_format(App\PostCategory::count())}}</div>
        {{--  <span class="count_bottom">
            <i class="green">
                <i class="fa fa-sort-asc"></i>34% </i> From last Week</span>  --}}
    </div>
    <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top">
            <i class="fa fa-user"></i> Tổng số bài viết</span>
        <div class="count">{{number_format(App\Post::count())}}</div>
        {{--  <span class="count_bottom">
            <i class="red">
                <i class="fa fa-sort-desc"></i>12% </i> From last Week</span>  --}}
    </div>
</div>
<!-- /top tiles -->
@endsection
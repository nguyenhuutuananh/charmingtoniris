<a href="{{ route('admin_project_edit', ['user' => $id]) }}" class="btn btn-success btn-sm">
    Chỉnh sửa
</a>
<a data-id="{{$id}}" data-name="{{$title}}" href="{{ route('admin_project_delete', ['user' => $id]) }}" class="btn btn-danger btn-sm btn-delete">
    Xóa
</a>
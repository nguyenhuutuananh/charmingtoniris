@extends('admin.layout') 
@section('content')
<!-- page content -->
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Chỉnh sửa dự án</h3>
        </div>
        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                &nbsp;
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <br /> {!! Form::open(['url' => route('admin_project_edit', ['user' => $project->id]), 'class' => 'form-horizontal form-label-left'])
    !!}
    <div class="row">
        {{-- <div class="col-md-8 col-xs-8 col-md-offset-2 col-xs-offset-2">
            <div class="x_panel">
                <div class="x_title">
                    <h2>SEO
                        <small>&nbsp;</small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" style="display: none;">
                    <br />
                    <div class="form-group">
                        {!! Form::label('seo_keywords', 'Keywords', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            {!! Form::text('seo_keywords', $project->seo_keywords, ['class' => 'form-control', 'placeholder' => "Keywords"]) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('seo_description', 'Descriptions', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            {!! Form::text('seo_description', $project->seo_description, ['class' => 'form-control', 'placeholder' => "Descriptions"])
                            !!}
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Tổng quan
                        <small>&nbsp;</small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                        <div class="col col-md-7 col-md-offset-5">
                            <div class="form-group">
                                {!! Form::label('category_id ', 'Loại dự án', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    
                                    <div class="">
                                        <label>
                                            {{--  {!!  Form::checkbox('category_id', '1', null, ['class' => 'js-switch']) !!}  --}}
                                            {!! Form::select('category_id', \App\ProjectCategory::pluck('title', 'id'), $project->category_id, ['class' => 'form-control']) !!}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col col-md-7 col-md-offset-5">
                            <div class="form-group">
                                {!! Form::label('furniture_id ', 'Nội thất', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    
                                    <div class="">
                                        <label>
                                            {{--  {!!  Form::checkbox('furniture_id', '1', null, ['class' => 'js-switch']) !!}  --}}
                                            {!! Form::select('furniture_id', \App\Furniture::pluck('title', 'id'), $project->furniture_id, ['class' => 'form-control']) !!}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        {{-- <div class="form-group">
                            {!! Form::label('isFeatured ', 'Nổi bật', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                
                                <div class="">
                                    <label>
                                        {!! Form::hidden('isFeatured', 0) !!}
                                        {!!  Form::checkbox('isFeatured', '1', null, ['class' => 'js-switch']) !!}
                                    </label>
                                </div>
                            </div>
                        </div> --}}

                        <div class="form-group">
                            {!! Form::label('title', 'Tên dự án', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('title', $project->title, ['class' => 'form-control', 'placeholder' => "Tên dự án"]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('acreages', 'Diện tích', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('acreages', $project->acreages, ['class' => 'form-control', 'placeholder' => "VD: 40"]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('bed_rooms', 'Số phòng ngủ', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('bed_rooms', $project->bed_rooms, ['class' => 'form-control', 'placeholder' => "VD: 2"]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('toilets', 'Số phòng WC', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('toilets', $project->toilets, ['class' => 'form-control', 'placeholder' => "VD: 2"]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('garages', 'Garage', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('garages', $project->garages, ['class' => 'form-control', 'placeholder' => "VD: 2"]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('price', 'Giá', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('price', $project->price, ['class' => 'form-control', 'placeholder' => "VD: 3000000000"]) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('price_text', 'Giá hiển thị dạng chữ', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('price_text', $project->price_text, ['class' => 'form-control', 'placeholder' => "VD: 3 tỉ"]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('management_fee', 'Phí quản lý', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('management_fee', $project->management_fee, ['class' => 'form-control', 'placeholder' => "VD: 150.000/năm"]) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('vat', 'Thuế VAT', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('vat', $project->vat, ['class' => 'form-control', 'placeholder' => "VD: Đã tính"]) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('motorcycle_fee', 'Phí giữ xe máy', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('motorcycle_fee', $project->motorcycle_fee, ['class' => 'form-control', 'placeholder' => "VD: 2.000/xe"]) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('car_fee', 'Phí giữ xe hơi', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::text('car_fee', $project->car_fee, ['class' => 'form-control', 'placeholder' => "VD: 15.000/xe"]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('description', 'Mô tả', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::textarea('description', $project->description, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('content', 'Nội dung', ['class'=> 'control-label col-md-2 col-sm-2 col-xs-12']) !!}
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                {!! Form::textarea('content', $project->content, ['class' => 'form-control ckeditor-all']) !!}
                            </div>
                        </div>
                        
                        {{--  <div class="ln_solid"></div>  --}}
                        
                </div>
            </div>
        </div>
        
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Hình ảnh (*)
                        <small>&nbsp;</small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br /> @foreach ($project->images as $project_image)
                    <div class="form-group project_images">
                        {!! Form::label('project_images[]', 'Ảnh dự án', ['class'=> 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <div class="input-group">
                                {!! Form::text('project_images[]', $project_image->imageUrl, ['id' => 'project_images_'.$loop->index, 'readonly' => 'readonly',
                                'class' => 'form-control', 'placeholder' => "Ảnh dự án"]) !!}
                                <span class="input-group-btn">
                                    <a id="lfm-project_images[]" data-input="project_images_{{$loop->index}}" data-preview="project_image_holder_{{$loop->index}}" class="btn btn-primary">
                                        <i class="fa fa-picture-o"></i> Chọn hình
                                    </a>
                                    <button type="button" class="removeProjectImage btn btn-danger"><i class="fa fa-times"></i>Bỏ chọn</button>
                                </span>
                            </div>
                            <img id="project_image_holder_{{$loop->index}}" style="margin-top:15px;max-height:100px;">
                        </div>
                    </div>
                    @endforeach

                    <div class="text-center">
                        <button id="addProjectImage" type="button" class="btn btn-success" style="width: 100px"><i class="fa fa-plus"></i> </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-md-offset-6">
            <div class="form-group">
                <button type="submit" class="btn btn-success">Cập nhật</button>
                <a class="btn btn-danger" href="{{ route('admin_project_list') }}">Trở lại</a>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection
 
@section('page_scripts')
<script type="text/javascript">
    var route_prefix = "{{ url(config('lfm.url_prefix')) }}";
    // Tags
    $('.tags').tagsInput({
        width: 'auto'
    });
    // Ckeditor
    {{--  $('#introduction').ckeditor({
        filebrowserImageBrowseUrl: route_prefix + '?type=Images',
        filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
        filebrowserBrowseUrl: route_prefix + '?type=Files',
        filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
    });  --}}
    (function( $ ){
        $.fn.filemanager = function(type, options) {
            type = type || 'file';
        
            this.on('click', function(e) {
            var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
            localStorage.setItem('target_input', $(this).find('a').attr('data-input'));
            localStorage.setItem('target_preview', $(this).find('a').attr('data-preview'));
            window.open(route_prefix + '?type=' + type, 'FileManager', 'width=900,height=600');
            return false;
            });
        }
      
      })(jQuery);
    function SetUrl(url, file_path){
        console.log(url, file_path);
        //set the value of the desired input to image url
        var target_input = $('#' + localStorage.getItem('target_input'));
        console.log(localStorage.getItem('target_input'));
        target_input.val(file_path);
      
        //set or change the preview image src
        var target_preview = $('#' + localStorage.getItem('target_preview'));
        target_preview.attr('src', url);
      }
    {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
    
    $('#lfm-location').filemanager('image', {prefix: route_prefix});
    $('#lfm-project_images\\[\\]').each((index, value) => {
        $(value).filemanager('image', {prefix: route_prefix});
    });
    // Default images
    $(document).ready(function() {
        if($('#location').val() !== '') {
            $('#' + $('#lfm-location').attr('data-preview')).attr('src', $('#location').val());
        }
        $('.project_images').each((index, value) => {
            $('#' + $(value).find('a').attr('data-preview')).attr('src', $(value).find('input').val());
        });
    });
    @if (old('project_images') && count(old('project_images')) > 0)
    let numImages = {{ count(old('project_images')) }};
    @else
    let numImages = 1;
    @endif
    // Add Image Input
    $('#addProjectImage').on('click', function(e) {
        let clonedProjectImages = $(`<div class="form-group project_images">
            {!! Form::label('project_images[]', 'Ảnh dự án', ['class'=> 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
            <div class="col-md-9 col-sm-9 col-xs-12">
                <div class="input-group">
                    {!! Form::text('project_images[]', '', ['id' => 'project_images_0', 'readonly' => 'readonly', 'class' => 'form-control', 'placeholder' => "Ảnh dự án"]) !!}
                    <span class="input-group-btn">
                        <a id="lfm-project_images[]" data-input="project_images_0" data-preview="project_image_holder_0" class="btn btn-primary">
                            <i class="fa fa-picture-o"></i> Chọn hình
                        </a>
                        <button type="button" class="removeProjectImage btn btn-danger"><i class="fa fa-times"></i>Bỏ chọn</button>
                    </span>
                </div>
                <img id="project_image_holder_0" style="margin-top:15px;max-height:100px;">
            </div>
        </div>`);
        clonedProjectImages.find('input').attr('id', 'project_images_' + (numImages+1));
        clonedProjectImages.find('img').attr('id', 'project_image_holder_' + (numImages+1));
        clonedProjectImages.find('a').attr('data-input', 'project_images_' + (numImages+1))
                                    .attr('data-preview', 'project_image_holder_' + (numImages+1));
        
        clonedProjectImages.find('a').filemanager = function(type, options) {
            type = type || 'file';
        
            this.on('click', function(e) {
            var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
            localStorage.setItem('target_input', clonedProjectImages.find('a').attr('data-input'));
            localStorage.setItem('target_preview', clonedProjectImages.find('a').attr('data-preview'));
            window.open(route_prefix + '?type=' + type, 'FileManager', 'width=900,height=600');
            return false;
            });
        };
        $('.project_images:last').after(clonedProjectImages);
        
        clonedProjectImages.find('a').filemanager('image', {prefix: route_prefix});
        numImages++;
    });
    $(document).on('click', '.removeProjectImage', function(e) {
        if($('.project_images').length > 1) {
            $(this).parents('.project_images').remove();
        }
    });
    @if ($errors->any())
    let errorMessages = `
    @foreach ($errors->all() as $error)
        {{$error}} \n
    @endforeach
    `;
    new PNotify({
        title: 'Lỗi nhập liệu',
        type: 'error',
        text: errorMessages,
        nonblock: {
            nonblock: true
        },
        styling: 'bootstrap3',
        hide: false
    });
    @endif
    @if(Session::has('success_message'))
    let successMessages = `{{session('success_message')}}`;
    new PNotify({
        title: 'Success',
        type: 'success',
        text: successMessages,
        nonblock: {
            // nonblock: true
        },
        styling: 'bootstrap3',
    });
    @endif
</script>
@endsection
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">
            <li>
                <a>
                    <i class="fa fa-home"></i> Home
                    <span class="fa fa-chevron-down"></span>
                </a>
                <ul class="nav child_menu">
                    <li>
                        <a href="{{route('admin_home')}}">Dashboard</a>
                    </li>
                    <li>
                        <a href="{{route('admin_config_page')}}">Thiết lập trang</a>
                    </li>
                    <li>
                        <a href="{{route('admin_config_tong_quan')}}">Thiết lập trang tổng quan</a>
                    </li>
                    <li>
                        <a href="{{route('admin_config_vi_tri')}}">Thiết lập vị trí</a>
                    </li>
                    <li>
                        <a href="{{route('admin_config_tien_ich')}}">Thiết lập trang tiện ích</a>
                    </li>
                    <li>
                        <a href="{{route('admin_config_chu_dau_tu')}}">Thiết lập chủ đầu tư</a>
                    </li>
                </ul>
            </li>
            <li>
                <a>
                    <i class="fa fa-edit"></i> Slider
                    <span class="fa fa-chevron-down"></span>
                </a>
                <ul class="nav child_menu">
                        <li>
                            <a href="{{ route('admin_slider_detail_list') }}">Danh sách Slider</a>
                        </li>
                        <li>
                            <a href="{{ route('admin_slider_detail_create') }}">Tạo Slider mới</a>
                        </li>
                </ul>
            </li>
            {{-- <li>
                <a>
                    <i class="fa fa-desktop"></i> Dự án
                    <span class="fa fa-chevron-down"></span>
                </a>
                <ul class="nav child_menu">
                    <li>
                        <a href="{{ route('admin_project_list') }}">Danh sách dự án</a>
                    </li>
                    <li>
                        <a href="{{ route('admin_project_create') }}">Tạo dự án mới</a>
                    </li>
                </ul>
            </li> --}}
            <li>
                <a>
                    <i class="fa fa-edit"></i> Trang
                    <span class="fa fa-chevron-down"></span>
                </a>
                <ul class="nav child_menu">
                        <li>
                            <a href="{{ route('admin_page_list') }}">Danh sách trang</a>
                        </li>
                </ul>
            </li>
            <li>
                <a>
                    <i class="fa fa-edit"></i> Bài viết
                    <span class="fa fa-chevron-down"></span>
                </a>
                <ul class="nav child_menu">
                        <li>
                            <a href="{{ route('admin_post_list') }}">Danh sách bài viết</a>
                        </li>
                        <li>
                            <a href="{{ route('admin_post_create') }}">Tạo bài viết mới</a>
                        </li>
                        {{-- <li>
                            <a href="{{ route('admin_post_category_list') }}">Danh sách danh mục bài viết</a>
                        </li>
                        <li>
                            <a href="{{ route('admin_post_category_create') }}">Tạo danh mục bài viết mới</a>
                        </li> --}}
                </ul>
            </li>
            <li>
                <a>
                    <i class="fa fa-edit"></i> Đối tác
                    <span class="fa fa-chevron-down"></span>
                </a>
                <ul class="nav child_menu">
                        <li>
                            <a href="{{ route('admin_partner_list') }}">Danh sách đối tác</a>
                        </li>
                        <li>
                            <a href="{{ route('admin_partner_create') }}">Tạo đối tác mới</a>
                        </li>
                </ul>
            </li>
            <li>
                <a>
                    <i class="fa fa-table"></i> Mail liên hệ
                    <span class="fa fa-chevron-down"></span>
                </a>
                <ul class="nav child_menu">
                    <li>
                        <a href="{{route('admin_contact_list')}}">Danh sách mail liên hệ</a>
                    </li>
                    <li>
                        <a href="{{route('admin_contact_special_list')}}">Danh sách mail ký gửi</a>
                    </li>
                </ul>
            </li>
            <li>
                <a>
                    <i class="fa fa-bar-chart-o"></i> Quản lý tập tin
                    <span class="fa fa-chevron-down"></span>
                </a>
                <ul class="nav child_menu">
                    <li>
                        <a href="{{url('filemanager?type=image')}}">Vào trang quản lý</a>
                    </li>
                    
                </ul>
            </li>
        </ul>
    </div>

</div>
<a href="{{ route('admin_slider_detail_edit', ['slider_detail' => $id]) }}" class="btn btn-success btn-sm">
    Chỉnh sửa
</a>
<a data-id="{{$id}}" data-title="{{$title}}" href="{{ route('admin_slider_detail_delete', ['slider_detail' => $id]) }}" class="btn btn-danger btn-sm btn-delete">
    Xóa
</a>
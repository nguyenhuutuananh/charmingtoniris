@extends('admin.layout') 
@section('content')
<!-- page content -->
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Danh sách slider</h3>
        </div>

        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <a href="{{ route('admin_slider_detail_create') }}" class="btn btn-success">Tạo slider mới</a>
                
            </div>
        </div>
        
    </div>
    <div class="clearfix"></div>
    <br />
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
        <div class="x_title">
            <h2>Danh sách slider <small></small></h2>
            <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <p class="text-muted font-13 m-b-30">
            
            </p>
            <table id="slider_detail_list" class="table table-striped table-bordered">
            <thead>
                <tr>
                <th>ID</th>
                <th>Tiêu đề slider</th>
                <th>Hình ảnh</th>
                <th>Danh mục</th>
                <th>Tác vụ</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
        </div>
        </div>
    </div>
</div>
<div class="modal fade deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel2">Xóa slider</h4>
            </div>
            <div class="modal-body">
                <h4></h4>
                <p>Bạn có chắc muốn xóa slider này?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-sure">Xóa</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>

        </div>
    </div>
</div>
@endsection
@section('page_scripts')
<script type="text/javascript">
    let slider_detail_list = $("#slider_detail_list").DataTable({
        fixedHeader: true,
        serverSide: true,
        processing: true,
        ajax: '{{ route("admin_slider_detail_ajax_list") }}',
        columns: [
                {data: 'id'},
                {data: 'title'},
                {data: 'imageUrl', orderable: false, searchable: false},
                {data: 'slider_id'},
                {data: 'action', orderable: false, searchable: false}
            ]
    });
    $(document).ready(function(e) {
        $(document).on('click', '.btn-delete', function(e) {
            e.preventDefault();
            $('.deleteModal').find('.modal-body h4').text('Xóa slider: ' + $(this).attr('data-name'));
            $('.deleteModal').attr('data-id', $(this).attr('data-id'));
            $('.deleteModal').modal('show');
        });
        $('.btn-sure').on('click', function() {
            $.ajax({
                url: "{{ route('admin_slider_detail_delete', ['slider_detail' => '']) }}/" + $('.deleteModal').attr('data-id'),
                method: "GET"
            })
            .always(() => {
                slider_detail_list.ajax.reload();
                $('.deleteModal').modal('hide');
            });
        });
    });
    @if(Session::has('success_message'))
    let successMessages = `{{session('success_message')}}`;
    new PNotify({
        title: 'Success',
        type: 'success',
        text: successMessages,
        nonblock: {
            // nonblock: true
        },
        styling: 'bootstrap3',
    });
    @endif
</script>
@endsection